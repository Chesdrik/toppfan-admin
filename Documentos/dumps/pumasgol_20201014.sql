-- MySQL dump 10.13  Distrib 8.0.21, for macos10.15 (x86_64)
--
-- Host: localhost    Database: pumasgol
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `checkpoint_ticket_type`
--

DROP TABLE IF EXISTS `checkpoint_ticket_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `checkpoint_ticket_type` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `checkpoint_id` bigint(20) unsigned NOT NULL,
  `ticket_type_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_checkpoints_has_seat_types_checkpoints1_idx` (`checkpoint_id`),
  KEY `fk_checkpoints_seat_types_ticket_types1_idx` (`ticket_type_id`),
  CONSTRAINT `fk_checkpoints_has_seat_types_checkpoints1` FOREIGN KEY (`checkpoint_id`) REFERENCES `checkpoints` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_checkpoints_seat_types_ticket_types1` FOREIGN KEY (`ticket_type_id`) REFERENCES `ticket_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkpoint_ticket_type`
--

LOCK TABLES `checkpoint_ticket_type` WRITE;
/*!40000 ALTER TABLE `checkpoint_ticket_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `checkpoint_ticket_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `checkpoints`
--

DROP TABLE IF EXISTS `checkpoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `checkpoints` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `venue_id` bigint(20) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_general_ci,
  `parent_checkpoint` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_checkpoints_venues1_idx` (`venue_id`),
  CONSTRAINT `fk_checkpoints_venues1` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `checkpoints`
--

LOCK TABLES `checkpoints` WRITE;
/*!40000 ALTER TABLE `checkpoints` DISABLE KEYS */;
INSERT INTO `checkpoints` VALUES (11,1,'Estacionamiento 8',NULL),(12,1,'Acceso I',NULL),(66,1,'T-14',NULL),(68,1,'T-16',NULL),(70,1,'T-18',NULL),(76,1,'T-24',NULL);
/*!40000 ALTER TABLE `checkpoints` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conversations`
--

DROP TABLE IF EXISTS `conversations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conversations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `support_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `message` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_conversations_supports1_idx` (`support_id`),
  KEY `fk_conversations_users1_idx` (`user_id`),
  CONSTRAINT `fk_conversations_supports1` FOREIGN KEY (`support_id`) REFERENCES `supports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_conversations_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conversations`
--

LOCK TABLES `conversations` WRITE;
/*!40000 ALTER TABLE `conversations` DISABLE KEYS */;
/*!40000 ALTER TABLE `conversations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `venue_id` bigint(20) unsigned NOT NULL,
  `ticket_template_id` bigint(20) unsigned NOT NULL,
  `name` text COLLATE utf8mb4_general_ci,
  `date` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `img` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_events.byVenues1_idx1` (`venue_id`),
  KEY `fk_events_ticket_templates1_idx` (`ticket_template_id`),
  CONSTRAINT `fk_events_ticket_templates1` FOREIGN KEY (`ticket_template_id`) REFERENCES `ticket_templates` (`id`),
  CONSTRAINT `fk_events.byVenues1` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` VALUES (1,1,10,'Pumas - Toluca','2020-10-18 12:00:00',1,'12.png','2020-10-06 00:27:40','2020-10-06 00:27:40');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `event_id` bigint(20) unsigned NOT NULL DEFAULT '4',
  `email` text COLLATE utf8mb4_general_ci,
  `total` float(10,2) DEFAULT NULL,
  `courtesy` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orders_users1_idx` (`user_id`),
  KEY `fk_orders_events1_idx` (`event_id`),
  CONSTRAINT `fk_orders_events1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (20,10,1,'ing._leopoldo_silva@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(21,11,1,'dr._miguel_robles@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(22,12,1,'jose_remirez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(23,13,1,'jaime_lozano@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(24,14,1,'miguel_montiel@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(25,15,1,'leonardo_ortega@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(26,16,1,'angel_ojeda@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(27,17,1,'monserrat_palacios@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(28,18,1,'raul_gonzalez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(29,19,1,'pamela_juarez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(30,20,1,'adrian_carpio@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(31,21,1,'juan_daniel_hernandez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(32,22,1,'ulises_baez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(33,23,1,'carlos_trejo@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(34,24,1,'mariela_diaz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(35,25,1,'alejandro_santiago@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(36,26,1,'francisco_de_dios@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(37,27,1,'cristian_hernandez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(38,28,1,'comunicacion@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(39,29,1,'gerson_zavala@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(40,30,1,'publicidad_virtual@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(41,31,1,'colocacion_de_vallas@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(42,27,1,'cristian_hernandez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(43,32,1,'elizabeth_lechuga@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(44,33,1,'joel_mendoza@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(45,34,1,'ximena_trejo__leon@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(46,35,1,'ricardo_leon_hernandez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(47,36,1,'gerardo_molina_gutierrez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(48,37,1,'erika_lopez_jorge@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(49,38,1,'genaro_montiel@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(50,39,1,'bianca_berenice_lopez_@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(51,40,1,'alejandro_ataniel_lopez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(52,41,1,'jorge_armando_lopez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(53,42,1,'daniela_romo_mendoza@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(54,43,1,'brenda_aguilar_esquivel@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(55,44,1,'ramiro_aguilar_esquivel@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(56,45,1,'carla_medina@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(57,46,1,'oscar_garcia@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(58,47,1,'victor_mendoza@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(59,48,1,'pedro_alfredo_cruz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(60,49,1,'dr._acevedo@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(61,50,1,'manuel_alcocer@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(62,51,1,'luis_de_buen@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(63,52,1,'abelardo_jimenez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(64,53,1,'gerardo_galindo@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(65,54,1,'ellan_quintero@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(66,55,1,'santiago_puente@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(67,56,1,'valeria_hernandez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(68,57,1,'pablo_leon@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(69,58,1,'ernesto_garcia@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(70,59,1,'fernando_fuentes@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(71,60,1,'luis_cervantes@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(72,33,1,'joel_mendoza@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(73,61,1,'rafael_mercado@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(74,62,1,'ryota_nishimura@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(75,30,1,'publicidad_virtual@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(76,63,1,'israel_aviles_pelayo@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(77,64,1,'maria_trade@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(78,62,1,'ryota_nishimura@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(79,65,1,'daniel_olvera@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(80,66,1,'daniel_garces@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(81,67,1,'rodrigo_ramirez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(82,68,1,'vanesa_jimenez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(83,69,1,'juan_francisco_simon@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(84,70,1,'raul_fernandez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(85,71,1,'hiromi_nakazawa@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(86,72,1,'antonio_romero@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(87,73,1,'jesus_hernandez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(88,74,1,'daniel_vega@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(89,26,1,'francisco_de_dios@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(90,28,1,'comunicacion@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(91,26,1,'francisco_de_dios@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(92,25,1,'alejandro_santiago@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(93,25,1,'alejandro_santiago@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(94,25,1,'alejandro_santiago@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(95,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(96,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(97,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(98,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(99,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(100,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(101,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(102,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(103,75,1,'luis_miguel_albarnoz@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(104,27,1,'cristian_hernandez@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(105,76,1,'pablo_ayala_zamora@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(106,77,1,'rodolfo_villalobos@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(107,78,1,'joaqui_ledesma_avila@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(108,79,1,'juan_angel_hernandez_escobedo@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(109,80,1,'marco_tulio_alvarado@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(110,26,1,'francisco_de_dios@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(111,26,1,'francisco_de_dios@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(112,26,1,'francisco_de_dios@pumasgol.lampserv.com',0.00,1,'2020-10-14 16:54:12','2020-10-14 16:54:12');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statistics`
--

DROP TABLE IF EXISTS `statistics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statistics` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `checkpoint_id` bigint(20) unsigned NOT NULL,
  `sync_id` bigint(20) unsigned DEFAULT NULL,
  `event_id` bigint(20) unsigned NOT NULL,
  `type` enum('flow','errors','readings') DEFAULT NULL,
  `value` int(6) DEFAULT NULL,
  `start` timestamp NULL DEFAULT NULL,
  `end` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_statistics_checkpoints1_idx` (`checkpoint_id`),
  KEY `fk_statistics_syncs1_idx` (`sync_id`),
  KEY `fk_statistics_events1_idx` (`event_id`),
  CONSTRAINT `fk_statistics_checkpoints1` FOREIGN KEY (`checkpoint_id`) REFERENCES `checkpoints` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_statistics_events1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_statistics_syncs1` FOREIGN KEY (`sync_id`) REFERENCES `syncs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statistics`
--

LOCK TABLES `statistics` WRITE;
/*!40000 ALTER TABLE `statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supports`
--

DROP TABLE IF EXISTS `supports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supports` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `topic_id` bigint(20) unsigned NOT NULL,
  `description` text,
  `status` enum('pending','finish','cancel') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_supports_users1_idx` (`user_id`),
  KEY `fk_supports_topics1_idx` (`topic_id`),
  CONSTRAINT `fk_supports_topics1` FOREIGN KEY (`topic_id`) REFERENCES `topics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_supports_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supports`
--

LOCK TABLES `supports` WRITE;
/*!40000 ALTER TABLE `supports` DISABLE KEYS */;
/*!40000 ALTER TABLE `supports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `syncs`
--

DROP TABLE IF EXISTS `syncs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `syncs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) unsigned NOT NULL,
  `recieved` longtext,
  `response` longtext,
  `error` tinyint(1) DEFAULT '0',
  `type` enum('users_sync','events_sync','config_sync','certificates_sync','access_upload','ticket_readings','statistics') DEFAULT NULL,
  `total_records_synced` int(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_syncs_events1_idx` (`event_id`),
  CONSTRAINT `fk_syncs_events1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `syncs`
--

LOCK TABLES `syncs` WRITE;
/*!40000 ALTER TABLE `syncs` DISABLE KEYS */;
/*!40000 ALTER TABLE `syncs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_templates`
--

DROP TABLE IF EXISTS `ticket_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `venue_id` bigint(20) unsigned NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ticket_templates_venues1_idx` (`venue_id`),
  CONSTRAINT `fk_ticket_templates_venues1` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_templates`
--

LOCK TABLES `ticket_templates` WRITE;
/*!40000 ALTER TABLE `ticket_templates` DISABLE KEYS */;
INSERT INTO `ticket_templates` VALUES (10,1,'Puerta Cerrada');
/*!40000 ALTER TABLE `ticket_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_type_templates`
--

DROP TABLE IF EXISTS `ticket_type_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket_type_templates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_type_template_id` bigint(20) unsigned NOT NULL,
  `ticket_type_id` bigint(20) unsigned NOT NULL,
  `price` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ticket_type_templates_has_ticket_types_ticket_types1_idx` (`ticket_type_id`),
  KEY `fk_ticket_type_templates_has_ticket_types_ticket_type_templ_idx` (`ticket_type_template_id`),
  CONSTRAINT `fk_ticket_type_templates_has_ticket_types_ticket_type_templat1` FOREIGN KEY (`ticket_type_template_id`) REFERENCES `ticket_templates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ticket_type_templates_has_ticket_types_ticket_types1` FOREIGN KEY (`ticket_type_id`) REFERENCES `ticket_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_type_templates`
--

LOCK TABLES `ticket_type_templates` WRITE;
/*!40000 ALTER TABLE `ticket_type_templates` DISABLE KEYS */;
INSERT INTO `ticket_type_templates` VALUES (34,10,1,0.00),(35,10,2,0.00),(36,10,3,0.00);
/*!40000 ALTER TABLE `ticket_type_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_type_user`
--

DROP TABLE IF EXISTS `ticket_type_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket_type_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_type_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ticket_types_has_users_users1_idx` (`user_id`),
  KEY `fk_ticket_types_has_users_ticket_types1_idx` (`ticket_type_id`),
  CONSTRAINT `fk_ticket_types_has_users_ticket_types1` FOREIGN KEY (`ticket_type_id`) REFERENCES `ticket_types` (`id`),
  CONSTRAINT `fk_ticket_types_has_users_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_type_user`
--

LOCK TABLES `ticket_type_user` WRITE;
/*!40000 ALTER TABLE `ticket_type_user` DISABLE KEYS */;
INSERT INTO `ticket_type_user` VALUES (1,1,6),(2,3,6),(3,2,6);
/*!40000 ALTER TABLE `ticket_type_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_types`
--

DROP TABLE IF EXISTS `ticket_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `venue_id` bigint(20) unsigned NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `color` varchar(6) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0',
  `free` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ticket_types_venues1_idx1` (`venue_id`),
  CONSTRAINT `fk_ticket_types_venues1` FOREIGN KEY (`venue_id`) REFERENCES `venues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_types`
--

LOCK TABLES `ticket_types` WRITE;
/*!40000 ALTER TABLE `ticket_types` DISABLE KEYS */;
INSERT INTO `ticket_types` VALUES (1,1,'Zona 1',NULL,0.00,1,1,'2020-10-08 17:33:43','2020-10-08 17:33:43'),(2,1,'Zona 2',NULL,0.00,1,1,'2020-10-08 17:34:39','2020-10-08 17:34:39'),(3,1,'Zona 3',NULL,0.00,1,1,'2020-10-08 17:39:31','2020-10-08 17:39:31');
/*!40000 ALTER TABLE `ticket_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tickets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` bigint(20) unsigned NOT NULL,
  `ticket_type_id` bigint(20) unsigned NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `used` tinyint(1) DEFAULT '0',
  `type` enum('parking','ticket') COLLATE utf8mb4_general_ci NOT NULL,
  `price` float(10,2) DEFAULT NULL,
  `discount` text COLLATE utf8mb4_general_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tickets_events1_idx` (`event_id`),
  KEY `fk_tickets_orders1_idx` (`order_id`),
  KEY `fk_tickets_ticket_types1_idx` (`ticket_type_id`),
  CONSTRAINT `fk_tickets_events1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tickets_orders1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_tickets_ticket_types1` FOREIGN KEY (`ticket_type_id`) REFERENCES `ticket_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` VALUES (35,1,1,20,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(36,1,1,21,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(37,1,1,22,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(38,1,1,23,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(39,1,1,24,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(40,1,1,25,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(41,1,1,26,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(42,1,1,27,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(43,1,1,28,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(44,1,1,29,1,0,'parking',0.00,NULL,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(45,1,1,30,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(46,1,1,31,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(47,1,1,32,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(48,1,1,33,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(49,1,1,34,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(50,1,1,35,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(51,1,1,36,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(52,1,1,37,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(53,1,1,38,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(54,1,1,39,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(55,1,1,40,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(56,1,1,41,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(57,1,1,42,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(58,1,2,43,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(59,1,2,44,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(60,1,2,45,1,0,'parking',0.00,NULL,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(61,1,2,46,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(62,1,2,47,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(63,1,2,48,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(64,1,2,49,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(65,1,2,50,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(66,1,2,51,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(67,1,2,52,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(68,1,2,53,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(69,1,2,54,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(70,1,2,55,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(71,1,2,56,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(72,1,2,57,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(73,1,2,58,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(74,1,2,59,1,0,'parking',0.00,NULL,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(75,1,2,60,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(76,1,2,61,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(77,1,2,62,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(78,1,2,63,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(79,1,2,64,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(80,1,2,65,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(81,1,2,66,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(82,1,2,67,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(83,1,2,68,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(84,1,2,69,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(85,1,2,70,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(86,1,2,71,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(87,1,2,72,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(88,1,2,73,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(89,1,2,74,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(90,1,2,75,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(91,1,2,76,1,0,'parking',0.00,NULL,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(92,1,2,77,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(93,1,2,78,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(94,1,2,79,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(95,1,2,80,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(96,1,2,81,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(97,1,3,82,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(98,1,3,83,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(99,1,3,84,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(100,1,3,85,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(101,1,3,86,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(102,1,3,87,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(103,1,3,88,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(104,1,3,89,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(105,1,3,90,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(106,1,3,91,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(107,1,3,92,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(108,1,3,93,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(109,1,3,94,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(110,1,3,95,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(111,1,3,96,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(112,1,3,97,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(113,1,3,98,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(114,1,3,99,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(115,1,3,100,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(116,1,3,101,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(117,1,3,102,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(118,1,3,103,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(119,1,3,104,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(120,1,3,105,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(121,1,3,106,1,0,'parking',0.00,NULL,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(122,1,3,107,1,0,'parking',0.00,NULL,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(123,1,3,108,1,0,'parking',0.00,NULL,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(124,1,3,109,1,0,'parking',0.00,NULL,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(125,1,3,110,1,0,'parking',0.00,NULL,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(126,1,3,111,1,0,'parking',0.00,NULL,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(127,1,3,112,1,0,'parking',0.00,NULL,'2020-10-14 16:54:12','2020-10-14 16:54:12');
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `topics`
--

DROP TABLE IF EXISTS `topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `topics` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `parent_topic` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `topics`
--

LOCK TABLES `topics` WRITE;
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;
/*!40000 ALTER TABLE `topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('admin','client','supervisor','stats','access','ticket_office','courtesies') COLLATE utf8mb4_unicode_ci DEFAULT 'client',
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Diego Filloy Ring','diego@filloy.com.mx',NULL,'$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS',NULL,'admin',1,'2020-03-09 20:40:13','2020-03-10 01:51:10'),(2,'Diego Filloy Ring','dfilloyr@hotmail.com',NULL,'$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS',NULL,'client',1,'2020-03-10 00:19:18','2020-03-10 00:19:18'),(5,'Alinka Fragoso','alinkafm@ciencias.unam.mx',NULL,'$2y$12$/Blb6oK1AgDPQqGP1HhB/OZfsmMHt3v4XB7buRl979/igPgAzd2TW',NULL,'admin',1,'2020-03-10 01:12:20','2020-09-08 01:17:30'),(6,'Cortesías','cortesias@pumasgol.com',NULL,'$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS',NULL,'courtesies',1,'2020-03-10 01:12:20','2020-03-10 01:12:20'),(7,'Jessica Jaime','jessjaime05@gmail.com',NULL,'$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS',NULL,'admin',1,'2020-03-10 01:12:20','2020-03-10 01:12:20'),(8,'Roberto Hernández','betohr92@gmail.com',NULL,'$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS',NULL,'admin',1,'2020-03-10 01:12:20','2020-03-10 01:12:20'),(9,'Luis Cervantes','cervants2802@gmail.com',NULL,'$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS',NULL,'admin',1,'2020-03-10 01:12:20','2020-03-10 01:12:20'),(10,'Ing. Leopoldo Silva','ing._leopoldo_silva@pumasgol.lampserv.com',NULL,'$2y$10$CaSJovHV0Or4rULR91PEpOJzko2xUeIefQVUbrBSfl7AYtYgDlTyq',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(11,'Dr. Miguel Robles','dr._miguel_robles@pumasgol.lampserv.com',NULL,'$2y$10$I37M1Ic2tYcomRrU154y8uHCyM9bmHtWqCB9UPuZbD9GFJzNVvhZm',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(12,'José Remirez','jose_remirez@pumasgol.lampserv.com',NULL,'$2y$10$UbxVmbbG5/7PZnE02nt5f.gK6vsP3GxZt25rOJ2owVvgYVt66Sdbq',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(13,'Jaime Lozano','jaime_lozano@pumasgol.lampserv.com',NULL,'$2y$10$JJ.jSNf9nG99fXPy2f11gOFapNsULC7YWpS0TubUf//Sw2gBZWoQ2',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(14,'Miguel Montiel','miguel_montiel@pumasgol.lampserv.com',NULL,'$2y$10$nIKXLWS0n69X1YPr7thUheos3DM4yqpVnzSvcqfEGbe3b.MnFXFOu',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(15,'Leonardo Ortega','leonardo_ortega@pumasgol.lampserv.com',NULL,'$2y$10$RHdBrDmlkjqZ3AWhofMn8.TIciCog0Q6uVsueNfQFHHrYrQYyoJw6',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(16,'Ángel Ojeda','angel_ojeda@pumasgol.lampserv.com',NULL,'$2y$10$Dk9QwFfqjClohOkz48nZGebW4iBfanV7fauZ.atyzflhuMf6gBsIy',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(17,'Monserrat Palacios','monserrat_palacios@pumasgol.lampserv.com',NULL,'$2y$10$BD3qGz6Sw01UujmLIrC9uuiMhl6OM18TH9.QJqVMsO5.j2acdhEtq',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(18,'Raúl González','raul_gonzalez@pumasgol.lampserv.com',NULL,'$2y$10$w4nPY7ONUtRmzk/7nqMvgOES4JMWP190bHUuyJlRqo/l.QmqjYusS',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(19,'Pamela Juárez','pamela_juarez@pumasgol.lampserv.com',NULL,'$2y$10$AMe1FSwMAJP8tmaugHBa5.kxCjw0D69tO97u0C2c/Bk6fYHpB4wlS',NULL,'courtesies',1,'2020-10-14 16:54:07','2020-10-14 16:54:07'),(20,'Adrián Carpio','adrian_carpio@pumasgol.lampserv.com',NULL,'$2y$10$opc5iaFn9BUDEbkUt/0VX.XyS4HbKq8Fc5Ir.Qx5RG8nss5c6T0xO',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(21,'Juan Daniel Hernandez','juan_daniel_hernandez@pumasgol.lampserv.com',NULL,'$2y$10$DdAT4Dxx1ykkZwsCA3p1SeQlYDpsNBi0VBrEUcZmX/YQnkJGh6HEu',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(22,'Ulises Baez','ulises_baez@pumasgol.lampserv.com',NULL,'$2y$10$WMiERDUz8nqVy0gF3ZFEdem0lV7j4Qb12h2vWgvXAGLdZCUbN9mxW',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(23,'Carlos Trejo','carlos_trejo@pumasgol.lampserv.com',NULL,'$2y$10$LaIWpwSsqmW5B4tLLlNoAeaHS1wyhN8rkAYByceNoboLsJl3Nr.U.',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(24,'Mariela Diaz','mariela_diaz@pumasgol.lampserv.com',NULL,'$2y$10$KSLkl1QkM.uMFLu4gLiA2eHtVpWfhLeBN/uNQ0KmcLq3gzeumqFD.',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(25,'Alejandro Santiago','alejandro_santiago@pumasgol.lampserv.com',NULL,'$2y$10$aK/jx2k/yMYWX8ZsWlNUw.VQuzed8MD8cFeEDnuwh688zsC3c46/y',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(26,'Francisco De Dios','francisco_de_dios@pumasgol.lampserv.com',NULL,'$2y$10$.00OOJK7s2KvOPRr9AZnuOI7ZIpkOHVv3PHqbm5jOGtd7waO91xyy',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(27,'Cristian Hernández','cristian_hernandez@pumasgol.lampserv.com',NULL,'$2y$10$QcpTlToxz5/T1La.M5BdXOIs7DWtcFWEbw13RqLedRv.gn/CUsQ2S',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(28,'COMUNICACIÓN','comunicacion@pumasgol.lampserv.com',NULL,'$2y$10$5JAs0/drr315l36vz.nX7eEvI9aKaKo0P/Zarlzg/FlAXiCVzKOOi',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(29,'Gerson Zavala','gerson_zavala@pumasgol.lampserv.com',NULL,'$2y$10$XVuhZzSrkEJzot/4IwTL8.EBXGEltazYTuZBONgwiFUk7mIUskhgW',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(30,'Publicidad Virtual','publicidad_virtual@pumasgol.lampserv.com',NULL,'$2y$10$/cjb8F6yvH8seGyT6Cw0j.xlqHb.nzxIhwGDIdTOybU3ASoTDrtq.',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(31,'Colocación de Vallas','colocacion_de_vallas@pumasgol.lampserv.com',NULL,'$2y$10$pX87QZBuiSYUBRV20hSSiO5LoQV4hHRmXD3psCrzRlkxuxwCkQO5a',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(32,'Elizabeth Lechuga','elizabeth_lechuga@pumasgol.lampserv.com',NULL,'$2y$10$IsRhkmgrvbqWTjY3BhNRWe2Dno84pRPYwYzTh/f5kU8tDjtVH2HpK',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(33,'Joel Mendoza','joel_mendoza@pumasgol.lampserv.com',NULL,'$2y$10$KMpx2WLkE5qYZTf1TKJu/uuOz70HIV6GHy3BeRBKpZbXyHnxXFBme',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(34,'Ximena Trejo  León','ximena_trejo__leon@pumasgol.lampserv.com',NULL,'$2y$10$R9cQGF.0Pdsli3Usg3.Ifu1jGJV/RCjmiQo1jlHytYE8JeJozo/eK',NULL,'courtesies',1,'2020-10-14 16:54:08','2020-10-14 16:54:08'),(35,'Ricardo León Hernández','ricardo_leon_hernandez@pumasgol.lampserv.com',NULL,'$2y$10$a1aQaGQ03LJgeSjqMPTQJ.BmbDRlBeP5o8.I4tNdTwmAZu3FRcgNa',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(36,'Gerardo Molina Gutiérrez','gerardo_molina_gutierrez@pumasgol.lampserv.com',NULL,'$2y$10$l5tnoAVJnPfmM8bteHzQvezkmPFk7opnKXmKAH1C..THM/zQYLtzW',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(37,'Erika López Jorge','erika_lopez_jorge@pumasgol.lampserv.com',NULL,'$2y$10$2o41hlebZPFh8fn9VZPvOu4LjH.IpwjXCfSUeKTlGNaz6DW3//jSG',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(38,'Genaro Montiel','genaro_montiel@pumasgol.lampserv.com',NULL,'$2y$10$8JEIw8rwMvezx5NHkBu7I.RKzmW/5/P20SkqhJGL5.8JF7UUtfha.',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(39,'Bianca Berenice López ','bianca_berenice_lopez_@pumasgol.lampserv.com',NULL,'$2y$10$o1YTuz8y6tL7BgDKhlUWcuWmgb.p8AIPBVugR417v1.ywT4VE4WNm',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(40,'Alejandro Ataniel López','alejandro_ataniel_lopez@pumasgol.lampserv.com',NULL,'$2y$10$kQ5PYIPeP7q5PbIQxUatV.2eLQU996UwC7yLxQOuZutpHiI9rIyly',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(41,'Jorge Armando López','jorge_armando_lopez@pumasgol.lampserv.com',NULL,'$2y$10$D4FMYkM1r7PsjI8pnesP5Om/7JDNRj.nL0jVh9zsQyd0bi4SgwldK',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(42,'Daniela Romo Mendoza','daniela_romo_mendoza@pumasgol.lampserv.com',NULL,'$2y$10$IQS43Tm7QCXFmkwcfYprWOmf9k6t5bHkYVOfJmiSVD4pxq7kRG7kq',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(43,'Brenda Aguilar Esquivel','brenda_aguilar_esquivel@pumasgol.lampserv.com',NULL,'$2y$10$FbafMG6h7DUKzykFS97eOe8vJZGNjRnl4ZJNmn1GhowT/2oFz32E.',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(44,'Ramiro Aguilar Esquivel','ramiro_aguilar_esquivel@pumasgol.lampserv.com',NULL,'$2y$10$81iINiptlipd4xV01m4cpeygD39NgSsE6krLDd1bBJJNMQaT4B1v.',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(45,'Carla Medina','carla_medina@pumasgol.lampserv.com',NULL,'$2y$10$bMxue6eiIYbWWF1GI6BbPeCAMWyLPsgD0XRLp85YGVOa0xcd3qzBq',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(46,'Oscar García','oscar_garcia@pumasgol.lampserv.com',NULL,'$2y$10$/UP9fJ0RmpSEoixCVMZPAeYd.Fio7rJ4tagyWxRZssPqQPvpbllUy',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(47,'Victor Mendoza','victor_mendoza@pumasgol.lampserv.com',NULL,'$2y$10$pHB0EqpjbwVGbXsOireZg.HFQQY8bE.Y0eI7Rp1TNTTS.oM9SXSS.',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(48,'Pedro Alfredo Cruz','pedro_alfredo_cruz@pumasgol.lampserv.com',NULL,'$2y$10$gmVxo3C2U5UWnlct6miJd.NqaY6nFUpf2hnT.3EGAXtGayLqBgh/q',NULL,'courtesies',1,'2020-10-14 16:54:09','2020-10-14 16:54:09'),(49,'Dr. Acevedo','dr._acevedo@pumasgol.lampserv.com',NULL,'$2y$10$8mW1QjoD9vXqWVQs9RPhJ.xecYZ4zv8iifiSBallB/Rw/sXV0lykC',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(50,'Manuel Alcocer','manuel_alcocer@pumasgol.lampserv.com',NULL,'$2y$10$LunULNUFihpnvF3ZGfcgJu7p6oJoEwNXY7GzK7GazuQXX2YSFqvHK',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(51,'Luis De Buen','luis_de_buen@pumasgol.lampserv.com',NULL,'$2y$10$VLd7EggysjRqILifQilIkOB53Vo72w5Dagchz/fgDIYGyj45BpCRC',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(52,'Abelardo Jiménez','abelardo_jimenez@pumasgol.lampserv.com',NULL,'$2y$10$DSNSzhjDtEkNhXOc9/dhJ..BqzKEkIhQ1Th/g8/kvBZgPuA2qQpIO',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(53,'Gerardo Galindo','gerardo_galindo@pumasgol.lampserv.com',NULL,'$2y$10$NZiVLNrdCEX3rUbnXY6qLOvuScfIVtY7esQ9eg9qLGKtxqhK/.nz2',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(54,'Ellan Quintero','ellan_quintero@pumasgol.lampserv.com',NULL,'$2y$10$i09LLIwUzBIL1ycDyitEWu1phEiIiTmPZqo83dDLBn.Wo0BJJc/ia',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(55,'Santiago Puente','santiago_puente@pumasgol.lampserv.com',NULL,'$2y$10$ukdQk6hrjka4UEus.ZbB8uJCMIMa7RWLakrR5H.Q5E9kEtfuM7FLu',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(56,'Valeria Hernández','valeria_hernandez@pumasgol.lampserv.com',NULL,'$2y$10$Fl2W3JwYPFtOObm.V54Dc.EeXvHAxSdgaIcl0LwX6Ggfsoa1pt2j6',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(57,'Pablo León','pablo_leon@pumasgol.lampserv.com',NULL,'$2y$10$nlG911hx./rPpn7dq.maQOpez4zSAb.3dRBjPJE6lJUeThAcPmDuK',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(58,'Ernesto García','ernesto_garcia@pumasgol.lampserv.com',NULL,'$2y$10$HR5eUTlqTKmrivbi7vuehuJiHWsrsJlCkqf6ad7GXoXUaWpM86NWm',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(59,'Fernando Fuentes','fernando_fuentes@pumasgol.lampserv.com',NULL,'$2y$10$9uxl8JGDlAK6lQA6Gl/r3.5mEzAqvspYuzs8iV9APV4buXQNAJaNC',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(60,'Luis Cervantes','luis_cervantes@pumasgol.lampserv.com',NULL,'$2y$10$LqCtsTinAv0/IGWlPviC9eDaDAn6Ii7jp44djBv/MWmxKgIDOTq3i',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(61,'Rafael Mercado','rafael_mercado@pumasgol.lampserv.com',NULL,'$2y$10$Op74ORdDEYjeVCUK/co4POtIZ9fx5WNY2kcL1abvwdGxiYKy9loRK',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(62,'Ryota Nishimura','ryota_nishimura@pumasgol.lampserv.com',NULL,'$2y$10$J9sLnFykaDqMsfoHjXHrwul4WFqBJ5d8AgCF3.2fMSPr5jJAqg0mW',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(63,'Israel Áviles Pelayo','israel_aviles_pelayo@pumasgol.lampserv.com',NULL,'$2y$10$QoGt1B3CZ0wuU1u.kL/euuE7/bQzIgSM3DFLVES.S2hR1uEPyGPGq',NULL,'courtesies',1,'2020-10-14 16:54:10','2020-10-14 16:54:10'),(64,'Maria Trade','maria_trade@pumasgol.lampserv.com',NULL,'$2y$10$gk6dxUsfd0J0mFnBpZ0Gxeitr1scOh6ILJ0R2seZlLJ265KnDcZ5C',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(65,'Daniel Olvera','daniel_olvera@pumasgol.lampserv.com',NULL,'$2y$10$AKXlGVN06GeJ58xNPMHkW.VM45jm6lPMF.JytSvqAztTNYyp3LK8G',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(66,'Daniel Garces','daniel_garces@pumasgol.lampserv.com',NULL,'$2y$10$w45.CslTsUj7LnaGYiq9/esL0qISsTIXOAO.5cZS8PUfBKB8cWuC.',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(67,'Rodrigo Ramírez','rodrigo_ramirez@pumasgol.lampserv.com',NULL,'$2y$10$TdszjOTL8LTYI68LoFiIPO7E7tmKDNEj9sDWRHrnF6nuusn3jRKKG',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(68,'Vanesa Jimenez','vanesa_jimenez@pumasgol.lampserv.com',NULL,'$2y$10$6/eRHD41QwcqcWrVH9keL.2K/DRMHAT4rGHfoCGkOW/T5f49E8NH.',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(69,'Juan Francisco Simón','juan_francisco_simon@pumasgol.lampserv.com',NULL,'$2y$10$0KyE72WWSZlORda/2U6PFOJift2y0pvDLK/U8nxgFZQ2qSIYvvNVy',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(70,'Raúl Fernández','raul_fernandez@pumasgol.lampserv.com',NULL,'$2y$10$Oqmedb84O7ZWHhaHtK205.Xh8LzSilSbO0UEH9elPFJCW5TJ./Phy',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(71,'Hiromi Nakazawa','hiromi_nakazawa@pumasgol.lampserv.com',NULL,'$2y$10$NVQFFqfLRrZ6Mn4n0/k3Uebeqenvv36Kt6z7ckuh/z2uQYVpcRPVu',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(72,'Antonio Romero','antonio_romero@pumasgol.lampserv.com',NULL,'$2y$10$H.gbF3ORxevXWEO/AUlbXOm5PVbLFj67XLwyiZDCx8wAogkq1Fo5y',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(73,'Jesús Hernández','jesus_hernandez@pumasgol.lampserv.com',NULL,'$2y$10$1DWHEfpjfGrtnyC/EAQacu9CFXdV8FPPH.uJRvOV8Ic.wh45MbRw6',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(74,'Daniel Vega','daniel_vega@pumasgol.lampserv.com',NULL,'$2y$10$8PKUrUNEkOJz5yuCPXbtZeVKoYSiwbXQb.sY1/5jasGl7RUZaElqW',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(75,'Luis Miguel Albarnoz','luis_miguel_albarnoz@pumasgol.lampserv.com',NULL,'$2y$10$p9Ab5eFqDVaPq/wAL7ybQOnTIYg2Qj7EfqTyMlAsVb99B5q9OYHba',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(76,'Pablo Ayala Zamora','pablo_ayala_zamora@pumasgol.lampserv.com',NULL,'$2y$10$J9ps623m.EHJuTYew/l7Ke4L1Gsgn9FzBnVXWE7ZFyEiZ6O.m7zcO',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(77,'Rodolfo Villalobos','rodolfo_villalobos@pumasgol.lampserv.com',NULL,'$2y$10$dVl26R9/bGH7Q46klbpwWesnbTj32V3XtmrJ/QfBZIO3jEbzgbpk.',NULL,'courtesies',1,'2020-10-14 16:54:11','2020-10-14 16:54:11'),(78,'Joaqui Ledesma Avila','joaqui_ledesma_avila@pumasgol.lampserv.com',NULL,'$2y$10$ffL0NnmD/nfW3HLfIhW0meS8X0deprUkmuHiJeJbOBIvX65DYPaEm',NULL,'courtesies',1,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(79,'Juan Ángel Hernández Escobedo','juan_angel_hernandez_escobedo@pumasgol.lampserv.com',NULL,'$2y$10$WKPIQcNYNciRQvfU//pjAex4Pcbc/GOcIKPq8HYbJRwwdcXhBCnhC',NULL,'courtesies',1,'2020-10-14 16:54:12','2020-10-14 16:54:12'),(80,'Marco Tulio Alvarado','marco_tulio_alvarado@pumasgol.lampserv.com',NULL,'$2y$10$LeOpY1Hcv1mR1kwV10UYduJyEvgcCCTTjh//Omtub39IYrWskuZHO',NULL,'courtesies',1,'2020-10-14 16:54:12','2020-10-14 16:54:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venues`
--

DROP TABLE IF EXISTS `venues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `venues` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_general_ci,
  `lat` decimal(10,8) DEFAULT NULL,
  `long` decimal(11,8) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venues`
--

LOCK TABLES `venues` WRITE;
/*!40000 ALTER TABLE `venues` DISABLE KEYS */;
INSERT INTO `venues` VALUES (1,'Estadio Olímpico Universitario',19.33207270,-99.19222550,'2020-03-15 16:21:00','2020-03-15 16:21:00'),(2,'La Cantera',19.31503260,-99.17108140,'2020-03-15 16:21:00','2020-03-15 16:21:00'),(3,'Estadio Olímpico de Villahermosa',17.97463380,-92.94478640,'2020-03-15 16:21:00','2020-03-15 16:21:00');
/*!40000 ALTER TABLE `venues` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-14 12:27:48
