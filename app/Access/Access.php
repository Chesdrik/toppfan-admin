<?php

namespace App\Access;

use Illuminate\Database\Eloquent\Model;
use Storage;
use QrCode;
use  \Intervention\Image\Facades\Image;
use App\Encrypter;
use DateTime;

class Access extends Model
{
    protected $fillable = ['user_id', 'venue_id', 'access_role_id', 'visitor_name', 'start', 'end', 'type'];
    public $table = 'access';
    // protected $dates = ['start', 'end'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function access_logs()
    {
        return $this->hasMany('App\Access\AccessLog');
    }

    public function access_role()
    {
        return $this->belongsTo('App\Access\AccessRole');
    }

    public function access_requirements()
    {
        return $this->hasMany('App\Access\AccessRequirement');
    }

    public function requirements()
    {
        return $this->belongsToMany('App\Access\Requirement')->withPivot('request', 'response', 'data_type');
    }

    public function datespan()
    {
        return $this->start . ' - ' . $this->end;
    }

    public function pretty_type()
    {
        switch ($this->type) {
            case 'foot':
                return 'A pie';
                break;
            case 'car':
                return 'En auto';
                break;
            case 'other':
                return 'Otro';
                break;
            default:
                return 'No definido';
                break;
        }
    }

    public function pretty_created_at()
    {
        return pretty_date($this->created_at);
    }

    public function generateQR()
    {
        $encrypter = new Encrypter();
        $encrypter->setEncryptionKeyAttribute(env('ACCESS_ENCRYPTION_KEY'));
        $encrypter->setInitializationVectorAttribute(env('ACCESS_ENCRYPTION_IV'));
        $encrypter->setContentAttribute(json_encode(['accessID' => $this->id]));

        // Generating PNG with QR
        $qr_img = QrCode::format('png')
            ->size(500)
            ->generate($encrypter->encrypted_content);

        // Saving to disk
        $qr_path = 'public/' . $this->qr_path();
        Storage::put($qr_path, $qr_img);

        // Adding text to QR so we know what venue - access id it has
        // $img = Image::make('storage/'.$this->qr_path());
        // $img->text($this->venue_id.' - '.str_pad($this->id, 4, '0', STR_PAD_LEFT), 250, 490, function($font){
        //     $font->size(25);
        //     $font->align('center');
        //     $font->file(base_path('storage/fonts/Roboto-Bold-webfont.ttf'));
        // });
        // $img->save('storage/'.$this->qr_path());
    }

    public function qr_path()
    {
        return 'access/' . $this->id . '.png';
    }

    public function qr()
    {
        $this->generateQR();

        return (\Storage::exists('public/' . $this->qr_path()) ? asset('storage/' . $this->qr_path()) : 'no_existe storage/');
    }

    public static function suzuki_report_data($start, $end)
    {
        $access = Access::suzuki_access()->pluck('access.id');
        $acces_logs = AccessLog::whereBetween('timestamp', [$start, $end])->whereIn('access_id', $access)->get();

        $processed_logs = array();
        foreach ($acces_logs as $acces_log) {
            // Checking if access_id exists in array
            if (!array_key_exists($acces_log->access_id, $processed_logs)) {
                $processed_logs[$acces_log->access_id]['data'] = Access::find($acces_log->access_id);
            }

            // Appending access log
            $processed_logs[$acces_log->access_id]['access'][] = $acces_log;
        }

        return $processed_logs;
    }

    // Fetching Suzuki requirement and corresponding access
    public static function suzuki_access()
    {
        return Access::requirement_access(2);
    }

    // Fetching access for requirement
    public static function requirement_access($id)
    {
        return Requirement::find($id)->access()->where('venue_id', 2);
    }

    public function access_requirements_API()
    {
        $return = array();
        foreach ($this->requirements as $requirement) {
            $return[] = [
                // 'description' => $requirement->description,
                'accessRequirementId' => $requirement->id,
                'type' =>  $requirement->type,
                'request' => $requirement->pivot->request,
                'dataType' => $requirement->pivot->data_type,
            ];
        }

        return $return;
    }

    public function assistance($boundries)
    {
        $x = $this->access_logs
            ->where('type', 'entry')
            ->whereBetween('timestamp', [$boundries[0], $boundries[1]])
            ->pluck('timestamp', 'id')
            ->toArray();

        $b = array();
        foreach ($x as $key => $value) {
            $time = date_create($value);
            $time = date_format($time, 'Y-m-d');

            $b[$key] = $time;
        }

        return $b;
    }

    public static function assistance_data($week_date)
    {
        $week_date = date_create($week_date)->format('Y-m-d');

        $d = $week_date;
        $date = new DateTime($week_date);
        $boundries = getStartAndEndDate($date->format("Y"), $date->format("W"));
        $datetime = new DateTime($boundries[0]);
        $nextandprev = nextAndPrevDate($date->getTimestamp());
        $boundries_date = $boundries[2];
        return [$datetime, $boundries];
    }
}
