<?php

namespace App\Access;

use Illuminate\Database\Eloquent\Model;

class AccessLog extends Model
{
    protected $fillable = ['access_id', 'type', 'log', 'timestamp'];

    protected $table = 'access_logs';

    // public function requirements(){
    //     return $this->belongsToMany('App\Access\Requirement')->withPivot('data');
    // }

    public function requirementsdata(){
        return $this->hasMany('App\Access\RequirementData')->withPivot('access_log_id');
    }



    public function pretty_type(){
        switch ($this->type) {
            case 'entry':
                return 'Ingreso';
                break;
            case 'exit':
                return 'Salida';
                break;
            case 'other':
                return 'Otro';
                break;
            case 'error':
                return 'Error';
                break;
        }
    }

    public function pretty_type_icon(){
        switch ($this->type) {
            case 'entry':
                return '<i class="zmdi zmdi-long-arrow-tab zmdi-hc-fw"></i>';
                break;
            case 'exit':
                return '<i class="zmdi zmdi-swap zmdi-hc-fw"></i>';
                break;
            case 'other':
                return '<i class="zmdi zmdi-help-outline zmdi-hc-fw"></i>';
                break;
            case 'error':
                return '<i class="zmdi zmdi-alert-circle-o zmdi-hc-fw"></i>';
                break;
        }
    }

    public function pretty_type_card_class(){
        switch ($this->type) {
            case 'entry':
                return 'bg-green card--inverse';
                break;
            case 'exit':
                return 'bg-blue card--inverse';
                break;
            case 'other':
                return 'bg-cyan card--inverse';
                break;
            case 'error':
                return 'bg-red card--inverse';
                break;
        }
    }

    public function requirement_data(){

        return $this->hasMany('App\Access\AccessRequirementData');
    
    }
}
