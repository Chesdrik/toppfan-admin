<?php

namespace App\Access;

use Illuminate\Database\Eloquent\Model;

class AccessRequirement extends Model
{
    protected $fillable = [
        'access_id', 'requirement_id', 'request', 'response', 'data_type'
    ];
    public $table = 'access_requirement';
    public $timestamps = false;
    

    public function access_data(){
    	return $this->hasMany('App\Access\AccessRequirementData');
    }
}
