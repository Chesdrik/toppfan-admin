<?php

namespace App\Access;

use Illuminate\Database\Eloquent\Model;

class AccessRequirementData extends Model
{
    protected $fillable = [
        'access_requirement_id', 'access_log_id', 'data',
    ];
    public $table = 'access_requirement_data';


    public function access_requirement(){
    	return $this->belongsTo('App\Access\AccessRequirement');
    }
    
    public function access_log(){
    	return $this->belongsTo('App\Access\AccessLog');
    }
}
