<?php

namespace App\Access;

use Illuminate\Database\Eloquent\Model;

class AccessRole extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'type'];
}
