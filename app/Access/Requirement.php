<?php

namespace App\Access;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    public $timestamps = false;
    protected $fillable = ['description', 'type'];

    public function access(){
        return $this->belongsToMany('App\Access\Access')->withPivot('request', 'response', 'data_type');
    }

    // public function access_requirement_data(){
    //
    // }

    // public function access_logs(){
    //     return $this->belongsToMany('App\Access\AccessLog')->withPivot('data');
    // }

    public function pretty_type(){
        switch ($this->type) {
            case 'photo':
                return 'Foto';
                break;
            case 'revision':
                return 'Revisión';
                break;
            case 'message':
                return 'Mensaje';
                break;
            case 'request':
                return 'Solicitud';
                break;
            default:
                return 'Otro';
                break;
        }
    }
}
