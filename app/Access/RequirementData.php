<?php

namespace App\Access;

use Illuminate\Database\Eloquent\Model;

class RequirementData extends Model
{
    public $timestamps = false;

    protected $fillable = ['id', 'access_requirement_id','access_log_id', 'data'];

    protected $table = 'access_requirement_data';

    public function accesslog()
    {
        return $this->belongsTo('App\Access\AccessLog');
    }

}
