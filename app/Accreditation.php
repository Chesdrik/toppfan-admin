<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use QrCode;
use App\Encrypter;
use Storage;
use  \Intervention\Image\Facades\Image;

class Accreditation extends Model
{
    protected $fillable = ['ticket_type_id', 'folio', 'name', 'last_names', 'email', 'type', 'version', 'system_id', 'order_id', 'seat_id', 'price', 'phone'];

    // TODO Borrar esta, no está en plurar. Se queda para no tronar el sistema mientras se hace el cambio
    public function event()
    {
        return $this->belongsToMany('App\Event')
            ->withPivot(['name', 'email', 'used', 'version']);
    }

    public function events()
    {
        return $this->belongsToMany('App\Event')
            ->withPivot(['name', 'last_names', 'email', 'used', 'version']);
    }

    public function seat()
    {
        return $this->belongsTo('App\Seat');
    }

    public function event_groups()
    {
        return $this->belongsToMany('App\EventGroup');
    }

    public function current_event($id)
    {
        return $this->belongsToMany('App\Event')
            ->where('event_id', $id)
            ->withPivot(['name', 'last_names', 'email', 'used', 'version'])
            ->get();
    }

    public function ticket_type()
    {
        // return $this->hasOne('App\TicketType', 'id', 'ticket_type_id');
        return $this->belongsTo('App\TicketType');
    }

    public function byEvent($event)
    {
        return $this->belongsToMany('App\Event')
            ->withPivot(['name', 'last_names', 'email', 'used', 'version'])
            ->where('event_id', $event)
            ->first();
    }

    public function pretty_type()
    {
        switch ($this->type) {
            case 'operative':
                return 'Operativa';
                break;
            case 'season_ticket':
                return 'Abono';
                break;
        }
    }

    /**
     * The roles that belong to the Accreditation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function teams()
    {
        return $this->belongsToMany(Team::class);
    }

    public static function folio()
    {
        return Accreditation::max('folio');
    }

    public function generate_qr()
    {
        // Generating PNG with QR
        $qr_img = QrCode::format('png')
            ->size(500)
            ->margin(7)
            ->generate($this->qr_content());


        // Saving to disk
        Storage::put($this->qr_path(), $qr_img);

        //
        if (env('FILESYSTEM_DRIVER', 'local') == 'local') {
            $path = "storage/" . $this->qr_path(false);
        } else {
            $path = Storage::url($this->qr_path(true));
        }

        // // Resizing QR and adding text
        // $img = Image::make($path);
        // $img->resizeCanvas(500, 520, 'top', false, 'ffffff');

        // // Adding accreditation or season ticket
        // $img->text(($this->type == 'season_ticket') ? 'Abono' : 'Acreditación', 250, 45, function ($font) {
        //     $font->size(25);
        //     $font->align('center');
        //     $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        //     // $font->file(1);
        //     // $font->file('fonts/Roboto-Bold-webfont.ttf');
        // });

        // // Adding folio
        // $img->text($this->folio . ' - v' . $this->version, 250, 470, function ($font) {
        //     $font->size(25);
        //     $font->align('center');
        //     $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        //     // $font->file(1);
        //     // $font->file('fonts/Roboto-Bold-webfont.ttf');
        // });

        // // Adding seats
        // if ($this->type == 'season_ticket') {
        //     $seat = $this->ticket->seats->first();
        //     $img->text('Asiento: ' . $seat->row->name . '-' . $seat->name, 250, 500, function ($font) {
        //         $font->size(20);
        //         $font->align('center');
        //         $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        //         // $font->file(1);
        //         // $font->file('fonts/Roboto-Bold-webfont.ttf');
        //     });
        // } else {
        //     $img->text($this->ticket_type->name, 250, 500, function ($font) {
        //         $font->size(20);
        //         $font->align('center');
        //         $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        //     });
        // }

        // // Saving image
        // Storage::put($this->qr_path(), $img->encode('jpg')->encoded);

        // Returning path
        return Storage::url($this->qr_path());
    }

    public function qr_content($encrypted = true)
    {
        $qr_content = json_encode([
            'aID' => $this->id, // Accreditation
            // 'checkpoints' => $this->ticket_type->checkpoints->pluck('id')->toArray(), // Checkpoints it has access to
            // 'folio' => $this->folio,
            'version' => $this->version,
            'type' => $this->type
        ]);

        // Returning plain json
        if (!$encrypted) {
            return $qr_content;
        } else {
            // Creating encrypter
            $encrypter = new Encrypter();
            $encrypter->setEncryptionKeyAttribute(env('ENCRYPTION_KEY'));
            $encrypter->setInitializationVectorAttribute(env('ENCRYPTION_IV'));

            // Creating QR content in JSON
            $encrypter->setContentAttribute($qr_content);

            return $encrypter->encrypted_content;
        }
    }

    public function qr()
    {
        // Checking if qr exists
        $this->verify_qr_existance();

        // Returning qr url
        return Storage::url($this->qr_path());
    }

    public function qr_path($with_public = true)
    {
        return ($with_public ? "public/" : "") . "accreditations/{$this->system_id}/{$this->id}_{$this->version}.png";
    }

    public function verify_qr_existance()
    {
        if (!Storage::exists($this->qr_path())) {
            $this->generate_qr();
        }
    }

    public function API_POS()
    {
        return [
            'id' => $this->id,
            'amount' => 1,
            'ticketTypeId' => $this->ticket_type->id,
            'ticketTypeName' => $this->ticket_type->name,
            'seat_name' => ($this->seat_id) ? $this->seat->full_name() : 'General',
            'short_name' => ($this->seat_id) ? $this->seat->row->name . '-' . $this->seat->name : 'General',
            'price' => $this->price,
            'access' => $this->ticket_type->checkpoints_string(),
            // 'qr' => $this->generateQRsSVG(),
            // 'qr_url' => asset($this->qr()),
        ];
    }
}
