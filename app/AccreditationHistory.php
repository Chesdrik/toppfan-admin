<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccreditationHistory extends Model
{
    protected $fillable = ['user_id', 'accreditation_id', 'version', 'motive'];
    protected $table = 'accreditation_history';


    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function acreditations(){
    	return $this->belongsTo('App\Accreditation');
    }
}
