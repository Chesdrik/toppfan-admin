<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccreditationReading extends Model
{
    protected $fillable = ['accreditation_event_id', 'sync_id', 'checkpoint_id', 'type', 'error', 'reading_time', 'device_id'];

    public function accreditation()
    {
        return $this->hasOne('App\AccreditationEvent', 'id', 'accreditation_event_id');
    }

    public function checkpoint()
    {
        return $this->belongsTo('App\Checkpoint');
    }

    public static function forEvent(Event $event)
    {
        $tickets = array();
        // dd($event->accreditations->pluck('id'));

        $tickets = array_merge($tickets, $event->accreditations->pluck('id')->toArray());


        return AccreditationReading::whereIn('accreditation_event_id', $tickets)->orderBy('reading_time', 'ASC')->get();
    }
}
