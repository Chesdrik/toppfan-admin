<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $fillable = ['type', 'notes', 'checkpoint_id', 'event_id'];
    public $timestamps = false;

    public function checkpoint()
    {
        return $this->belongsTo('App\Checkpoint');
    }

    public function event()
    {
        return $this->belongsTo('App\Event');
    }
}
