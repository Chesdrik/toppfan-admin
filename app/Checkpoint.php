<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkpoint extends Model
{
    protected $fillable = [
        'id', 'venue_id', 'name', 'parent_checkpoint', 'type'
    ];

    public $timestamps = false;

    public function venue()
    {
        return $this->belongsTo('App\Venue');
    }

    public function sub_checkpoints()
    {
        return Checkpoint::where('parent_checkpoint', $this->id)->get();
    }

    public function parent_checkpoint()
    {
        return (is_null($this->parent_checkpoint) ? null : $this->belongsTo('App\Checkpoint', 'parent_checkpoint', 'id'));
    }

    public function ticket_types()
    {
        return $this->hasMany('App\SeatTypes');
    }

    public static function rootCheckpoints($venue_id)
    {
        return Checkpoint::where('parent_checkpoint', null)->where('venue_id', $venue_id)->get();
    }

    public static function checkpoint_options()
    {
        return Checkpoint::pluck('name', 'id')->toArray();
    }

    public static function checkpointOptionsForVenue($venue_id, $type = null)
    {
        // Base checkpoint filter
        $checkpoints = Checkpoint::where('venue_id', $venue_id);

        // Filtering by checkpoint type
        if (!is_null($type)) {
            $checkpoints->where('type', $type);
        }

        // Returning data
        return $checkpoints->pluck('name', 'id')->toArray();
    }

    public static function options()
    {
        return [
            'parking' => 'Estacionamiento',
            'perimeter' => 'Perimetral',
            'tunnel' => 'Túnel',
            'other' => 'Otro'
        ];
    }

    public function pretty_type()
    {
        switch ($this->type) {
            case 'parking':
                return 'Estacionamiento';
                break;
            case 'perimeter':
                return 'Perimetral';
                break;
            case 'tunnel':
                return 'Túnel';
                break;
            case 'other':
                return 'Otro';
                break;
            case '':
                return 'Sin definir';
                break;
            default:
                return $this->type;
                break;
        }
    }
}
