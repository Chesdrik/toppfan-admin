<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckpointData extends Model
{
    protected $fillable = ['device_id', 'event_id', 'checkpoint_id', 'initial_battery', 'final_battery', 'start', 'end'];
    public $timestamps = false;
    
    public function checkpoint()
    {
        return $this->hasOne('App\Checkpoint', 'id', 'checkpoint_id');
    }
}
