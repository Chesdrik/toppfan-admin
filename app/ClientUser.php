<?php

namespace App;

use App\Notifications\ClientUserResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

use App\TicketDistributionSystem\TicketGroup;
use Carbon\Carbon;

class ClientUser extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    protected $fillable = ['name', 'last_name', 'password', 'email', 'active', 'remember_token', 'type', 'phone', 'distributor', 'active_on_admin'];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ClientUserResetPasswordNotification($token));
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    // public function innactive_orders()
    // {
    //     return $this->orders()->where('date', '>=', Carbon::now()->endOfDay());
    // }

    public function full_name()
    {
        return $this->name . ' ' . $this->last_name;
    }

    public function API()
    {
        return [
            'id' => $this->id,
            'name' => $this->full_name(),
            'email' => $this->email,
        ];
    }

    /**
     * The ticket groups that belong to the ClientUser
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function ticket_groups()
    {
        return $this->belongsToMany(TicketGroup::class);
        // return $this->belongsToMany(TicketGroup::class, 'role_user_table', 'user_id', 'role_id');
    }

    public static function clientUserExists($email, $except = null)
    {
        if (is_null($except)) {
            $client_user = ClientUser::where('email', $email)->first();
        } else {
            $client_user = ClientUser::where('email', $email)->where('id', '!=', $except)->first();
        }

        return ($client_user ? true : false);
    }

    public static function byEmail($email)
    {
        return ClientUser::where('email', $email)->first();
    }

    public function order_temps()
    {
        return $this->hasMany('App\OrderTemp');
    }

    public function order_temps_for_seat_selection()
    {
        // Defining array
        $order_temps = [
            'general' => [],
            'seated' => [],
        ];

        // Checking seats in temporary order for current user
        foreach ($this->order_temps as $order_temp) {
            if (is_null($order_temp->seat_id)) {
                // General zone
                $order_temps['general'][$order_temp->ticket_type_id] = $order_temp->amount;
            } else {
                // Seated
                $order_temps['seated'][$order_temp->ticket_type_id][] = $order_temp->seat_id;
            }
        }

        // Returning data
        return $order_temps;
    }
}
