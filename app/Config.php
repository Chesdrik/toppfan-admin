<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'system_id', 'key', 'value'
    ];
    public $timestamps = false;

    public function system()
    {
        return $this->belongsTo('App\System');
    }
}
