<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Event;
use App\TicketType;
use App\Order;
use App\Ticket;
use App\ClientUser;
use App\EventSeatAvailability;

class CreateOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:orders {event_id} {order_amount} {tickets_per_order} {ticket_type_id} {client_user_id} {type} {courtesy} {ticket_price}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates bulk orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    // - php artisan create:orders 71 32 1 331 1 other true 1.00
    // - php artisan create:orders 71 32 1 332 1 other true 1.00

    // All Access = 9 * 80 boletos
    // php artisan create:orders 44 80 1 9 true

    // Medios = 7 * 56 boletos
    // php artisan create:orders 44 56 1 7 true

    // STUNAM =
    // 2165 Planta alta - ticket_type_id=79
    // - php artisan create:orders 69 2165 1 79 54 stunam true
    // 325 Planta baja - ticket_type_id=80
    // - php artisan create:orders 69 325 1 80 54 stunam true

    // AAPAUNAM
    // 900 Planta alta - ticket_type_id=79
    // - php artisan create:orders 69 900 1 79 55 aapaunam true
    // 1300  Planta baja - ticket_type_id=80
    // - php artisan create:orders 69 1300 1 80 55 aapaunam true



    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Fetching user
        $client_user = ClientUser::find($this->argument('client_user_id'));

        // Defining name and price on ticket
        if ($this->argument('type') == 'stunam') {
            $name = "STUNAM";
            $price = "1.00";
        } else if ($this->argument('type') == 'aapaunam') {
            $name = "AAPAUNAM";
            $price = "1.00";
        } else {
            $name = "Boleto autogenerado";
            $price = $this->argument('ticket_price');
        }

        // Generating orders
        for ($i = 1; $i <= $this->argument('order_amount'); $i++) {
            $this->line('Creating order with ' . $this->argument('tickets_per_order') . ' tickets of type ' . $this->argument('ticket_type_id') . ' for event ' . $this->argument('event_id'));

            // Fetching ticket type
            $ticket_type = TicketType::find($this->argument('ticket_type_id'));
            $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($this->argument('event_id'), $ticket_type->id);

            // Creating order
            $order = Order::create([
                'client_user_id' => $client_user->id,
                'event_id' => $this->argument('event_id'),
                'email' => $client_user->email,
                'total' => $price * $this->argument('tickets_per_order'),
                'type' => ($this->argument('courtesy') == "true" ? 'courtesy' : 'regular'),
                'payment_method' => 'credit_card',
                'status' => 'payed',
                'selling_point' => 'web',
                'event_type' => 'App\Event'
            ]);

            // Creating tickets
            for ($j = 1; $j <= $this->argument('tickets_per_order'); $j++) {
                Ticket::create([
                    'ticket_type_id' => $ticket_type->id,
                    'order_id' => $order->id,
                    'event_id' => $this->argument('event_id'),
                    'amount' => 1,
                    'price' => $price,
                    'name' => $name,
                    'type' => 'ticket',
                    'event_type' => 'App\Event'
                ]);

                // Decrement available, increment sold
                $event_seat_availability->decrement('available');

                if ($this->argument('courtesy')) {
                    $event_seat_availability->increment('courtesy');
                } else {
                    $event_seat_availability->increment('sold');
                }
            }
        }
    }
}
