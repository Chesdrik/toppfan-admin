<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EventGroup;
use App\EventGroupSeat;
use App\Seat;
use App\TicketType;

class EventGroupTicketStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'eventGroup:ticketStatus {event_group_id} {ticket_type_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process the status of event group tickets and a specific ticket type';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Fetching event to process and relation
        $event_group = EventGroup::find($this->argument('event_group_id'));
        $ticket_type = TicketType::find($this->argument('ticket_type_id'));
        $egtt = $event_group->ticket_types()->where('ticket_type_id', $ticket_type->id)->first();

        // Rows
        $total = 0;

        $row_ids = array();
        foreach ($ticket_type->rows as $row) {
            // Adding total seats
            $total += $row->total;

            // Adding id to array
            $row_ids[] = $row->id;
        }

        // Fetching seat ids for rows
        $seats = Seat::whereIn('row_id', $row_ids)->pluck('id');

        // Fetching data for event group seat
        $on_hold = EventGroupSeat::where('event_group_id', $event_group->id)->whereIn('seat_id', $seats)->where('status', 'on_hold')->count();
        $on_hold_sale = EventGroupSeat::where('event_group_id', $event_group->id)->whereIn('seat_id', $seats)->where('status', 'on_hold_sale')->count();
        $available = ($total - $on_hold - $on_hold_sale);

        // \Log::info([
        //     'on_hold' => $on_hold,
        //     'on_hold_sale' => $on_hold_sale,
        //     'available' => $available,
        //     'total' => $total,
        // ]);

        // Updating pivot
        $egtt->pivot->update([
            'on_hold' => $on_hold,
            'on_hold_sale' => $on_hold_sale,
            'available' => $available,
            'total' => $total,
        ]);
    }
}
