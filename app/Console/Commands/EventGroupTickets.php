<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EventGroup;
use App\Statistic;
use Log;

class EventGroupTickets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event_group:tickets {event_group_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Fetching event to process
        $event_group = EventGroup::find($this->argument('event_group_id'));

        // Processing
        $tickets = Statistic::updateOrCreate(
            ['event_id' => $event_group->id, 'statistic_type_id' => 22],
            ['data' => json_encode(EventGroup::seats_by_zone($event_group), true)]
        );

        // Printing result
        $this->line('Boletos procesados del evento: ' . $event_group->name);
    }
}
