<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Event;
use \PDF;
use Storage;

class ExportEventPDFsByIDs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:eventPDFsByIDs {order_ids*}';
    // php artisan export:eventPDFsByIDs {5377,5376,5375,5374,5373,5372,5371,5370,5369,5368}

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates PDFs for the order IDs passed as argument';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Increasing time limit
        set_time_limit(1200);

        // Setting custom paper | 9.3cm = 263.622pt | 23cm = 651.969pt |
        $customPaper = array(0, 0, 263.622, 651.969);

        // Iterating through orders
        foreach ($this->argument('order_ids') as $order_id) {
            // Fetching order
            $order = Order::find($order_id);
            $this->line("Order ID: {$order->id}");
            
            // Fetching order data


            // Generating QRs
            foreach ($order->tickets as $ticket) {
                $ticket->verify_qr_existance();
                $filename = "public/export/{$order->event_id}/ids/{$order->id}_{$ticket->id}.pdf";

                if (!\Storage::exists($filename)) {
                    $start = microtime(true);

                    // Generating PDF
                    $pdf = PDF::loadView('admin.orders.pdf.courtesy', compact('order', 'ticket'))->setPaper($customPaper, 'portrait');
                    $content = $pdf->download()->getOriginalContent();

                    // Saving to disk
                    \Storage::put($filename, $content);

                    // Output line
                    $time_elapsed_secs = microtime(true) - $start;

                    $this->line("PDF orden {$order->id} generado en {$time_elapsed_secs} segundos");

                    $order->update(['pdf_status' => 'processed']);
                }
            }
        }
    }
}
