<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\TicketType;
use App\Row;
use App\Seat;

class GenerateSeats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:seats {ticket_type_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Fetching ticket type
        $ticket_type = TicketType::find($this->argument('ticket_type_id'));
        $this->line("Ticket type {$ticket_type->name}: ");

        // Iterating through rows
        foreach ($ticket_type->rows as $row) {
            $this->line("Row {$row->id} with name {$row->name} startinng at {$row->start} with {$row->total} seats");

            // Iterating through seats
            for ($i = 1; $i <= $row->total; $i++) {
                $this->line("   - Seat {$i}");

                // Creating seat
                Seat::updateOrCreate([
                    'row_id' => $row->id,
                    'name' => $i,
                ]);
            }
        }
    }
}
