<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Ticket;
use App\User;

class GroupOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'group:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Group courtesy orders in one user/order, adds order name on ticket';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Fetching all orders and defining base order
        $orders = Order::where('event_id', 15)->get();
        $orders->shift();

        // Base data
        $user_id = 6;
        $order_id = 1095;

        // Parsing orders
        foreach ($orders as $order) {
            // Updating tickets
            foreach ($order->tickets as $ticket) {
                $ticket->update([
                    'order_id' => $order_id,
                    'name' => $order->client_user->name,
                ]);
            }

            // Deleting order
            $order->delete();
        }
    }
}
