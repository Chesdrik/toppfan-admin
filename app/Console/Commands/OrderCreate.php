<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\EventSeatAvailability;
use App\TicketType;
use App\Order;
use App\Ticket;

class OrderCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $events = EventSeatAvailability::all();

            // Creating order
            foreach ($events as $event) {
                $order = Order::create([
                    'client_user_id' => 1,
                    'event_id' => $event->event_id,
                    'email' => 'diego@filloy.com.mx',
                    'total' => 0,
                ]);

                $ticket_type = TicketType::find($event->ticket_type_id);
                $total = ($ticket_type->price * $event->total);

                // Adding tickets to order
                $tickets = Ticket::create([
                    'event_id' => $event->event_id,
                    'ticket_type_id' => $ticket_type->id,
                    'order_id' => $order->id,
                    'amount' => $event->total,
                    'type' => 'ticket',
                    'price' => $ticket_type->price,
                    'name' => 'Diego Filloy POS'
                ]);

                // Updating order total
                $order->total = $total;
                $order->save();

                // Generating QRs
                // $qrs = $order->generateQRs();
            }

            echo 'se han generado las ordenes';
        } catch (\Throwable $th) {
            \Log::info($th);
            echo 'No se pudieron generar las ordenes';
        }
    }
}
