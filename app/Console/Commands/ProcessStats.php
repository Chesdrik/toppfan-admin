<?php

/**
 *
 *
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Event;
use App\Statistic;
use App\TicketReading;
use Log;

class ProcessStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process all orders';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            foreach(Event::all() as $event){

                // Processing
                if(count($event->orders) > 0){
                    $orders = Statistic::updateOrCreate(
                        ['event_id' => $event->id, 'statistic_type_id' => 17],
                        ['data' => json_encode(Event::ordersForChart($event), true)]
                    );
                    Log::info($orders);
                }
            }

            $this->line('Estadísticas de órdenes procesadas');
            
            $all_stats = Statistic::select('event_id', 'data')
            ->where('statistic_type_id', 17)
            ->get();
            
            if(count($all_stats) > 0){
                // Processing
                $all_orders = Statistic::updateOrCreate(
                    ['event_id' => $event->id, 'statistic_type_id' => 18],
                    ['data' => json_encode(Statistic::allOrdersForChart($event), true)]
                );

                Log::info($all_orders);
            
                $this->line('Estadísticas generales de órdenes procesadas');
            }

        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
