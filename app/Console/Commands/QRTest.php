<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Ticket;
use Illuminate\Support\Facades\Http;

class QRTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:qr';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Testing QR reading in server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Fetch 5 unused tickets
        $tickets = Ticket::where('amount', '>', 'used')->take(5)->get();

        // POST to API /qr_read
        foreach($tickets as $ticket){
            // One request per unused QR
            for($i = $ticket->used; $i < $ticket->amount; $i++){
                // Generating request
                $response = Http::post(url('/pumasgol/api/qr_read'), [
                    'qr_content' => encrypt(json_encode([
                        'ticket_id' => $ticket->id,
                        'order_id' => $ticket->order_id
                    ])),
                ]);

                print('Reading ticket '.$ticket->id.' from order '.$ticket->order_id.' ('.($i+1).' of '.$ticket->amount.')
');
                // Sleeping for simulation
                sleep(2);
            }
        }

        return 0;
    }
}
