<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Event;
use App\EventSeatAvailability;

class RecountSeats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'count:seats {event_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count the available seats of an event';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $event = Event::find($this->argument('event_id'));

        foreach($event->ticket_template->ticket_types->where('type', 'seat') as $type){
            $total = Event::total_tickets_by_type($type, $event);
            $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $type->id);
            $e_s->update(['available' => $e_s->total - ($total['on_hold'] + $total['on_hold_sale'])]);
        }

        $this->line('Disponibilidad actualizada');
    }
}
