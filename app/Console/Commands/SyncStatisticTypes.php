<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\StatisticType;
use DB;

class SyncStatisticTypes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:stattypes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        StatisticType::truncate();

        $types = Http::post(env('SERVER_URL').'/api/sync/statistic_types', [])->json();

        foreach($types as $type){
            $stat = StatisticType::create([
                'id' => $type['id'],
                'name' => $type['name']
            ]);
        }

        $this->line('Sincronización de tipos de estadísticas completada');
    }
}
