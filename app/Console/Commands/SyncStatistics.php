<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Statistic;

class SyncStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:stats {event}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $statistics = Http::post(env('SERVER_URL') . '/api/sync/event/statistics', ['event_id' => $this->argument('event')])->json();
        foreach ($statistics as $statistic) {

            $new_stat = Statistic::updateOrCreate([
                'event_id' => $statistic['event_id'],
                'statistic_type_id' => $statistic['statistic_type_id'],
            ], [
                'data' => $statistic['data']
            ]);
        }

        $this->line(count($statistics) . ' estadísticas sincronizadas');
        $this->line('Sincronización finalizada');
    }
}
