<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;

class SynchronizeOrderAssignment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assigned_by:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach(Order::where('type', 'courtesy')->get() as $order){
            if($order->tickets->where('assigned_by', '!=', null)->count() > 0){
                $order->update(['assigned_by' => $order->tickets->where('assigned_by', '!=', null)->first()->assigned_by]);
            }
        }

        $this->line('Órdenes sincronizadas');
    }
}
