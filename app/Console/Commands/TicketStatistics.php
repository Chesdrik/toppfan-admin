<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Event;
use App\Statistic;
use Log;

class TicketStatistics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticket:status {event_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process the status of event tickets';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Fetching event to process
        $event = Event::find($this->argument('event_id'));

        // Processing
        $tickets = Statistic::updateOrCreate(
            ['event_id' => $event->id, 'statistic_type_id' => 21],
            ['data' => json_encode(Event::seats_by_zone($event), true)]
        );

        // Printing result
        $this->line('Boletos procesados del evento: ' . $event->name);
    }
}
