<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\OrderTemp;
use App\EventSeatAvailability;
use App\EventSeat;
use App\EventGroupSeat;
use Carbon\Carbon;

class cleanTempOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:tempOrders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $current_time = Carbon::now()->timezone('America/Mexico_City')->format('Y-m-d H:i:s');
        $expired_orders = OrderTemp::where('expiration_time', '<', $current_time)->get();

        foreach ($expired_orders as $order_temp) {
            // Removing seat availability
            if ($order_temp->seat_id != null) {
                $event_seat = $order_temp->eventSeat();
                $event_seat->update(['status' => 'available']);
            }

            // Returning stock to table
            if ($order_temp->eventable_type = 'App\Event') {
                \Log::info('Event temp_order_id = ' . $order_temp->eventable_id);
                $esa = EventSeatAvailability::where('event_id', $order_temp->eventable_id)
                    ->where('ticket_type_id', $order_temp->ticket_type_id);

                $esa->increment('available', $order_temp->amount);
                $esa->decrement('on_hold_sale', $order_temp->amount);

                if (!is_null($order_temp->seat_id)) {
                    // Releasing seat holding
                    EventSeat::where('event_id', $order_temp->eventable_id)->where('seat_id', $order_temp->seat_id)->delete();
                }
            } else {
                \Log::info('EventGroup temp_order_id = ' . $order_temp->eventable_id);
                $egt = $order_temp->eventable->get_ticket($order_temp->ticket_type_id);

                $egt->increment('available', $order_temp->amount);
                $egt->decrement('on_hold_sale', $order_temp->amount);

                if (!is_null($order_temp->seat_id)) {
                    // Releasing seat holding
                    EventGroupSeat::where('event_group_id', $order_temp->eventable_id)->where('seat_id', $order_temp->seat_id)->delete();
                }
            }

            // Deleting temporary order
            $order_temp->delete();
        }

        $this->line('Limpieza de temp orders completada');
    }
}
