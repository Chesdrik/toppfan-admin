<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class cleanTicketReadings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:tickets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleans ticket readings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::table('tickets')->update(['used' => 0]);
    }
}
