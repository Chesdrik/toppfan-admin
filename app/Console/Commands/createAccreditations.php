<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Accreditation;
// use App\AccreditationHistory;

class createAccreditations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:accreditations {amount} {system_id} {team_id} {ticket_type_id} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates bulk accreditations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $this->argument('ticket_type_id')
        // $this->argument('system_id')
        // $this->argument('name')


        for ($i = 1; $i <= $this->argument('amount'); $i++) {
            $this->line("Accreditation {$i} for ticket_type_id = {$this->argument('ticket_type_id')}");

            // Creating accreditation
            $accreditation = Accreditation::create([
                'system_id' => $this->argument('system_id'),
                'price' => 0.0,
                'ticket_type_id' => $this->argument('ticket_type_id'),
                'folio' => "SUP-" . str_pad($i, 5, '0', STR_PAD_LEFT),
                'name' => str_pad($i, 5, '0', STR_PAD_LEFT) . "-" . $this->argument('name'),
                'type' => 'operative',
                'version' => 1
            ]);

            // Associating team
            $accreditation->teams()->sync($this->argument('team_id'));

            // Generating QR
            $accreditation->verify_qr_existance();

            // $accreditation_history = AccreditationHistory::create([
            //     'accreditation_id' => $accreditation->id,

            // ])
        }
    }
}
