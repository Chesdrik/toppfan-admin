<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Event;
use App\TicketReading;

class deleteTicketReadings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticketReadings:delete {event}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes all ticket readings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $event = Event::find($this->argument('event'));

        foreach ($event->orders as $order) {
            foreach ($order->tickets as $ticket) {
                foreach ($ticket->ticket_readings as $ticket_reading) {
                    $ticket_reading->delete();
                }
            }
        }
    }
}
