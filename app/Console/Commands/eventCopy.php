<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Event;
use App\Order;
use App\Ticket;
use App\TicketReading;

class eventCopy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:copy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a duplicate event with new id (ideal for keeping data from old event and using same QR access)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Asking for data
        $original_event_id = $this->ask('Event id to copy?');
        $new_event_id = $this->ask('New event id?');

        $original_event = Event::find($original_event_id);

        // Copy orders
        foreach($original_event->orders as $original_order){
            // Copy original order
            $new_order = $original_order->toArray();

            // Updating some info
            unset($new_order['id']);
            $new_order['event_id'] = $new_event_id;

            // Creating order
            $new_order = Order::create($new_order);

            // Copying tickets
            foreach($original_order->tickets as $original_ticket){
                // Copy original ticket data
                $new_ticket = $original_ticket->toArray();

                // Updating some info
                unset($new_ticket['id']);
                $new_ticket['event_id'] = $original_event_id;
                $new_ticket['order_id'] = $original_order->id;

                // Creating new ticket
                $new_ticket = Ticket::create($new_ticket);

                // Updating ticket event_id to the new one
                $original_ticket->update(['event_id' => $new_event_id, 'order_id' => $new_order->id]);

                foreach($original_ticket->ticket_readings as $ticket_reading){
                    // Update ticket reading ticket_id al nuevo ticket (evento original)
                    $ticket_reading->update(['ticket_id' => $new_ticket->id]);
                }
            }
        }
    }
}
