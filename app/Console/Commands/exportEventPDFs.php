<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\Event;
use \PDF;
use Storage;

class exportEventPDFs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:eventPDFs {event_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates PDFs for the frikking sindicate who can\'t use a damn app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        set_time_limit(1200);

        $event = Event::find($this->argument('event_id'));
        $this->line("Generando PDFs para el evento {$event->id}");

        $orders = $event->courtesy_orders()
            ->where('pdf_status', 'pending')
            ->take(130)
            ->get();


        // Marking orders as 'processing'
        foreach ($orders as $order) {
            $order->update(['pdf_status' => 'processing']);
        }

        // 9.3cm = 263.622pt
        // 23cm = 651.969pt
        $customPaper = array(0, 0, 263.622, 651.969);

        foreach ($orders as $order) {
            $start = microtime(true);
            // Fetching order data
            $filename = "public/export/{$event->id}/{$order->id}.pdf";
            if (!\Storage::exists($filename)) {
                // Generating QRs
                foreach ($order->tickets as $ticket) {
                    $ticket->verify_qr_existance();
                }

                // Generating PDF
                $pdf = PDF::loadView('admin.orders.pdf.courtesy', compact('order'))->setPaper($customPaper, 'portrait');;
                $content = $pdf->download()->getOriginalContent();

                // // Saving to disk
                \Storage::put($filename, $content);

                // Output line
                $time_elapsed_secs = microtime(true) - $start;

                $this->line("PDF orden {$order->id} generado en {$time_elapsed_secs} segundos");

                $order->update(['pdf_status' => 'processed']);
            }
        }
    }
}
