<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \DB;
use App\Row;
use App\Seat;
use App\EventSeat;

class reprocessRows extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reprocess:rows {event_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Takes all rows and recreats seats with new structure';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('event_seat')->truncate();
        DB::table('seats')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach (Row::orderBy('order', 'DESC')->get() as $row) {
            for ($i = 1; $i <= $row->total; $i++) {
                $seat = Seat::create([
                    'row_id' => $row->id,
                    'name' => $i,
                ]);

                // This is aprox 20% of seats sold
                $on_hold_sale = false;
                if (rand(1, 100) > 80) {
                    EventSeat::create([
                        'event_id' => $this->argument('event_id'),
                        'seat_id' => $seat->id,
                        'status' => 'on_hold_sale'
                    ]);

                    $on_hold_sale = true;
                }

                // And aprox 4% on_hold
                if (!$on_hold_sale && rand(1, 100) > 80) {
                    EventSeat::create([
                        'event_id' => $this->argument('event_id'),
                        'seat_id' => $seat->id,
                        'status' => 'on_hold'
                    ]);
                }
            }
        }
    }
}
