<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Event;
use App\Order;
use App\Ticket;

class spreadOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'spread:orders {event_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transform tickets with amount > 1 to individual tickets because PANIC!';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $event = Event::find($this->argument('event_id'));
        $this->line('Event ' . $event->name);

        $i = 1;
        foreach ($event->regular_orders as $order) {
            foreach ($order->tickets as $ticket) {
                if ($ticket->amount > 1) {
                    $this->line("{$i} - ticket_id {$ticket->id} in order_id {$order->id} spread into {$ticket->amount} tickets!");
                    $i++;

                    for ($j = 1; $j <= $ticket->amount; $j++) {
                        $this->line("   {$i} - {$j} - ticket_id {$ticket->id} !");

                        $new_ticket = $ticket->replicate();
                        $new_ticket->amount = 1;
                        $new_ticket->price = '50.00';
                        $new_ticket->save();
                    }

                    // Removing original ticket
                    $ticket->delete();
                }
            }
        }
    }
}
