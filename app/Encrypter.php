<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
// use Illuminate\Encryption\Encrypter;


// https://stackoverflow.com/questions/34871579/how-to-encrypt-plaintext-with-aes-256-cbc-in-php-using-openssl
// http://phpfiddle.org/lite/code/9epi-j5v2
// Define encryption key and initialization vector

// $encryption_key = openssl_random_pseudo_bytes(32);
// openssl rand -base64 32
// openssl rand -base64 16
// $encryption_key = base64_decode(env('ENCRYPTION_KEY'));
// $iv = base64_decode(env('ENCRYPTION_IV'));

class Encrypter extends Model
{
    protected $fillable = ['content', 'encryption_key', 'initialization_vector'];

    public $content;
    public $encryption_key;
    public $initialization_vector;
    public $encrypted_content;

    public function setContentAttribute($value){
        $this->attributes['content'] = $value;
        $this->attributes['encrypted_content'] = openssl_encrypt($value, 'aes-256-cbc', $this->encryption_key, 0, $this->initialization_vector);

        $this->content = $this->attributes['content'];
        $this->encrypted_content = $this->attributes['encrypted_content'];
    }

    // Try mutators
    public function setEncryptionKeyAttribute($value){
        $this->attributes['encryption_key'] = base64_decode($value);
        $this->encryption_key = $this->attributes['encryption_key'];
    }

    public function setInitializationVectorAttribute($value){
        $this->attributes['initialization_vector'] = base64_decode($value);
        $this->initialization_vector = $this->attributes['initialization_vector'];
    }
}
