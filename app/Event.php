<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TicketReading;
use App\OrderTemp;
use Carbon\Carbon;
use Log;
use Storage;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'team_id', 'date', 'active', 'venue_id', 'ticket_template_id', 'img', 'img_slideshow', 'final_score', 'won', 'on_sale', 'data_readings'
    ];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function regular_orders()
    {
        return $this->hasMany('App\Order')->where('type', 'regular')->orderBy('created_at', 'DESC');
    }

    public function courtesy_orders()
    {
        return $this->hasMany('App\Order')->where('type', 'courtesy')->orderBy('created_at', 'DESC');
    }

    // public function tickets()
    // {
    //     return $this->hasMany('App\Ticket');
    // }

    public function venue()
    {
        return $this->belongsTo('App\Venue');
    }

    public function checkpoint_data()
    {
        return $this->hasMany('App\CheckpointData');
    }

    public function team()
    {
        return $this->belongsTo('App\Team');
    }

    public function alerts()
    {
        return $this->hasMany('App\Alert');
    }

    public function accreditations()
    {
        return $this->belongsToMany('App\Accreditation')
            ->withPivot(['name', 'email', 'used', 'version']);
    }

    public function accreditations_event($id)
    {
        return $this->belongsToMany('App\Accreditation')
            ->withPivot(['name', 'email', 'used', 'version'])
            ->where('accreditation_event.accreditation_id', $id);
    }

    public function event_groups()
    {
        return $this->belongsToMany('App\EventGroup');
    }

    public function event_seat_availability()
    {
        return $this->hasMany('App\EventSeatAvailability');
    }

    public function seats()
    {
        return $this->belongsToMany('App\Seat')->withPivot('status');
    }

    public function ticket_groups()
    {
        return $this->hasMany('App\TicketDistributionSystem\TicketGroup');
    }

    // public function pending_accreditations()
    // {
    //     return $this->belongsToMany('App\Accreditation')
    //         ->withPivot(['event_id', 'name', 'email', 'used'])
    //         ->where('accreditation_event.name', null)
    //         ->where('accreditation_event.email', null)
    //         ->get();
    // }

    public function assigned_accreditations()
    {
        return $this->belongsToMany('App\Accreditation', 'accreditation_event', 'event_id', 'accreditation_id')
            ->withPivot(['event_id', 'name', 'email', 'used', 'version']);
        // ->where('accreditation_event.name', '!=', null)
        // ->orWhere('accreditation_event.email', '!=', null)
        // ->where('accreditation_event.email', '!=', null)
        // ->get();
    }

    /**
     * Get all of the temp_orders for the Event
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function temp_orders()
    {
        // return $this->hasMany(OrderTemp::class, 'event_id', 'id');
        return $this->morphMany(OrderTemp::class, 'eventable');
    }
    // public function temp_orders()
    // {
    //     return $this->hasMany(OrderTemp::class, 'event_id', 'id');
    // }



    public function API()
    {
        Carbon::setLocale('es');
        $date = Carbon::parse($this->date);
        $date = $date->format('l jS F Y'); //falta una paquetería para mostrarlo en español
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date' => $this->date,
            'team' => $this->team->name,
            'active' => $this->active,
            'venue_id' => $this->venue->id,
            'venue' => $this->venue->name,
        ];
    }

    public function API_POS()
    {
        setlocale(LC_ALL, 'es_ES');
        Carbon::setLocale('es');
        $date = Carbon::parse($this->date);

        $month_txt = mb_strtoupper($date->formatLocalized('%B'));

        // $date = $date->format('l jS F Y'); //falta una paquetería para mostrarlo en español
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date' => [
                'full' => $date->format('Y-m-d H:i:s'),
                'pretty' => $date->format('j') . ' de ' . ucfirst(strtolower($month_txt)) . ' ' . $date->format('Y') . ' - ' . $date->format('H:i') . ' hrs',
                'day' => $date->format('d'),
                'time' => $date->format('H:i'),
                'month_txt' => $month_txt,
            ],
            'image' => asset($this->img_slideshow_path()),
            'team' => $this->team->name,
            'active' => $this->active,
            'venue_id' => $this->venue->id,
            'venue' => $this->venue->name,
        ];
    }

    public function date_start()
    {
        return pretty_date($this->date);
    }

    public function date_end()
    {
        $date = Carbon::parse($this->date);

        return pretty_date($date->add(105, 'minute'));
    }

    public function datespan($multiline = false)
    {
        if ($multiline) {
            $start = explode(' ', $this->date_start());
            return $start[0] . '<br />' . $start[1] . ' - ' . substr($this->date_end(), -5, 5);
        } else {
            return $this->date_start() . ' - ' . substr($this->date_end(), -5, 5);
        }
    }

    public static function active()
    {
        return Event::where('date', '>=', Carbon::now()->subDay()->format('Y-m-d H:i:s'))
            ->where('active', 1)
            ->orderBy('date')
            ->get();
    }

    public function ticket_template()
    {
        // return $this->hasOne('App\TicketTemplate', 'id', 'ticket_template_id');
        return $this->belongsTo('App\TicketTemplate');
    }

    public function base_path()
    {
        return "public/events/$this->id";
    }

    public function img_path($full = true)
    {
        $img_path = $this->base_path() . "/{$this->id}_cover.png";

        if (!Storage::exists($img_path)) {
            return ($full ? asset('/assets/images/img_not_found.png') : '/public/assets/images/img_not_found.png');
        } else {
            return ($full ? Storage::url($img_path) : $img_path);
        }
    }

    public function img_slideshow_path($full = true)
    {
        $img_slideshow_path = $this->base_path() . "/{$this->id}_slideshow.jpg";

        if (!Storage::exists($img_slideshow_path)) {
            return ($full ? asset('/assets/images/img_not_found.png') : '/public/assets/images/img_not_found.jpg');
        } else {
            return ($full ? Storage::url($img_slideshow_path) : $img_slideshow_path);
        }
    }

    public function courtesies()
    {
        return $this->hasMany('App\Order')
            // ->where('courtesy', 1)
            ->where('type', 'courtesy')
            ->get();
    }

    public function syncs()
    {
        return $this->hasMany('App\Sync');
    }

    public function statistics()
    {
        return $this->hasMany('App\Statistic');
    }

    public function get_stats($type)
    {
        return $this->hasMany('App\Statistic')
            ->where('statistic_type_id', $type)
            ->first()->data ?? null;
    }

    public function parser_stats()
    {
        return [
            'general_info_tickets' => json_decode($this->get_stats(5), true),
            'sales' => json_decode($this->get_stats(1), true),
            'readings' => json_decode($this->get_stats(2), true),
            'exits' => json_decode($this->get_stats(3), true),
            'errors' => json_decode($this->get_stats(4), true),
            'general_info_accreditations' => json_decode($this->get_stats(10), true),
            'readings_accreditations' => json_decode($this->get_stats(7), true),
            'exits_accreditations' => json_decode($this->get_stats(8), true),
            'errors_accreditations' => json_decode($this->get_stats(9), true),
            'tickets_by_device' => json_decode($this->get_stats(11), true),
            'checkpoint_alerts' => json_decode($this->get_stats(12), true),
            'device_alerts' => json_decode($this->get_stats(13), true),
            'checkpoint_data' => json_decode($this->get_stats(14), true),
            'incidences' => json_decode($this->get_stats(15), true),
            'accreditations_by_device' => json_decode($this->get_stats(16), true),
            'order_event' => json_decode($this->get_stats(17), true),
        ];
    }

    public function general_info_stats()
    {
        return [
            'general_info' => json_decode($this->get_stats(5), true),
            'general_info_accreditations' => json_decode($this->get_stats(10), true),
        ];
    }

    public function ticket_analysis()
    {
        return [
            'data' => json_decode($this->get_stats(19), true)
        ];
    }

    public function accreditation_analysis()
    {
        return [
            'data' => json_decode($this->get_stats(20), true)
        ];
    }

    public function ticket_status()
    {
        return [
            'data' => json_decode($this->get_stats(21), true)
        ];
    }

    public function sales()
    {
        return [
            'sales' => json_decode($this->get_stats(1), true),
        ];
    }

    /*
        Stat processing
     */

    public static function eventStatisticsforChart(Event $event) //bucardespues
    {
        return [
            'general_info' => Event::eventGeneralInfo($event),
            'sales' => Event::eventSalesForChart($event),
            'readings' => Event::eventReadingsForChart($event),
            'exits' => Event::eventExitsForChart($event),
            'errors' => Event::eventErrorsForChart($event),
            'checkpoint_data' => Event::checkpointDataForChart($event),
        ];
    }


    public static function eventSalesForChart(Event $event, $json = false, $type_order)
    {

        $sold_tickets = array();
        // TODO Agrupar ventas por tipo de boleto y hacer en stack (horizontal creo que conviene)
        // TODO Procesar ventas y guardar en JSON para no calcular cada vez que se carga la pagina
        // Parsing event reading
        $tickets = collect();
        foreach ($event->orders->where('type', $type_order) as $order) {
            foreach ($order->tickets()->orderBy('ticket_type_id')->get() as $ticket) {
                $tickets[] = $ticket;
            }
        }

        // Checking if there are tickets for this event
        if (count($tickets) == 0) {
            return ($json ? json_encode([], true) : []);
        }

        $sold_tickets = [
            'colors' => '',
            'labels' => [
                'data' => array(),
            ],
            'data' => [
                'data' => array(),
            ],
        ];

        foreach ($tickets as $ticket) {
            // Fetching dates
            $created_at = new Carbon($ticket->created_at);

            // Appending data and labels
            $sold_tickets['labels']['data'][$created_at->format('Y-m-d H:i')] = $created_at->format('Y-m-d H:i');
            if (!array_key_exists($created_at->format('Y-m-d H:i'), $sold_tickets['data']['data'])) {
                $sold_tickets['data']['data'][$created_at->format('Y-m-d H:i')] = 0;
            }
            $sold_tickets['data']['data'][$created_at->format('Y-m-d H:i')] += $ticket->amount;
            $r = rand(0, 255);
            $g = rand(0, 255);
            $b = rand(0, 255);
            $a = rand(0, 255);
            $sold_tickets['colors'] .= "'rgba(" . $r . "," . $g . "," . $b . ", " . $a . ")', ";
        }

        $l_data = array_unique($sold_tickets['labels']['data']);
        $data = array_unique($sold_tickets['data']['data']);

        $new_ldata = array();
        foreach ($l_data as $key => $value) {
            $new_ldata[] = $value;
        }
        $new_data = array();
        foreach ($data as $key => $value) {
            $new_data[] = $value;
        }
        $sold_tickets['labels']['data'] = $new_ldata;
        $sold_tickets['data']['data'] = $new_data;

        // Processing data for ChartJS use
        $sold_tickets['labels']['label'] = "'" . implode("', '", $new_ldata) . "'";
        $sold_tickets['data']['string'] = implode(", ", $new_data);

        // Returning processed data as json or array
        if ($json) {
            return json_encode($sold_tickets, true);
        } else {
            return $sold_tickets;
        }
    }

    public static function eventReadingsForChart(Event $event, $json = false, $type_order)
    {
        return Event::eventStatsForChart($event, 2, $json, $type_order);
    }

    public static function eventExitsForChart(Event $event, $json = false, $type_order)
    {
        return Event::eventStatsForChart($event, 3, $json, $type_order);
    }

    public static function eventErrorsForChart(Event $event, $json = false, $type_order)
    {
        return Event::eventStatsForChart($event, 4, $json, $type_order);
    }

    public static function checkpointDataForChart(Event $event, $json = false)
    {
        return Event::checkpointStatsForChart($event, $json);
    }

    public static function eventGeneralInfo(Event $event, $json = false, $type_order)
    {
        // Fetching all tickets for event
        $tickets = collect();
        foreach ($event->orders->where('type', $type_order) as $order) {
            foreach ($order->tickets()->orderBy('ticket_type_id')->get() as $ticket) {
                $tickets[] = $ticket->id;
            }
        }

        // Checking if there are tickets for this event
        if (count($tickets) == 0) {
            return ($json ? json_encode([], true) : []);
        }

        // Defining timestamp
        $timespan = TicketReading::timespan($tickets);

        // If there are no tickets
        if ($timespan == null) {
            return ($json ? json_encode([], true) : []);
        }

        // First and last ticket reading
        $first_ticket_reading = $timespan['start'];
        $last_ticket_reading = $timespan['end'];

        // Total tickets
        $total_tickets = count($tickets);

        // Total tickets read
        $total_tickets_read = TicketReading::whereIn('ticket_id', $tickets)
            ->distinct('ticket_id')
            ->where('error', 0)
            ->count();

        // Total readings
        $total_readings = TicketReading::whereIn('ticket_id', $tickets)
            ->where('error', 0)
            ->count();

        // Total errors
        $total_errors = TicketReading::whereIn('ticket_id', $tickets)
            ->where('error', 1)
            ->count();

        // Compiling data
        $return = [
            'first_ticket_reading' => $first_ticket_reading,
            'last_ticket_reading' => $last_ticket_reading,
            'total_tickets' => $total_tickets,
            'total_tickets_read' => $total_tickets_read,
            'total_readings' => $total_readings,
            'total_errors' => $total_errors,
        ];


        if ($json) {
            return json_encode($return, true);
        } else {
            return $return;
        }
    }

    /*
        Past and upcoming events
     */
    // public function passedEvent()
    public static function past_events($take = null, $venue = null, $team = null)
    {
        // Fetching events with order
        $events = Event::where('date', '<=', Carbon::now())->orderBy('date', 'DESC');

        if (!is_null($venue)) {
            $events->where('venue_id', $venue);
        } // Filtering by venue
        if (!is_null($team)) {
            $events->where('team_id', $team);
        } // Filtering by team
        if (!is_null($take)) {
            $events->take($take);
        } // Limiting results

        // Returning results
        return $events;
    }

    // public static function upcoming()
    public static function upcoming_events($take = null, $venue = null, $team = null, $reverse = true, $public_event = null)
    {
        // Fetching events with order
        $events = Event::where('date', '>=', Carbon::now()->startOfDay())
            ->orderBy('date', 'ASC'); //

        // Filtering by venue
        if (!is_null($venue)) {
            $events->where('venue_id', $venue);
        }

        // Filtering by team
        if (!is_null($team)) {
            $events->where('team_id', $team);
        }

        // Filtering public_event
        if (!is_null($public_event)) {
            $events->where('public_event', ($public_event ? 1 : 0));
        }

        // Limiting results
        if (!is_null($take)) {
            $events->take($take);
        }


        // Returning results
        if ($reverse) {
            return $events->get()->reverse();
        } else {
            return $events->get();
        }
    }

    public static function upcoming_on_sale_events()
    {
        $return_events = array();

        // Fetching all teams
        foreach (Team::all() as $team) {
            // Fetching team on sale events
            $events = $team->events->where('date', '>', Carbon::now()->startOfDay())
                ->where('public_event', 1)
                ->where('on_sale', 1);

            // Parsing events for POS API
            $team_events = array();
            foreach ($team->events_on_sale() as $event) {
                $team_events[] = $event->API_POS();
            }

            // Fetching events with order
            $return_events[] = [
                'team' => [
                    'id' => $team->id,
                    'name' => $team->name,
                ],
                'events' => $team_events
            ];
        }

        // Returning data
        return $return_events;
    }

    public static function upcoming_on_sale_events_all()
    {
        $return_events = array();

        // Fetching team on sale events
        $events = Event::where('date', '>', Carbon::now()->startOfDay())
            ->where('public_event', 1)
            ->where('on_sale', 1)
            ->get();

        // Parsing events for POS API
        foreach ($events as $event) {
            $return_events[] = $event->API_POS();
        }

        // Returning data
        return $return_events;
    }

    public static function upcoming_on_sale_events_slideshow()
    {
        $events_slideshow = array();

        foreach (Event::upcoming_on_sale_events() as $team) {
            foreach ($team['events'] as $event) {
                $events_slideshow[] = [
                    'id' => $event['id'],
                    'name' => $event['name'],
                    'date' => $event['date']['pretty'],
                    'image' => asset($event['image']),
                    'venue' => $event['venue'],
                ];
            }
        }

        return $events_slideshow;
    }

    public function finished_event()
    {
        return ($this->date > Carbon::now()) ? false : true;
    }

    public function get_ticket($type_id)
    {
        return $this->ticket_template->ticket_types()->where('ticket_type_id', $type_id)->first();
    }

    public static function alertStatsForChart(Event $event, $json)
    {
        // Checking if there are any alerts to process
        if ($event->alerts()->count() == 0) {
            return ($json ? json_encode([], true) : []);
        }

        $return = array();
        $return['colors'] = "";
        $tipo = 1;
        $check = 1;
        $x = array();
        $y = array();
        foreach ($event->alerts as $alert) {
            if (!in_array($alert->type, $x)) {
                $alertas[$alert->type] = 1;
                $x[] =  $alert->type;
            } else {
                $alertas[$alert->type] += $tipo;
            }

            if (!in_array($alert->checkpoint_id, $y)) {
                $checkpoints[$alert->checkpoint->name] = 1;
                $y[] = $alert->checkpoint_id;
            } else {
                $checkpoints[$alert->checkpoint->name] += $check;
            }
            $r = rand(0, 255);
            $g = rand(0, 255);
            $b = rand(0, 255);
            $a = rand(0, 255);
            $return['colors'] .= "'rgba(" . $r . "," . $g . "," . $b . ", " . $a . ")', ";
        }
        foreach ($checkpoints as $check_name => $check_value) {
            $return['checkpoints']['label'][] = $check_name;
            $return['checkpoints']['data'][] = $check_value;
        }

        foreach ($alertas as $alert_type => $alert_value) {
            $return['alerts']['label'][] = $alert_type;
            $return['alerts']['data'][] =  $alert_value;
        }

        $return['alerts']['data'] = implode(", ", $return['alerts']['data']);
        $return['alerts']['label'] = "'" . implode("', '", $return['alerts']['label']) . "'";
        $return['checkpoints']['data'] = implode(", ", $return['checkpoints']['data']);
        $return['checkpoints']['label'] = "'" . implode("', '", $return['checkpoints']['label']) . "'";

        return json_encode($return);
    }

    public static function ordersForChart($event, $json = false)
    {
        $data = [
            'payment_methods' => [
                'colors' => '',
                'labels' => '',
                'string' => array()
            ],
            'selling_points' => [
                'colors' => '',
                'labels' => '',
                'string' => array()
            ],
            'status' => [
                'colors' => '',
                'labels' => '',
                'string' => array()
            ],
        ];

        $payment_methods = array('cash', 'debit_card', 'credit_card');
        $selling_points = array('ticket_office', 'web', 'kiosk');
        $status = array('registered', 'payed', 'cancelled');

        foreach ($payment_methods as $type) {
            $color = rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255);
            $rows = count($event->orders->where('payment_method', $type));

            $data['payment_methods']['colors'] .= "'rgba(" . $color . ")', ";
            $data['payment_methods']['labels'] .= "'" . pretty_type($type) . "',";
            $data['payment_methods']['string'][$type] = $rows;
        }

        foreach ($selling_points as $type) {
            $color = rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255);
            $rows = count($event->orders->where('selling_point', $type));

            $data['selling_points']['colors'] .= "'rgba(" . $color . ")', ";
            $data['selling_points']['labels'] .= "'" . pretty_type($type) . "',";
            $data['selling_points']['string'][$type] = $rows;
        }

        foreach ($status as $type) {
            $color = rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255);
            $rows = count($event->orders->where('status', $type));

            $data['status']['colors'] .= "'rgba(" . $color . ")', ";
            $data['status']['labels'] .= "'" . pretty_type($type) . "',";
            $data['status']['string'][$type] = $rows;
        }

        return ($json ? json_encode($data, true) : $data);
    }



    public static function selectize($event_group = null)
    {
        return Event::orderBy('name')->pluck('name', 'id')->toArray();

        // if (is_null($event_group)) {
        //     return Event::orderBy('name')->pluck('name', 'id')->toArray();
        // } else {
        //     $e = $event_group->events()->orderBy('name')->pluck('name', 'id')->toArray();
        //     dd($e);
        //     return Event::where($types)->orderBy('name')->pluck('name', 'id')->toArray();
        // }
    }


    public function seats_by_zone_sidebar($json = false)
    {
        $ticket_types = array();
        foreach ($this->ticket_template->ticket_types as $type) {
            // Adding zone to array in case it dosnt exist
            if (!array_key_exists($type->zone->parent_zone, $ticket_types)) {
                $parent_zone = Zone::find($type->zone->parent_zone);

                if (!is_null($parent_zone)) {
                    $ticket_types[$parent_zone->id] = array();
                    $ticket_types[$parent_zone->id]['zone'] = [
                        'name' => $parent_zone->name,
                        'zone_id' => $parent_zone->id,
                        'color' => '',
                    ];
                }
            }

            if (!is_null($parent_zone)) {
                // Appending subzone
                $ticket_types[$parent_zone->id]['subzones'][] = [
                    'name' => $type->name,
                    'zone_id' => $type->zone->id,
                    'color' => $type->color
                ];

                // Defining zone color with ticket type color since we are not storing that info on db
                $ticket_types[$parent_zone->id]['zone']['color'] = $type->color;
            }
        }

        return $ticket_types;
    }

    public static function seats_by_zone($event, $json = false)
    {
        $data = array();
        $total = 0;
        foreach ($event->ticket_template->ticket_types as $type) {
            // $sold = $type->sold_tickets($event);
            // $reserved = $type->temps_by_event($event->id);
            $seats = EventSeatAvailability::seatsForEventAndTicket($event->id, $type->id);

            $total += $seats->available;

            $data['zones'][$type->zone->id]['labels'] = ['Apartados', 'Vendidos', 'Disponibles', 'Cortesías'];
            $data['zones'][$type->zone->id]['colors'] = ['rgba(255 ,138, 101, 1)', 'rgba(189, 189, 189, 1)', 'rgba(179, 229, 252, 1)', 'rgba(139, 195, 74, 1)'];

            $data['zones'][$type->zone->id]['on_hold'] = $seats->on_hold;
            $data['zones'][$type->zone->id]['on_hold_sale'] = $seats->on_hold_sale - $seats->courtesy;
            $data['zones'][$type->zone->id]['available'] = $seats->available;
            $data['zones'][$type->zone->id]['courtesy'] = $seats->courtesy;

            $data['zones'][$type->zone->id]['data'][] = $seats->on_hold;
            $data['zones'][$type->zone->id]['data'][] = $seats->on_hold_sale - $seats->courtesy;
            $data['zones'][$type->zone->id]['data'][] = $seats->available;
            $data['zones'][$type->zone->id]['data'][] = $seats->courtesy;
        }

        $data['general'] = [
            'labels' => ['Apartados', 'Vendidos', 'Disponibles'],
            'colors' => ['rgba(255 ,138, 101, 1)', 'rgba(189, 189, 189, 1)', 'rgba(179, 229, 252, 1)', 'rgba(139, 195, 74, 1)'],
            'data' => array()
        ];

        $total_on_hold = $event->seats()->wherePivot('status', 'on_hold')->count();
        $total_on_hold_sale = $event->seats()->wherePivot('status', 'on_hold_sale')->count();

        $data['general']['data'][] = $total_on_hold;
        $data['general']['data'][] = $total_on_hold_sale;
        $data['general']['data'][] = $total;

        return ($json ? json_encode($data, true) : $data);
    }

    public static function total_tickets_by_type($type, $event)
    {
        $on_hold = $on_hold_sale = 0;
        foreach ($type->rows_for_map($event) as $row) {
            $on_hold += count($row['seats']['on_hold']);
            $on_hold_sale += count($row['seats']['on_hold_sale']);
        }

        return [
            'on_hold' => $on_hold,
            'on_hold_sale' => $on_hold_sale
        ];
    }

    public function tickets()
    {
        return $this->morphMany('App\Ticket', 'event');
    }

    public function generate_accreditations()
    {
        $sold_season_tickets = 0;
        if ($this->event_groups->first()) {
            $sold_season_tickets = $this->event_groups->first()->accreditations->where('type', 'season_ticket')->count();
        }
        $assigned_season_tickets = $this->assigned_accreditations()->where('type', 'season_ticket')->count();
        $accreditations = $this->team->accreditations->where('type', 'operative')->count();
        $assigned_accreditations = $this->assigned_accreditations()->where('type', 'operative')->count();

        return [
            'generated_accreditations' => $accreditations == $assigned_accreditations ? true : false,
            'generated_season_tickets' => $sold_season_tickets == $assigned_season_tickets ? true : false
        ];
    }

    public function sales_statistics()
    {
        return \DB::table('event_seat_availability')
            ->join('ticket_types', 'event_seat_availability.ticket_type_id', '=', 'ticket_types.id')
            ->select(
                'ticket_types.id',
                'ticket_types.name',
                'ticket_types.type',
                'event_seat_availability.total',
                'event_seat_availability.available',
                'event_seat_availability.on_hold',
                'event_seat_availability.on_hold_sale',
                'event_seat_availability.courtesy',
                'event_seat_availability.sold'
            )
            ->where('event_id', $this->id)
            ->get();
    }
}
