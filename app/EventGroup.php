<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;

class EventGroup extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'system_id', 'name', 'slug', 'tagline', 'description', 'img', 'img_slideshow', 'accreditations_on_sale'
    ];

    public function events()
    {
        return $this->belongsToMany('App\Event');
    }

    /**
     * Get all of the temp_orders for the EventGroup
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function temp_orders()
    {
        return $this->morphMany(OrderTemp::class, 'eventable');
    }

    public function orders()
    {
        return $this->morphMany(Order::class, 'event');
    }

    public function accreditations()
    {
        return $this->belongsToMany('App\Accreditation');
    }

    public function system()
    {
        return $this->belongsTo('App\System');
    }

    public function ticket_types()
    {
        return $this->belongsToMany('App\TicketType')
            ->withPivot(['id', 'price', 'total', 'available', 'on_hold', 'on_hold_sale', 'sold'])
            ->withTimestamps();
    }

    public function seats()
    {
        return $this->belongsToMany('App\Seat')->withPivot('status');
    }

    public function get_ticket($type_id)
    {
        return $this->ticket_types()->where('ticket_type_id', $type_id)->first();
    }

    public function seats_by_zone_sidebar($json = false)
    {
        $ticket_types = array();
        foreach ($this->ticket_types as $type) {
            // Adding zone to array in case it dosnt exist
            if (!array_key_exists($type->zone->parent_zone, $ticket_types)) {
                $parent_zone = Zone::find($type->zone->parent_zone);
                $ticket_types[$parent_zone->id] = array();
                $ticket_types[$parent_zone->id]['zone'] = [
                    'name' => $parent_zone->name,
                    'zone_id' => $parent_zone->id,
                    'color' => '',
                ];
            }

            // Appending subzone
            $ticket_types[$parent_zone->id]['subzones'][] = [
                'name' => $type->name,
                'zone_id' => $type->zone->id,
                'color' => $type->color
            ];

            // Defining zone color with ticket type color since we are not storing that info on db
            $ticket_types[$parent_zone->id]['zone']['color'] = $type->color;
        }

        return $ticket_types;
    }

    public function compressed_ticket_types()
    {
        $compressed_ticket_types = array();
        foreach ($this->ticket_types as $ticket_type) {
            $compressed_ticket_types[$ticket_type->id] = [
                'price' => $ticket_type->pivot->price,
                'total' => $ticket_type->pivot->total,
                'price' => $ticket_type->pivot->price,
            ];
        }

        return $compressed_ticket_types;
    }

    public function expanded_ticket_types()
    {
        $compressed_ticket_types = array();
        foreach ($this->ticket_types as $ticket_type) {
            $compressed_ticket_types[] = [
                // 'ticket_type' => $ticket_type,
                'ticket_type' => $ticket_type->API_POS(),
                'price' => $ticket_type->pivot->price,
                'total' => $ticket_type->pivot->total,
                'available' => $ticket_type->pivot->available,
                'on_hold' => $ticket_type->pivot->on_hold,
                'on_hold_sale' => $ticket_type->pivot->on_hold_sale,
                'sold' => $ticket_type->pivot->sold,
            ];
        }

        return $compressed_ticket_types;
    }

    public static function selectize()
    {
        return EventGroup::orderBy('name')->pluck('name', 'id')->toArray();
    }

    public function base_path()
    {
        return "public/event_groups/{$this->id}";
    }

    public function img_path($full = true)
    {
        $img_path = $this->base_path() . "/{$this->id}_cover.png";

        if (!Storage::exists($img_path)) {
            return ($full ? asset('/assets/images/img_not_found.png') : '/public/assets/images/img_not_found.png');
        } else {
            return ($full ? Storage::url($img_path) : $img_path);
        }
    }

    public function img_slideshow_path($full = true)
    {
        $img_slideshow_path = $this->base_path() . "/{$this->id}_slideshow.jpg";

        if (!Storage::exists($img_slideshow_path)) {
            return ($full ? asset('/assets/images/img_not_found.png') : '/public/assets/images/img_not_found.jpg');
        } else {
            return ($full ? Storage::url($img_slideshow_path) : $img_slideshow_path);
        }
    }

    public static function on_sale_event_groups($system_id)
    {
        $event_groups = array();

        // Fetching all event groups on sale
        foreach (EventGroup::where('system_id', $system_id)->where('accreditations_on_sale', 1)->get() as $group) {
            $event_groups[] = $group->API_POS();
        }

        // Returning data
        return $event_groups;
    }

    public function API_POS()
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name,
            'venue_id' => $this->system->venues->first()->id,
            'venue' => $this->system->venues->first()->name,
            'tagline' => $this->tagline ?? '',
            'description' => $this->description ?? '',
            'image' => asset($this->img_slideshow_path()),
            'ticket_types' => $this->expanded_ticket_types(),
        ];
    }

    public static function bySlug($slug)
    {
        return EventGroup::where('slug', $slug)->firstOrFail();
    }

    public function tickets()
    {
        return $this->morphMany('App\Ticket', 'event');
    }

    public static function total_tickets_by_type($type, $event)
    {
        $on_hold = $on_hold_sale = 0;
        foreach ($type->rows_for_map($event) as $row) {
            $on_hold += count($row['seats']['on_hold']);
            $on_hold_sale += count($row['seats']['on_hold_sale']);
        }

        return [
            'on_hold' => $on_hold,
            'on_hold_sale' => $on_hold_sale
        ];
    }
}
