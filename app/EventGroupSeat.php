<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventGroupSeat extends Model
{
    public $timestamps = false;
    protected $table = 'event_group_seat';

    protected $fillable = [
        'event_group_id', 'seat_id', 'status'
    ];


    public static function setSeatAvailabilityForEventGroup($event_group_id, $seat_id, $status)
    {
        EventGroupSeat::updateOrCreate([
            'event_group_id' => $event_group_id,
            'seat_id' => $seat_id,
        ], [
            'status' => $status
        ]);
    }

    public static function seatAvailabilityForEventGroup($event_group_id, $row_id, $seat_index)
    {
        $seat = Seat::where('row_id', $row_id)->where('name', $seat_index)->first();
        \Log::info('seat_id = ' . $seat->id);

        $event_seat = EventGroupSeat::where('event_group_id', $event_group_id)
            ->where('seat_id', $seat->id)
            ->first();

        if ($event_seat == null) {
            return [
                'status' => 'available',
                'seat_id' => $seat->id
            ];
        } else {
            return [
                'status' => $event_seat->status,
                'seat_id' => $seat->id
            ];
        }
    }
}
