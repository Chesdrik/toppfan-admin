<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSeat extends Model
{
    public $timestamps = false;
    protected $table = 'event_seat';

    protected $fillable = [
        'event_id', 'seat_id', 'status'
    ];


    public static function setSeatAvailabilityForEvent($event_id, $seat_id, $status)
    {
        EventSeat::updateOrCreate([
            'event_id' => $event_id,
            'seat_id' => $seat_id,
        ], [
            'status' => $status
        ]);
    }


    public static function seatAvailabilityForEvent($event_id, $row_id, $seat_index)
    {
        // $seat = Seat::where('row_id', $row_id)->where('name', $seat_index)->first();
        $seat_id = Seat::getSeatId($row_id, $seat_index);
        // \Log::info('seat_id = ' . $seat->id);

        $event_seat = EventSeat::where('event_id', $event_id)
            ->where('seat_id', $seat_id)
            ->first();

        if ($event_seat == null) {
            return [
                'status' => 'available',
                'seat_id' => $seat_id
            ];
        } else {
            return [
                'status' => $event_seat->status,
                'seat_id' => $seat_id
            ];
        }
    }
}
