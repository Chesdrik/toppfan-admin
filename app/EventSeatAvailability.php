<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSeatAvailability extends Model
{
    protected $table = 'event_seat_availability';

    protected $fillable = [
        'event_id', 'ticket_type_id', 'total', 'available', 'on_hold', 'on_hold_sale', 'courtesy', 'sold'
    ];

    public function ticket_type()
    {
        return $this->belongsTo('App\TicketType');
    }

    // public $timestamps = false;

    public function percentage_available($pretty = false)
    {
        $percentage = round(100 - $this->percentage_sold(), 2);

        return ($pretty ? $percentage . '%' : $percentage);
    }

    public function percentage_sold($pretty = false)
    {
        $percentage = round(($this->tickets_sold() * 100) / $this->total, 2);

        return ($pretty ? $percentage . '%' : $percentage);
    }

    public function tickets_sold()
    {
        return $this->total - $this->available;
    }

    public static function seatsForEvent($event_id)
    {
        return EventSeatAvailability::where('event_id', $event_id)->get();
    }

    public static function seatsForEventAndTicket($event_id, $ticket_type_id)
    {
        return EventSeatAvailability::where('event_id', $event_id)
            ->where('ticket_type_id', $ticket_type_id)
            ->first();
    }
}
