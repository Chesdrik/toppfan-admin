<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use App\TicketDistributionSystem\TicketGroup;
use App\Event;
use App\Order;

class AssignmentsExport implements FromView
{
    public $event_id;
    public $user_id;

    function __construct($event_id, $user_id, $type) {
        $this->event_id = $event_id;
        $this->user_id = $user_id;
        $this->type = $type;
    }

    public function view(): View
    {
        $event = Event::find($this->event_id);
        $total = 0;

        if($this->type == 'fans'){
            $assignedTickets = array();
            foreach (Order::where('assigned_by', $this->user_id)->where('type', 'courtesy')->where('event_id', $this->event_id)->get() as $order) {
                $total += $order->tickets->where('assigned_by', $this->user_id)->count();

                if ($order->tickets->where('assigned_by', $this->user_id)->count() > 0) {
                    $assignedTickets = array_merge($assignedTickets, $order->assigned_tickets());
                }
            }

            return view('tds.exports.fans', [
                'event_name' => $event->name,
                'assigned_tickets' => $assignedTickets,
                'total' => $total
            ]);

        }else{
            $assignedTicketLots = array();
            $total += TicketGroup::where('assigned_by', $this->user_id)->where('event_id', $this->event_id)->count();
            $total_assigned = 0;
            
            foreach(TicketGroup::where('assigned_by', $this->user_id)->where('event_id', $this->event_id)->get() as $ticket_group){
                $total_assigned += $ticket_group->amount;
                $assignedTicketLots[] = $ticket_group->assigned_ticket_lots(); 
            }

            return view('tds.exports.groups', [
                'event_name' => $event->name,
                'ticket_groups' => $assignedTicketLots,
                'total' => $total,
                'total_assigned' => $total_assigned
            ]);
        }
    }
}