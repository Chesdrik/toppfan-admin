<?php

namespace App\Http\Controllers;

use App\Event;
use App\SeatType;
use Illuminate\Http\Request;
use App\Ticket;
use App\Venue;
use App\Order;
use App\TicketType;
use App\User;
use App\Alert;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class APIController extends Controller
{

    public function access(Request $request)
    {
        // Logging request
        // $log = Log::create([
        //   'log' => json_encode($request->all()),
        // 'type' => 'access'
        //]);

        // Fetching Sale object based on QR read
        $qr = json_decode(decrypt($request->qr));
        // dump($qr);

        if (isset($qr->order_id)) {
            $eu = Ticket::where('order_id', $qr->order_id)->where('ticket_type->_id', $qr->type)->first();
            // dd($eu);
            if ($eu->used == 1) {
                // Return
                $return = [
                    'error' => true,
                    'message' => 'Este acceso ya fue utilizado a las ' . pretty_date($eu->access)
                ];
            } else {
                // First QR scan
                $eu->update(['used' => 1]);

                // Return
                $return = [
                    'error' => false,
                    'message' => 'Bienvenido '
                ];
            }
        }


        // Logging response
        // $log = Log::create([
        //   'log' => json_encode($return),
        // 'type' => 'access'
        //]);

        return $return;
    }

    public function tickets(Request $request)
    {
        // Fetching orders for user
        $user = User::find($request->user_id);
        $orders = $user->orders()->whereHas('event', function ($query) {
            return $query->where('date', '>=', Carbon::now()->format('Y-m-d H:i:s'));
        })->get();

        // Building ticket list
        $tickets = array();
        foreach ($orders as $order) {
            foreach ($order->tickets as $ticket) {
                $tickets[] = [
                    'id' => $ticket->id,
                    'eventId' => $ticket->event->id,
                    'eventName' => $ticket->event->name, // 'amount' => $ticket->amount,
                    // 'datespan' => $ticket->event->datespan(),
                    'amount' => $ticket->amount,
                    'seatTypeId' => $ticket->ticket_type->id,
                    'seatTypeName' => $ticket->ticket_type->name,
                    'orderId' => $order->id,
                    'used' => false,
                ];
            }
        }

        // Returning data
        return [
            'error' => false,
            'tickets' => $tickets,
        ];
    }

    /**
     * All orders
     * @param $id user id
     * @return response with last and next list of tickets
     */
    public function allOrders($id)
    {
        // Fetching orders for user
        $user = User::find($id);
        $netx_orders = $user->orders()->whereHas('event', function ($query) {
            return $query->where('date', '>=', Carbon::now()->format('Y-m-d H:i:s'));
        })->get();
        //dd($netx_orders);
        // Building next ticket list
        $next = array();
        foreach ($netx_orders as $order) {
            foreach ($order->tickets as $ticket) {
                $next[] = [
                    'id' => $order->id,
                    'eventName' => $order->event->name,
                ];
            }
        }


        $last_orders = $user->orders()->whereHas('event', function ($query) {
            return $query->where('date', '<=', Carbon::now()->format('Y-m-d H:i:s'));
        })->get();
        //dd($last_orders);
        // Building last ticket list
        $last = array();
        foreach ($last_orders as $order) {
            //dd($order->event);
            foreach ($order->tickets as $ticket) {
                $last[] = [
                    'id' => $order->id,
                    'eventName' => $order->event->name,
                ];
            }
        }

        // Returning data
        return [
            'error' => false,
            'next_events' => $next,
            'last_events' => $last,
        ];
    }

    /**
     * get information about an order
     * @param id order id
     */
    public function order($id)
    {

        error_log($id);
        $order = Order::find($id);
        $event = Event::find($order->event_id);
        $venue = Venue::find($event->venue_id);

        $completeOrder = array();

        $completeOrder = [
            'id' => $order->id,
            'event_id' => $event->id,
            'event_name' => $event->name,
            'event_date' => $event->date,
            'venue' => $venue->name,
            'total' => $order->total,
            'tickets' => []
        ];

        foreach ($order->tickets as $key => $ticket) {
            $completeOrder['tickets'][$key] = [
                'id' => $ticket->id,
                'ticket_type' => $ticket->ticket_type->name,
                'ticket_color' => $ticket->ticket_type->color,
                'amount' => $ticket->amount,
                'used' => $ticket->used,
                'qr' => $ticket->qr(),
            ];
        }

        error_log($completeOrder['total']);
        return [
            'error' => false,
            'order' => $completeOrder,
        ];
    }

    public function order_qr_sva($id)
    {
        $order = Order::find($id);
        $event = Event::find($order->event_id);
        $venue = Venue::find($event->venue_id);

        $completeOrder = array();

        $completeOrder = [
            'id' => $order->id,
            'event_id' => $event->id,
            'event_name' => $event->name,
            'event_date' => $event->date,
            'venue' => $venue->name,
            'total' => $order->total,
            'tickets' => []
        ];

        foreach ($order->tickets as $key => $ticket) {
            $completeOrder['tickets'][$key] = [
                'id' => $ticket->id,
                'ticket_type' => $ticket->ticket_type->name,
                'ticket_color' => $ticket->ticket_type->color,
                'amount' => $ticket->amount,
                'used' => $ticket->used,
                'qr' =>  $ticket->generateQRsSVG($order->id),
                'qr_url' => $ticket->qr(),
            ];
        }

        error_log($completeOrder['total']);
        return [
            'error' => false,
            'order' => $completeOrder,
        ];
    }

    public function nextTickets($id)
    {
        error_log('entre');
        $orders = Order::where('user_id', $id)->get();
        $allOrders = array();
        foreach ($orders as $order) {
            $t = Event::find($order->event_id);

            if ($t->date >= Carbon::now()->format('Y-m-d H:i:s')) {
                error_log($order->id);
                error_log($order->event_id);
                error_log($t->name);
                error_log($t->date);


                $allOrders[] = [
                    'id' => $order->id,
                    'event_id' => $order->event_id,
                    'event_name' => $t->name,
                    'event_date' => $t->date,
                    // 'tickets' => [],
                    // 'ticket_type->_id' => $order->ticket_type->_id,
                    // 'used' => $order->used,
                ];

                // foreach ($order->tickets as $key => $ticket) {
                //     error_log('entre sip');

                //     $allOrders['tickets'][$key] = [
                //         'id' => $ticket->id,
                //     ];

                // }
            }
        }
        error_log('super si amiga');

        // Returning data
        return [
            'error' => 'false',
            'orders' => $allOrders,
        ];
    }

    public function pastTickets($id)
    {
        $ti = Ticket::where('user_id', $id)->get();
        $tickets = array();
        foreach ($ti as $ticket) {
            $t = Event::find($ticket->event_id);
            if ($t->date < Carbon::now()->format('Y-m-d H:i:s')) {
                $tickets[] = [
                    'ticket' => [
                        'id' => $ticket->id,
                        'event_id' => $ticket->event_id,
                        'event_name' => $t->name,
                        'event_date' => $t->date,
                        'ticket_type->_id' => $ticket->ticket_type->_id,
                        'order_id' => $ticket->order_id,
                        'used' => $ticket->used,
                    ]
                ];
            }
        }

        // Returning data
        return [
            'error' => 'false',
            'ticket' => $tickets,
        ];
    }


    public function QRRead(Request $request)
    {
        // $data = json_decode(decrypt($request->qr_content));
        $data = json_decode(base64_decode($request->qr_content));

        $ticket = Ticket::find($data->ticket_id);

        if ($ticket->used < $ticket->amount) {
            // $ticket->used += 1;
            // $ticket->save();

            $order = Order::find($data->order_id);

            // Returning data
            return [
                'error' => 'false',
                'message' => 'Bienvenido ' . $order->user->name . ' (' . $ticket->used . ' / ' . $ticket->amount . ')',
            ];
        } else {
            // Returning data
            return [
                'error' => 'true',
                'error_message' => "Límite de usos (" . $ticket->used . " de " . $ticket->amount . ") alcanzado",
            ];
        }
    }

    /**
     * Get information about a event
     * @param $id id of event
     */
    public function getEvent($id)
    {
        $ev = Event::find($id);
        $event = array();

        $event = [
            'id' => $ev->id,
            'event' => $ev->name,
            'event_date' => $ev->date,
            'active' => $ev->active,
            'id_venue' => $ev->venue->id,
            'venue' => $ev->venue->name,
            'tickets_types' => []
        ];

        // dd($ev->ticket_template->ticket_types);
        // foreach ($ev->venue->ticket_types() as $key => $type) {
        foreach ($ev->ticket_template->ticket_types as $type) {

            $sold = $type->sold_tickets($ev);
            $reserved = $type->temps_by_event($id);
            // dd($type, $sold, $reserved);

            if ($type->active == 1) {
                if ($type->available_seats > $sold + $reserved) {

                    $event['tickets_types'][] = [
                        'id' => $type->id,
                        'name' => $type->name,
                        'color' => '#' . $type->color,
                        'price' => number_format($type->pivot->price, 2, '.', ','),
                        'active' => $type->active,
                    ];
                }
            }
        }

        // dd($event);

        return [
            'error' => false,
            'event' => $event,
        ];
    }

    public function add_order(Request $request)
    {
        $order = Order::create($request->except('_token', 'tickets'));

        //  dd($request->tickets['ticket_type_id']);
        foreach ($request->tickets as $ticket) {
            $price = TicketType::find($ticket['ticket_type_id'])->price;

            $tickets = Ticket::create(['ticket_type_id' => $ticket['ticket_type_id'], 'order_id' => $order->id, 'amount' => $ticket['amount'], 'price' => $price]);
        }


        return ['error' => false, 'order_id' => $order->id];
    }

    public function register_alert(Request $request)
    {
        try {
            $alert = Alert::create([
                'checkpoint_id' => $request->checkpointID,
                'type' => $request->type,
                'notes' => $request->notes,
            ]);

            return [
                'error' => false,
                'message' => 'Alerta enviada'
            ];
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo registrar alerta',
            ];
        }
    }

    public function registerOrder(Request $request, $event_id)
    {
        // Fetching event
        $event = Event::find($event_id);

        // Registering order
        $order = Order::create([
            'user_id' => 1,
            'event_id' => $event_id,
            'email' => 'diego@filloy.com.mx',
            'total' => '100.00',
            'courtesy' => 0,
            'type' => 'regular',
        ]);

        // Registering tickets
        foreach ($request->all() as $ticket_type_id => $amount) {
            $t = Ticket::create([
                'event_id' => $event_id,
                'order_id' => $order->id,
                'ticket_type_id' => $ticket_type_id,
                'amount' => $amount,
                'used' => 0,
                'type' => 'parking',
                'name' => 'Diego Filloy Ring',
                'price' => '1.00',
                'folio' => 'PF-001',
            ]);
            dump($t);
        }

        // Returning data
        return [
            'event' => $event->name,
            'recieved' => $request->all(),
        ];
    }

    public function updateAcreditationVersion(Request $request){

        $event = Event::find($request->event_id);
        $accreditations = array();
        foreach ($event->accreditations as $ac) {
            $accreditations[] = [
                'accreditation_id' => $ac->id,
                'event_id' => $event->id,
                'version' => $ac->pivot->version,

            ];
        }

        return [
            'error' => false,
            'accreditations' => $accreditations
        ];

    }
}
