<?php

namespace App\Http\Controllers\Access;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use \Response;
use \Carbon\Carbon;
use DateTime;

use App\Venue;
use App\Http\Requests\Access\AccessCreate;
use App\Access\AccessRequirementData;
use App\Access\AccessRequirement;
use App\Access\AccessLog;
use App\Access\AccessRole;
use App\Access\Access;
use App\Access\Requirement;
use App\Access\RequirementData;

class AccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $access = Access::all();
        $requirements = Requirement::pluck('description', 'id');
        $roles = AccessRole::pluck('name', 'id');

        return view('admin.access.index', compact('access', 'requirements', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccessCreate $request)
    {
        $data = $request->except('_token', 'requirement_id', 'request');
        $data['user_id'] = Auth::user()->id;
        $data['venue_id'] = 2;
        $access = Access::create($data);

        // dd($request->all());
        $requeriment = AccessRequirement::create(['access_id' => $access->id, 'requirement_id' => $request->requirement_id, 'request' => $request->detail]);


        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Access\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $access = Access::find($id);
        $access->generateQR();
        $requirements = Requirement::all();

        return view('admin.access.show', compact('access', 'requirements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Access\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function update(AccessCreate $request, $id)
    {
        $access = Access::find($id);
        $access->update($request->except('_token'));

        return redirect()
            ->route('system.access.show', ['acceso' => $access->id])
            ->with('success_message', 'El acceso ha sido editado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Access\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $access = Access::find($id);
        $access->delete();

        return redirect()->route('accesos.index')
            ->with('success_message', 'El acceso ha sido eliminado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Access\Access  $access
     * @return \Illuminate\Http\Response
     */
    public function qrs($id)
    {
        $requirement = Requirement::find($id);
        $access = Access::requirement_access($id)->get();

        return view('admin.access.qrs', compact('requirement', 'access'));
    }



    /**************************************************
     *                  Access functions               *
     ***************************************************/

    /**
     * Uploads the access list to the PDA device
     *
     * @param  \Illuminate\Http\Request  $request with Venue ID
     * @return \Illuminate\Http\Response
     */
    public function syncAccessList(Request $request)
    {
        $venue = Venue::find($request->venue_id);

        return Response::json([
            'error' => false,
            'accesses' => $venue->activeAccess()
        ]);
    }

    public function syncAccess(Request $request)
    {
        error_log($request);
        $array = json_decode($request->input('data'), true);

        return $this->syncData($array, 'entry');
    }

    public function syncExits(Request $request)
    {
        error_log($request);
        $array = json_decode($request->input('data'), true);

        return $this->syncData($array, 'exit');
    }

    public function syncData($access_data, $type)
    {
        foreach ($access_data as $ad) {
            AccessLog::create([
                'access_id' => $ad['accessLogs']['accessID'],
                'type' => $type,
                'log' => json_encode($ad),
                'timestamp' => $ad['accessLogs']['readingTime'],
            ]);

            foreach ($ad['requirements'] as $re) {
                RequirementData::create([
                    'access_requirement_id' => $re['accessRequirementId'],
                    'access_log_id' => $re['idAccess'],
                    'data' => $re['status']
                ]);
            }
        }

        return [
            'error' => false,
        ];
    }

    public function requirement_checkout()
    {
        $access = Access::suzuki_access()->get();
        $max_date = request()->max_date ?? date('Y-m-d');
        $p = Access::assistance_data($max_date);
        $datetime = $p[0];
        $boundries = $p[1];

        // Creating week collection
        $week = collect([]);
        $datetime = new DateTime($boundries[0]);
        for ($i = 0; $i < 7; $i++) {
            // Pushing date to collection
            $week->push(
                collect([
                    'label' => days_short($i) . ' ' . $datetime->format('d'),
                    'day' => $datetime->format('Y-m-d'),
                ])
            );

            // Adding one day
            $datetime->modify('+1 day');
        }

        // Creating attendance collection
        $access_collection = collect([]);
        foreach ($access as $assistance) {
            $access_collection_tmp = collect([]);
            $access_collection_tmp->put('name', $assistance->visitor_name);

            $days_attended = $assistance->assistance($boundries);
            $attended_tmp = array();

            foreach ($week as $i => $day) {
                $attended_tmp[$i] = (in_array($day->get('day'), $days_attended) ? true : false);
            }

            $access_collection_tmp->put('days_attended', $attended_tmp);
            $access_collection->push($access_collection_tmp);
        }

        $access_data = collect(['week' => $week, 'data' => $access_collection]);

        return view('admin.access.panel', compact('access', 'max_date', 'access_data', 'boundries'));
    }
}
