<?php

namespace App\Http\Controllers\Access;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Carbon\Carbon;

use App\Access\Access;
// use App\Access\AccessLog;
use App\Access\Requirement;

class ReportController extends Controller
{
    public function reports(){
        $reports = [
            1 => 'Accesos generales',
            2 => 'Accesos Suzuki',
        ];

        return view('admin.access.reports', compact('reports'));
    }

    public function report($id){
        switch ($id) {
            case 1:
                return $this->general_access_report();
                break;
            case 2:
                return $this->suzuki_report();
                break;
        }
    }

    public function general_access_report(){
        return 'general_access_report';
    }

    public function suzuki_report(){
        $now = Carbon::now();
        $access_logs = Access::suzuki_report_data($now->startOfWeek()->format('Y-m-d H:i:s'), $now->endOfWeek()->format('Y-m-d H:i:s'));

        $datespan = $now->startOfWeek()->format('Y-m-d H:i').' - '.$now->endOfWeek()->format('Y-m-d H:i');

        return view('admin.access.reports.suzuki', compact('access_logs', 'datespan'));
    }
}
