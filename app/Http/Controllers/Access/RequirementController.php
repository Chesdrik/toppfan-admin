<?php

namespace App\Http\Controllers\Access;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Access\Requirement;
use App\Access\Access;

class RequirementController extends Controller
{
    public function create(Request $request){
        Requirement::create([
            'description' => $request->description,
            'type' => $request->type,
        ]);

        return back();
    }

    public function associate($requirement_id, $access_id){
        $access = Access::find($access_id);

        if($access->requirements->contains($requirement_id)){
            $access->requirements()->detach($requirement_id);
            $access->save();
        }else{
            $access->requirements()->attach($requirement_id);
            $access->save();
        }

        return back();
    }
}
