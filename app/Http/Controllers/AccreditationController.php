<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\System;
use App\Accreditation;
use App\Event;
use App\TicketTemplate;
use App\TicketType;
use App\Team;
use App\Sync;
use App\AccreditationReading;
use App\AccreditationHistory;
use App\EventSeat;

use Carbon\Carbon;
use Auth;


class AccreditationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($system_slug)
    {
        $system = System::bySlug($system_slug);
        $accreditations = $system->accreditations;
        $teams = Team::all();

        return view('admin.accreditations.index', [
            'accreditations' => $accreditations,
            'ticket_types' => TicketType::pluck('name', 'id')->toArray(),
            'teams' => $teams,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => 'Acreditaciones', 'url' => '#', 'active' => false],
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($system_slug)
    {
        // $system = System::bySlug($system_slug);

        return view('admin.accreditations.create', [
            'teams' => Team::dropdown(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => 'Acreditaciones', 'url' => route('system.accreditations.index', [session('system_slug')]), 'active' => false],
                ['label' => 'Crear', 'url' => '#', 'active' => false],
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $system_slug, $id)
    {
        try {
            $event = Event::find($id);
            foreach (session('tickets') as $data) {
                $ticket_type = TicketType::find($data['ticket_type_id']);
                for ($i = 0; $i <= $data['total']; $i++) {
                    $folio = Accreditation::folio() + 1;
                    $accreditation = Accreditation::create([
                        'team_id' => $event->team_id,
                        'ticket_type_id' => $data['ticket_type_id'],
                        'folio' => $folio
                    ]);
                    $event->accreditations()->attach($accreditation);
                    $i++;
                }
            }

            return redirect(route('system.events.accreditations.list', $event->id))
                ->with('success', 'Acreditaciones creadas correctamente');
        } catch (\Throwable $th) {
            return back()->with('error', 'No se pudieron generar las acreditaciones.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($system_slug, $event_id, $id)
    {
        try {
            $accreditation = Accreditation::find($id);
            $ac = $accreditation->byEvent($event_id);
            $event = Event::find($event_id);

            return view('admin.accreditations.show', [
                'accreditation' => $accreditation,
                'ac' => $ac,
                'event' => $event,
                'breadcrumb' => [
                    ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                    ['label' => 'Eventos', 'url' => route('system.events.index', [session('system_slug')]), 'active' => false],
                    ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $event->id]), 'active' => false],
                    ['label' => ($accreditation->type == 'operative') ? 'Acreditaciones' : 'Abonos', 'url' => route(($accreditation->type == 'operative') ? 'system.events.accreditations.list' : 'system.events.season.tickets', [session('system_slug'), $event->id]), 'active' => true],
                    ['label' => ($accreditation->type == 'operative') ? 'Acreditación ' . $accreditation->folio : 'Abono ' . $accreditation->folio, 'url' => '#', 'active' => true],
                ]
            ]);
        } catch (\Throwable $th) {
            return back()->with('error', 'No se encontró información.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $system_slug, $event_id, $accreditation_id)
    {
        try {
            $accreditation = Accreditation::find($accreditation_id);
            $event = Event::find($event_id);
            $accreditation->event()->updateExistingPivot($event->id, [
                'name' => $request->name,
                'email' => $request->email
            ]);

            $accreditation->generate_qr($event_id);

            // Sending notification

            return back()->with('success', 'Acreditación asignada correctamente');
        } catch (\Throwable $th) {
            return back()->with('error', 'No se pudo editar la acreditación.');
        }
    }

    public function confirm_accreditations($system_slug)
    {
        try {
            $event = Event::find(session('id'));
            $tickets = session('tickets');
            $template = TicketTemplate::find($event->ticket_template_id);
            $data = array();
            foreach ($tickets as $item) {
                $temp = array();

                foreach ($template->ticket_types as $ticket_type) {
                    if ($ticket_type->id == $item['ticket_type_id']) {
                        $temp['ticket'] = $ticket_type;
                        $temp['total'] = $item['total'];
                        $data[] = $temp;
                    }
                }
            }

            return view('admin.accreditations.confirm', [
                'data' => $data,
                'email' => session('email'),
                'event' => $event,
                'breadcrumb' => [
                    ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                    ['label' => 'Equipos', 'url' => route('teams'), 'active' => false],
                    ['label' => 'Eventos de ' . $event->venue->pretty_team(), 'url' => route('team_events', $event->venue->id), 'active' => false],
                    ['label' => 'Generación de acreditaciones para ' . $event->name, 'url' => route('tickets_sale', [$event->venue->id, $event->id]), 'active' => false],
                    ['label' => 'Confirmación', 'url' => '#', 'active' => true],
                ]
            ]);
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    public function qr($system_slug, $team_id = null)
    {
        // $accreditations = Accreditation::all();
        $teams = ($team_id == null ? Team::all() : [Team::find($team_id)]);

        return view('admin.imports.qr', [
            'teams' => $teams,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => 'qr', 'url' => route('system.events.all', [session('system_slug')]), 'active' => false],
            ]
        ]);
    }

    public function syncAcreditationReadings(Request $request)
    {
        //dd($request->all());
        if (count($request->accreditation_readings) > 0) {
            // Clean accreditation readings


            // Creating sync
            /* $sync = Sync::create([
                'event_id' => 15,
                'recieved' => json_encode($request->accreditation_readings),
                'type' => 'accreditation_readings',
            ]);*/

            // Processing accreditation readings
            foreach ($request->accreditation_readings as $tr) {

                // Saving accreditation reading
                $accreditation_readings = AccreditationReading::create([
                    'accreditation_event_id' => $tr['accreditation_event_id'],
                    // 'sync_id' => $sync->id,
                    'checkpoint_id' => $tr['checkpoint_id'],
                    'type' => $tr['type'],
                    'error' => $tr['error'],
                    'reading_time' => $tr['reading_time'],
                    'device_id' => $tr['device_id'],
                ]);
            }

            // TODO Process total used tickets with readings for statistics

            // Returning response
            return [
                'error' => false,
            ];
        } else {
            return [
                'error' => true,
            ];
        }
    }

    public function accreditation_history($system_slug, $id)
    {

        return view('admin.accreditations.history', [
            'histories' => AccreditationHistory::where('accreditation_id', $id)->get(),
            'accreditation' => Accreditation::find($id),
            'i' => 1,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => 'Acreditaciones', 'url' => route('system.accreditations.index', [session('system_slug')]), 'active' => false],
                ['label' => 'Detealle de acreditaciones', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    public function change_version(Request $request, $system_slug, $accreditation_id)
    {
        $events = Event::where('date', '>', Carbon::now())->get();

        $accreditation = Accreditation::find($accreditation_id);
        $history = AccreditationHistory::create(['user_id' => Auth::user()->id, 'accreditation_id' => $accreditation->id, 'version' => $accreditation->version, 'motive' => $request->motive]);
        $version = $accreditation->version + 1;
        $accreditation->update(['version' => $version]);

        foreach ($events as $event) {
            if (in_array($event->id, $accreditation->event->pluck('id')->toArray())) {
                $event->accreditations_event($accreditation_id)->syncWithoutDetaching([$accreditation_id => ['version' => $version]]);
                $accreditation->generate_qr($event->id, $event->team_id);
            }
        }

        return back()->with('success', 'La version de la acreditacion se actualizó');
    }

    public function no_system_redirect(Request $request)
    {
    }

    public function seasonTickets($system_slug, $id)
    {
        $system = System::bySlug($system_slug);

        return view('admin.events.season_tickets', [
            'event' => Event::find($id),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => 'Abonos', 'url' => '#', 'active' => false],
            ]
        ]);
    }

    public function generateSeasonTickets($system_slug, $id)
    {
        $system = System::bySlug($system_slug);
        $event = Event::find($id);

        foreach ($event->event_groups->first()->accreditations->where('type', 'season_ticket') as $accreditation) {
            $seat = $accreditation->ticket->seats->first();
            $accreditation->events()->syncWithoutDetaching([$event->id => ['version' => $accreditation->version]]);

            $event_seat = EventSeat::updateOrCreate([
                'event_id' => $event->id,
                'seat_id' => $seat->id
            ], [
                'status' => 'on_hold_sale'
            ]);
        }

        return back()->with('success', 'Abonos generados correctamente');
    }
}
