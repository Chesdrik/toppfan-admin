<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Order;
use App\Ticket;
use App\System;

use Carbon\Carbon;


class AdminController extends Controller
{
    public function dashboard_redirect()
    {
        return redirect()->route('system.select');
    }

    public function dashboard($system_slug)
    {
        // Defining system slug if redirect
        session(['system_slug' => $system_slug]);
        $system = System::bySlug($system_slug);

        //
        $total = Order::all()->sum('total');
        $total_tickets = Ticket::all();
        $ticket_shop = Ticket::count();

        $dia = '';
        $i = 1;
        $datos = array();
        $labes = array();
        $data = array();

        foreach ($total_tickets as $ticket) {
            $fecha = date_format($ticket->created_at, 'd-m-Y');

            if ($fecha != $dia) {
                $datos[$fecha] = $i;
                $dia = $fecha;
                $j = $i;
            } else {
                $j++;
                $datos[$fecha] = $j;
            }
        }

        foreach ($datos as $fechas => $numeros) {
            $labes[] = $fechas;
            $data[] = $numeros;
        }


        $upcoming_events = Event::upcoming_events(null, null, $system->teams()->take(1)->first()->id);
        $active_events = Event::where('active', 1)->get();


        return view('admin.dashboard', compact('upcoming_events', 'active_events', 'total', 'labes', 'data', 'ticket_shop'));
    }


    public function root_dashboard()
    {
        $systems = System::orderBy('name')->get();

        return view('admin.root_dashboard', compact('systems'));
    }

    public function events()
    {
        return view('admin.events');
    }

    public function sales()
    {
        return view('admin.sales');
    }

    public function prices()
    {
        return view('admin.prices');
    }

    public function users()
    {
        return view('admin.users');
    }
}
