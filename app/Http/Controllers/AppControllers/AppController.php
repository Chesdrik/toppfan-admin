<?php

namespace App\Http\Controllers\AppControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Vedmant\FeedReader\Facades\FeedReader;
use Faker;
use \Carbon\Carbon;


use App\Event;
use App\Order;

class AppController extends Controller
{
    public function home(Request $request)
    {
        // \Log::info('Requesting home service');

        return [
            'error' => false,
            // 'logo' => asset('/assets/logo/toppfan_logo.png'),
            'logo' => asset('/assets/images/logo_pumas.png'),
            'title' => 'Toppfan',
        ];
    }

    public function privacy()
    {
        // \Log::info('Requesting privacy service');

        return [
            'error' => false,
            'privacy' => '<p style="font-size: 1.3em;">Tolman S. A. DE C. V. (en adelante TOPPFAN), en cumplimiento con lo dispuesto con la Ley de Protección de Datos Personales en Posesión de Particulares tiene entre sus objetivos la protección de los datos personales de sus usuarios (en adelante “Información”) proporcionada por cada uno, por lo que TOPPFAN ha establecido las siguientes directrices para proteger dicha información.<br /><br />

<strong>1. Información personal recolectada</strong><br />
TOPPFAN solicitará datos que pueden vincularse a las personas a los usuarios de los servicios, titulares de información personal, con el fin de poder brindar la mejor experiencia personalizada en el uso de los servicios que brinda TOPPFAN, para que, según sus posibilidades, los usuarios puedan recibir, a su entera discreción, los mensajes de TOPPFAN que puedan ser de su interés, ya sean nuestros o nuestros anunciantes y publicidad de terceros con los que TOPPFAN tenga acuerdos. Con el fin de brindarle una mejor experiencia, por este motivo y con fines de calidad en el servicio, le enviaremos una encuesta de satisfacción. El tipo de información que se solicita a los usuarios incluye: nombre, dirección de correo electrónico (e-mail), domicilio, código postal, teléfonos de contacto, fecha de nacimiento, intereses personales e información de la tarjeta de crédito o débito. No obstante, no es obligatorio proporcionar toda la información solicitada al momento de registrarse para los servicios de TOPPFAN, excepto aquella que TOPPFAN considere apropiada y que se le dé a conocer al titular en el momento de registrarse.<br /><br />
TOPPFAN solicitará dicha Información para poder vender entradas al titular o cuando lo considere oportuno o necesario. Por ejemplo, cuando el titular quiera participar en concursos o promociones patrocinadas por TOPPFAN o por terceros, en las encuestas realizadas para evaluar y / o prestar los servicios de forma más satisfactoria, cuando los usuarios quieran realizar una compra, etc. La información personal que recabe TOPPFAN podrá ser utilizada para promocionar eventos, análisis de datos y mejorar nuestros servicios, publicidad de terceros con los que TOPPFAN tenga acuerdos. Si desea ser excluido de una lista de correo, puede actualizar sus preferencias en su cuenta en cualquier momento accediendo a su panel de usuario. Al momento del registro, el titular de los datos otorga su pleno consentimiento libre y voluntariamente para facilitar los datos personales que se le requieran, en el entendido de que si el usuario decide no facilitar información obligatoria, no podrá acceder a los servicios que requieren dicha información. Sin embargo, puede tener acceso a todos los demás servicios que no requieran dicha información. TOPPFAN en ningún caso y bajo ninguna circunstancia almacena información personal sensible como origen racial o étnico, estado de salud actual, información genética, creencias religiosas, filosóficas y morales, afiliación sindical, opiniones políticas, preferencia sexual, etc.<br /><br /><br />

<strong>2. Privacidad y protección de datos personales</strong><br />
TOPPFAN utiliza la tecnología y procesos más avanzados para la protección de la información facilitada por los titulares de los datos personales. Esta tecnología cifra, codifica y evita la interceptación de la información proporcionada por Internet, incluidas las tarjetas de crédito y las direcciones de correo electrónico. TOPPFAN establece y mantiene medidas de seguridad administrativas, técnicas y físicas que permitan la protección de los datos personales frente a: daño, pérdida o alteración, destrucción, uso, acceso o tratamiento no autorizado. TOPPFAN contiene hipervínculos o hipertextos "links", banners, botones y herramientas de búsqueda en la World Wide Web que cuando son presionados o utilizados por los usuarios conducen a otros portales o sitios de Internet propiedad de terceros. Aunque en algunos casos estos sitios o portales de terceros están enmarcados con la barra de navegación o el look & feel de TOPPFAN, la Información que el titular vino a proporcionar a través de esos sitios o portales no está cubierta ni contemplada por este Aviso de Privacidad y su manejo o El uso no es responsabilidad de TOPPFAN, por lo que recomendamos a nuestros usuarios verificar los avisos y políticas de privacidad mostrados o aplicables a estos sitios o portales de terceros.<br /><br />
TOPPFAN implementa medidas cautelares administrativas, técnicas y físicas para salvaguardar la información personal del titular contra pérdida, robo o mal uso, así como acceso, divulgación, modificación o destrucción no autorizados.<br /><br />
Los servicios en línea utilizan el cifrado SSL (Secure Sockets Layer) en todas las páginas web donde se solicitan datos personales para proteger la confidencialidad de los mismos durante su transmisión a través de Internet.<br /><br />
Al utilizar algunos productos, servicios o aplicaciones online de TOPPFAN, propios o de terceros como foros, blogs o redes sociales, los datos personales compartidos por el titular son visibles para otros usuarios, que pueden leerlos, recopilarlos o utilizarlos. El titular será responsable de los datos personales que decida facilitar en tales casos.<br /><br />
Al dar su consentimiento para la cesión de datos personales en TOPPFAN, el titular de los derechos reconoce y acepta que TOPPFAN podrá ceder los datos del titular a terceros, incluidos patrocinadores, publicistas, contratistas y / o socios comerciales. TOPPFAN también recabará información que se derive de los gustos, preferencias y en general del uso que hacen los usuarios de los servicios. Dicha información derivada, como los datos personales que proporciona el titular, puede ser utilizada para diversos fines comerciales, como proporcionar datos estadísticos a potenciales anunciantes, enviar publicidad a los usuarios según sus intereses específicos, realizar investigaciones de mercado y otras actividades o promociones que TOPPFAN considere apropiado. TOPPFAN también puede divulgar información si así lo requiere la ley y / o la autoridad pertinente o si considera de buena fe que dicha divulgación es necesaria para: I) cumplir con los procedimientos legales; II) cumplir con el Acuerdo de usuario; III) responder a reclamos que involucren cualquier Contenido que socave los derechos de terceros o; IV) proteger los derechos, propiedad o seguridad de TOPPFAN, sus Usuarios y público en general.<br /><br />
Para autorizar la transferencia de sus datos, seleccione la casilla a continuación.<br /><br />
<strong>Autorizo la transferencia de mis datos personales a terceros.</strong><br /><br />
<strong>Responsabilidad por el uso de la información financiera.</strong><br />
TOPPFAN puede usar su información financiera para verificar que cumpla con los requisitos de las instituciones financieras del país, para realizar ventas y otras transacciones en este Sitio de manera eficiente, para entregar los bienes y servicios que ha solicitado, para inscribirlo en programas de descuento, reembolso y otros programas en los que elija participar y realizar controles de calidad. En algunos casos, podemos utilizar y, en consecuencia, podemos proporcionar su información financiera a los proveedores de servicios para ayudarnos a realizar algunas de estas tareas. En tales casos, requerimos que los proveedores de servicios establezcan acuerdos con nosotros sobre el manejo confidencial de su información que será al menos el mismo que el tratamiento que TOPPFAN le da a la información. Además, podemos proporcionar su información financiera a emisores de tarjetas de crédito, instituciones financieras o procesadores de tarjetas de crédito para procesar transacciones y para otros fines. También podemos proporcionar su información financiera a los organizadores del evento, principalmente con el propósito de facilitar la entrega de boletos, resolver disputas y servicio al cliente en el lugar del evento y en la fecha del evento, y podemos proporcionar su información financiera. información a aquellos comerciantes que brindan bienes, servicios o reservas que usted compró u ordenó, o que administran las ofertas de los programas que ingresó en este Sitio (o que ingresaron a través de otro sitio después de hacer clic en un enlace de este Sitio que promocionó tal oferta o programa).<br /><br />
Asimismo, podemos divulgar información financiera específica cuando determinamos que dicha divulgación puede ser útil para cumplir con la ley, para cooperar o buscar ayuda de las autoridades de seguridad pública o para proteger los intereses o la seguridad de TOPPFAN u otros visitantes del Sitio o usuarios de servicios. o productos proporcionados por TOPPFAN o para participar conjuntamente con otras entidades en actividades antifraude. Asimismo, su información financiera podrá ser facilitada a terceros en el caso de una transferencia de propiedad o activos, o en un proceso concursal de TOPPFAN. TOPPFAN se exime de toda responsabilidad por cualquier otra divulgación de su información financiera por parte de un tercero con quien TOPPFAN pueda divulgar su información financiera bajo este aviso de privacidad.<br /><br /><br />

<strong>3. Uso de "cookies"</strong><br />
TOPPFAN hace uso de herramientas que permiten a los usuarios obtener información de forma pasiva como cookies como una herramienta de uso común para registrar el comportamiento de navegación en línea (seguimiento).<br />
Las cookies son archivos de texto que se descargan y almacenan automáticamente en el navegador y / o disco duro del equipo informático del usuario cuando navega por un sitio de Internet y que permiten al sitio recordar algunos datos del usuario. TOPPFAN utiliza cookies para recordar su nombre de usuario de TOPPFAN y permitirle recordar al navegador los datos que registró en TOPPFAN y la ciudad o zona geográfica que seleccione al consultar los eventos, sus datos de pago, historial de compras, preferencias, así como los datos para poder realizar una compra online en TOPPFAN.com. Además, para que la próxima vez que ingrese al sitio y tenga una nueva sesión a través del mismo navegador, ya estará precargado con su nombre.<br /><br />

TOPPFAN o sus anunciantes dentro de las aplicaciones móviles de TOPPFAN y las cookies pueden utilizar cookies como herramientas de seguimiento del comportamiento. Se utilizan las siguientes herramientas:<br />
• Reconocer clientes nuevos o pasados<br />
• Para almacenar su contraseña con la que está registrado en nuestros sitios.<br />
• Para mejorar la experiencia del usuario en nuestros sitios.<br />
• Mostrar contenido publicitario que pueda interesarle. Para lograrlo, podemos observar su comportamiento de navegación en nuestro sitio y otros. También podemos recopilar información sobre su historial de navegación.<br />
• Comprender mejor los intereses de los consumidores y visitantes de los sitios en línea de TOPPFAN.<br />
• Medir el flujo de tráfico al sitio.<br /><br />

Parte de la publicidad en TOPPFAN.com puede estar basada en el comportamiento de navegación, en TOPPFAN o sitios externos.<br /><br />
Cómo controlar el uso de cookies.<br /><br />
Su navegador puede darle la opción de controlar el uso de cookies. Cómo hacerlo depende del tipo de cookies. Algunos navegadores pueden estar configurados para rechazar el uso de cookies.<br /><br />
Si decide bloquear las cookies en su navegador, es posible que las características y funcionalidades del sitio en línea no funcionen, como la compra de boletos.<br /><br /><br />

<strong>4. Medios y procedimiento de actualización del aviso de privacidad.</strong><br />
Este aviso de privacidad será el medio de notificación de cambios o actualizaciones, el cual será publicado en http://www.TOPPFAN.com/aviso_de_privacidad.html<br /><br />


<strong>5. Mecanismos para el ejercicio de los derechos relacionados con la protección de datos personales (derechos ARCO).</strong><br />
Con el fin de dotar al titular de los medios para el ejercicio de los derechos ARCO, TOPPFAN pone a disposición de los titulares el siguiente formulario para la presentación y seguimiento de las solicitudes supervisadas, rectificadas, canceladas u opuestas.<br /><br />
El titular podrá remitir por este medio una solicitud de acceso, rectificación, cancelación u oposición, respecto de sus datos personales.<br /><br />

Esta solicitud debe contener:<br />
• El nombre completo del titular y toda la información que se solicitó para registrarse así como un correo electrónico para comunicar la respuesta a su solicitud.<br />
• Copia electrónica de los documentos que acrediten la identidad del titular de los datos personales (RFC, IFE).<br />
• Descripción clara y precisa de los datos personales sobre los que se busca ejercer alguno de los derechos antes mencionados.<br />
• Cualquier otro elemento o documento que facilite la localización de datos personales.<br /><br />

Indicar los cambios a realizar y / o las limitaciones en el uso de los datos personales y aportar la documentación que sustente su solicitud.<br /><br />
TOPPFAN notificará al titular de los datos personales de la resolución aprobada, en un plazo no superior a 20 días hábiles contados desde la fecha de recepción de la solicitud. Este plazo podrá ser prorrogado por TOPPFAN en una sola ocasión por igual plazo, siempre que las circunstancias del caso lo justifiquen.<br /><br />
TOPPFAN informará al titular de los datos personales el significado y motivación de la resolución mediante correo electrónico y acompañará a dicha resolución de las pruebas pertinentes, en su caso.<br /><br />
TOPPFAN podrá denegar el acceso total o parcial a los datos personales o la rectificación, cancelación u oposición al tratamiento de los mismos, en los siguientes casos:<br />
• Cuando el solicitante no sea el titular o el representante legal no esté debidamente acreditado.<br />
• Cuando los datos personales del solicitante no se encuentren en la base de datos de TOPPFAN.<br />
• Cuando se infrinjan los derechos de un tercero.<br />
• Cuando exista impedimento legal o resolución de una autoridad.<br />
• Cuando la rectificación, cancelación u oposición se haya realizado previamente, por lo que la solicitud carece de fundamento.<br /><br />

La cancelación de los datos personales dará lugar a un plazo de bloqueo tras el cual TOPPFAN procederá a la supresión de los datos correspondientes. Una vez cancelados los datos personales correspondientes, TOPPFAN notificará a su titular.<br /><br />
Hecho esto, TOPPFAN podrá conservar los datos personales exclusivamente a los efectos de las responsabilidades derivadas del tratamiento a que se refiere el Aviso de Privacidad.<br /><br />
TOPPFAN no estará obligado a cancelar los datos personales en el caso de los supuestos establecidos en el artículo vigésimo sexto de la Ley Federal de Protección de Datos en Posesión de los Particulares.<br /><br />
Asimismo, cuando la información recabada en los datos personales ya no sea necesaria para el cumplimiento de las finalidades previstas en este Aviso de Privacidad y en las disposiciones legales aplicables, sus datos personales serán cancelados de la base de datos de TOPPFAN.<br /><br /><br />

<strong>6. Información de niños menores de 13 años </strong><br />
TOPPFAN no recopila información de usuarios menores de 13 años bajo ningún supuesto.<br /><br />
SI TIENE ALGUNA DUDA O PREGUNTA CON RESPECTO AL AVISO DE PRIVACIDAD, POR FAVOR CONTACTE CON NOSOTROS A LA SIGUIENTE DIRECCIÓN: fans@TOPPFAN.com.mx<br /><br />
Fecha de última actualización: 15 de marzo de 2021</p>',
        ];
    }

    public function news(Request $request)
    {
        $rss_news = FeedReader::read('https://e00-marca.uecdn.es/rss/portada.xml');
        // return $rss_news;

        foreach ($rss_news->get_items() as $rss_new) {
            // Parsing date
            $date = Carbon::createFromFormat('j F Y, g:i a', $rss_new->get_date());

            // Parsing image
            // dd()

            if (array_key_exists('content', $rss_new->data['child']['http://search.yahoo.com/mrss/'])) {
                $img = $rss_new->data['child']['http://search.yahoo.com/mrss/']['content'][0]['attribs'][''];
                if (array_key_exists('medium', $img)) {
                    $img = ($img['medium'] == 'image' ? $img['url'] : '');

                    // Parsing content
                    $content = explode("<a href=", trim($rss_new->get_content()), 2);

                    // Parsing url
                    $url = explode("\"", $content[1])[1];

                    // Returning data
                    $news[] = [
                        'title' => $rss_new->get_title(),
                        'date' => $date->format('Y-m-d H:i'),
                        'content' => $content[0],
                        'img' => $img,
                        'url' => $url,
                    ];
                }
            }
        }

        return [
            'error' => false,
            'news' => $news,
        ];
    }

    public function news_bak(Request $request)
    {
        $faker = Faker\Factory::create();

        $news = array();
        for ($i = 1; $i <= 10; $i++) {
            $news[] = [
                'title' => $faker->name,
                'date' => $faker->date('Y-m-d H:i'),
                'content' => $faker->text,
                'img' => $faker->imageUrl,
            ];
        }

        return [
            'error' => false,
            'news' => $news,
        ];
    }

    public function events(Request $request)
    {
        // Preparing return array
        $return = array();

        // Fetching active events
        foreach (Event::upcoming_events(null, null, null, false, true) as $event) {
            // Appending event and venue data
            $event_array = [
                'event' => [
                    'id' => $event->id,
                    'name' => $event->name,
                    'date' => substr($event->date, 0, -3),
                    'venueId' => $event->venue_id,
                    'img' => asset($event->img_slideshow_path()) . "?t=2",
                ],
                'venue' => [
                    'id' => $event->venue->id,
                    'name' => $event->venue->name,
                    'lat' => (float)$event->venue->lat,
                    'long' => (float)$event->venue->long,
                    'img' => "https://www.milenio.com/uploads/media/2019/07/08/estadio-olimpico-de-ciudad-universitaria_0_71_800_497.jpg",
                ],
            ];

            // Appending data to return array
            $return[] = $event_array;
        }

        // Returning data
        return [
            'error' => false,
            'events' => $return,
        ];
    }


    public function tickets(Request $request)
    {
        // Preparing return array
        $return = array();

        // Appending tickets for event
        if ($request->client_user_id != 'na') {
            // Fetching active events
            foreach (Event::upcoming_events(null, null, null, false) as $event) {
                // Appending event and venue data
                $event_array = [
                    'event' => [
                        'id' => $event->id,
                        'name' => $event->name,
                        'date' => substr($event->date, 0, -3),
                        'venueId' => $event->venue_id,
                        'img' => asset($event->img_slideshow_path()) . "?t=2",
                    ],
                    'venue' => [
                        'id' => $event->venue->id,
                        'name' => $event->venue->name,
                        'lat' => (float)$event->venue->lat,
                        'long' => (float)$event->venue->long,
                        'img' => "https://www.milenio.com/uploads/media/2019/07/08/estadio-olimpico-de-ciudad-universitaria_0_71_800_497.jpg",
                    ],
                    'tickets' => [],
                ];

                // Appending tickets
                foreach ($event->orders()->where('client_user_id', $request->client_user_id)->where('status', '!=', 'cancelled')->get() as $order) {
                    foreach ($order->tickets as $ticket) {
                        $event_array['tickets'][] = $ticket->API();
                    }
                }

                // Appending data to return array
                if (count($event_array['tickets']) > 0) {
                    $return[] = $event_array;
                }
            }
        }

        // Returning data
        return [
            'error' => false,
            'tickets' => $return,
        ];
    }
}
