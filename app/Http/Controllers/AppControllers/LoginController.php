<?php

namespace App\Http\Controllers\AppControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// Models
use App\User;
use App\ClientUser;

// Notifications
use App\Jobs\sendNotificationCodeForAppLogin;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        // If user sent is an email
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            // Checking if user exists
            if (Auth::guard('client_users')->attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1])) {
                // Fetching user
                $user = Auth::guard('client_users')->user();

                // Creates token for user
                $token = $user->createToken('API Token')->accessToken;

                // Returning user data
                $return['error'] = false;
                $return['token'] = $token;
                $return['user'] = $user->API();
            } else {
                $return['error'] = true;
                $return['error_message'] = "Usuario o password inválido";
            }
        } else {
            $return['error'] = true;
            $return['error_message'] = "Email inválido";
        }

        // Returning data
        return $return;
    }

    public function check_mail(Request $request)
    {
        // If user sent is an email
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            // Verify if email exists
            $user_exists = ClientUser::where('email', $request->email)->count();

            if ($user_exists > 0) {
                // Fetch user
                $user = ClientUser::where('email', $request->email)->first();

                if ($user->active) {
                    // Returning data
                    return [
                        'error' => false,
                        'active_user' => true,
                    ];
                } else {
                    // Generate validation code
                    $code = \Str::random(8);

                    // Update user password with generated code
                    $user->update(['password' => \Hash::make($code)]);

                    // Dispatching email job
                    sendNotificationCodeForAppLogin::dispatch([
                        'email' => $user->email,
                        'name' => $user->name,
                        'code' => $code
                    ]);

                    // Returning data
                    return [
                        'error' => false,
                        'active_user' => false,
                        'code' => $code
                    ];
                }
            } else {
                // Returning data
                return [
                    'error' => true,
                    'error_message' => "El email no existe"
                ];
            }
        } else {
            // Returning data
            return [
                'error' => true,
                'error_message' => "Email inválido"
            ];
        }
    }

    public function check_code(Request $request)
    {
        // If user sent is an email
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            // Checking if user exists
            if (Auth::guard('client_users')->attempt(['email' => $request->email, 'password' => $request->code])) {
                // Fetching user
                $client_user = Auth::guard('client_users')->user();

                // Activate user
                $client_user->update(['active' => 1]);

                // Creates token for user
                $token = $client_user->createToken('API Token')->accessToken;

                // Returning user data
                return [
                    'error' => false,
                    'token' => $token,
                    'user' => $client_user->API(),
                ];
            } else {
                // Returning code validation error
                return [
                    'error' => true,
                    'error_message' => "Usuario o código inválido",
                ];
            }
        } else {
            // Returning data
            return [
                'error' => true,
                'error_message' => "Email inválido"
            ];
        }

        // Recibo
        // - email
        // - code

        // - error = bool true
        // - error_message = String con el error (probablemente “El código es incorrecto”)

        // - error = bool false
        // - token = String con el token a guardar en la variable global globalUser
        // - user =
    }

    public function register(Request $request)
    {
    }

    public function forgot_password(Request $request)
    {
    }
}
