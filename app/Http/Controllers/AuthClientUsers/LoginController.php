<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();

            if ($user->active) {
                if ($user->type == 'root') {
                    // Redirecting root user to its dashboard
                    return redirect()->route('root.dashboard');
                } else {
                    // The user is active, not suspended, and exists. So we make it select the system it wants
                    return redirect()->route('system.select');
                }
            } else {
                // Innactive user
                return back()->with(['error' => 'Cuenta  desactivada']);
            }
        } else {
            // Attempting login for client_user
            if (Auth::guard('client_users')->attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::guard('client_users')->user();

                if (!$user->active_on_admin) {
                    return back()->with(['error' => 'El usuario no tiene acceso al panel administrador.']);
                }

                return redirect()->route('tds');
            }

            return back()->with(['error' => 'No existe el usuario con esa contraseña']);
        }
    }
}
