<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Checkpoint;
use App\TicketType;
use App\CheckpointData;
use App\Venue;
use App\Event;

use Carbon\Carbon;
use Response;

class CheckpointController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($system_slug, $id)
    {
        return view('admin.checkpoints.list', [
            'checkpoints' => Checkpoint::where('venue_id', $id)->get(),
            'venue' => Venue::find($id),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => Venue::find($id)->name, 'url' => route('system.venues.config', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Checkpoints', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $system_slug, $id)
    {
        // Creating checkpoint
        Checkpoint::create([
            "name" => $request->name,
            "type" => $request->type,
            "venue_id" => $id
        ]);

        // Redirecting to checkpoint list
        return redirect()
            ->back()
            ->with('success', 'Checkpoint creado correctamente');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $system_slug, $venue, $check)
    {
        $checkpoint = Checkpoint::find($check);
        $checkpoint->update($request->except('_token', '_method'));

        return back()
            ->with('success', 'Checkpoint actualizado correctamente');
    }

    public function syncCheckpoints()
    {
        \Log::info('sync checkpoints');

        $checkpoints = array();
        foreach (Checkpoint::all() as $checkpoint) {
            $checkpoints[] = [
                'id' => $checkpoint->id,
                'venue_id' => $checkpoint->venue_id,
                'name' => $checkpoint->name,
                'parent_checkpoint' => $checkpoint->parent_checkpoint,
                // 'type' => $checkpoint->pretty_type()
                'type' => $checkpoint->type
            ];
        }

        return Response::json([
            'error' => false,
            'checkpoints' => $checkpoints
        ]);
    }

    /**
     * Return all checkpoints in the event
     * @param $id id of the event
     */
    public function syncEventCheckpoints($id)
    {
        $event = Event::find($id);
        $checkpoints = array();
        foreach ($event->ticket_template->ticket_types as $ticket) {
            foreach ($ticket->checkpoints as $checkpoint) {
                $checkpoints[$checkpoint->id] = [
                    'id' => $checkpoint->id,
                    'venue_id' => $checkpoint->venue_id,
                    'name' => $checkpoint->name,
                    'parent_checkpoint' => $checkpoint->parent_checkpoint,
                    'type' => $checkpoint->pretty_type()
                ];
            }
        }

        return $checkpoints;
    }

    public function checkpointTicket()
    {
        $checkpoint_tickets = array();
        foreach (TicketType::all() as $ticket) {
            foreach ($ticket->checkpoints as $check) {
                $checkpoint_tickets[] = [
                    'checkpoint_id' => $check->pivot->checkpoint_id,
                    'ticket_type_id' => $check->pivot->ticket_type_id,
                ];
            }
        }
        return $checkpoint_tickets;
    }

    /**
     * Save the battery status of device
     * @param $id id of checkpoint
     */

    public function batteryStatus(Request $request, $id)
    {
        try {
            $checkpoint = Checkpoint::find($id);
            $data = CheckpointData::updateOrCreate(
                [
                    'device_id' => $request->device_id,
                    'event_id' => $request->event_id,
                    'checkpoint_id' => $id
                ],
                $request->except(['device_id', 'event_id'])
            );

            return [
                'error' => false,
                'message' => 'Información guardada correctamente'
            ];
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo guardar la información'
            ];
        }
    }

    /**
     * Save the opening/closing time of the checkpoint
     * @param $id id of checkpoint
     * @param $status opening/closing
     */

    public function checkpointStatus(Request $request, $id, $status)
    {
        try {
            $checkpoint = Checkpoint::find($id);
            $checkpoint_data = CheckpointData::updateOrCreate(
                [
                    'device_id' => $request->device_id,
                    'event_id' => $request->event_id,
                    'checkpoint_id' => $id
                ],
                ($status == 'start') ? ['start' => Carbon::now()] : ['end' => Carbon::now()]
            );

            return [
                'error' => false,
                'message' => 'Información guardada correctamente'
            ];
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo guardar la información'
            ];
        }
    }
}
