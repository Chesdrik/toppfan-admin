<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketDistributionSystem\SendTicketDistribution;
use Illuminate\Http\Request;
use App\Event;
use App\Venue;
use App\Team;
use App\TicketTemplate;
use App\TicketReading;
use App\Ticket;
use App\TicketType;
use App\EventSeatAvailability;
use App\System;
use App\Order;
use App\ClientUser;
use App\TicketDistributionSystem\TicketGroup;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Storage;
use Response;
use Faker\Factory as Faker;
use Auth;
use App\Jobs\SendTicketLotDistributionEmail;

class EventController extends Controller
{
    /**
     * Display a view with list options (all, byVenue, byTeam)
     *
     * @return \Illuminate\Http\Response
     */
    public function index($system_slug)
    {
        $system = System::bySlug($system_slug);

        return view('admin.events.main', [
            'venues' => $system->venues,
            'teams' => $system->teams,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => true],
            ]
        ]);
    }

    public function all($system_slug)
    {
        return view('admin.events.showCalendar', [
            'title' => 'Todos los eventos',
            'upcoming_events' => Event::upcoming_events(4, null, null),
            'past_events' => Event::past_events(3, null, null)->get(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Todos los eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => true]
            ]
        ]);
    }

    public function byVenue($system_slug, Venue $venue)
    {
        return view('admin.events.showCalendar', [
            'title' => $venue->name,
            'upcoming_events' => Event::upcoming_events(4, $venue->id, null),
            'past_events' => Event::past_events(3, $venue->id, null)->get(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => $venue->name, 'url' => route('system.events.byVenue', [session('system_slug'), $venue]), 'active' => true]
            ]
        ]);
    }

    public function byTeam($system_slug, Team $team)
    {
        return view('admin.events.showCalendar', [
            'title' => $team->name,
            'upcoming_events' => Event::upcoming_events(4, null, $team->id),
            'past_events' => Event::past_events(3, null, $team->id)->get(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => $team->name, 'url' => route('system.events.byTeam', [$system_slug, $team]), 'active' => true]
            ]
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($system_slug)
    {
        $system = System::bySlug($system_slug);
        $teams = $system->teams()->pluck('teams.name', 'teams.id');

        return view('admin.events.create', [
            // 'teams' => Team::dropdown(),
            'venues' => $system->venues,
            'teams' => $teams,
            // 'template' => (!is_null($first)) ? TicketTemplate::find($first) : null,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Crear', 'url' => route('system.events.create', session('system_slug')), 'active' => true]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $system_slug)
    {
        // if ($request->costs) {
        //     $template = TicketTemplate::create([
        //         'name' => $request->name_template,
        //         'venue_id' => $request->venue_id,
        //     ]);

        //     foreach ($request->costs as $ticket => $cost) {
        //         $template->ticket_types()->attach($ticket, ['price' => $cost]);
        //     }

        //     $request->request->add([
        //         'ticket_template_id' => $template->id
        //     ]);
        // }

        // Appending venue_id and making event active
        $request->request->add([
            'venue_id' => $request->venue_id,
            'active' => 1,
        ]);

        // Creating event
        $event = Event::create(
            $request->except('_token', 'img', 'img_slideshow')
        );

        // Creating event_seat_availablity for event
        $ticket_template = TicketTemplate::find($request->ticket_template_id);
        foreach ($ticket_template->ticket_types as $ticket_type) {
            EventSeatAvailability::create([
                'event_id' => $event->id,
                'ticket_type_id' => $ticket_type->id,
                'total' => $ticket_type->total_seats,
                'available' => $ticket_type->total_seats,
            ]);
        }

        // Running script with
        // \Artisan::call('ticket:status ' . $event->id);

        // Returning user to event overview
        return redirect(route('system.events.show', [$system_slug, $event->id]))
            ->with('success', 'Evento creado correctamente');
    }

    public function upload_img(Request $request, $system_slug, $id)
    {
        $event = Event::find($id);

        // Uploading image
        if ($request->hasFile('img') && $request->file('img')->isValid()) {
            // Verifying img is PNG
            if ($request->file('img')->getClientOriginalExtension() != 'png') {
                return back()->with('error', 'La imagen debe tener extensión .png');
            }

            // Uploading file
            $filename = $event->id . '_cover.png';
            Storage::putFileAs("public/events/{$event->id}", $request->file('img'), $filename);
            $event->update([
                'img' => $filename
            ]);
        }

        // Uploading slideshow image
        if ($request->hasFile('img_slideshow') && $request->file('img_slideshow')->isValid()) {
            // Verifying img is JPG
            if ($request->file('img_slideshow')->getClientOriginalExtension() != 'jpg') {
                return back()->with('error', 'La imagen debe tener extensión .jpg');
            }

            // Uploading file
            $filename = $event->id . '_slideshow.jpg';
            Storage::putFileAs("public/events/{$event->id}", $request->file('img_slideshow'), $filename);
            $event->update([
                'img_slideshow' => $filename
            ]);
        }

        // Returning user to event overview
        return [
            'error' => false,
        ];
        // return redirect(route('system.events.show', [session('system_slug'), $event->id]))
        //     ->with('success', 'Evento creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($system_slug, $id)
    {
        $event = Event::find($id);
        $dates = array();
        $labels = array();
        $event_statistics = (count($event->statistics) > 0) ? $event->general_info_stats() : null;

        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
            ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
            ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => true],
        ];

        return view('admin.events.show', compact('breadcrumb', 'event', 'dates', 'event_statistics'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($system_slug, $id)
    {
        $event = Event::find($id);
        $venue = Venue::find($event->venue_id);
        $selected_event_groups = $event->event_groups->pluck('id')->toArray();

        // Verify event has ticket template
        if ($event->ticket_template_id == null) {
            $event->update(['ticket_template_id' => $venue->ticket_templates->first()->id]);
        }

        // Returning view
        return view('admin.events.edit', [
            'event' => $event,
            'teams' => Team::dropdown(),
            'venue' => Venue::find($event->venue_id),
            'template' => TicketTemplate::find($event->ticket_template_id),
            'selected_event_groups' => $selected_event_groups,
            'event_seats' => $event->event_seat_availability,
            'info' => $event->generate_accreditations(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Editar ', 'url' => route('system.events.edit', [session('system_slug'), $event->id]), 'active' => true]
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $system_slug, $id)
    {
        // Fetching event
        $event = Event::find($id);

        // Updating event
        $event->update([
            "name" => $request->name,
            "team_id" => $request->team_id,
            "date" => $request->date,
            "ticket_template_id" => $request->ticket_template_id,
            "on_sale" => $request->on_sale == 'on' ? 1 : 0,
        ]);

        // if (isset($request->availables)) {
        //     foreach ($request->availables as $id => $seats) {
        //         $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $id);
        //         $e_s->update(['available' => $seats]);
        //     }
        // }

        // Sync event groups
        $event->event_groups()->sync($request->event_groups);

        // Returning to event view
        return redirect()
            ->route('system.events.show', [session('system_slug'), $event->id])
            ->with('success', 'Información actualizada correctamente');
    }

    public function change($system_slug, $id)
    {
        $event = Event::find($id);
        $event->update(['active' => ($event->active == 1) ? 0 : 1]);

        return back();
    }

    public function processStatistics($id)
    {
        Artisan::call('process:stats ' . $id);

        return back()->with('session_message', 'El procesamiento de las estadisticas se completo');
    }


    public function eventsAPI()
    {
        $events = array();

        foreach (Event::upcoming_events(3, 1, 1) as $event) {
            $events[] = $event->API_POS();
        }

        // Returning data
        return [
            'error' => false,
            'events' => $events,
        ];
    }


    public function eventAPI($id)
    {
        $event = Event::find($id);

        // Returning data
        return [
            'error' => false,
            'event' => $$event->API(),
        ];
    }



    public function syncEvents()
    {
        \Log::info('sync events');
        $events = array();
        foreach (Event::active() as $event) {
            $events[] = [
                'id' => $event->id,
                'venue_id' => $event->venue_id,
                'name' => $event->name,
                'date' => $event->date,
            ];
        }

        return Response::json([
            'error' => false,
            'events' => $events
        ]);
    }

    public function accreditations($id)
    {
        $event = Event::find($id);
        return view('admin.accreditations.selection', [
            'event' => Event::find($id),
            'venue' => Venue::find($event->venue_id),
            'user_tickets' => Auth::user()->user_tickets(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => 'Listado de ' . $event->venue->name, 'url' => route('system.events.byVenue', [session('system_slug'), $event->venue->id]), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                // ['label' => 'Generar cortesías', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    public function statistics($system_slug, $id)
    {
        $event = Event::find($id);
        $event_statistics = (count($event->statistics) > 0) ? $event->parser_stats() : null;
        $sales = $event->sales_statistics();

        return view('admin.events.statistics', [
            'event' => Event::find($id),
            'event_statistics' => $event_statistics,
            'sales' => $sales,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => 'Listado de ' . $event->venue->name, 'url' => route('system.events.byVenue', [session('system_slug'), $event->venue->id]), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
            ]
        ]);
    }

    public function ticket_readings($system_slug, $id)
    {
        $event = Event::find($id);
        return view('admin.events.ticket_readings', [
            'event' => Event::find($id),
            'readings' => json_decode($event->data_readings, true),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => 'Listado de ' . $event->venue->name, 'url' => route('system.events.byVenue', [session('system_slug'), $event->venue->id]), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                // ['label' => 'Generar cortesías', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    public function ticket_list($system_slug, $id)
    {
        $event = Event::find($id);

        return view('admin.events.orders', [
            'event' => $event,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Boletos', 'url' => route('system.events.tickets.list', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function courtesy_list($system_slug, $id)
    {
        $event = Event::find($id);

        return view('admin.events.courtesies', [
            'event' => $event,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Cortesías', 'url' => route('system.events.courtesy.list', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function accreditations_list($system_slug, $id)
    {
        $event = Event::find($id);
        return view('admin.events.accreditations', [
            'event' => $event,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Acreditaciones', 'url' => route('system.events.accreditations.list', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function qrs($system_slug, $id)
    {
        $event = Event::find($id);
        $order = \App\Order::find(1479);
        $order->generateQRs();
        return view('admin.courtesies.qrs', [
            'event' => $event,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Cortesías generadas', 'url' => route('system.events.courtesies_list', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function stadistic_pdf(Request $request, $system_slug, $id)
    {

        $img_sales = $request->base64_sales;
        $img_readings = $request->base64_readings;
        $img_exits = $request->base64_exits;
        $img_device = $request->base64_device;
        $img_devicesPie = $request->base64_devicesPie;
        $img_checkpoint_alerts = $request->base64_checkpoint_alerts;
        $img_device_alertsPieChart = $request->base64_device_alertsPieChart;
        $img_checkpointChart = $request->base64_checkpointChart;
        $img_incidences = $request->base64_incidences;

        //dd($img);
        // $url = Storage::url('events/tmp.png');

        // $image = Event::base64_to_jpeg( $request->base64,  $url );

        $event = Event::find($id);
        $event_statistics = (count($event->statistics) > 0) ? $event->parser_stats() : null;
        $readings = TicketReading::forEvent($event);
        //dd($event->orders);


        $pdf = \PDF::loadView('admin.events.event_pdf', compact('event', 'event_statistics', 'readings', 'img_sales', 'img_readings', 'img_exits', 'img_device', 'img_devicesPie', 'img_checkpoint_alerts', 'img_device_alertsPieChart', 'img_checkpointChart', 'img_incidences'));

        return $pdf->stream('Evento' . $event->id . '.pdf');
    }

    public function seatsForMap($system_slug, $id)
    {
        $event = Event::find($id);

        $data = array();
        foreach ($event->venue->parent_zones() as $zone) {
            $data['zones'][] = [
                'name' => $zone->name,
                'subzones' => [
                    $zone->subzonesForMap()
                ]
            ];
        }
        return $data;
    }

    public function generateAccreditations($system_slug, $id)
    {
        $event = Event::find($id);
        foreach ($event->team->accreditations->where('type', 'operative') as $accreditation) {
            $accreditation->event()->syncWithoutDetaching([$id => ['version' => $accreditation->version]]);
        }

        return back()->with('success', 'Acreditaciones asociadas');
    }

    public function ticket_qrs_single_page($system_slug, $event_id)
    {
        $event = Event::find($event_id);
        $orders = $event->regular_orders->pluck('id')->toArray();
        $tickets = Ticket::whereIn('order_id', $orders)->get();

        return $this->qrs_single_page($system_slug, $tickets);
    }

    public function courtesy_qrs_single_page($system_slug, $event_id)
    {
        set_time_limit(120);

        $event = Event::find($event_id);
        $orders = $event->courtesy_orders->pluck('id')->toArray();
        $tickets = Ticket::whereIn('order_id', $orders)->get();

        return $this->qrs_single_page($system_slug, $tickets);
    }

    public function qrs_single_page($system_slug, $tickets)
    {
        // Verify ticket qr in filesystem
        foreach ($tickets as $ticket) {
            $ticket->verify_qr_existance();
        }

        // Returning view with tickets
        return view('admin.events.ticket_qrs_single_page', [
            'tickets' => $tickets,
        ]);
    }

    public function analysis($system_slug, $id)
    {
        $event = Event::find($id);
        return view('admin.events.analysis', [
            'event' => $event,
            'ticket_data' => $event->ticket_analysis(),
            'accreditation_data' => $event->accreditation_analysis(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Acreditaciones', 'url' => route('system.events.accreditations.list', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function holding($system_slug, $id)
    {
        $event = Event::find($id);

        return view('admin.events.holding', [
            'event' => $event,
            'zones' => $event->seats_by_zone_sidebar(),
            'data' => $event->ticket_status(),
            'type' => 'event',
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Holding', 'url' => route('system.events.tickets.holding', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function general_statistics($system_slug, $id)
    {
        $event = Event::find($id);
        return view('admin.events.statistics.holding_stats', [
            'event' => $event,
            'data' => $event->ticket_status(),
            'type' => 'event',
            'event_statistics' => $event->sales(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Holding', 'url' => route('system.events.tickets.holding', [session('system_slug'), $id]), 'active' => true],
                ['label' => 'Generales', 'url' => route('system.events.general.statistics', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function ticket_distribution($system_slug, $id)
    {
        $event = Event::find($id);
        return view('admin.events.ticket_distribution', [
            'event' => $event,
            'ticket_types' => $event->ticket_template->ticket_types->where('type', 'general'),
            'ticket_groups' => $event->ticket_groups,
            'distributors' => $event->ticket_groups->groupBy('client_user_id'),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Distribución de boletos', 'url' => route('system.events.tickets.distribution', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function send_ticket_distribution(SendTicketDistribution $request, $system_slug, $id)
    {
        try {

            $system = System::bySlug($system_slug);

            if (!$request->types) {
                return back()->with('error', 'Es necesario seleccionar al menos un tipo de boleto para asignar.');
            }

            foreach ($request->types as $key => $value) {
                $event_seat_availability = EventSeatAvailability::where('event_id', $id)->where('ticket_type_id', $key)->first();

                if (!$request->amount[$key]) {
                    return back()->with('error', 'Asegúrate de asignar una cantidad a cada tipo de boleto que hayas seleccionado.');
                }

                if ($event_seat_availability->available < $request->amount[$key]) {
                    return back()->with('error', 'La zona ' . $event_seat_availability->ticket_type->name . ' sólo tiene ' . $event_seat_availability->available . ' boletos disponibles.');
                }
            }

            // Checking if user exists
            $user = ClientUser::where('email', $request->email)->first();
            if (!$user) {
                $user = ClientUser::create([
                    'name' => $request->name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'password' => Hash::make(Str::random(15)),
                    'type' => 'distributor',
                    'active' => 0
                ]);
            }

            foreach ($request->types as $key => $value) {
                // Creating ticket group
                $ticket_group = TicketGroup::create([
                    'event_id' => $id,
                    'client_user_id' => $user->id,
                    'assigned_by' => $system->client_user->id,
                    'ticket_type_id' => $key,
                    'amount' => $request->amount[$key],
                    'available' => $request->amount[$key],
                    'distributable' => $request->distributable[$key]
                ]);

                $event_seat_availability = EventSeatAvailability::where('event_id', $id)->where('ticket_type_id', $key)->first();
                $event_seat_availability->update([
                    'courtesy' => $event_seat_availability->courtesy + $request->amount[$key],
                    'available' => $event_seat_availability->available - $request->amount[$key]
                ]);
            }

            // Preparing data for mail
            $data = [
                'id' => $user->id,
                'email' => $user->email,
                'ticket_type_name' => TicketType::find($key)->name,
                'event_name' => Event::find($id)->name,
                'amount' => $ticket_group->amount,
                'activation_pending' => ($user->active == 1) ? false : true,
                'group' => (count($request->types) > 1) ? true : false,
                'date' => pretty_short_date($ticket_group->created_at)
            ];

            // Dispatching email job
            SendTicketLotDistributionEmail::dispatch($data);

            return back()->with('success', 'Boletos asignados y notificación enviada correctamente');
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('error', 'Algo salió mal, no fue posible completar el proceso');
        }
    }

    public function resend_email($system_slug, $event_id, $ticket_group_id)
    {
        try {
            // Fetching ticket group
            $ticket_group = TicketGroup::find($ticket_group_id);

            // Preparing data for mail
            $data = [
                'id' => $ticket_group->client_user_id,
                'email' => $ticket_group->client_user->email,
                'ticket_type_name' => $ticket_group->ticket_type->name,
                'event_name' => $ticket_group->event->name,
                'amount' => $ticket_group->amount,
                'activation_pending' => ($ticket_group->client_user->active == 1) ? false : true,
                'group' => false,
                'date' => pretty_short_date($ticket_group->created_at)
            ];

            // Dispatching email job
            SendTicketLotDistributionEmail::dispatch($data);

            return back()->with('success', 'Email reenviado correctamente');
        } catch (\Throwable $th) {
            return back()->with('error', 'Algo salió mal, no fue posible reenviar el email.');
        }
    }

    public function order_report($system_slug, $event_id)
    {
        $event = Event::find($event_id);
        $file_content = "ID orden,Nombre,Email,Total,ID,Autorizacion,4 digitos,Banco,Hora\n";

        foreach ($event->regular_orders as $order) {
            $total = pretty_money($order->total);
            $file_content .= "{$order->id},{$order->client_user->full_name()},{$order->client_user->email},{$total},";

            if (in_array($order->payment_method, ['credit_card', 'debit_card'])) {
                if (!is_null($order->latest_log())) {
                    $affipay_log = json_decode($order->latest_log()->response);

                    $file_content .= "{$affipay_log->id},{$affipay_log->dataResponse->authorization},{$affipay_log->dataResponse->binInformation->bin},{$affipay_log->dataResponse->binInformation->bank}";
                }
            }

            $file_content .= ",{$order->created_at}\n";
        }

        return response()->attachment($file_content, "{$system_slug}_reporte_{$event->id}.csv");

        // return response()->streamDownload(function ($file_content) {
        //     echo $file_content;
        // }, "{$system_slug}_reporte_{$event->id}.csv");
    }

    public function search(Request $request, $system_slug, $event_id)
    {
        $event = Event::find($event_id);
        $search_results = [
            ['title' => 'Nombre', 'results' => Order::searchBy($event_id, 'name', $request->search)],
            ['title' => 'E-mail', 'results' => Order::searchBy($event_id, 'email', $request->search)],
            ['title' => 'ID Orden', 'results' => Order::searchBy($event_id, 'order_id', $request->search)],
        ];

        return view('admin.events.orders_search', [
            'search_results' => $search_results,
            'event' => $event,
            'search' => $request->search,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $event->id]), 'active' => false],
                ['label' => 'Boletos', 'url' => route('system.events.tickets.list', [session('system_slug'), $event->id]), 'active' => true],
            ]
        ]);
    }

    public function tickets_generate(Request $request, $system_slug, $event_id)
    {
        $event = Event::find($event_id);

        return view('admin.events.orders_generate', [
            'event' => $event,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $event->id]), 'active' => false],
                ['label' => 'Boletos', 'url' => route('system.events.tickets.list', [session('system_slug'), $event->id]), 'active' => true],
                ['label' => 'Generar', 'url' => '#', 'active' => true],
            ]
        ]);
    }
}
