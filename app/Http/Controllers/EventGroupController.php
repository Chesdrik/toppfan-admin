<?php

namespace App\Http\Controllers;

use App\EventGroup;
use Illuminate\Http\Request;
use App\Http\Requests\EventGroupStore;
use App\Http\Requests\EventGroupUpdate;

use App\System;
use App\Event;
use App\Ticket;
use Storage;



class EventGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($system_slug)
    {
        $system = System::bySlug($system_slug);

        return view('admin.events.groups.index', [
            'system' => $system,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Grupos de eventos', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($system_slug)
    {
        $system = System::bySlug($system_slug);

        return view('admin.events.groups.create', [
            'system' => $system,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Grupos de eventos', 'url' => route('system.events.groups.index', session('system_slug')), 'active' => false],
                ['label' => 'Crear', 'url' => '#', 'active' => true]
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventGroupStore $request, $system_slug)
    {
        // Compiling data
        $system = System::bySlug($system_slug);
        $data = array_merge($request->only('name', 'slug', 'tagline', 'description'), ['system_id' => $system->id]);
        $event_group = EventGroup::create($data);

        // Returning to listing
        return redirect()->route('system.events.groups.show', [$system_slug, $event_group->id])->with('success', 'Grupo de eventos creado');
    }

    public function upload_img(Request $request, $system_slug, $id)
    {
        $event_group = EventGroup::find($id);

        // Uploading image
        if ($request->hasFile('img') && $request->file('img')->isValid()) {
            // Verifying img is PNG
            if ($request->file('img')->getClientOriginalExtension() != 'png') {
                return back()->with('error', 'La imagen debe tener extensión .png');
            }

            // Uploading file
            $filename = $event_group->id . '_cover.png';
            Storage::putFileAs("public/event_groups/{$event_group->id}", $request->file('img'), $filename);
            $event_group->update([
                'img' => $filename
            ]);
        }


        // Uploading image
        if ($request->hasFile('img_slideshow') && $request->file('img_slideshow')->isValid()) {
            // Verifying img is JPG
            if ($request->file('img_slideshow')->getClientOriginalExtension() != 'jpg') {
                return back()->with('error', 'La imagen debe tener extensión .jpg');
            }

            // Uploading file
            $filename = $event_group->id . '_slideshow.jpg';
            Storage::putFileAs("public/event_groups/{$event_group->id}", $request->file('img_slideshow'), $filename);
            $event_group->update([
                'img' => $filename
            ]);
        }

        // Returning response
        return [
            'error' => false,
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventGroup  $eventGroup
     * @return \Illuminate\Http\Response
     */
    public function show($system_slug, $event_group_id)
    {
        $event_group = EventGroup::findOrFail($event_group_id);

        return view('admin.events.groups.show', [
            'event_group' => $event_group,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Grupos de eventos', 'url' => route('system.events.groups.index', session('system_slug')), 'active' => false],
                ['label' => $event_group->name, 'url' => '#', 'active' => true]
            ]
        ]);
    }

    public function edit($system_slug, $event_group_id)
    {
        // Compiling data
        $system = System::bySlug($system_slug);
        $event_group = EventGroup::findOrFail($event_group_id);
        $selected_events = $event_group->events()->pluck('events.id')->toArray();
        $compressed_ticket_types = $event_group->compressed_ticket_types();

        return view('admin.events.groups.edit', [
            'system' => $system,
            'event_group' => $event_group,
            'selected_events' => $selected_events,
            'compressed_ticket_types' => $compressed_ticket_types,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Grupos de eventos', 'url' => route('system.events.groups.index', session('system_slug')), 'active' => false],
                ['label' => $event_group->name, 'url' => route('system.events.groups.show', [session('system_slug'), $event_group_id]), 'active' => false],
                ['label' => 'Editar', 'url' => '#', 'active' => true]
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventGroup  $eventGroup
     * @return \Illuminate\Http\Response
     */
    public function update(EventGroupUpdate $request, $system_slug, $event_group_id)
    {
        // Compiling data
        $event_group = EventGroup::findOrFail($event_group_id);

        // Updating event group info
        $event_group->update([
            'name' => $request->name,
            'tagline' => $request->tagline,
            'description' => $request->description,
            'accreditations_on_sale' => $request->accreditations_on_sale == 'on' ? 1 : 0,
        ]);

        // Sync associated events
        $event_group->events()->sync($request->events);

        // Returning to listing
        return back()->with('success', 'Grupo de eventos actualizado');
    }

    public function updateTicketTypes(Request $request, $system_slug, $event_group_id)
    {
        // TODO Si se quita un ticket type, validar que NO exista una orden con ese tipo. Checar que sold = 0
        // Extracting all ticket_type keys from request and defining total to be sold
        $ticket_types = array();
        foreach ($request->except('_token', '_method') as $key => $value) {
            $tt = explode('_', $key);
            if ($tt[0] == 'tt') {
                // dd($tt[1]);
                if (!empty($request->{$tt[1] . '_total'} != 0) && $request->{$tt[1] . '_total'} != 0) {
                    $ticket_types[$tt[1]] = [
                        'price' => $request->{$tt[1] . '_price'},
                        'total' => $request->{$tt[1] . '_total'}
                    ];
                }
            }
        }

        // Compiling data
        $event_group = EventGroup::findOrFail($event_group_id);

        // Sync ticket types with max values
        $event_group->ticket_types()->sync($ticket_types);

        // Returning to listing
        return back()->with('success', 'Tipos de tickets actualizados');
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventGroup  $eventGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventGroup $eventGroup)
    {
        //
    }

    public function holding($system_slug, $id)
    {
        $event_group = EventGroup::find($id);
        return view('admin.events.groups.holding', [
            'event' => $event_group,
            'zones' => $event_group->seats_by_zone_sidebar(),
            'event_group' => $event_group,
            'type' => 'event_group',
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Grupos de eventos', 'url' => route('system.events.groups.index', session('system_slug')), 'active' => false],
                ['label' => $event_group->name, 'url' => route('system.events.groups.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Holding', 'url' => route('system.events.groups.holding', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function orders($system_slug, $id)
    {
        $event_group = EventGroup::find($id);
        return view('admin.events.groups.orders', [
            'event_group' => $event_group,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Grupos de eventos', 'url' => route('system.events.groups.index', session('system_slug')), 'active' => false],
                ['label' => $event_group->name, 'url' => route('system.events.groups.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Órdenes y boletos', 'url' => route('system.events.groups.orders', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    public function zones($system_slug, $event_group_id)
    {
        $system = System::bySlug($system_slug);
        $event_group = EventGroup::findOrFail($event_group_id);
        $selected_events = $event_group->events()->pluck('events.id')->toArray();
        $compressed_ticket_types = $event_group->compressed_ticket_types();

        return view('admin.events.groups.zones', [
            'system' => $system,
            'event_group' => $event_group,
            'selected_events' => $selected_events,
            'compressed_ticket_types' => $compressed_ticket_types,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'active' => false],
                ['label' => 'Grupos de eventos', 'url' => route('system.events.groups.index', session('system_slug')), 'active' => false],
                ['label' => $event_group->name, 'url' => route('system.events.groups.show', [session('system_slug'), $event_group_id]), 'active' => false],
                ['label' => 'Zonas y precios', 'url' => '#', 'active' => true]
            ]
        ]);
    }

    public function ticket_qrs_single_page($system_slug, $event_id)
    {
        $event_group = EventGroup::find($event_id);
        $orders = $event_group->orders->pluck('id')->toArray();
        $tickets = Ticket::whereIn('order_id', $orders)->get();

        $event_object = new EventController;
        return $event_object->qrs_single_page($system_slug, $tickets);
    }
}
