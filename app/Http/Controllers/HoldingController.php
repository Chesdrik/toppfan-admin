<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\EventGroup;
use App\Seat;
use App\Row;
use App\EventSeat;
use App\EventGroupSeat;
use App\EventSeatAvailability;
use App\Ticket;
use App\TicketType;
use App\System;
use App\Order;

use App\Jobs\SendOrderEmail;

class HoldingController extends Controller
{
    public function seats_set_on_hold(Request $request)
    {
        try {
            $object = ($request->type == 'event') ? new Event : new EventGroup;
            $seat_object = ($request->type == 'event') ? new EventSeat : new EventGroupSeat;
            $column_id = ($request->type == 'event') ? 'event_id' : 'event_group_id';

            $event = $object::find($request->event_id);
            $error = false;
            $error_messages = "";
            $amount = 0;
            foreach ($request->tickets as $row_id => $indexes) {
                $row = Row::find($row_id);
                foreach ($indexes as $index) {
                    // Definig seat
                    $seat = Seat::where('row_id', $row->id)->where('name', $index)->first();

                    // Verifying if event_seat is not sold (on_hold_sale)
                    $event_seat = $seat_object::where($column_id, $request->event_id)->where('seat_id', $seat->id)->first();
                    if ($event_seat == null || $event_seat->status != 'on_hold_sale') {
                        $event_seat = $seat_object::updateOrCreate(
                            [$column_id => $request->event_id, 'seat_id' => $seat->id],
                            ['status' => 'on_hold']
                        );
                        $amount++;
                    } elseif ($event_seat->status == 'on_hold_sale') {
                        $error = true;
                        $error_messages .= "El asiento en la fila " . $row->name . " - " . $seat->name . " no se encuentra disponible y por lo tanto no puede ser apartado<br />";
                    }
                }
            }

            // Updating the number of available seats
            $type = TicketType::find($row->ticket_type_id);
            $total = $object::total_tickets_by_type($type, $event);

            if ($request->type == 'event') {
                // Incrementing on hold seat count and substracting from available
                $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $type->id);
                $e_s->increment('on_hold', $amount);
                $e_s->decrement('available', $amount);
            } else {
                // $event->ticket_types()->updateExistingPivot($type, ['sold' => $total['on_hold'] + $total['on_hold_sale']]);
            }

            if ($error) {
                return [
                    'error' => true,
                    'message' => $error_messages
                ];
            } else {
                return [
                    'error' => false,
                    'message' => 'Asientos apartados'
                ];
            }
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => $th->getMessage()
            ];
        }

        // Returning response without error
        return ['error' => false];
    }

    public function seats_set_available(Request $request)
    {
        try {
            $object = ($request->type == 'event') ? new Event : new EventGroup;
            $seat_object = ($request->type == 'event') ? new EventSeat : new EventGroupSeat;
            $column_id = ($request->type == 'event') ? 'event_id' : 'event_group_id';

            $event = $object::find($request->event_id);
            $error = false;
            $error_messages = "";
            $amount = 0;
            foreach ($request->tickets as $row_id => $indexes) {
                $row = Row::find($row_id);
                foreach ($indexes as $index) {
                    $seat = Seat::where('row_id', $row->id)->where('name', $index)->first();

                    // Verifying if eventseat is not sold (on_hold_sale)
                    $event_seat = $seat_object::where($column_id, $request->event_id)->where('seat_id', $seat->id)->first();
                    if ($event_seat == null || $event_seat->status != 'on_hold_sale') {
                        $event_seat = $seat_object::updateOrCreate(
                            [$column_id => $request->event_id, 'seat_id' => $seat->id],
                            ['status' => 'available']
                        );
                        $amount++;
                    } elseif ($event_seat->status == 'on_hold_sale') {
                        $error = true;
                        $error_messages .= "El asiento en la fila " . $row->name . " - " . $seat->name . " no se encuentra disponible y por lo tanto no puede ser liberado<br />";
                    }
                }
            }

            // Updating the number of available seats
            $type = TicketType::find($row->ticket_type_id);
            $total = $object::total_tickets_by_type($type, $event);

            if ($request->type == 'event') {
                // Incrementing available count and substracting from hold seat
                $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $type->id);
                $e_s->decrement('on_hold', $amount);
                $e_s->increment('available', $amount);
            } else {
                // $event->ticket_types()->updateExistingPivot($type, ['sold' => $total['on_hold'] + $total['on_hold_sale']]);
            }

            if ($error) {
                return [
                    'error' => true,
                    'message' => $error_messages
                ];
            } else {
                return [
                    'error' => false,
                    'message' => 'Asientos liberados'
                ];
            }
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => $th->getMessage()
            ];
        }
    }

    public function seats_courtesy_sale(Request $request)
    {
        try {
            $object = ($request->type == 'event') ? new Event : new EventGroup;
            $seat_object = ($request->type == 'event') ? new EventSeat : new EventGroupSeat;
            $column_id = ($request->type == 'event') ? 'event_id' : 'event_group_id';

            $event = $object::find($request->event_id);
            $system = System::bySlug(session('system_slug'));
            $error = false;
            $error_messages = "";

            foreach ($request->tickets as $row_id => $indexes) {
                $row = Row::find($row_id);

                foreach ($indexes as $index) {
                    try {
                        // Defining seat
                        $seat = Seat::where('row_id', $row->id)->where('name', $index)->first();

                        // Verifying if eventseat is not sold (on_hold_sale)
                        $event_seat = $seat_object::where($column_id, $request->event_id)->where('seat_id', $seat->id)->first();
                        if ($event_seat == null || $event_seat->status != 'on_hold_sale') {
                            // Creating order
                            $order = Order::create([
                                'client_user_id' => $system->client_user_id,
                                'event_id' => $request->event_id,
                                'event_type' => ($request->type == 'event') ? 'App\Event' : 'App\EventGroup',
                                'email' => $system->client_user->email,
                                'total' => 0,
                                'type' => 'courtesy',
                                'payment_method' => 'cash',
                                'selling_point' => 'ticket_office',
                                'status' => 'payed'
                            ]);

                            // Creating ticket
                            $ticket = Ticket::create([
                                'ticket_type_id' => $row->ticket_type_id,
                                'order_id' => $order->id,
                                'event_id' => $request->event_id,
                                'event_type' => ($request->type == 'event') ? 'App\Event' : 'App\EventGroup',
                                'amount' => 1,
                                'price' => 0,
                                'name' => 'Boleto de cortesía',
                                'type' => ($request->type == 'event') ? 'courtesy' : 'season_ticket'
                            ]);

                            // Appending seat to ticket
                            $ticket->seats()->attach($seat->id);

                            // Updating event seat status
                            $previous_event_seat_status = $event_seat->status ?? 'available';
                            $event_seat = $seat_object::updateOrCreate(
                                [
                                    $column_id => $request->event_id,
                                    'seat_id' => $seat->id
                                ],
                                [
                                    'status' => 'on_hold_sale'
                                ]
                            );

                            // Generating qrs
                            $qrs = $order->qrPaths();

                            $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $row->ticket_type_id);
                            $e_s->increment('courtesy');
                            $e_s->increment('on_hold_sale');
                            if ($previous_event_seat_status == 'on_hold') {
                                // If it was on hold, we decrement it
                                $e_s->decrement('on_hold');
                            } else {
                                // Otherwise, we take it from available
                                $e_s->decrement('available');
                            }

                            // Dispatching email job
                            SendOrderEmail::dispatch($event->name, $order, $qrs);
                        } elseif ($event_seat->status == 'on_hold_sale') {
                            $error = true;
                            $error_messages .= "El asiento en la fila " . $row->name . " - " . $seat->name . " no se encuentra disponible y por lo tanto no puede ser registrado como cortesía<br />";
                        }
                    } catch (\Throwable $th) {
                        return [
                            'error' => true,
                            'message' => $th->getMessage()
                        ];
                    }
                }
            }

            // Updating the number of available seats
            $type = TicketType::find($row->ticket_type_id);
            $total = $object::total_tickets_by_type($type, $event);

            if ($request->type == 'event') {
                // $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $type->id);
                // $e_s->update(['available' => $e_s->total - ($total['on_hold'] + $total['on_hold_sale'])]);
            } else {
                // $event->ticket_types()->updateExistingPivot($type, ['sold' => $total['on_hold'] + $total['on_hold_sale']]);
            }

            if ($error) {
                return [
                    'error' => true,
                    'message' => $error_messages
                ];
            } else {
                return [
                    'error' => false,
                    'message' => 'Compra exitosa'
                ];
            }
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo completar la compra',
                'error_message' => $th->getMessage()
            ];
        }


        // Returning data
        return ['error' => false];
    }

    public function unblock_all($event_id, $zone_id, $type)
    {
        $object = ($type == 'event') ? new Event : new EventGroup;
        $seat_object = ($type == 'event') ? new EventSeat : new EventGroupSeat;
        $column_id = ($type == 'event') ? 'event_id' : 'event_group_id';

        $event = $object::find($event_id);
        $ticket_types_for_zone = ($type == 'event') ? $event->ticket_template->ticket_types()->where('type', 'seat')->where('zone_id', $zone_id)->get() : $event->ticket_types()->where('zone_id', $zone_id)->get();
        // dd($ticket_types_for_zone);

        // Fetching rows
        $rows = array();
        foreach ($ticket_types_for_zone as $ticket_type) {
            $rows = array_merge($rows, $ticket_type->rows->pluck('id')->toArray());
        }

        // All seats
        $seats = Seat::whereIn('row_id', $rows)->pluck('id');

        // Unblocking all seats not on_hold_sale
        $seat_object::where($column_id, $event_id)->whereIn('seat_id', $seats)->where('status', '!=', 'on_hold_sale')->delete();
        $available = $seat_object::where($column_id, $event_id)->whereIn('seat_id', $seats)->where('status', 'on_hold_sale')->count();

        // Updating the number of available seats
        // dd($ticket_type, $event);
        $total = $object::total_tickets_by_type($ticket_type, $event);

        if ($type == 'event') {
            $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket_type->id);
            $e_s->update([
                'available' => $e_s->total - ($total['on_hold'] + $total['on_hold_sale']),
                'on_hold' => 0,
            ]);
        } else {
            // $event->ticket_types()->updateExistingPivot($ticket_type, ['sold' => $total['on_hold'] + $total['on_hold_sale']]);
        }

        return [
            'error' => false
        ];
    }

    public function block_all($event_id, $zone_id, $type)
    {
        $object = ($type == 'event') ? new Event : new EventGroup;
        $seat_object = ($type == 'event') ? new EventSeat : new EventGroupSeat;
        $column_id = ($type == 'event') ? 'event_id' : 'event_group_id';

        $event = $object::find($event_id);
        $ticket_types_for_zone = ($type == 'event') ? $event->ticket_template->ticket_types()->where('type', 'seat')->where('zone_id', $zone_id)->get() : $event->ticket_types()->where('zone_id', $zone_id)->get();

        // Fetching rows
        $rows = array();
        foreach ($ticket_types_for_zone as $ticket_type) {
            $rows = array_merge($rows, $ticket_type->rows->pluck('id')->toArray());
        }

        // All seats
        $seats = Seat::whereIn('row_id', $rows)->pluck('id');

        // Updating seat to on_hold
        $amount = 0;
        foreach ($seats as $seat_id) {
            $event_seat = $seat_object::where($column_id, $event_id)->where('seat_id', $seat_id)->first();

            // Blocking seat if its not on_hold_sale
            if (is_null($event_seat) || $event_seat->status != 'on_hold_sale') {
                $seat_object::updateOrCreate([
                    $column_id => $event_id,
                    'seat_id' => $seat_id
                ], [
                    'status' => 'on_hold'
                ]);
                $amount++;
            }
        }

        // Updating the number of available seats
        $total = $object::total_tickets_by_type($ticket_type, $event);

        if ($type == 'event') {
            $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket_type->id);
            $e_s->update([
                'available' => 0,
                'on_hold' => $amount,
            ]);
        } else {
            $e_s = $event->ticket_types->where('id', $ticket_type->id)->first();
            $event->ticket_types()->updateExistingPivot($ticket_type, [
                'available' => 0,
                // 'sold' => $e_s->pivot->sold
            ]);
        }

        return [
            'error' => false
        ];
    }

    public function block_percentage($event_id, $zone_id, $percentage, $type)
    {
        // Unblocking all seats
        $this->unblock_all($event_id, $zone_id, $type);

        // Fetching data
        $object = ($type == 'event') ? new Event : new EventGroup;
        $seat_object = ($type == 'event') ? new EventSeat : new EventGroupSeat;
        $column_id = ($type == 'event') ? 'event_id' : 'event_group_id';

        $event = $object::find($event_id);
        $ticket_types_for_zone = ($type == 'event') ? $event->ticket_template->ticket_types()->where('type', 'seat')->where('zone_id', $zone_id)->get() : $event->ticket_types()->where('zone_id', $zone_id)->get();

        // Fetching rows
        $rows = array();
        foreach ($ticket_types_for_zone as $ticket_type) {
            $rows = array_merge($rows, $ticket_type->rows->pluck('id')->toArray());
        }

        // All seats
        $seats = Seat::whereIn('row_id', $rows)->pluck('id');

        // Updating seat to on_hold
        $i = $counter = 0;
        $percentage /= 10;
        foreach ($seats as $seat_id) {
            if ($i <= $percentage) {
                $event_seat = $seat_object::where($column_id, $event_id)->where('seat_id', $seat_id)->first();

                // Blocking seat if its not on_hold_sale
                if (is_null($event_seat) || $event_seat->status != 'on_hold_sale') {
                    $counter++;
                    $seat_object::updateOrCreate([
                        $column_id => $event_id,
                        'seat_id' => $seat_id
                    ], [
                        'status' => 'on_hold'
                    ]);
                }
            }

            if ($i == 10) {
                $i = 1;
            } else {
                $i++;
            }
        }


        // Updating the number of available seats
        if ($type == 'event') {
            $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket_type->id);
            $e_s->update([
                'available' => $e_s->total - $counter,
                'on_hold' => $counter
            ]);
        } else {
            // $event->ticket_types()->updateExistingPivot($ticket_type, ['sold' => $counter]);
        }

        return [
            'error' => false
        ];
    }

    public function block_pattern($event_id, $zone_id, $pattern, $type)
    {
        // Unblocking all seats
        $this->unblock_all($event_id, $zone_id, $type);

        // Fetching data
        $object = ($type == 'event') ? new Event : new EventGroup;
        $seat_object = ($type == 'event') ? new EventSeat : new EventGroupSeat;
        $column_id = ($type == 'event') ? 'event_id' : 'event_group_id';

        $event = $object::find($event_id);
        $ticket_types_for_zone = ($type == 'event') ? $event->ticket_template->ticket_types()->where('type', 'seat')->where('zone_id', $zone_id)->get() : $event->ticket_types()->where('zone_id', $zone_id)->get();

        // Processing pattern
        $pattern = explode('-', $pattern);
        $free = $pattern[0];
        $blocked = $pattern[1];
        $start = ($pattern[2] == 'l' ? 'left' : 'right');

        // Fetching data for ticket_type
        $amount = 0;
        foreach ($ticket_types_for_zone as $ticket_type) {
            $counter = 0;
            // Fetching rows
            foreach ($ticket_type->rows->pluck('id')->toArray() as $row_index => $row_id) {
                // All seats for row
                $seats = Seat::where('row_id', $row_id)->pluck('id')->toArray();

                // Checking if we start by the left or right side of the row
                if ($start == 'right') {
                    $seats = array_reverse($seats);
                }

                // Updating seat to on_hold
                // $i = 1;
                // $i = ($row_index % 2 == 0 ? 1 : 4);
                $mod = $row_index % 3;
                if ($mod == 0 || $mod == 1) {
                    $i = 0;
                } else {
                    $i = 2;
                }
                $j = 1;

                foreach ($seats as $seat_id) {
                    $event_seat = $seat_object::where($column_id, $event_id)->where('seat_id', $seat_id)->first();

                    if ($i <= $free) {
                        // Unblocking seat if its not on_hold_sale
                        if (is_null($event_seat) || $event_seat->status != 'on_hold_sale') {
                            if (!is_null($event_seat)) {
                                $event_seat->delete(); // Deleting record to make it available
                            }
                            $i++;
                        }
                    } elseif ($j <= $blocked) {
                        // Unblocking seat if its not on_hold_sale
                        if (is_null($event_seat) || $event_seat->status != 'on_hold_sale') {
                            $counter++;
                            $seat_object::updateOrCreate([
                                $column_id => $event_id,
                                'seat_id' => $seat_id
                            ], [
                                'status' => 'on_hold'
                            ]);
                            $j++;
                            $amount++;
                        }
                    } else {
                        $i = 2;
                        $j = 1;
                    }
                }
            }
        }

        // Updating the number of available seats
        if ($type == 'event') {
            $e_s = EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket_type->id);

            $e_s->update([
                'available' => $e_s->total - $counter,
                'on_hold' => $amount
            ]);
        } else {
            // $event->ticket_types()->updateExistingPivot($ticket_type, ['sold' => $counter]);
        }

        return [
            'error' => false
        ];
    }
}
