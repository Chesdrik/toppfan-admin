<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Order;
use App\Ticket;
use App\Sync;
use App\Event;
use App\Team;
use App\System;
use App\Accreditation;
use App\Access\Access;
use App\Access\Requirement;
use App\Access\AccessRole;
use Carbon\Carbon;
use File;
use Auth;
use DB;

class ImportController extends Controller
{

    public function index($system_slug)
    {
        return view('admin.imports.import', [
            'team' => Team::pluck('name', 'id'),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [$system_slug]), 'active' => false],
                ['label' => 'Importar', 'url' => '#', 'active' => false],
            ]
        ]);
    }

    // public function import(Request $request, $id) {
    public function import(Request $request, $system_slug)
    {
        set_time_limit(400);

        // System
        $system = System::bySlug($system_slug);

        // Verify file extension
        $extension = $request->file('file')->getClientOriginalExtension();
        if ($extension != 'tsv')
            return back()->with('error', 'El archivo debe tener extensión .tsv');

        // Parsing lines
        $lines = explode(PHP_EOL, File::get($request->file));
        array_shift($lines);
        // array_shift($lines);
        // array_shift($lines);
        // array_shift($lines);
        // dd($lines);


        foreach ($request->teams as $id) {
            $team = Team::find($id);

            foreach ($lines as $line) {
                // Parsing line
                $line = explode("\t", str_replace("\r", "", $line));
                $folio = $line[0];
                if ($folio) {
                    // if (Accreditation::where('folio', $folio)->where('system_id', $system->id,)->count() == 0) {
                    // Creating accreditation
                    $accreditation = Accreditation::updateOrCreate([
                        'system_id' => $system->id,
                        'type' => 'operative',
                        'folio' => $folio,
                    ], [
                        // 'ticket_type_id' => get_ticket_type($line[3]),
                        'ticket_type_id' => $line[3],
                        'name' => $line[1],
                        'price' => 0,
                        'type' => 'operative',
                        'version' => 1,
                    ]);

                    $accreditation->teams()->sync($team->id);

                    // Generating QR
                    $accreditation->verify_qr_existance();
                    // $accreditation->generate_qr($id);
                    // }
                }
            }
        }


        /*  } catch (\Throwable $th) {

            return back()->with('error', 'No fue posible importar el archivo');
        }*/

        return back()->with('success', 'Archivo importado correctamente');
    }


    public function import_access(Request $request)
    {
        // Delete everything before importing
        DB::statement("SET foreign_key_checks=0");
        Access::truncate();
        AccessRole::truncate();
        DB::table('access_requirement')->truncate();
        Requirement::truncate();
        DB::statement("SET foreign_key_checks=1");

        // Fetching and checking file extension
        $extension = $request->file('file')->getClientOriginalExtension();
        if ($extension != 'tsv')
            return back()->with('error', 'El archivo debe tener extensión .tsv');

        // Exploding lines
        $lines = explode(PHP_EOL, File::get($request->file));
        array_shift($lines);

        // Fetching Suzuki requirement
        $id_requirement = Requirement::create([
            'description' => 'Solicitar indentificación',
            'type' => 'revision',
        ]);
        $requirement = Requirement::create([ // Suzuki requirement
            'description' => 'Revisar placa',
            'type' => 'revision',
        ]);

        // Parsing lines from source TSV
        foreach ($lines as $line) {
            $line = explode("\t", $line);
            if (!empty($line[1])) {
                // Fetching access role or creating in case its a new one
                $access_role = AccessRole::where('name', ucfirst(mb_strtolower($line[2])))->first();
                if (is_null($access_role)) {
                    $access_role = AccessRole::create([
                        'name' => ucfirst(mb_strtolower($line[2])),
                    ]);
                }

                // Creating acces
                $access = Access::create([
                    'user_id' => Auth::user()->id,
                    'venue_id' => $request->venue_id,
                    'access_role_id' => $access_role->id,
                    'visitor_name' => ucwords(mb_strtolower($line[1])),
                    'start' => Carbon::now()->startOfDay(),
                    'end' =>  Carbon::now()->addYear()->endOfDay(),
                    'type' => 'car'
                ]);


                // Associating tiwth suzuki requirement
                $access->requirements()->attach([
                    $requirement->id => [
                        'request' => 'Revisar placas: ' . $line[3],
                        'data_type' => 'bool',
                    ]
                ]);
            }
        }

        // Returning to prev. screen
        return back();
    }
}
