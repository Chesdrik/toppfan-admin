<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Event;
use App\Ticket;
use App\Venue;
use App\TicketType;
use App\TicketTemplate;

use Response;
use Storage;

use Auth;

//use App\Notifications\courtesy;
use App\Mail\OrderConfirmed;
use App\Mail\CourtesyPDF;
use \PDF;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\System;

//Jobs
use App\Jobs\SendOrderConfirmation;

class OrderController extends Controller
{
    public function show($system_slug, $id)
    {
        $system = System::bySlug($system_slug);
        $order = Order::find($id);
        $order_stats = $order->access_percentage();
        $order->generateQRs();

        switch ($order->type) {
                // case 'regular':
                //     return view('admin.orders.show', compact('breadcrumb', 'order', 'order_stats'));
                //     break;
            case 'courtesy':
                $breadcrumb = [
                    ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                    ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                    ['label' => $order->event->name, 'url' => route('system.events.show', [session('system_slug'), $order->event->id]), 'active' => false],
                    ['label' => 'Cortesías', 'url' => route('system.events.courtesy.list', [session('system_slug'), $order->event->id]), 'active' => true],
                    ['label' => 'Detalle #' . $id, 'url' => route('system.orders.show', [session('system_slug'), $order->id]), 'active' => true],
                ];
                return view('admin.orders.show_courtesy', compact('breadcrumb', 'order', 'order_stats', 'system'));
                break;
            case 'season_ticket':
                $breadcrumb = [
                    ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                    ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                    ['label' => $order->event->name, 'url' => route('system.events.show', [session('system_slug'), $order->event->id]), 'active' => false],
                    ['label' => 'Abonos', 'url' => route('system.events.groups.orders', [session('system_slug'), $order->event->id]), 'active' => true],
                    ['label' => 'Detalle #' . $id, 'url' => route('system.orders.show', [session('system_slug'), $order->id]), 'active' => true],
                ];
                return view('admin.orders.show_seasson', compact('breadcrumb', 'order', 'order_stats', 'system'));
                break;
            default:
                $breadcrumb = [
                    ['label' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'active' => false],
                    ['label' => 'Eventos', 'url' => route('system.events.all', session('system_slug')), 'active' => false],
                    ['label' => $order->event->name, 'url' => route('system.events.show', [session('system_slug'), $order->event->id]), 'active' => false],
                    ['label' => 'Cortesías', 'url' => route('system.events.tickets.list', [session('system_slug'), $order->event->id]), 'active' => true],
                    ['label' => 'Detalle #' . $id, 'url' => route('system.orders.show', [session('system_slug'), $order->id]), 'active' => true],
                ];
                return view('admin.orders.show', compact('breadcrumb', 'order', 'order_stats'));
                break;
        }
    }

    // public function buyTickets($id)
    // {
    //     $event = Event::find($id);
    //     $breadcrumb = [
    //         ['label' => 'Eventos', 'url' => '/eventos/', 'active' => false],
    //         ['label' => $event->name, 'url' => '/eventos/' . $event->id, 'active' => false],
    //         ['label' => 'Comprar', 'url' => '/eventos/' . $event->id . '/comprar', 'active' => true],
    //     ];

    //     return view('admin.orders.create', compact('breadcrumb', 'event'));
    // }

    // public function createOrder(Request $request, $id)
    // {
    //     // Fetching event
    //     $event = Event::find($id);

    //     // \DB::statement("SET foreign_key_checks=0");
    //     // Ticket::truncate();
    //     // Order::truncate();
    //     // \DB::statement("SET foreign_key_checks=1");

    //     // Creating order
    //     $order = Order::create([
    //         'user_id' => Auth::user()->id,
    //         'event_id' => $event->id,
    //         'email' => $request->email,
    //         'total' => 0,
    //     ]);

    //     $total = 0;
    //     foreach ($request->tickets as $ticket) {
    //         $ticket_type = TicketType::find($ticket['ticket_type_id']);
    //         $total += ($ticket_type->price * $ticket['amount']);

    //         // Adding tickets to order
    //         $tickets = Ticket::create([
    //             'event_id' => $event->id,
    //             'ticket_type_id' => $ticket_type->id,
    //             'user_id' => Auth::user()->id,
    //             'order_id' => $order->id,
    //             'amount' => $ticket['amount']
    //         ]);
    //     }

    //     // Updating order total
    //     $order->total = $total;
    //     $order->save();

    //     // Generating QRs
    //     $qrs = $order->generateQRs();


    //     /*
    //     if(Auth::user()->type == 'ticket_office'){
    //         $step = 'resumen';
    //         $pdf = \PDF::loadView('checkout.ordenPDF', compact('order', 'step'));
    //         return $pdf->stream('TLU_orden_'.$order->id.'.pdf');

    //         }else{

    //         // Sending notification
    //         // Mail::to('diego@filloy.com.mx')->send(new OrderConfirmed($event->name, $order, $qrs));
    //     }
    //     */



    //     // Returning data to user
    //     return [
    //         'error' => false,
    //         'order_id' => $order->id,
    //     ];
    // }

    // public function pay_order(Request $request, $id)
    // {
    //     $event = Event::find($id);
    //     $template = TicketTemplate::find($event->ticket_template_id);
    //     $tickets = array();
    //     foreach (session('tickets') as $item) {
    //         $temp = array();

    //         foreach ($template->ticket_types as $ticket_type) {
    //             if ($ticket_type->id == $item['ticket_type_id']) {
    //                 $temp['ticket'] = $ticket_type;
    //                 $temp['total'] = $item['total'];
    //                 $tickets[] = $temp;
    //             }
    //         }
    //     }

    //     $order = Order::create([
    //         'user_id' => Auth::id(),
    //         'event_id' => $id,
    //         'email' => session('email')
    //     ]);

    //     $total = 0;
    //     foreach ($tickets as $data) {
    //         $total += ($data['ticket']->pivot->price * $data['total']);

    //         // Adding tickets to order
    //         $ticket = Ticket::create([
    //             'event_id' => $id,
    //             'ticket_type_id' => $data['ticket']->id,
    //             'user_id' => Auth::id(),
    //             'order_id' => $order->id,
    //             'amount' => $data['total']
    //         ]);
    //     }

    //     $order->update(['total' => $total]);

    //     return view('user.shopping.resumen', compact('ticket', 'event', 'order'));
    // }

    public function PDFresumen($system_slug, $id)
    {
        $order = Order::find($id);
        $event_title = $order->event->name;

        $qrs = $order->generateQRs();

        $step = 'resumen';
        $nombreArchivo = "resumen.pdf";

        $pdf = \PDF::loadView('user.tickets.orderPDF', compact('order', 'step', 'qrs', 'event_title'));

        return $pdf->stream('Boletos' . $order->id . '.pdf');
    }


    public function qrs($system_slug, $id, $order_id)
    {
        $event = Event::find($id);
        $order = Order::find($order_id);
        $order->generateQRs(true);

        return view('admin.orders.qrs', [
            'order' => $order,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => 'Eventos', 'url' => route('system.events.all', [session('system_slug')]), 'active' => false],
                // ['label' => $event->name, 'url' => route('system.events.show', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'QRs Orden', 'url' => '#', 'active' => true],
            ]
        ]);
    }


    public function courtesy_detail($system_slug, $id)
    {
        $order = Order::find($id);
        // $order->generateQRs();

        return view('admin.courtesies.resumen', [
            'order' => $order,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => 'Resumen', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    public function pdf($system_slug, $id)
    {
        // Fetching order data
        $order = Order::find($id);

        // Generating PDF
        $pdf = PDF::loadView('admin.orders.pdf.courtesy', compact('order'));

        // Returning PDF
        return $pdf->stream("Toppfan orden_{$order->id}.pdf");
    }

    // Este si se debe de quedar, ver donde se acomoda en el flujo
    // public function zip_qr(Event $event)
    // {
    //     $qrs = array();

    //     foreach (Order::forEvent($event->id) as $order) {
    //         $order_qrs = $order->generateQRs();
    //         // $qrs[] = $order_qrs;

    //         // foreach($order_qrs as $qr){
    //         foreach ($order->tickets as $ticket) {
    //             $zf = explode('-', $ticket->folio);
    //             $type_name = str_replace(' ', '_', mb_strtolower($ticket->ticket_type->name));
    //             $new_qr_path = '/public/orders/zip/' . $event->id . '/' . $zf[0] . '/' . str_pad($zf[1], 3, '0', STR_PAD_LEFT) . '.png';
    //             // dump('public/'.$ticket->qr_path(), $new_qr_path);

    //             Storage::copy('public/' . $ticket->qr_path(), $new_qr_path);
    //         }
    //     }

    //     // dd($qrs);
    // }

    public function resend_email($system_slug, $id){
        try {
            // Fetching order
            $order = Order::find($id);

            // Fetching email data
            $data = [
                'email' => $order->email,
                'tickets' => ($order->type == 'season_ticket' ) ? $order->seasson_tickets :  $order->tickets,
                'event_name' => $order->event->name,
                'date' => pretty_short_date($order->created_at),
                'order_id' => $order->id,
                'username' => $order->client_user->name,
                'type' => $order->type
            ];

            // Dispatching email job
            SendOrderConfirmation::dispatch($data);

            return back()->with('success', 'Email reenviado correctamente');

        } catch (\Throwable $th) {
            return back()->with('error', 'Algo salió mal, no fue posible reenviar el email.');
        }
    }

    public function update(Request $request, $system_slug, $id){
        try {
            // Fetching order
            $order = Order::find($id);
            
            // Updating order data
            $order->update(['email' => $request->email]);

            // Updating user data
            $order->client_user->update(['name' => $request->name, 'last_name' => $request->last_name, 'email' => $request->email]);
            
            return back()->with('success', 'Información actuaizada correctamente');

        } catch (\Throwable $th) {
            return back()->with('error', 'Algo salió mal, no fue posible editar la información de la orden.');
        }
    }
}
