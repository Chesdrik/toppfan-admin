<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Accreditation;

class PDAController extends Controller
{
    public function accreditations(Request $request)
    {
        $event = Event::find($request->event_id);
        $accreditations = array();

        foreach ($event->assigned_accreditations as $accreditation) {
            $accreditations[] = [
                'id' => $accreditation->id,
                'eventID' => $accreditation->pivot->event_id,
                'ticketTypeID' => $accreditation->ticket_type_id,
                'name' => $accreditation->pivot->name ?? $accreditation->name,
                'email' => $accreditation->pivot->email ?? $accreditation->email,
                'used' => $accreditation->pivot->used,
                'version' => $accreditation->version,
            ];
        }

        return \Response::json([
            'error' => false,
            'accreditations' => $accreditations,
        ]);
    }
}
