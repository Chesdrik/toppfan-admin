<?php

namespace App\Http\Controllers\POS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Order;
use App\OrderTemp;
use \Carbon\Carbon;

// use App\Ticket;
// use App\Event;
// use App\EventSeatAvailability;

// use Illuminate\Support\Facades\Mail;

class CheckoutController extends Controller
{
    // Process seat selection
    public function seat_selection(Request $request)
    {
        // Redirecting request processing
        return $this->checkoutController($request)->seat_selection($request);
    }

    // Register order
    public function register_order(Request $request)
    {
        // Redirecting request processing
        return $this->checkoutController($request)->register_order($request);
    }

    // Determine CheckoutController to use based on event type
    public function checkoutController(Request $request)
    {
        if ($request->event_type == 'event') {
            return new EventCheckoutController();
        } else {
            return new EventGroupCheckoutController();
        }
    }



    public function order_details($id)
    {
        // Fetching order
        $order = Order::find($id);

        return $order->API_POS();

        $tickets = array();
        foreach ($order->tickets as $key => $ticket) {
            $tickets[$key] = [
                'id' => $ticket->id,
                'ticket_type' => $ticket->ticket_type->name,
                'ticket_color' => $ticket->ticket_type->color,
                'amount' => $ticket->amount,
                'used' => $ticket->used,
                'qr_url' => $ticket->qr(),
            ];
        }

        // Returning data
        return [
            'error' => false,
            'order' => [
                'id' => $order->id,
                'event_id' => $order->event->id,
                'event_name' => $order->event->name,
                'event_date' => $order->event->date,
                'venue' => $order->event->venue->name,
                'total' => $order->total,
                'tickets' => $tickets
            ],
        ];
    }
    public function extend_expiration(Request $request)
    {
        $order_temp = OrderTemp::where('eventable_type', ($request->event_type == 'event' ? 'App\Event' : 'App\EventGroup'))
        ->where('eventable_id', $request->eventable_id)
        ->where('client_user_id', $request->client_user_id)
        ->first();
        
        $expiration_time = new Carbon($order_temp->expiration_time);

        \Log::info("New expiration time " . $expiration_time->format('Y-m-d H:i:s'));

        $order_temp->update([
            'expiration_time' => $expiration_time->addMinutes(10)->format('Y-m-d H:i:s')
        ]);
    }
}
