<?php

namespace App\Http\Controllers\POS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Event;
use App\Seat;
use App\EventSeat;
use App\EventSeatAvailability;
use App\Ticket;
use App\TicketType;
use App\Row;
use App\ClientUser;
use App\OrderTemp;
use App\Order;
use App\OrderLog;
use Faker\Factory as Faker;
use \Carbon\Carbon;
use App\Jobs\SendOrderEmail;

//Jobs
use App\Jobs\SendOrderConfirmation;

class EventCheckoutController extends Controller
{
    public function seat_selection(Request $request)
    {
        // \Log::info($request->all());

        // Fetching event and creating basic response
        $event = Event::find($request->event_id);

        $response_time = env('CHECKOUT_TIMEOUT') ?? 10;
        $response = [
            'error' => false,
            'event' => $event->API_POS(),
            'total' => 0,
            'time' => $response_time,
        ];
        $new_expiration_time = Carbon::now()->addMinutes($response_time)->format('Y-m-d H:i:s');

        // Creating temp user id
        $new_user = false;
        if (is_null($request->client_user_id)) {
            $faker = Faker::create();
            $client_user = ClientUser::create([
                'name' => 'Cliente',
                'last_name' => 'Temporal',
                'email' => time() . $faker->unique()->email,
                'password' => bcrypt($faker->password),
                'type' => 'temporary',
                'active' => 0
            ]);
            $new_user = true;
        } else {
            // Fetching existing ClientUser
            $client_user = ClientUser::find($request->client_user_id);
        }



        // Getting current user seat selection
        $current_temp_order = $client_user->order_temps_for_seat_selection();

        $return = array();
        $error_message = "";
        if (array_key_exists('tickets', $request->all())) {
            foreach ($request->tickets as $ticket) {
                // Available seats
                $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket['ticket_type_id']);

                // Checking if its a new ticket type
                if (array_key_exists($ticket['ticket_type_id'], $current_temp_order['general'])) {
                    $current_tickets_on_hold = $current_temp_order['general'][$ticket['ticket_type_id']] ?? 0;
                } else {
                    $current_tickets_on_hold = 0;
                }

                // Tickets to hold in diff (what we had - what we want)
                $diff_tickets_to_hold = $ticket['amount'] - $current_tickets_on_hold;


                if ($diff_tickets_to_hold == 0) {
                    // Same amount of tickets, so theres nothing to do, just removing it from array
                    unset($current_temp_order['general'][$ticket['ticket_type_id']]);

                    // Updating order temp expiration time
                    OrderTemp::updateOrderTempExpiration($client_user->id, $request->event_id, 'App\Event', $ticket['ticket_type_id'], $new_expiration_time);
                } else if ($diff_tickets_to_hold > 0) {

                    // Enough tickets in stock to hold them for user
                    if ($diff_tickets_to_hold <= $event_seat_availability->available) {
                        // Updating ticket_type_id stock
                        $event_seat_availability->decrement('available', abs($diff_tickets_to_hold));
                        $event_seat_availability->increment('on_hold_sale', abs($diff_tickets_to_hold));

                        // Updating order temp
                        OrderTemp::updateOrderTemp($client_user->id, $request->event_id, 'App\Event', $ticket['ticket_type_id'], $ticket['amount'], $new_expiration_time);
                    } else {
                        // Not enough tickets, give error
                        $ticket_type = TicketType::find($ticket['ticket_type_id']);

                        $response['error'] = true;
                        $error_message = 'Ya no hay disponibilidad de boletos para ' . $ticket_type->name;

                        // Updating order temp expiration time
                        OrderTemp::updateOrderTempExpiration($client_user->id, $request->event_id, 'App\Event', $ticket['ticket_type_id'], $new_expiration_time);
                    }

                    // Ticket amout updated, removing from array
                    unset($current_temp_order['general'][$ticket['ticket_type_id']]);
                } else if ($diff_tickets_to_hold < 0) {
                    // Updating ticket_type_id stock available and removing from on_hold_sale
                    $event_seat_availability->increment('available', abs($diff_tickets_to_hold));
                    $event_seat_availability->decrement('on_hold_sale', abs($diff_tickets_to_hold));

                    // Updating order temp
                    OrderTemp::updateOrderTemp($client_user->id, $request->event_id, 'App\Event', $ticket['ticket_type_id'], $ticket['amount'], $new_expiration_time);

                    // Ticket amout updated, removing from array
                    unset($current_temp_order['general'][$ticket['ticket_type_id']]);
                }
            }
        }

        // Tickets remove from order are reintegrated to stock
        if (!empty($current_temp_order['general'])) {
            $return['to_remove'] = $current_temp_order['general'];

            foreach ($current_temp_order['general'] as $ticket_type_id => $amount) {
                // Fetching seat availability
                $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket_type_id);

                // Updating ticket_type_id stock
                $event_seat_availability->increment('available', $amount);
                $event_seat_availability->decrement('on_hold_sale', $amount);

                // Removing from temporary order
                OrderTemp::where('client_user_id', $client_user->id)
                    ->where('eventable_id', $request->event_id)
                    ->where('eventable_type', 'App\Event')
                    ->where('ticket_type_id', $ticket_type_id)
                    ->delete();
            }
        }

        // Check tickets for seated areas
        $seated_tickets_array = array();
        $remove_tickets = array();
        if (array_key_exists('seating_tickets', $request->all())) {
            // $return['request_seating_tickets'] = $request->seating_tickets;

            foreach ($request->seating_tickets as $seat_key => $seat) {
                // Available seats
                $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($event->id, $seat['ticket_type_id']);

                // Parsing seat info
                $seat_id = explode("_", $seat['id']);
                $row_id = $seat_id[0];
                $seat_index = $seat_id[1];
                $seat_status = EventSeat::seatAvailabilityForEvent($event->id, $row_id, $seat_index);
                $seat_id = Seat::getSeatId($row_id, $seat_index);

                if ($seat_status['status'] == 'available') {
                    // Apartar asiento para usuario
                    EventSeat::setSeatAvailabilityForEvent($event->id, $seat_id, 'on_hold_sale');

                    // Updating ticket_type_id stock
                    $event_seat_availability->decrement('available', 1);
                    $event_seat_availability->increment('on_hold_sale', 1);

                    // Appending to user temporary order
                    OrderTemp::updateSeatedOrderTemp($client_user->id, $request->event_id, 'App\Event', $seat['ticket_type_id'], $seat_id, $new_expiration_time);

                    // Removing from array
                    if (array_key_exists($seat['ticket_type_id'], $current_temp_order['seated'])) {
                        $array_key = array_search($seat_id, $current_temp_order['seated'][$seat['ticket_type_id']]);
                        unset($current_temp_order['seated'][$seat['ticket_type_id']][$array_key]);
                    }
                } else {
                    // Fetching seat
                    $user_seat = OrderTemp::fetchUserSeatForEvent($client_user->id, $request->event_id, 'App\Event', $seat['ticket_type_id'], $seat_id);

                    if (is_null($user_seat)) {
                        // Not mine!
                        $response['error'] = true;

                        $ticket_type = TicketType::find($seat['ticket_type_id']);
                        $row = Row::find($row_id);
                        $error_message .= "El asiento de la zona " . $ticket_type->name . ", fila " . $row->name . ', asiento ' . $seat_index . ' ya no está disponible<br />';
                        $remove_tickets[] = [
                            'ticket_type_id' => $ticket_type->id,
                            'seat_id' => $seat['id'],
                            'row_id' => $row->id,
                            'seat_index' => $seat_index,
                        ];
                    } else {
                        // Updating expiration time
                        OrderTemp::updateSeatOrderTempExpiration($client_user->id, $request->event_id, 'App\Event', $seat['ticket_type_id'], $seat_id, $new_expiration_time);

                        // Removing from array
                        if (array_key_exists($seat['ticket_type_id'], $current_temp_order['seated'])) {
                            $array_key = array_search($seat_id, $current_temp_order['seated'][$seat['ticket_type_id']]);
                            unset($current_temp_order['seated'][$seat['ticket_type_id']][$array_key]);
                        }
                    }
                }
            }
        } else {
            // Delete every seated_ticket
            $seated_tickets = OrderTemp::where('client_user_id', $client_user->id)
                ->where('eventable_id', $request->event_id)
                ->where('eventable_type', 'App\Event')
                ->where('seat_id', '!=', null)
                ->get();

            foreach ($seated_tickets as $st) {
                $current_temp_order['seated'][$st->ticket_type_id][] = $st->seat_id;
            }
        }

        // Removing unused seats
        foreach ($current_temp_order['seated'] as $ticket_type_id => $ticket_type_seats) {
            // Event seat availability for ticket type
            $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket_type_id);

            foreach ($ticket_type_seats as $unused_seat) {
                // Reset seat status
                EventSeat::where('event_id', $event->id)->where('seat_id', $unused_seat)->delete();

                // Increment stock
                $event_seat_availability->increment('available', 1);
                $event_seat_availability->decrement('on_hold_sale', 1);

                // Remove OrderTemp
                OrderTemp::where('client_user_id', $client_user->id)
                    ->where('eventable_type', 'App\Event')
                    ->where('seat_id', '!=', null)
                    ->where('ticket_type_id', $ticket_type_id)
                    ->where('seat_id', $unused_seat)
                    ->delete();
            }
        }

        // Returning error if not all seats are available
        if ($response['error']) {
            return [
                'error' => true,
                'error_message' => $error_message,
                'new_user' => $new_user,
                'new_client_user_id' => ($new_user ? $client_user->id : null),
                'remove_tickets' => $remove_tickets,
            ];
        }



        // Adding seats to orderTemp
        $response['type'] = 'event';
        $response['seated_tickets'] = array();
        foreach ($seated_tickets_array as $ticket_type_id => $seats) {
            $ticket_type = TicketType::find($ticket_type_id);

            foreach ($seats as $seat_id) {

                // Creating temp order
                $temp = OrderTemp::updateOrCreate([
                    'ticket_type_id' => $ticket_type->id,
                    'client_user_id' => $client_user->id,
                    'eventable_id' => $event->id,
                    'eventable_type' => 'App\Event',
                    'seat_id' => $seat_id
                ], [
                    'amount' => 1,
                    'expiration_time' => $response_time
                ]);

                // Appending ticket to response
                $price = $event->get_ticket($ticket_type->id)->price;
                $selected_seat = Seat::find($seat_id);

                // Appending ticket data to return variable
                $response['seated_tickets'][] = [
                    'ticket_type_id' => $ticket_type->id,
                    'name' => $ticket_type->name,
                    'seat_name' => $selected_seat->full_name(),
                    'seat_id' => $seat_id,
                    'amount' => 1,
                    'price' => $price,
                    'subtotal' => $price,
                    'row_id' => $selected_seat->row_id,
                    'seat_index' => (int)$selected_seat->name,
                    'zone_id' => $ticket_type->zone_id,
                    'parent_zone_id' => $ticket_type->zone->parent_zone,
                ];

                // Change seat status to on_hold_sale
                EventSeat::setSeatAvailabilityForEvent($event->id, $seat_id, 'on_hold_sale');

                // Updating available tickets on table
                EventSeatAvailability::where('event_id', $event->id)->where('ticket_type_id', $ticket_type->id)->decrement('available');

                // Updating total
                $response['total'] += $price;
            }
        }

        // Returning response
        ($client_user->type == 'temporary') ? $response['temporary_user_id'] = $client_user->id : $response['user_id'] = $client_user->id;
        $response['error'] = false;

        $response['new_user'] = $new_user;
        if ($new_user) {
            $response['new_client_user_id'] = $client_user->id;
        }

        \Log::info($response);
        return $response;
    }

    public function register_order(Request $request)
    {
        \Log::info($request->all());

        $event = Event::find($request->event_id);
        $ticket_types = $event->ticket_template->ticket_types_simple_array();

        // Client user who owns the tickets
        $client_user = ClientUser::find($request->client_user_id);
        // Check if user with  $request->contact_info['email'] exists, we append the order to it


        if (ClientUser::clientUserExists($request->contact_info['email'], $client_user->id)) {
            // Existe un usuario con ese email que no es $client_user->id
            $order_user = ClientUser::byEmail($request->contact_info['email']);
            $order_user->update([
                'name' => $request->contact_info['name'],
                'last_name' => $request->contact_info['last_name'],
                'phone' => $request->contact_info['phone']
            ]);

            $existing_user = true;
        } else {
            // Updating temporary user with data
            $client_user->update([
                'name' => $request->contact_info['name'],
                'last_name' => $request->contact_info['last_name'],
                'email' => $request->contact_info['email'],
                'phone' => $request->contact_info['phone']
            ]);
            $order_user = $client_user;
            $existing_user = false;
        }

        // Creating order
        $order = Order::create([
            'client_user_id' => $order_user->id,
            'email' => $order_user->email,
            'event_type' => 'App\Event',
            'total' => $request->order_info['total'],
            'event_id' => $event->id,
            'type' => 'regular',
            // 'payment_method' => ($order_user->type == 'ticket_office') ? 'cash' : 'debit_card',
            'payment_method' => 'credit_card',
            'status' => 'registered',
            'selling_point' => ($order_user->type == 'client') ? 'web' : 'ticket_office'
        ]);

        // Registering log
        $order->logs()->save(new OrderLog([
            'status' => 'success',
            'request' => $request->logs['request'],
            'response' => $request->logs['response'],
        ]));

        // Fetching temp order tickets
        $tickets_to_buy = OrderTemp::where('client_user_id', $client_user->id)
            ->where('eventable_id', $event->id)
            ->where('eventable_type', 'App\Event')
            ->where('type', 'ticket')
            ->get();

        // Associating tickets
        $total = 0;
        foreach ($tickets_to_buy as $temp_ticket) {
            // Adding subtototal
            $subtotal = $ticket_types[$temp_ticket->ticket_type_id]['price'] * $temp_ticket->amount;
            $total += $subtotal;

            // Creating ticket for order
            for ($k = 1; $k <= $temp_ticket->amount; $k++) {
                $new_ticket = Ticket::create([
                    'event_id' => $event->id,
                    'ticket_type_id' => $temp_ticket->ticket_type_id,
                    'order_id' => $order->id,
                    'amount' => 1,
                    'used' => 0,
                    'price' => $ticket_types[$temp_ticket->ticket_type_id]['price'],
                    'folio' => "OR-" . str_pad($order->id, 5, "0", STR_PAD_LEFT) . $k . "N",
                    'event_type' => 'App\Event',
                    'type' => 'ticket',
                    'name' => $order_user->name,
                ]);
            }

            // Updating on_hold_sale and sold variables
            $event_seat_availability = EventSeatAvailability::where('ticket_type_id', $temp_ticket->ticket_type_id)->where('event_id', $event->id)->first();
            $event_seat_availability->decrement('on_hold_sale', $temp_ticket->amount);
            $event_seat_availability->increment('sold', $temp_ticket->amount);

            // Appending seat to ticket
            if (isset($temp_ticket->seat_id) && !is_null($temp_ticket->seat_id)) {
                $new_ticket->seats()->attach($temp_ticket->seat_id);
            }
        }

        // Validating if total is ok
        $tampered = false;
        if ($request->order_info['total'] != $total) {
            \Log::emergency("Orden {$order->id} alterada. Request {$request->order_info['total']} != {$total}");
            // $tampered = true;
        }

        // Deleting temporary order
        OrderTemp::where('client_user_id', $client_user->id)
            ->where('eventable_id', $event->id)
            ->where('eventable_type', 'App\Event')
            ->where('type', 'ticket')
            ->delete();

        // If the contact info had an existing user, we destroy the temporary user
        if ($existing_user) {
            // Destroy data for temp user, only good for transaction
            $client_user->delete();
        }

        // Updating seat availability
        // \Artisan::call('count:seats ' . $event->id);

        // Checking if order was not tampered in the process
        if (!$tampered) {

            // Fetching email data
            $data = [
                'email' => $order->email,
                'tickets' => $order->tickets,
                'event_name' => $order->event->name,
                'date' => pretty_short_date($order->created_at),
                'order_id' => $order->id,
                'username' => $order->client_user->name,
                'type' => $order->type
            ];

            // Dispatching email job
            SendOrderConfirmation::dispatch($data);

            // Returning data
            return [
                'error' => false,
                'order_id' => $order->id,
            ];
        } else {
            // Returning data
            return [
                'error' => false,
                'tampered' => true,
            ];
        }
    }
}
