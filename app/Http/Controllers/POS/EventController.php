<?php

namespace App\Http\Controllers\POS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Event;
use App\EventSeatAvailability;

class EventController extends Controller
{
    public function events(Request $request)
    {
        // Returning data
        return [
            'error' => false,
            'events' => Event::upcoming_on_sale_events(),
        ];
    }

    public function eventsSlideshow(Request $request)
    {
        // Returning data
        return [
            'error' => false,
            'slideshows' => Event::upcoming_on_sale_events_slideshow(),
        ];
    }

    public function event(Request $request)
    {
        // Event
        $ev = Event::find($request->event_id);

        // Checking if event is still on sale
        if (!$ev->on_sale) {
            // Returning data
            return [
                'error' => true,
                'error_message' => 'Este evento ya no se encuentra disponible',
            ];
        }

        // Compiling event info
        $ticket_types = array();

        foreach ($ev->ticket_template->ticket_types as $type) {
            if ($type->active == 1) {
                $seats = EventSeatAvailability::seatsForEventAndTicket($ev->id, $type->id);

                if ($seats->available > 0) {
                    // Grouping by zones in Villahermosa stadium
                    $zone = $type->zone->API();
                    $ticket_types[$zone['parent_zone']['id']]['tickets'][$type->id] = [
                        'id' => $type->id,
                        'name' => $type->name,
                        'color' => '#' . $type->color,
                        'zone' => $zone,
                        'price' => number_format($type->pivot->price, 2, '.', ','),
                        'active' => $type->active,
                        'available' => $seats->available,
                    ];

                    $ticket_types[$zone['parent_zone']['id']]['type'] = $type->type;
                    $ticket_types[$zone['parent_zone']['id']]['name'] = $zone['parent_zone']['name']; // TODO Fetch zones by groups, because this is a shitty solution
                    $ticket_types[$zone['parent_zone']['id']]['color'] = '#' . $type->color; // TODO define zone color, because this is a shitty solution
                }
            }
        }

        // Returning data
        return [
            'error' => false,
            'event' => [
                'id' => $ev->id,
                'event' => $ev->name,
                'event_date' => $ev->date,
                'active' => $ev->active,
                'on_sale' => $ev->on_sale,
                'id_venue' => $ev->venue->id,
                'venue' => $ev->venue->name,
            ],
            'ticket_types' => $ticket_types,
            // 'stadium_configuration' => $ev->venue->stadiumConfiguration()
        ];
    }
}
