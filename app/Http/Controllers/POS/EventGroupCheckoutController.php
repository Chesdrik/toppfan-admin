<?php

namespace App\Http\Controllers\POS;

use App\Accreditation;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\EventGroup;
use App\Seat;
use App\EventGroupSeat;
use App\TicketType;
use App\Ticket;
// use App\Accreditation;
use App\Row;
use App\ClientUser;
use App\OrderTemp;
use App\Order;
use App\OrderLog;

//Jobs
use App\Jobs\SendOrderConfirmation;


use Faker\Factory as Faker;
use \Carbon\Carbon;

class EventGroupCheckoutController extends Controller
{
    public function seat_selection(Request $request)
    {
        // Fetching event and creating basic response
        $event_group = EventGroup::find($request->event_id);

        $response_time = env('CHECKOUT_TIMEOUT') ?? 10;
        $response = [
            'error' => false,
            'event' => $event_group->API_POS(),
            'total' => 0,
            'time' => $response_time,
        ];
        $new_expiration_time = Carbon::now()->addMinutes($response_time)->format('Y-m-d H:i:s');

        // Creating temp user id
        $new_user = false;
        if (is_null($request->client_user_id)) {
            $faker = Faker::create();
            $client_user = ClientUser::create([
                'name' => 'Cliente',
                'last_name' => 'Temporal',
                'email' => $faker->unique()->email,
                'password' => bcrypt($faker->password),
                'type' => 'temporary',
                'active' => 0
            ]);
            $new_user = true;
        } else {
            // Fetching existing ClientUser
            $client_user = ClientUser::find($request->client_user_id);
        }


        // Getting current user seat selection
        $current_temp_order = $client_user->order_temps_for_seat_selection();

        $return = array();
        $error_message = "";
        if (array_key_exists('tickets', $request->all())) {
            foreach ($request->tickets as $ticket) {
                // Available seats
                // $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($event_group->id, $ticket['ticket_type_id']);
                $egtt = $event_group->get_ticket($ticket['ticket_type_id']);


                // Checking if its a new ticket type
                if (array_key_exists($ticket['ticket_type_id'], $current_temp_order['general'])) {
                    $current_tickets_on_hold = $current_temp_order['general'][$ticket['ticket_type_id']] ?? 0;
                } else {
                    $current_tickets_on_hold = 0;
                }

                // Tickets to hold in diff (what we had - what we want)
                $diff_tickets_to_hold = $ticket['amount'] - $current_tickets_on_hold;

                \Log::info('Available: ' . $egtt->pivot->available);
                \Log::info('diff_tickets_to_hold: ' . $diff_tickets_to_hold);

                if ($diff_tickets_to_hold == 0) {
                    // Same amount of tickets, so theres nothing to do, just removing it from array
                    unset($current_temp_order['general'][$ticket['ticket_type_id']]);

                    // Updating order temp expiration time
                    \Log::info([$client_user->id, $request->event_id, $ticket['ticket_type_id'], $new_expiration_time]);
                    OrderTemp::updateOrderTempExpiration($client_user->id, $request->event_id, 'App\EventGroup', $ticket['ticket_type_id'], $new_expiration_time);
                } else if ($diff_tickets_to_hold > 0) {

                    // Enough tickets in stock to hold them for user
                    if ($diff_tickets_to_hold <= $egtt->pivot->available) {
                        // Updating ticket_type_id stock
                        // $egtt->decrement('available', abs($diff_tickets_to_hold));
                        // $egtt->increment('on_hold_sale', abs($diff_tickets_to_hold));
                        $egtt->pivot->decrement('available', abs($diff_tickets_to_hold));
                        $egtt->pivot->increment('on_hold_sale', abs($diff_tickets_to_hold));

                        // Updating order temp
                        OrderTemp::updateOrderTemp($client_user->id, $request->event_id, 'App\EventGroup', $ticket['ticket_type_id'], $ticket['amount'], $new_expiration_time);
                    } else {
                        // Not enough tickets, give error
                        $ticket_type = TicketType::find($ticket['ticket_type_id']);

                        $response['error'] = true;
                        $error_message = 'Ya no hay disponibilidad de boletos para ' . $ticket_type->name;

                        // Updating order temp expiration time
                        \Log::info([$client_user->id, $request->event_id, $ticket['ticket_type_id'], $new_expiration_time]);
                        OrderTemp::updateOrderTempExpiration($client_user->id, $request->event_id, 'App\EventGroup', $ticket['ticket_type_id'], $new_expiration_time);
                    }

                    // Ticket amout updated, removing from array
                    unset($current_temp_order['general'][$ticket['ticket_type_id']]);
                } else if ($diff_tickets_to_hold < 0) {
                    // Updating ticket_type_id stock available and removing from on_hold_sale
                    $egtt->pivot->increment('available', abs($diff_tickets_to_hold));
                    $egtt->pivot->decrement('on_hold_sale', abs($diff_tickets_to_hold));

                    // Updating order temp
                    OrderTemp::updateOrderTemp($client_user->id, $request->event_id, 'App\EventGroup', $ticket['ticket_type_id'], $ticket['amount'], $new_expiration_time);

                    // Ticket amout updated, removing from array
                    unset($current_temp_order['general'][$ticket['ticket_type_id']]);
                }
            }
        }

        // Tickets remove from order are reintegrated to stock
        if (!empty($current_temp_order['general'])) {
            $return['to_remove'] = $current_temp_order['general'];

            foreach ($current_temp_order['general'] as $ticket_type_id => $amount) {
                // Fetching seat availability
                // $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($event_group->id, $ticket_type_id);
                $egtt = $event_group->get_ticket($ticket_type_id);

                // Updating ticket_type_id stock
                $egtt->pivot->increment('available', $amount);
                $egtt->pivot->decrement('on_hold_sale', $amount);

                // Removing from temporary order
                OrderTemp::where('client_user_id', $client_user->id)
                    ->where('eventable_id', $request->event_id)
                    ->where('eventable_type', 'App\EventGroup')
                    ->where('ticket_type_id', $ticket_type_id)
                    ->delete();
            }
        }

        // Check tickets for seated areas
        $seated_tickets_array = array();
        $remove_tickets = array();
        if (array_key_exists('seating_tickets', $request->all())) {
            // $return['request_seating_tickets'] = $request->seating_tickets;

            foreach ($request->seating_tickets as $seat_key => $seat) {
                // Available seats
                // $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($event_group->id, $seat['ticket_type_id']);
                $egtt = $event_group->get_ticket($seat['ticket_type_id']);

                // Parsing seat info
                $seat_id = explode("_", $seat['id']);
                $row_id = $seat_id[0];
                $seat_index = $seat_id[1];
                $seat_status = EventGroupSeat::seatAvailabilityForEventGroup($event_group->id, $row_id, $seat_index);
                $seat_id = Seat::getSeatId($row_id, $seat_index);



                if ($seat_status['status'] == 'available') {
                    // Apartar asiento para usuario
                    EventGroupSeat::setSeatAvailabilityForEventGroup($event_group->id, $seat_id, 'on_hold_sale');

                    // Updating ticket_type_id stock
                    $egtt->pivot->decrement('available', 1);
                    $egtt->pivot->increment('on_hold_sale', 1);

                    // Appending to user temporary order
                    OrderTemp::updateSeatedOrderTemp($client_user->id, $request->event_id, 'App\EventGroup', $seat['ticket_type_id'], $seat_id, $new_expiration_time);

                    // Removing from array
                    if (array_key_exists($seat['ticket_type_id'], $current_temp_order['seated'])) {
                        $array_key = array_search($seat_id, $current_temp_order['seated'][$seat['ticket_type_id']]);
                        unset($current_temp_order['seated'][$seat['ticket_type_id']][$array_key]);
                    }
                } else {
                    // Fetching seat
                    $user_seat = OrderTemp::fetchUserSeatForEvent($client_user->id, $request->event_id, 'App\EventGroup', $seat['ticket_type_id'], $seat_id);

                    if (is_null($user_seat)) {
                        // Not mine!
                        $response['error'] = true;

                        $ticket_type = TicketType::find($seat['ticket_type_id']);
                        $row = Row::find($row_id);
                        $error_message .= "El asiento de la zona " . $ticket_type->name . ", fila " . $row->name . ', asiento ' . $seat_index . ' ya no está disponible<br />';
                        $remove_tickets[] = [
                            'ticket_type_id' => $ticket_type->id,
                            'seat_id' => $seat['id'],
                            'row_id' => $row->id,
                            'seat_index' => $seat_index,
                        ];
                    } else {
                        // Updating expiration time
                        OrderTemp::updateSeatOrderTempExpiration($client_user->id, $request->event_id, 'App\EventGroup', $seat['ticket_type_id'], $seat_id, $new_expiration_time);

                        // Removing from array
                        if (array_key_exists($seat['ticket_type_id'], $current_temp_order['seated'])) {
                            $array_key = array_search($seat_id, $current_temp_order['seated'][$seat['ticket_type_id']]);
                            unset($current_temp_order['seated'][$seat['ticket_type_id']][$array_key]);
                        }
                    }
                }
            }
        } else {
            // Delete every seated_ticket
            $seated_tickets = OrderTemp::where('client_user_id', $client_user->id)
                ->where('eventable_id', $request->event_id)
                ->where('eventable_type', 'App\EventGroup')
                ->where('seat_id', '!=', null)
                ->get();

            foreach ($seated_tickets as $st) {
                $current_temp_order['seated'][$st->ticket_type_id][] = $st->seat_id;
            }
        }

        // Removing unused seats
        foreach ($current_temp_order['seated'] as $ticket_type_id => $ticket_type_seats) {
            // Event seat availability for ticket type
            // $event_seat_availability = EventSeatAvailability::seatsForEventAndTicket($event_group->id, $seat['ticket_type_id']);
            $egtt = $event_group->get_ticket($ticket_type_id);

            foreach ($ticket_type_seats as $unused_seat) {
                // Reset seat status
                EventGroupSeat::where('event_group_id', $event_group->id)->where('seat_id', $unused_seat)->delete();

                // Increment stock
                $egtt->pivot->increment('available', 1);
                $egtt->pivot->decrement('on_hold_sale', 1);

                // Remove OrderTemp
                OrderTemp::where('client_user_id', $client_user->id)
                    ->where('ticket_type_id', $ticket_type_id)
                    ->where('eventable_id', $request->event_id)
                    ->where('eventable_type', 'App\EventGroup')
                    ->where('seat_id', $unused_seat)
                    ->delete();
            }
        }

        // Returning error if not all seats are available
        if ($response['error']) {
            return [
                'error' => true,
                'error_message' => $error_message,
                'new_user' => $new_user,
                'new_client_user_id' => ($new_user ? $client_user->id : null),
                'remove_tickets' => $remove_tickets,
            ];
        }



        // Adding seats to orderTemp
        $response['type'] = 'event';
        $response['seated_tickets'] = array();
        foreach ($seated_tickets_array as $ticket_type_id => $seats) {
            $ticket_type = TicketType::find($ticket_type_id);

            foreach ($seats as $seat_id) {

                // Creating temp order
                $temp = OrderTemp::updateOrCreate([
                    'ticket_type_id' => $ticket_type->id,
                    'client_user_id' => $client_user->id,
                    'eventable_id' => $event_group->id,
                    'eventable_type' => 'App\EventGroup',
                    'seat_id' => $seat_id
                ], [
                    'amount' => 1,
                    'expiration_time' => $response_time
                ]);

                // Appending ticket to response
                $price = $event_group->get_ticket($ticket_type->id)->price;
                $selected_seat = Seat::find($seat_id);

                // Appending ticket data to return variable
                $response['seated_tickets'][] = [
                    'ticket_type_id' => $ticket_type->id,
                    'name' => $ticket_type->name,
                    'seat_name' => $selected_seat->full_name(),
                    'seat_id' => $seat_id,
                    'amount' => 1,
                    'price' => $price,
                    'subtotal' => $price,
                    'row_id' => $selected_seat->row_id,
                    'seat_index' => (int)$selected_seat->name,
                    'zone_id' => $ticket_type->zone_id,
                    'parent_zone_id' => $ticket_type->zone->parent_zone,
                ];

                // Change seat status to on_hold_sale
                EventGroupSeat::setSeatAvailabilityForEventGroup($event_group->id, $seat_id, 'on_hold_sale');

                // Updating available tickets on table
                $event_group->get_ticket($ticket_type->id)->pivot->decrement('available');

                // Updating total
                $response['total'] += $price;
            }
        }

        // Returning response
        ($client_user->type == 'temporary') ? $response['temporary_user_id'] = $client_user->id : $response['user_id'] = $client_user->id;
        $response['error'] = false;

        $response['new_user'] = $new_user;
        if ($new_user) {
            $response['new_client_user_id'] = $client_user->id;
        }

        \Log::info($response);
        return $response;
    }

    // 'App\EventGroup'

    public function register_order(Request $request)
    {
        \Log::info($request->all());

        // Fetching event group
        $event_group = EventGroup::find($request->event_id);

        // Client user who owns the tickets
        $client_user = ClientUser::find($request->client_user_id);

        if (ClientUser::clientUserExists($request->contact_info['email'], $client_user->id)) {
            // Existe un usuario con ese email que no es $client_user->id
            $order_user = ClientUser::byEmail($request->contact_info['email']);
            $order_user->update([
                'name' => $request->contact_info['name'],
                'last_name' => $request->contact_info['last_name'],
                'phone' => $request->contact_info['phone']
            ]);

            $existing_user = true;
        } else {
            \Log::info("User with mail {$request->contact_info['email']} created");
            // Updating temporary user with data
            $client_user->update([
                'name' => $request->contact_info['name'],
                'last_name' => $request->contact_info['last_name'],
                'email' => $request->contact_info['email'],
                'phone' => $request->contact_info['phone']
            ]);
            $order_user = $client_user;
            $existing_user = false;
        }


        // Creating order
        $order = Order::create([
            'client_user_id' => $order_user->id,
            'email' => $order_user->email,
            'event_type' => 'App\EventGroup',
            'total' => $request->order_info['total'],
            'event_id' => $event_group->id,
            'type' => 'season_ticket',
            // 'payment_method' => ($order_user->type == 'ticket_office') ? 'cash' : 'debit_card',
            'payment_method' => 'credit_card',
            'status' => 'registered',
            'selling_point' => ($order_user->type == 'client') ? 'web' : 'ticket_office',
            'status' => 'registered'
        ]);

        // Registering log
        $order->logs()->save(new OrderLog([
            'status' => 'success',
            'request' => $request->logs['request'],
            'response' => $request->logs['response'],
        ]));

        // Fetching temp order tickets
        $tickets_to_buy = OrderTemp::where('client_user_id', $client_user->id)
            ->where('eventable_id', $event_group->id)
            ->where('eventable_type', 'App\EventGroup')
            ->where('type', 'ticket')
            ->get();

        // Associating tickets
        $total = 0;
        // $folio = Accreditation::where('type', 'season_ticket')->get()->last();
        foreach ($tickets_to_buy as $temp_ticket) {
            // Adding subtototal
            // $subtotal = $ticket_types[$temp_ticket->ticket_type_id]['price'] * $temp_ticket->amount;
            $egt = $event_group->get_ticket($temp_ticket->ticket_type_id);
            $tt = TicketType::find($temp_ticket->ticket_type_id);
            $total += $egt->pivot->price * $temp_ticket->amount;

            // \Log::info($tt->type);
            // \Log::info($request->seasson_tickets_info);

            if ($tt->type == 'general') {
                $acc_name = $request->seasson_tickets_info['general'][$tt->id]['name'];
                $acc_last_names = $request->seasson_tickets_info['general'][$tt->id]['last_names'];
                $acc_email = $request->seasson_tickets_info['general'][$tt->id]['email'];
                $acc_phone = $request->seasson_tickets_info['general'][$tt->id]['phone'];
                $seat_id = null;
            } else {
                $seat = Seat::find($temp_ticket->seat_id);
                $seat_id = $seat->id;

                $acc_name = $request->seasson_tickets_info['seated'][$seat->seat_key()]['name'];
                $acc_last_names = $request->seasson_tickets_info['seated'][$seat->seat_key()]['last_names'];
                $acc_email = $request->seasson_tickets_info['seated'][$seat->seat_key()]['email'];
                $acc_phone = $request->seasson_tickets_info['seated'][$seat->seat_key()]['phone'];
            }

            $accreditation = Accreditation::create([
                'system_id' => $event_group->system_id,
                'order_id' => $order->id,
                'seat_id' => $seat_id,
                'ticket_type_id' => $temp_ticket->ticket_type_id,
                'folio' => 'AB-' . str_pad($order->id, 6, "0", STR_PAD_LEFT),
                'name' => $acc_name,
                'last_names' => $acc_last_names,
                'email' => $acc_email,
                'phone' => $acc_phone,
                'type' => 'season_ticket',
                'price' => $egt->pivot->price,
            ]);

            // Updating on_hold_sale and sold variables
            $egt->pivot->decrement('on_hold_sale', $temp_ticket->amount);
            $egt->pivot->increment('sold', $temp_ticket->amount);
        }

        // Validating if total is ok
        $tampered = false;
        if ($request->order_info['total'] != $total) {
            \Log::emergency("Orden {$order->id} alterada. Request {$request->order_info['total']} != {$total}");
            // $tampered = true;
        }

        // Deleting temporary order
        OrderTemp::where('client_user_id', $client_user->id)
            ->where('eventable_id', $event_group->id)
            ->where('eventable_type', 'App\EventGroup')
            ->where('type', 'ticket')
            ->delete();

        // If the contact info had an existing user, we destroy the temporary user
        if ($existing_user) {
            // Destroy data for temp user, only good for transaction
            $client_user->delete();
        }

        // Updating seat availability
        // \Artisan::call('count:seats ' . $event->id);

        // Checking if order was not tampered in the process
        if (!$tampered) {
            // Fetching email data
            $data = [
                'email' => $order->email,
                'tickets' => $order->tickets,
                'event_name' => $order->event->name,
                'date' => pretty_short_date($order->created_at),
                'order_id' => $order->id,
                'username' => $order->client_user->name
            ];

            // Dispatching email job
            SendOrderConfirmation::dispatch($data);

            // Returning data
            return [
                'error' => false,
                'order_id' => $order->id,
            ];
        } else {
            // Returning data
            return [
                'error' => false,
                'tampered' => true,
            ];
        }
    }

    // public function seat_selection(Request $request)
    // {
    //     try {
    //         \Log::info($request->all());
    //         // Fetching event and creating basic response
    //         $event = EventGroup::where('slug', $request->slug)->first();

    //         $response = ['error' => false, 'event' => $event->API_POS(), 'total' => 0];

    //         // Check tickets for seated areas∂
    //         $seated_tickets_array = array();
    //         foreach ($request->seating_tickets as $ticket_type_id => $seating_tickets) {
    //             $ticket_type = TicketType::find($ticket_type_id);

    //             $error_message = '';
    //             foreach ($seating_tickets as $row_id => $seats) {
    //                 foreach ($seats as $seat_index) {

    //                     $seat_status = EventGroupSeat::seatAvailabilityForEventGroup($event->id, $row_id, $seat_index);

    //                     \Log::info($seat_status);
    //                     if (!is_null($seat_status)) {
    //                         $temp_seat = OrderTemp::where('client_user_id', $request->client_user_id)->where('event_id', $event->id)->where('seat_id', $seat_status['seat_id'])->where('type', 'season_ticket')->first();
    //                     } else {
    //                         $temp_seat = null;
    //                     }

    //                     if (!is_null($seat_status) && $seat_status['status'] != 'available' && is_null($temp_seat)) {
    //                         $response['error'] = true;
    //                         $row = Row::find($row_id);
    //                         $error_message .= "El asiento de la zona " . $ticket_type->name . ", fila " . $row->name . ', asiento ' . $seat_index . ' ya no está disponible<br />';
    //                     } else {
    //                         // Appending seat_index to array
    //                         $seated_tickets_array[$ticket_type_id][] = $seat_status['seat_id'];
    //                     }
    //                 }
    //             }
    //         }

    //         // Returning error if not all seats are available
    //         if ($response['error']) {
    //             return [
    //                 'error' => true,
    //                 'error_message' => $error_message
    //             ];
    //         }

    //         $expiration_time = Carbon::now()->addMinutes(10);
    //         $response['time'] = (env('CHECKOUT_TIMEOUT')) ? env('CHECKOUT_TIMEOUT') : 10;
    //         $response['tickets'] = array();
    //         $response['type'] = 'event_group';

    //         // Creating temporary user
    //         if (!$request->client_user_id) {
    //             $faker = Faker::create();
    //             $client_user = ClientUser::create([
    //                 'name' => $faker->name,
    //                 'email' => $faker->unique()->email,
    //                 'password' => bcrypt($faker->password),
    //                 'type' => 'temporary',
    //                 'active' => 0
    //             ]);
    //         } else {
    //             $client_user = ClientUser::find($request->client_user_id);
    //         }

    //         // Adding seats to orderTemp
    //         $response['seated_tickets'] = array();
    //         foreach ($seated_tickets_array as $ticket_type_id => $seats) {
    //             $ticket_type = TicketType::find($ticket_type_id);

    //             foreach ($seats as $seat_id) {

    //                 // Creating temp order
    //                 $temp = OrderTemp::updateOrCreate([
    //                     'ticket_type_id' => $ticket_type->id,
    //                     'client_user_id' => $client_user->id,
    //                     'event_id' => $event->id,
    //                     'seat_id' => $seat_id,
    //                     'type' => 'season_ticket'
    //                 ], [
    //                     'amount' => 1,
    //                     'expiration_time' => $expiration_time
    //                 ]);

    //                 // Appending ticket to response
    //                 $price = $event->get_ticket($ticket_type->id)->pivot->price;
    //                 $selected_seat = Seat::find($seat_id);

    //                 // Appending ticket data to return variable
    //                 $response['seated_tickets'][] = [
    //                     'ticket_type_id' => $ticket_type->id,
    //                     'name' => $ticket_type->name,
    //                     'seat_name' => $selected_seat->full_name(),
    //                     'seat_id' => $seat_id,
    //                     'amount' => 1,
    //                     'price' => $price,
    //                     'subtotal' => $price,
    //                     'row_id' => $selected_seat->row_id,
    //                     'seat_index' => (int)$selected_seat->name,
    //                     'zone_id' => $ticket_type->zone_id,
    //                     'parent_zone_id' => $ticket_type->parent_zone_id,
    //                 ];

    //                 // Change seat status to on_hold_sale
    //                 EventGroupSeat::setSeatAvailabilityForEventGroup($event->id, $seat_id, 'on_hold_sale');

    //                 // Updating available tickets on table
    //                 $event->ticket_types()->where('event_group_id', $event->id)->where('ticket_type_id', $ticket_type->id)->increment('sold');

    //                 // Updating total
    //                 $response['total'] += $price;
    //             }
    //         }

    //         // Returning response
    //         ($client_user->type == 'temporary') ? $response['temporary_user_id'] = $client_user->id : $response['user_id'] = $client_user->id;
    //         $response['error'] = false;


    //         \Log::info($response);
    //         return $response;
    //     } catch (\Throwable $th) {
    //         return [
    //             'error' => true,
    //             'error_message' => $th->getMessage()
    //         ];
    //     }
    // }

    public function payment(Request $request)
    {
        try {
            $event_group = EventGroup::where('slug', $request->slug)->first();
            $system = System::find($event_group->system_id);
            $client_user = ClientUser::find($request->client_user_id);
            $total = 0;

            $client_user->update([
                'name' => $request->contact_info['name'],
                'last_name' => $request->contact_info['last_name'],
                'email' => $request->contact_info['email'],
                'phone' => $request->contact_info['phone']
            ]);

            // Creating order
            $order = Order::create([
                'client_user_id' => $client_user->id,
                'event_id' => $event_group->id,
                'event_type' => 'App\EventGroup',
                'email' => $client_user->email,
                'total' => 0,
                'type' => 'season_ticket',
                'payment_method' => ($client_user->type == 'ticket_office') ? 'cash' : 'debit_card',
                'selling_point' => ($client_user->type == 'client') ? 'web' : 'ticket_office',
                'status' => 'registered'
            ]);

            OrderTemp::where('client_user_id', $client_user->id)->where('event_id', $event_group->id)->where('type', 'season_ticket')->delete();

            foreach ($request->tickets as $key => $ticket) {
                $season_ticket_data = $request->season_ticket_data[$key];
                $user = ClientUser::where('email', $season_ticket_data['email'])->first();

                try {
                    // Checking if user exists
                    if (!$user) {
                        $faker = Faker::create();
                        $user = ClientUser::create([
                            'name' => $season_ticket_data['name'],
                            'last_name' => $season_ticket_data['last_name'],
                            'email' => $season_ticket_data['email'],
                            'password' => bcrypt($faker->password),
                            'type' => 'temporary',
                            'active' => 0
                        ]);
                    }

                    $ticket_price = $event_group->ticket_types()->where('ticket_type_id', $ticket['ticket_type_id'])->first();
                    $total +=  $ticket_price->pivot->price * $ticket['amount'];

                    // Defining seat
                    $seat = Seat::find($ticket['seat_id']);

                    // Defining the folio
                    $folio = Accreditation::where('type', 'season_ticket')->get()->last();
                    $folio = ($folio) ? 'AB-' . str_pad(explode('-', $folio->folio)[1] + 1, 5, '0', STR_PAD_LEFT) : 'AB-00001';

                    // Creating ticket
                    $ticket = Ticket::create([
                        'ticket_type_id' => $ticket['ticket_type_id'],
                        'order_id' => $order->id,
                        'event_id' => $event_group->id,
                        'event_type' => 'App\EventGroup',
                        'amount' => 1,
                        'price' => $ticket_price->pivot->price * $ticket['amount'],
                        'name' => 'Boleto de abono',
                        'type' => 'season_ticket'
                    ]);

                    $accreditation = Accreditation::create([
                        'system_id' => $event_group->system_id,
                        'ticket_type_id' => $ticket['ticket_type_id'],
                        'name' => $user->name . ' ' . $user->last_name,
                        'email' => $user->email,
                        'type' => 'season_ticket',
                        'ticket_id' => $ticket->id,
                        'folio' => $folio
                    ]);

                    //Apending accreditation to team
                    $accreditation->teams()->attach($system->teams->first()->id);

                    // Appending seat to ticket
                    $ticket->seats()->attach($seat->id);
                } catch (\Throwable $th) {
                    return [
                        'error' => true,
                        'message' => $th->getMessage()
                    ];
                }
            }

            // Updating order total
            $order->update(['total' => $total]);

            // Fetching email data
            $data = [
                'email' => $order->email,
                'tickets' => $order->seasson_tickets,
                'event_name' => $order->event->name,
                'date' => pretty_short_date($order->created_at),
                'order_id' => $order->id,
                'username' => $order->client_user->name,
                'type' => $order->type
            ];

            // Dispatching email job
            SendOrderConfirmation::dispatch($data);

            return [
                'error' => false,
                'order_id' => $order->id,
                'message' => 'Compra exitosa'
            ];
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo completar la compra',
                'error_message' => $th->getMessage()
            ];
        }
    }
}
