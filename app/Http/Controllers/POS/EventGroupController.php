<?php

namespace App\Http\Controllers\POS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\EventGroup;
use App\Zone;
use App\TicketType;

class EventGroupController extends Controller
{
    public function event_groups(Request $request)
    {
        // Returning data
        return [
            'error' => false,
            'event_groups' => EventGroup::on_sale_event_groups($request->system_id),
        ];
    }

    public function event_group($slug)
    {
        $event_group = EventGroup::bySlug($slug);

        // Checking if event is still on sale
        if (!$event_group->accreditations_on_sale) {
            // Returning data
            return [
                'error' => true,
                'error_message' => 'Este abono ya no se encuentra disponible',
            ];
        }

        // Compiling ticket types info
        $tickets_types = array();


        // Returning data
        return [
            'error' => false,
            'event' => $event_group->API_POS()
        ];
    }

    public function event_group_sale(Request $request)
    {
        // Fetch event group
        $event_group = EventGroup::where('slug', $request->slug)->first();



        // Checking if event is still on sale
        if (!$event_group->accreditations_on_sale) {
            // Returning data
            return [
                'error' => true,
                'error_message' => 'Este abono ya no se encuentra disponible',
            ];
        }


        // Compiling event info
        $ticket_types = array();
        foreach ($event_group->ticket_types as $type) {
            if ($type->active == 1 && $type->free == 0) {
                // if ($event_group->system->venues->first()->id == 3) {
                // Grouping by zones in Villahermosa stadium
                $zone = $type->zone->API();
                $ticket_types[$zone['parent_zone']['id']]['tickets'][$type->id] = [
                    'id' => $type->id,
                    'name' => $type->name,
                    'color' => '#' . $type->color,
                    'zone' => $zone,
                    'price' => $type->pivot->price,
                    // 'price' => $type->pivot->price,
                    'active' => $type->active,
                    'available' => $type->pivot->available,
                    // 'available' => (($type->pivot->max - $type->pivot->sold) > 0) ? true : false
                ];

                $ticket_types[$zone['parent_zone']['id']]['type'] = $type->type;
                $ticket_types[$zone['parent_zone']['id']]['name'] = $zone['parent_zone']['name']; // TODO Fetch zones by gorups, because this is a shitty solution
                $ticket_types[$zone['parent_zone']['id']]['color'] = '#' . $type->color; // TODO define zone color, because this is a shitty solution
                // }
            }
        }

        // Returning data
        return [
            'error' => false,
            'event' => [
                'id' => $event_group->id,
                'slug' => $event_group->slug,
                'event' => $event_group->name,
                'event_date' => $event_group->date,
                'active' => $event_group->active,
                'on_sale' => $event_group->on_sale,
                'id_venue' => $event_group->system->venues->first()->id,
                'venue' => $event_group->system->venues->first()->name,
            ],
            'ticket_types' => $ticket_types,
        ];
    }
}
