<?php

namespace App\Http\Controllers\POS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Event;
use App\User;
use App\ClientUser;
use App\Order;

class OrderController extends Controller
{
    public function active_orders(Request $request)
    {
        $user = ClientUser::find($request->user_id);

        $orders = array();
        foreach (Event::upcoming_events(5) as $active_event) {
            // $orders['events'][] = $active_event;
            foreach ($active_event->orders()->where('client_user_id', $user->id)->get() as $order) {
                $orders[] = $order->API_POS();
            }
        }

        return [
            'error' => false,
            'orders' => $orders,
        ];
    }

    public function inactive_orders(Request $request)
    {
        try {
            $user = ClientUser::find($request->user_id);

            $orders = array();
            foreach (Event::past_events(5)->get() as $active_event) {
                foreach ($active_event->orders()->where('client_user_id', $user->id)->get() as $order) {
                    $orders[] = $order->API_POS();
                }
            }

            return [
                'error' => false,
                'orders' => $orders,
            ];
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'error_message' => 'No es posible desplegar las órdenes.',
            ];
        }
    }

    public function order_details(Request $request, $id)
    {
        // Fetching order
        $order = Order::find($id);

        if ($request->client_user_id != 'order_confirm' && $request->client_user_id != $order->client_user_id) {
            return [
                'error' => true,
                'error_message' => 'Ésta orden no existe'
            ];
        }

        return [
            'error' => false,
            'order' => $order->API_POS(false)
        ];
    }
}
