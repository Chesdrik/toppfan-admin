<?php

namespace App\Http\Controllers\POS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use App\Event;
use App\EventGroup;
use App\OrderTemp;
use App\TicketType;
use App\Zone;
use App\ClientUser;

// use Illuminate\Support\Facades\Log;

class POSController extends Controller
{
    public function home(Request $request)
    {
        // Returning data
        return [
            'error' => false,
            'slideshows' => Event::upcoming_on_sale_events_slideshow(),
            'events' => Event::upcoming_on_sale_events_all(),
        ];
    }

    public function login(Request $request)
    {
        // If user sent is an email
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            // Checking if user exists
            $user = ClientUser::where('email', $request->email)->first();
            if ($user) {
                // Checking password
                if (Hash::check($request->password, $user->password)) {
                    // Generating token
                    $token = $user->createToken('API Token')->accessToken;

                    // Returning user data
                    $return['error'] = false;
                    $return['token'] = $token;
                    $return['user'] = $user;
                } else {
                    $return['error'] = true;
                    $return['error_message'] = "Usuario o password inválido";
                }
            } else {
                $return['error'] = true;
                $return['error_message'] = "Usuario o password inválido";
            }
        } else {
            $return['error'] = true;
            $return['error_message'] = "Email inválido";
        }

        // Returning data
        return $return;
    }

    public function seating_layout(Request $request)
    {
        $object = ($request->type == 'event') ? new Event : new EventGroup;
        $event = $object::find($request->event_id);
        $zone = Zone::find($request->zone_id);
        $type = TicketType::find($zone->ticket_types->first()->id);

        // Fetching price for event
        if ($request->type == 'event') {
            $price = $event->ticket_template->ticket_types()->where('ticket_type_id', $type->id)->first()->pivot->price;
        } else {
            $event_group_ticket_type = $event->get_ticket($type->id);
            // Seasson ticket cost
            $price = $event_group_ticket_type->pivot->price;
        }

        if (getenv('APP_ENV') == 'local') {
            sleep(1);
        }

        if ($zone->type == 'row' || $zone->type == 'seat') {
            return [
                'error' => false,
                'zone_type' => $zone->type,
                'price' => $price,
                'seat_layout_data' => [
                    'zone_name' => $zone->get_parent_zone->name ?? '',
                    'section_name' => $type->name,
                    'number' => 1,
                    'rows' => $type->rows_for_map($event)
                ]
            ];
        } elseif ($zone->type == 'general') {
            return [
                'error' => false,
                'zone_type' => $zone->type,
                'price' => $price,
                'seat_layout_data' => [
                    'zone_name' => $zone->get_parent_zone->name ?? '',
                    'section_name' => $type->name,
                    'number' => 1,
                    'rows' => $type->rows_for_map($event)
                ]
            ];
        }
    }

    public function timeout(Request $request)
    {
        try {
            foreach ($request->tickets as $ticket) {
                // Find and delete temp order
                $temp = OrderTemp::where('client_user_id', $request->user_id)
                    ->where('event_id', $request->event_id)
                    ->where('ticket_type_id', $ticket['ticket_type_id'])
                    ->first();

                $temp->delete();
            }

            $response['error'] = false;
            $response['message'] = 'Todo fine';
        } catch (\Throwable $th) {
            $response['error'] = true;
            $response['message'] = $th;
        }

        return $response;
    }
}
