<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientUser;
use App\Event;

class QRServerController extends Controller
{
    public function syncClientUsers(Request $request)
    {
        $return = array();

        $client_users_ids = array();

        // Cycle through event
        foreach ($request->events as $ev) {
            // Fetch event
            $event = Event::find($ev);

            // Merge client users
            $client_users_ids = array_merge($client_users_ids, $event->orders()->pluck('client_user_id')->toArray());
        }

        // Generate array
        foreach (array_unique($client_users_ids) as $client_user) {
            $client_user = ClientUser::find($client_user);
            $return[] = $client_user->API();
        }

        return $return;
    }

    public function finish_event(Request $request)
    {
        try {
            $event = Event::find($request->event_id);

            if ($event) {
                $event->update(['active' => 0, 'on_sale' => 0, 'data_readings' => $request->data]);
                $response = [
                    'error' => false,
                    'message' => 'Evento finalizado'
                ];
            } else {
                $response = [
                    'error' => true,
                    'message' => 'El evento ya no existe'
                ];
            }

            return $response;
        } catch (\Throwable $th) {
            return [
                'error' => true,
                'message' => 'No se pudo finalizar el evento'
            ];
        }
    }
}
