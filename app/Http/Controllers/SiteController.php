<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Event;
use App\User;
use App\Ticket;

// Notifications
use App\Notifications\Orderconfirm;


// Imports
use App\Imports\TicketsImport;

use Intervention\Image\Facades\Image;
use Storage;
use Excel;
use File;

class SiteController extends Controller
{
    public function home()
    {
        return redirect()->route('system.select');
    }

    public function client_user_after_password()
    {
        return \Redirect::to("https://boletos.pumas.mx");
    }

    public function login()
    {
        return view('login');
    }

    public function qrImage($order_id, $type_id)
    {
        $qr_path = '/orders/' . $order_id . '/' . $type_id;
        $file = Storage::allFiles($qr_path)[0];
        $file = Storage::get($file);

        return Image::make($file)->response('png');
    }

    public function message()
    {
        return view('create_user');
    }

    public function capacity()
    {
        $zones = array();
        $total = 0;
        foreach ($this->zones() as $key => $zone) {
            $zones[$key]['sections'] = $this->capacity_by_zone($zone)['sections'];
            $zones[$key]['total'] = $this->capacity_by_zone($zone)['total_section'];
            $total +=  $this->capacity_by_zone($zone)['total_section'];
        }

        return [
            'zones' => $zones,
            'total' => $total
        ];
    }

    public function capacity_by_zone($zone)
    {
        $sections = array();
        $total_section = 0;
        foreach ($zone['sections'] as $i => $seats) {
            $rest = ($seats % 5) >= 2 ? 2 : $seats % 5;
            $sections[$i] =  floor(($seats / 5)) * 2 + $rest;
            $total_section += floor(($seats / 5)) * 2 + $rest;
        }

        return [
            'sections' => $sections,
            'total_section' => $total_section
        ];

        return $total_section;
    }

    public function zones()
    {
        $zones = [
            '1' => [
                'sections' => [
                    '1' => 156,
                    '2' => 259,
                    '3' => 406,
                    '4' => 540,
                    '5' => 626
                ]
            ],
            '2' => [
                'sections' => [
                    '6' => 668,
                    '7' => 774,
                    '8' => 634,
                    '9' => 785,
                    '10' => 662,
                ]
            ],
            '3' => [
                'sections' => [
                    '11' => 628,
                    '12' => 534,
                    '13' => 412,
                    '14' => 272,
                    '15' => 157,
                ]
            ],
            '4' => [
                'sections' => [
                    '16' => 167,
                    '17' => 229,
                    '18' => 592,
                    '19' => 468,
                    '20' => 536,
                ]
            ],
            '5' => [
                'sections' => [
                    '21' => 488,
                    '22' => 502,
                    '23' => 487,
                ]
            ],
            '6' => [
                'sections' => [
                    '24' => 515,
                    '25' => 481,
                    '26' => 323,
                    '27' => 218,
                    '28' => 115,
                ]
            ]

        ];

        return $zones;
    }
}
