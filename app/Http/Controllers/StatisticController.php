<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Statistic;
use App\Sync;
use App\ProcessedStats;
use App\Venue;
use App\Team;
use Log;

class StatisticController extends Controller
{
    public function syncStatistics(Request $request)
    {
        Log::info('Recieving stats for event ' . $request->event_id);
        $event = Event::find($request->event_id);

        Log::info($request->processed_stats);

        try {
            foreach ($request->processed_stats as $processed_stat) {
                Statistic::updateOrCreate([
                    'event_id' => $processed_stat['event_id'],
                    'statistic_type_id' => $processed_stat['statistic_type_id'],
                ], [
                    'data' => $processed_stat['data']
                ]);
            }
            /*$sales = ProcessedStats::updateOrCreate(
                ['event_id' => $event->id, 'type' => 'sales'],
                ['data' => $request->processed_stats['sales']]
            );*/

            // $readings = ProcessedStats::updateOrCreate(
            //     ['event_id' => $event->id, 'stat_type_id' => '2'],
            //     ['data' => $request->processed_stats['readings']]
            // );

            // $exits = ProcessedStats::updateOrCreate(
            //     ['event_id' => $event->id, 'stat_type_id' => '3'],
            //     ['data' => $request->processed_stats['exits']]
            // );

            // $errors = ProcessedStats::updateOrCreate(
            //     ['event_id' => $event->id, 'stat_type_id' => '4'],
            //     ['data' => $request->processed_stats['errors']]
            // );

            // $general_info = ProcessedStats::updateOrCreate(
            //     ['event_id' => $event->id, 'stat_type_id' => '5'],
            //     ['data' => $request->processed_stats['general_info']]
            // );
        } catch (\Throwable $th) {
            echo $th;
            echo ('No se pudieron procesar las estadísticas');
        }
    }

    public function syncStatistics_OLD(Request $request)
    {
        // dd('yes');
        // return ['yes' => 'no'];

        // Fetching data and event
        $data = $request->all()[0];
        $event = Event::find($data['event_id']);
        $sync = Sync::create([
            'recieved' => json_encode($data),
            'type' => 'statistics',
            'event_id' => $event->id
        ]);
        $total_synced = 0;


        // Processing registered errors
        foreach ($data['statistics']['errors'] as $stat) {
            $statistic = Statistic::updateOrCreate([
                'checkpoint_id' => $stat['checkpoint_id'],
                'event_id' => $event->id,
                'type' => 'errors',
                'start' => $stat['start'],
                'end' => $stat['end'],
            ], [
                'sync_id' => $sync->id,
                'value' => $stat['value'],
            ]);
            $total_synced++;
        }

        // Processing registered readings
        foreach ($data['statistics']['readings'] as $stat) {
            $statistic = Statistic::updateOrCreate([
                'checkpoint_id' => $stat['checkpoint_id'],
                'event_id' => $event->id,
                'type' => 'readings',
                'start' => $stat['start'],
                'end' => $stat['end'],
            ], [
                'sync_id' => $sync->id,
                'value' => $stat['value'],
            ]);
            $total_synced++;
        }

        // Updating Sync
        // $sync->response = '';
        $sync->error = 0;
        $sync->total_records_synced = $total_synced;
        $sync->save();

        // Returning response
        return [
            'error' => false
        ];
    }

    public function index()
    {
        return view('admin.statistics.main', [
            'venues' => Venue::all(),
            'teams' => Team::all(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                ['label' => 'Estadísticas', 'url' => route('system.statistics.index'), 'active' => true],
            ]
        ]);
    }

    public function all()
    {
        return view('admin.statistics.index', [
            'general_statistics' => (!is_null(ProcessedStats::get_stats(7, null, null)) ? ProcessedStats::parser_stats() : null),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                ['label' => 'Estadísticas', 'url' => route('system.statistics.index'), 'active' => true],
            ]
        ]);
    }

    public function byTeam($id)
    {
        return view('admin.statistics.team', [
            'team' => Team::find($id),
            'team_statistics' => (!is_null(ProcessedStats::get_stats(8, null, $id)) ? ProcessedStats::parser_stats(null, $id) : null),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                ['label' => 'Estadísticas', 'url' => route('system.statistics.index'), 'active' => true],
                ['label' => Team::find($id)->name, 'url' => route('system.statistics.byTeam', $id), 'active' => true],
            ]
        ]);
    }

    public function byVenue($id)
    {
        return view('admin.statistics.venue', [
            'venue' => Venue::find($id),
            'venue_statistics' => (!is_null(ProcessedStats::get_stats(9, $id, null)) ? ProcessedStats::parser_stats($id, null) : null),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                ['label' => 'Estadísticas', 'url' => route('system.statistics.index'), 'active' => true],
                ['label' => Venue::find($id)->name, 'url' => route('system.statistics.byVenue', $id), 'active' => true],
            ]
        ]);
    }

    /*
    * API services for Stats
    */

    public function statsForEvent(Request $request)
    {
        try {
            $event = Event::find($request->input('eventID'));
            $data = array();

            if (!is_null($event)) {
                switch ($request->input('type')) {
                    case 'sales':
                        $stat_type = 1;
                        break;
                    case 'readings':
                        $stat_type = 2;
                        break;
                    case 'exits':
                        $stat_type = 3;
                        break;
                    case 'errors':
                        $stat_type = 4;
                        break;
                    case 'checkpoint_data':
                        $stat_type = 6;
                        break;
                    default:
                        $stat_type = $request->input('type');
                        break;
                }

                $stats = $event->parser_stats()[$request->input('type')];
                if (!is_null($stats)) {
                    $data['error'] = false;
                    $data['eventID'] = $event->id;
                    $data['type'] = $request->input('type');
                    $data['data'] = $stats;
                } else {
                    $data['error'] = false;
                    $data['message'] = 'No se encontraron datos estadísticos con la información dada.';
                }
            } else {
                $data['error'] = true;
                $data['message'] = 'No se encontró evento con el ID dado.';
            }

            return $data;
        } catch (\Throwable $th) {
            return [
                'app_error' => true,
                'message' => 'Error. No se pudieron obtener los datos estadísticos.'
            ];
        }
    }

    public function getStats(Request $request)
    {
        try {

            if (!$request->input('teamID') && !$request->input('venueID'))
                return [
                    'error' => true,
                    'message' => 'Es necesario ingresar un ID'
                ];

            $object = ($request->input('teamID')) ? Team::find($request->input('teamID')) : Venue::find($request->input('venueID'));
            $data = array();

            if (!is_null($object)) {
                $type = (get_class($object) == 'App\Team') ? 'team' : 'venue';

                switch ($type) {
                    case 'team':
                        $stat_type = 8;
                        $stats = (!is_null(ProcessedStats::get_stats($stat_type, null, $object->id)) ? ProcessedStats::parser_stats(null, $object->id) : null);
                        break;
                    case 'venue':
                        $stat_type = 9;
                        $stats = (!is_null(ProcessedStats::get_stats($stat_type, $object->id, null)) ? ProcessedStats::parser_stats($object->id, null) : null);
                        break;
                }

                $data['error'] = false;
                $data['teamID'] = $object->id;
                $data['type'] = $request->input('type');

                if ($type == 'team') {
                    (!is_null($stats['team_stats'])) ? $data['data'] = $stats['team_stats'] : $data['message'] = 'No se encontraron datos estadísticos con la información dada.';
                } else {
                    (!is_null($stats['venue_stats'])) ? $data['data'] = $stats['venue_stats'] : $data['message'] = 'No se encontraron datos estadísticos con la información dada.';
                }
            } else {
                $data['error'] = true;
                $data['message'] = 'No se encontró información con el ID dado.';
            }

            return $data;
        } catch (\Throwable $th) {
            return [
                'app_error' => true,
                'message' => 'Error. No se pudieron obtener los datos estadísticos.'
            ];
        }
    }

    public function statsAllEvents(Request $request)
    {
        try {
            $data = array();
            $stat_type = 5;
            $stats = (!is_null(ProcessedStats::get_stats($stat_type, null, null)) ? ProcessedStats::parser_stats(null, null) : null);

            if (!is_null($stats['general_stats'])) {
                $data['error'] = false;
                $data['type'] = $request->input('type');
                $data['data'] = $stats['general_stats'];
            } else {
                $data['error'] = false;
                $data['message'] = 'No se encontraron estadísticas de todos los eventos.';
            }

            return $data;
        } catch (\Throwable $th) {
            return [
                'app_error' => true,
                'message' => 'Error. No se pudieron obtener los datos estadísticos.'
            ];
        }
    }
}
