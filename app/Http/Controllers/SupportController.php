<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Support;
use App\Conversation;
use Auth;

class SupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->type == 'admin') {
            
            $supports = Support::all();
            return view('admin.supports.list', compact('supports'));
        }else{
            $supports = Support::where('user_id', Auth::user()->id)->get();

            return view('support.list', compact('supports'));

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('support.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $support = Support::create(['topic_id' => $request->topic_id, 'description' =>$request->description , 'user_id' => Auth::user()->id]);
        $conversation = Conversation::create(['support_id' => $support->id, 'user_id' => Auth::user()->id, 'message' => $request->description]);

        return view('support.message', compact('conversation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $support = Support::find($id);
        $conversation = $support->convesation;
        //dd($support, $conversation);
        return view('support.message', compact('support', 'convesation'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function change_status($id, $status){
        //dd($status);
        $support = Support::find($id);
        $support->update(['status' => $status]);
        return back();
    }
}
