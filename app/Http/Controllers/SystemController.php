<?php

namespace App\Http\Controllers;

use App\System;
use App\User;
use App\Module;
use App\Config;
use App\Venue;
use Storage;
use Illuminate\Http\Request;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Forgetting Sarah Marshal
        session()->forget('system_slug');

        // Redirecting root user to global admin
        if (\Auth::user()->type == 'root') {
            $systems = System::all();
            $breadcrumb = [
                ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
                ['label' => 'Sistemas', 'url' => '#', 'active' => true],
            ];

            return view('admin.system.index', compact('breadcrumb', 'systems'));
        } else {
            $systems = \Auth::user()->systems();

            if ($systems->count() == 1) {
                // Redirecting to dashboard for the only system associated
                session(['system_slug' => $systems->first()->slug]);
                return redirect()->route('system.dashboard', $systems->first()->slug);
            } else {
                // Fetching systems from db
                $systems = $systems->get();
                $breadcrumb = [
                    ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
                ];

                // Redirecting user to its associated systems for selection
                return view('admin.system.select', compact('breadcrumb', 'systems'));
            }
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('type', 'admin')->pluck('name', 'id')->toArray();
        $modules = Module::pluck('name', 'id')->toArray();
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Sistemas', 'url' => route('admin.system.index'), 'active' => false],
            ['label' => 'Crear', 'url' => '#', 'active' => true],
        ];

        return view('admin.system.create', compact('breadcrumb', 'users', 'modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('file')->getClientOriginalExtension() != 'png') {
            return back()->with('error', 'El logo debe tener extensión .png');
        } else {
            $system = System::create($request->except('_token', 'user', 'file', 'primary_color', 'secondary_color'));
            $filename = $system->id . '_logo.png';

            // Uploading file
            if ($request->file != null) {
                Storage::putFileAs("public/system/{$system->id}", $request->file('file'), $system->id . "_logo.png");
            }

            $configs = Config::create([
                'system_id' => $system->id,
                'key' => 'logo',
                'value' => '/public/system/' . $system->id . '/' . $filename,
            ]);

            $configs = Config::create([
                'system_id' => $system->id,
                'key' => 'primary_color',
                'value' => $request->primary_color,
            ]);

            $configs = Config::create([
                'system_id' => $system->id,
                'key' => 'secondary_color',
                'value' =>  $request->secondary_color,
            ]);

            $configs = Config::create([
                'system_id' => $system->id,
                'key' => 'title',
                'value' =>  $request->title,
            ]);

            foreach ($request->user as $user) {
                $system->user()->attach($user);
            }
            foreach ($request->module as $module) {
                $system->module()->attach($module);
            }
            return redirect(route('admin.system.index'))
                ->with('success', 'Sistema creado correctamente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $system = System::findOrFail($id);
        $selected_users = $system->users->pluck('id')->toArray();
        $selected_modules = $system->modules->pluck('id')->toArray();
        $system_config = $system->configs->pluck('value', 'key')->toArray();
        $selected_venues = $system->venues->pluck('id')->toArray();

        // dd(Storage::get("system/{$system->id}/{$system->id}_logo.png"));
        // dd($selected_venues);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Sistemas', 'url' => route('admin.system.index'), 'active' => false],
            ['label' => $system->name, 'url' => '#', 'active' => true],
        ];

        return view('admin.system.edit', compact('breadcrumb', 'system', 'selected_users', 'selected_modules', 'system_config', 'selected_venues'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\System  $system
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $system = System::findOrFail($id);

        if ($request->file != null) {
            if ($request->file('file')->getClientOriginalExtension() != 'png') {
                return back()->with('error', 'El logo debe tener extensión .png');
            } else {
                // Uploading file
                Storage::putFileAs("public/system/{$system->id}", $request->file('file'), $system->id . "_logo.png");
            }
        }

        // Updating config
        Config::updateOrCreate(['system_id' => $system->id, 'key' => 'primary_color'], ['value' => $request->primary_color]);
        Config::updateOrCreate(['system_id' => $system->id, 'key' => 'secondary_color'], ['value' => $request->secondary_color]);
        Config::updateOrCreate(['system_id' => $system->id, 'key' => 'title'], ['value' => $request->title]);

        // Updating all data
        $system->update($request->except('_token', 'users', 'file', 'primary_color', 'secondary_color'));
        $system->users()->sync($request->users);
        $system->modules()->sync($request->modules);
        $system->venues()->sync($request->venues);

        // dd($system->venues, $request->venues, $request->modules);

        // Returning to prev view
        return back()
            ->with('success', 'Sistema se actualizo correctamente');
    }
}
