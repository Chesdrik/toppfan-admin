<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Event;
use App\Venue;
use App\Sync;
use App\TicketTemplate;
use App\Ticket;
use App\TicketReading;
use App\TicketType;
use Carbon\Carbon;
use DB;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Boletos', 'url' => '/admin/boletos', 'active' => true],
        ];
        $tickets = Ticket::all()->groupBy('event_id');

        return view('admin.tickets.list', compact('breadcrumb', 'tickets'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Show tickets of user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function myTickets()
    {
        $orders = Auth::user()->orders()->whereHas('event', function ($query) {
            return $query->where('date', '>=', Carbon::now()->format('Y-m-d H:i:s'));
        })->get();

        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Mis Boletos', 'url' => '/mis_boletos', 'active' => true],
        ];

        return view('user.tickets.list', compact('breadcrumb', 'orders'));
    }

    /**
     * Show a specific ticket.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ticketView($id)
    {
        $ticket = Ticket::find($id);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Mis Boletos', 'url' => '/user/boleto/' . Auth::user()->id, 'active' => true],
            ['label' => 'Boletos para ' . $ticket->event->name, 'url' => '/user/view/boleto/' . $ticket->id, 'active' => true],
        ];

        return view('user.tickets.view', compact('breadcrumb', 'ticket'));
    }

    /**
     * Show tickets of user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function myOldTickets($id)
    {
        $orders = Auth::user()->orders()->whereHas('event', function ($query) {
            return $query->where('date', '<=', Carbon::now()->format('Y-m-d H:i:s'));
        })->get();

        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Mis Boletos', 'url' => '/mis_boletos', 'active' => false],
            ['label' => 'Anteriores', 'url' => '/mis_boletos/anteriores', 'active' => true],
        ];

        return view('user.tickets.old_list', compact('breadcrumb', 'orders'));
    }

    /**
     * Display a listing the tickets for sale.
     *
     * @param  int  $venue_id
     * @param  int  $event_id
     * @return \Illuminate\Http\Response
     */
    public function select_tickets($venue_id, $event_id)
    {
        $venue = Venue::find($venue_id);
        return view('user.shopping.tickets', [
            'venue' => $venue,
            'event' => Event::find($event_id),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                ['label' => 'Equipos', 'url' => route('teams'), 'active' => false],
                ['label' => 'Eventos de ' . $venue->pretty_team(), 'url' => route('team_events', $venue->id), 'active' => false],
                ['label' => 'Boletos para ' . Event::find($event_id)->name, 'url' => route('tickets_sale', [$venue_id, $event_id]), 'active' => true],
            ]
        ]);
    }

    public function confirm_sale(Request $request)
    {
        $request->session()->put('tickets', $request->tickets);
        $request->session()->put('email', $request->email);
        $request->session()->put('id', $request->id);

        return ['error' => false, 'tickets' => $request->tickets, 'email' => $request->email, 'id' => $request->id];
    }

    public function confirm_form()
    {
        $event = Event::find(session('id'));
        $tickets = session('tickets');
        $template = TicketTemplate::find($event->ticket_template_id);
        $data = array();
        foreach ($tickets as $item) {
            $temp = array();

            foreach ($template->ticket_types as $ticket_type) {
                if ($ticket_type->id == $item['ticket_type_id']) {
                    $temp['ticket'] = $ticket_type;
                    $temp['total'] = $item['total'];
                    $data[] = $temp;
                }
            }
        }

        return view('user.shopping.confirm', [
            'data' => $data,
            'email' => session('email'),
            'event' => $event,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                ['label' => 'Equipos', 'url' => route('teams'), 'active' => false],
                ['label' => 'Eventos de ' . $event->venue->pretty_team(), 'url' => route('team_events', $event->venue->id), 'active' => false],
                ['label' => 'Boletos para ' . $event->name, 'url' => route('tickets_sale', [$event->venue->id, $event->id]), 'active' => false],
                ['label' => 'Confirmar compra', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    public function pay_form($id)
    {
        $event = Event::find($id);
        return view('user.shopping.pay', [
            'event' => $event,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                ['label' => 'Equipos', 'url' => route('teams'), 'active' => false],
                ['label' => 'Eventos de ' . $event->venue->pretty_team(), 'url' => route('team_events', $event->venue->id), 'active' => false],
                ['label' => 'Boletos para ' . $event->name, 'url' => route('tickets_sale', [$event->venue->id, $event->id]), 'active' => false],
                ['label' => 'Confirmar compra', 'url' => route('confirm_form'), 'active' => false],
                ['label' => 'Pagar', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    public function confirm_courtesies()
    {
        $event = Event::find(session('id'));
        $tickets = session('tickets');
        $template = TicketTemplate::find($event->ticket_template_id);
        $data = array();
        foreach ($tickets as $item) {
            $temp = array();

            foreach ($template->ticket_types as $ticket_type) {
                if ($ticket_type->id == $item['ticket_type_id']) {
                    $temp['ticket'] = $ticket_type;
                    $temp['total'] = $item['total'];
                    $data[] = $temp;
                }
            }
        }

        return view('admin.courtesies.confirm', [
            'data' => $data,
            'email' => session('email'),
            'event' => $event,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard'), 'active' => false],
                ['label' => 'Equipos', 'url' => route('teams'), 'active' => false],
                ['label' => 'Eventos de ' . $event->venue->pretty_team(), 'url' => route('team_events', $event->venue->id), 'active' => false],
                ['label' => 'Boletos para ' . $event->name, 'url' => route('tickets_sale', [$event->venue->id, $event->id]), 'active' => false],
                ['label' => 'Confirmar compra', 'url' => '#', 'active' => true],
            ]
        ]);
    }




    public function syncTicketReadings(Request $request)
    {
        if (count($request->ticket_readings) > 0) {
            // Clean ticket readings
            DB::statement("SET foreign_key_checks=0");
            TicketReading::truncate();
            DB::statement("SET foreign_key_checks=1");

            // Creating sync
            $sync = Sync::create([
                'event_id' => 39,
                'recieved' => json_encode($request->ticket_readings),
                'type' => 'ticket_readings',
            ]);

            // Processing ticket readings
            foreach ($request->ticket_readings as $tr) {
                // Fetching ticket and adding extra use
                $ticket = Ticket::find($tr['ticket_id']);
                // $event_id = $ticket->event_id;

                if ($tr['error'] == 0) {
                    // If there is no error, the ticket was used
                    // TODO change this since on a real event, we have more than one ticket on each QR.
                    // It might be a good reason to define perimetral access to stadium to determine this
                    $ticket->used = 1;
                    $ticket->save();
                }

                // Saving ticket reading
                $ticket_reading = TicketReading::create([
                    'ticket_id' => $ticket->id,
                    'sync_id' => $sync->id,
                    'checkpoint_id' => $tr['checkpoint_id'],
                    'type' => $tr['type'],
                    'error' => $tr['error'],
                    'reading_time' => $tr['reading_time'],
                ]);
            }

            // TODO Process total used tickets with readings for statistics

            // Returning response
            return [
                'error' => false,
            ];
        } else {
            return [
                'error' => true,
            ];
        }
    }

    public function syncTickets(Request $request)
    {
        $tickets = array();
        $event = Event::find($request->event_id);

        // Processing tickets
        if ($event->orders->count() > 0) {
            foreach ($event->orders as $order) {
                if ($order->tickets->count() > 0) {
                    foreach ($order->tickets as $ticket) {
                        // Fetching tickets
                        $tickets[] = [
                            'id' => $ticket->id,
                            'event_id' => $event->id,
                            'ticket_type_id' => $ticket->ticket_type_id,
                            'client_user_id' => $ticket->order->client_user_id,
                            'order_id' => $order->id,
                            'name' => $ticket->name,
                            'amount' => $ticket->amount,
                        ];
                    }
                }
            }
        }

        // Returning response
        return [
            'error' => false,
            'tickets' => $tickets
        ];
    }

    public function syncTicketTypes(Request $request)
    {
        $ticket_types = array();

        // Processing ticket types
        foreach (TicketType::all() as $ticket_type) {
            $ticket_types[] = [
                'id' => $ticket_type->id,
                'name' => $ticket_type->name,
                'price' => $ticket_type->price,
                'color' => $ticket_type->color,
                'venue_id' => $ticket_type->venue_id,
                'checkpoints' => $ticket_type->checkpoints_array(),
            ];
        }

        // Returning response
        return [
            'error' => false,
            'ticket_types' => $ticket_types
        ];
    }

    public function verify_access(Request $request)
    {
        try {
            $ticket = Ticket::find($request->ticketID);
            $ticket->update(['used' => 1]);

            $data = [
                'ticket_id' => $request->ticketID,
                'sync_id' => 1,
                'checkpoint_id' => $request->checkpointID,
                'reading_time' => date('Y-m-d H:i:s')
            ];

            if (count($ticket->ticket_readings) == 0) {
                $data['type'] = 'access';
                $data['error'] = 0;

                $return = [
                    'error' => false,
                    'message' => 'Lectura de acceso exitosa.'
                ];
            } else {
                try {
                    $last = $ticket->ticket_readings->where('type', '!=', 'error')->last();
                    if ($last->type == $request->type) {
                        $type = ($request->type == 'access') ? 'salida' : "acceso";

                        $data['type'] = 'error';
                        $data['error'] = 1;


                        $return = [
                            'error' => true,
                            'message' => 'Lectura denegada. Se requiere lectura de ' . $type
                        ];
                    } else {
                        $type = ($request->type == 'access') ? 'acceso' : "salida";
                        $data['type'] = $request->type;
                        $data['error'] = 0;

                        $return = [
                            'error' => false,
                            'message' => 'Lectura de ' . $type . ' exitosa.'
                        ];
                    }
                } catch (\Throwable $th) {
                    return [
                        'app_error' => true,
                        'message' => 'No se pudo completar lectura del ticket.'
                    ];
                }
            }

            $reading = TicketReading::create($data);

            return $return;
        } catch (\Throwable $th) {
            return [
                'app_error' => true,
                'message' => 'No se pudo completar lectura del ticket.'
            ];
        }
    }
}
