<?php

namespace App\Http\Controllers\TicketDistributionSystem;

use App\Http\Requests\TicketDistributionSystem\DistributorAccountValidation;
use App\Http\Requests\TicketDistributionSystem\TDSPasswordReset;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;
use Auth;
use Excel;

use App\Event;
use App\ClientUser;
use App\TicketDistributionSystem\TicketGroup;
use App\TicketType;
use App\Ticket;
use App\Order;

//Jobs
use App\Jobs\SendTicketDistributionEmail;
use App\Jobs\SendTicketLotDistributionEmail;
use App\Jobs\SendPasswordResetEmail;

// Exports
use App\Exports\AssignmentsExport;

class TicketDistributionSystemController extends Controller
{
    public function login_form()
    {
        return view('admin.ticket_distribution_system.login');
    }

    public function login(Request $request)
    {
        // Attempting login for client_user
        if (Auth::guard('client_users')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::guard('client_users')->user();

            if (!$user->active) {
                return back()->with(['error' => 'Es necesario que actives tu cuenta. Accede desde el correo de asignación de boletos.']);
            }

            return redirect()->route('tds');
        }

        return back()->with(['error' => 'No existe el usuario con esa contraseña']);
    }

    public function index()
    {
        $user = Auth::guard('client_users')->user();
        $eventId = TicketGroup::where('client_user_id', $user->id)->firstOrFail()->event_id;
        $ticketGroupId = null;
        $ids = TicketGroup::where('client_user_id', $user->id)->pluck('event_id');
        $events = Event::whereIn('id', $ids)->pluck('name', 'id')->toArray();

        return view('tds.index', compact('eventId', 'ticketGroupId', 'events'));
    }

    public function register($id)
    {
        $client_user = ClientUser::find($id);
        if ($client_user->active == 1 && $client_user->active_on_admin == 1) {
            return redirect(route('tds.login'));
        }

        return view('admin.ticket_distribution_system.register', [
            'user' => $client_user
        ]);
    }

    public function account_activation(DistributorAccountValidation $request)
    {
        $client_user = ClientUser::where('id', $request->user_id)->where('email', $request->email)->first();

        $client_user->update([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'password' => Hash::make($request->password),
            'type' => 'distributor',
            'active' => 1,
            'active_on_admin' => 1
        ]);

        Auth::guard('client_users')->login($client_user);
        return redirect(route('tds'));
    }

    public function assign($id)
    {
        $ticketGroup = TicketGroup::find($id);

        return view('tds.assign', [
            'ticketGroupId' => $id,
            'eventId' => $ticketGroup->event_id,
            'ticketGroup' => $ticketGroup
        ]);
    }

    public function logout(Request $request)
    {
        Auth::guard('client_users')->logout();

        return redirect(route('tds.login'));
    }

    public function resend_email(Request $request)
    {
        try {

            if ($request->type == 'fan') {

                // Fetching user
                $user = ClientUser::where('email', $request->email)->first();
                $order = Order::find($request->order_id);

                // Preparing data for mail
                $data = [
                    'id' => $user->id,
                    'email' => $request->email,
                    'ticket_type_name' => $order->tickets->first()->ticket_type->name,
                    'event_name' => $order->event->name,
                    'activation_pending' => ($user->active == 1) ? false : true,
                    'group' => false,
                    'amount' => Order::find($request->order_id)->tickets->count(),
                    'date' => pretty_short_date($order->created_at),
                    'order_id' => $order->id,
                    'username' => $user->name
                ];

                // Dispatching email job
                SendTicketDistributionEmail::dispatch($data);
            } else {

                // Fetching ticket group
                $ticket_group = TicketGroup::find($request->ticket_group_id);

                // Preparing data for mail
                $data = [
                    'id' => $ticket_group->client_user_id,
                    'email' => $ticket_group->client_user->email,
                    'ticket_type_name' => $ticket_group->ticket_type->name,
                    'event_name' => $ticket_group->event->name,
                    'amount' => $ticket_group->amount,
                    'activation_pending' => ($ticket_group->client_user->active == 1) ? false : true,
                    'group' => false,
                    'date' => pretty_short_date($ticket_group->created_at)
                ];

                // Dispatching email job
                SendTicketLotDistributionEmail::dispatch($data);
            }

            return back()->with('success', 'Email reenviado correctamente a ' . $request->email);
        } catch (\Throwable $th) {
            return back()->with('error', 'Algo salió mal, no fue posible reenviar el email.');
        }
    }

    public function password_request_form(){
        return view('admin.ticket_distribution_system.passwords.request');
    }

    public function password_request(Request $request){
        try {
            $user = ClientUser::where('email', $request->email)->first();
    
            if(!$user){
                return back()->with('error', 'No existe usuario con ese correo.');
            }
    
            // Preparing data for mail
            $data = [
                'id' => $user->id,
                'email' => $user->email,
            ];
    
            // Dispatching email job
            SendPasswordResetEmail::dispatch($data);
    
            return back()->with('success', 'Hemos enviado un correo a la dirección que indicaste, sigue las intrucciones para restablecer tu contraseña.');

        } catch (\Throwable $th) {
            return back()->with('error', 'No fue posible restablecer la contraseña.');
        }
    }

    public function password_reset_form($id){
        $user = ClientUser::find($id);

        return view('admin.ticket_distribution_system.passwords.reset', compact('user'));
    }

    public function password_reset(TDSPasswordReset $request){
        try {
            $user = ClientUser::find($request->user_id);
            $user->update(['password' => bcrypt($request->password), 'active' => 1, 'active_on_admin' => 1]);

            Auth::guard('client_users')->login($user);
            return redirect(route('tds'))->with('success', 'Contraseña actualizada exitosamente.');

        } catch (\Throwable $th) {
            return back()->with('error', 'No fue posible restablecer la contraseña.');
        }
    }

    public function export(Request $request) {
        try {
            $event = Event::find($request->event_id);
            $filename = ($request->type == 'fans') ? 'Boletos-Fans-'.$event->name.'.xlsx' : 'Boletos-Distribuidores-'.$event->name.'.xlsx';

            return Excel::download(new AssignmentsExport($event->id, $request->user_id, $request->type), $filename);
            
        } catch (\Throwable $th) {
            return back()->with('error', 'Algo salió mal, no se pudo descargar el archivo.');
            return $th->getMessage();
        }
    }
}
