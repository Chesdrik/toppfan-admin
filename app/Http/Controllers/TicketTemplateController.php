<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\TicketTemplate;
use App\Venue;

class TicketTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     return view('admin.tickets.templates.main', [
    //         'venues' => Venue::all(),
    //         'breadcrumb' => [
    //             ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
    //             ['label' => 'Sedes', 'url' => route('system.venues.templates.index'), 'active' => true],
    //         ]
    //     ]);
    // }

    public function index($system_slug, $id)
    {
        return view('admin.tickets.templates.list', [
            'venue' => Venue::find($id),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => '', 'active' => false],
                ['label' => Venue::find($id)->name, 'url' => route('system.venues.config', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Templates de boletos', 'url' => route('system.venues.templates.index', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($system_slug, $id)
    {
        return view('admin.tickets.templates.create', [
            'venue' => Venue::find($id),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => Venue::find($id)->name, 'url' => route('system.venues.config', [session('system_slug'), $id]), 'active' => false],
                ['label' => 'Templates de boletos', 'url' => route('system.venues.templates.index', [session('system_slug'), $id]), 'active' => true],
                ['label' => 'Crear', 'url' => '#', 'active' => true],
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $system_slug, $id)
    {
        $request->request->add([
            'venue_id' => $id,
        ]);

        $template = TicketTemplate::create(
            $request->except('_token')
        );

        foreach ($request->costs as $ticket => $cost) {
            $template->ticket_types()->attach($ticket, ['price' => $cost]);
        }

        return redirect()
            ->route('system.venues.templates.index', [session('system_slug'), $template->venue_id])
            ->with('success', 'Templeate creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($system_slug, $venue_id, $template_id)
    {
        $venue = Venue::find($venue_id);
        $ticket_template = TicketTemplate::find($template_id);

        $template_costs = [];
        foreach ($ticket_template->ticket_types as $ticket_type) {
            $template_costs[$ticket_type->id] = $ticket_type->pivot->price ?? $ticket_type->price;
        }

        return view('admin.tickets.templates.show', [
            'ticket_template' => $ticket_template,
            'venue' => $venue,
            'template_costs' => $template_costs,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => route('system.dashboard', [session('system_slug')]), 'active' => false],
                ['label' => $venue->name, 'url' => route('system.venues.templates.index', [session('system_slug'), $venue->id]), 'active' => false],
                ['label' => 'Templates de boletos', 'url' => route('system.venues.templates.index', [session('system_slug'), $venue_id]), 'active' => true],
                ['label' => $ticket_template->name, 'url' => '#', 'active' => true],
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $system_slug, $venue_id, $template_id)
    {
        $request->request->add([
            'venue_id' => $venue_id,
        ]);
        $template = TicketTemplate::find($template_id);
        $template->update(['name' => $request->name]);
        $template->ticket_types()->sync($request->costs);



        return redirect()
            ->route('system.venues.templates.show', [session('system_slug'), $venue_id, $template->id])
            ->with('success', 'Información actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Get tickets for AJAX petition
    public function getTickets($system_slug, $id)
    {
        $data = array();
        try {
            $template = TicketTemplate::find($id);

            foreach ($template->ticket_types as $ticket) {
                $data[$ticket->name] = $ticket->pivot->price;
            }

            return ['error' => false, 'data' => $data];
        } catch (\Throwable $th) {
            return ['error' => true, 'data' => $data];
        }
    }
}
