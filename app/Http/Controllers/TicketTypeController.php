<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\TicketType;
use App\Venue;
use App\Zone;
use App\Checkpoint;

class TicketTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($system_slug, $id)
    {
        $venue = Venue::find($id);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => $venue->name, 'url' => route('system.venues.templates.index', [session('system_slug'), $venue->id]), 'active' => false],
            ['label' => 'Tipos de boletos', 'url' => '#', 'active' => true],
        ];

        return view('admin.tickets.types.list', compact('breadcrumb', 'venue'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $system_slug, $id)
    {
        //dd($request->all());
        ($request->price) ? $request : $request->request->add([
            'price' => 0,
        ]);

        $zone = Zone::find($request->id);
        $ticket_type_array = $request->except('_token', 'checkpoints');
        $ticket_type_array['venue_id'] = $zone->venue_id;

        $new_ticket = TicketType::create($ticket_type_array);

        if ($request->checkpoints) {
            foreach ($request->checkpoints as $key) {
                $new_ticket->checkpoints()->attach($key);
            }
        }

        return redirect(route('system.venues.ticket_types.index', [session('system_slug'), $id]))
            ->with('success', 'Tipo creado correctamente');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $system_slug
     * @param  int  $id
     * @param  int  $venue
     * @param  int  $ticket_type
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $system_slug, $venue_id, $ticket_type)
    {
        $venue = Venue::find($venue_id);
        $ticket_type = TicketType::find($ticket_type);
        $checkpoints_array = $ticket_type->checkpoints_array();
        $perimeters = Checkpoint::checkpointOptionsForVenue($venue->id, 'perimeter');
        $tunnels = Checkpoint::checkpointOptionsForVenue($venue->id, 'tunnel');

        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => $venue->name, 'url' => route('system.venues.index', [session('system_slug'), $venue->id]), 'active' => false],
            ['label' => 'Tipos de boletos', 'url' => route('system.venues.ticket_types.index', [session('system_slug'), $venue->id]), 'active' => true],
            ['label' => $ticket_type->name, 'url' => '#', 'active' => true],
        ];

        return view('admin.tickets.types.edit', compact('breadcrumb', 'venue', 'ticket_type', 'checkpoints_array', 'perimeters', 'tunnels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $system_slug, $venue, $ticket)
    {
        $ticket_type = TicketType::find($ticket);
        ($request->price) ? $request : $request->request->add(['price' => 0]);

        $ticket_type->update(
            $request->except('_token', 'method', 'checkpoints')
        );

        if ($request->checkpoints) {
            $ticket_type->checkpoints()->sync($request->checkpoints);
        }

        return back()
            ->with('success', 'Información actualizada correctamente');
    }
}
