<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\User;
use App\System;

// Authentication
use Auth;

// Requests
use App\Http\Requests\UserCreate;
use App\Http\Requests\UserUpdate;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Usuarios', 'url' => '/admin/usuarios', 'active' => true],
        ];
        $users = User::orderBy('name')->get();
        $user_types = (in_array(Auth::user()->type, ['root', 'admin']) ? User::userTypes() : User::restrictedUserTypes());
        $systems = System::pluck('name', 'id')->toArray();

        return view('admin.users.list', compact('breadcrumb', 'users', 'user_types', 'systems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Usuarios', 'url' => '/admin/usuarios', 'active' => false],
            ['label' => 'Crear', 'url' => '/admin/usuarios/create', 'active' => true],
        ];

        return view('admin.users.create', compact('breadcrumb'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreate $request)
    {
        // Encrypting password
        $request->request->add([
            'password' => bcrypt($request->password)
        ]);

        // Creating user
        $user = User::create($request->except('systems'));

        // Associating systems
        $user->systems()->sync($request->systems);

        // Redirecting user to listing
        // '/admin/usuarios'
        return redirect()->route('system.users.index')
            ->with('success', 'Usuario creado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($system_slug, $id)
    {
        $user = User::find($id);
        $user_types = (in_array(Auth::user()->type, ['root', 'admin']) ? User::userTypes() : User::restrictedUserTypes());
        $systems = System::pluck('name', 'id')->toArray();
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Usuarios', 'url' => '/admin/usuarios', 'active' => false],
            ['label' => $user->name, 'url' => '/admin/usuarios/' . $user->id, 'active' => true],
        ];

        return view('admin.users.show', compact('breadcrumb', 'user', 'user_types', 'systems'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdate $request, $system_slug, $id)
    {
        // Updating user
        $user = User::find($id);
        $user->update($request->except('systems'));

        // Reassociating systems
        $user->systems()->sync($request->systems);

        // $tickets = $user->type == 'courtesies' ? $request->tickets : null;
        // $user->ticket_types()->sync($tickets);

        return redirect()->route('system.users.show', [session('system_slug'), $user->id])
            ->with('success', 'Usuario actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loginAPI(Request $request)
    {
        // If user sent is an email
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            // Checking for users in table
            if (\Auth::attempt(['email' => $request->email, 'password' => $request->password, 'active' => 1])) {
                // , 'active' => 1 , 'type' => 'admin'
                $user = User::find(\Auth::user()->id);

                // Checking if user has app access
                $return['error'] = false;
                $return['user'] = $user->userAPI();
            } else {
                $return['error'] = true;
                $return['error_message'] = "Usuario/o password inválido";
            }
        } else {
            $return['error'] = true;
            $return['error_message'] = "Email inválido";
        }

        return $return;
    }

    public function syncUsers()
    {
        $users = array();
        foreach (User::qrServerUsers() as $user) {
            $users[] = [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'password' => $user->password,
                'type' => $user->type,
            ];
        }

        return $users;
    }

    public function user_profile($id)
    {
        $user = User::find($id);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/dashboard', 'active' => false],
            ['label' => 'Mi perfil', 'url' => '/', 'active' => true],
        ];

        return view('user.profile', compact('user', 'breadcrumb'));
    }

    public function update_profile(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->except('_token'));

        return back()->with('success', 'Información actualizada correctamente');
    }

    public function unsubscribe($id)
    {
        $user = User::find($id);
        $user->update(['active' => 0]);

        Auth::logout();
        return redirect('/');
    }

    public function client_create()
    {
        return view('create_user');
    }

    public function create_client(Request $request)
    {
        $user = User::create(['name' => $request->name, 'email' => $request->email, 'password' => bcrypt($request->password), 'type' => 'client']);
        Mail::to($user->email)->send(new Welcom());
        return redirect('/');
    }

    public function register_client(Request $request)
    {
        $response = ['error' => false];

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        return $response;
    }
}
