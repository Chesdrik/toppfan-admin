<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Venue;
use App\System;
use Response;

class VenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($system_slug)
    {
        $system = System::bySlug($system_slug);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Sedes', 'url' => '/admin/sedes', 'active' => true],
        ];
        $venues = $system->venues;

        return view('admin.venues.list', compact('breadcrumb', 'venues'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($system_slug, $id)
    {
        $venue = Venue::find($id);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Sedes', 'url' => '/admin/sedes', 'active' => false],
            ['label' => $venue->name, 'url' => '/admin/sedes/' . $venue->id, 'active' => true],
        ];

        return view('admin.venues.show', compact('breadcrumb', 'venue'));
    }

    public function list_user()
    {
        return view('user.venues.list', [
            'venues' => Venue::list(),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => '', 'active' => false],
                ['label' => 'Equipos', 'url' => route('teams'), 'active' => true]
            ]
        ]);
    }

    public function events_by_venue($system_slug, $id)
    {
        return view('user.venues.events', [
            'venue' => Venue::find($id),
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => '', 'active' => false],
                ['label' => 'Equipos', 'url' => route('teams'), 'active' => false],
                ['label' => 'Eventos de ' . Venue::find($id)->pretty_team(), 'url' => route('team_events', Venue::find($id)->id), 'active' => false],
            ]
        ]);
    }

    // Get venue tickets for AJAX petition
    public function getTickets($id)
    {
        $data = array();
        try {
            $venue = Venue::find($id);

            foreach ($venue->ticket_types as $ticket) {
                $data[$ticket->id . '-' . $ticket->price] = $ticket->name;
            }

            return ['error' => false, 'data' => $data];
        } catch (\Throwable $th) {
            return ['error' => true, 'data' => $data];
        }
    }

    public function syncVenues(Request $request)
    {
        $venues = array();

        foreach (Venue::all() as $venue) {
            $venues[] = [
                'id' => $venue->id,
                'name' => $venue->name,
                'lat' => $venue->lat,
                'long' => $venue->long,
            ];
        }

        return Response::json([
            'error' => false,
            'venues' => $venues
        ]);
    }

    public function config($system_slug, $id)
    {
        $venue = Venue::find($id);
        return view('admin.venues.main', [
            'venue' => $venue,
            'breadcrumb' => [
                ['label' => 'Dashboard', 'url' => '', 'active' => false],
                // ['label' => 'Equipos', 'url' => route('teams'), 'active' => false],
                ['label' => 'Configuración ' . $venue->name, 'url' => route('system.venues.config', [session('system_slug'), $id]), 'active' => true],
            ]
        ]);
    }
}
