<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\System;
use App\Venue;
use App\Zone;

class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($system_slug, $venue_id)
    {
        $system = System::bySlug($system_slug);
        $venue = Venue::find($venue_id);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Sedes', 'url' => '/admin/sedes', 'active' => true],
            ['label' => $venue->name, 'url' => "/admin/sedes/{$venue->id}", 'active' => true],
            ['label' => 'Zonas', 'url' => "#", 'active' => false],
        ];

        return view('admin.zones.index', compact('breadcrumb', 'venue'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($system_slug, $venue_id, $id)
    {
        $system = System::bySlug($system_slug);
        $venue = Venue::find($venue_id);
        $zone = Zone::find($id);
        $breadcrumb = [
            ['label' => 'Dashboard', 'url' => '/admin/dashboard', 'active' => false],
            ['label' => 'Sedes', 'url' => '/admin/sedes', 'active' => true],
            ['label' => $venue->name, 'url' => "/admin/sedes/{$venue->id}", 'active' => true],
            ['label' => 'Zonas', 'url' => route('system.venues.zones.index', [$system_slug, $venue->id]), 'active' => true],
            ['label' => $zone->name, 'url' => "#", 'active' => false],
        ];

        return view('admin.zones.show', compact('breadcrumb', 'venue', 'zone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $system_slug, $venue_id, $id)
    {
        //
        $zone_content = str_replace('                 ', '\\
', $request->svg_content);

        // dd($zone_content);
        $zone = Zone::find($id);
        $zone->update([
            'name' => $request->name,
            'type' => $request->type,
            'active' => ($request->active == 'on' ? 1 : 0),
            'svg_id' => $request->svg_id,
            'label_matrix' => $request->label_matrix,
            'label_x' => $request->label_x,
            'label_y' => $request->label_y,
            'fill' => $request->fill,
            'svg_content' => $zone_content
        ]);

        return back()->with('success', 'Información actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download_config($system_slug, $venue_id)
    {
        $venue = Venue::find($venue_id);
        // $zones = Zone::whereIn('id', [80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125])->get();


        $file_content = "<script type=\"text/javascript\">

    // Defining perimeters
    var perimeters = [];


    // Definind Access
    var access = [];

    // Defining active zones
    window.active_zones = [];
";

        //         foreach ($zones as $zone) {
        //             $file_content .= "    window.active_zones[{$zone->id}] = true;
        // ";
        //         }


        $file_content .= "
    // Defining zones
    var zones = [];
";
        foreach ($venue->parent_zones() as $parent_zone) {
            foreach ($parent_zone->childZones() as $zone) {
                // \Log::info($zone->id . " - " . $zone->name);
                $ticket_type_id = $zone->ticket_types->first()->id;
                $file_content .= "    window.active_zones[{$ticket_type_id}] = true;
";

                $file_content .= "    zones.push({ // " . strtoupper($zone->name) . "
        id: '{$zone->svg_id}',
        type: '{$zone->type}',
        ticket_type_id: {$ticket_type_id},
        zone_id: {$zone->id},
        label: '{$zone->name}',
        labelMatrix: '{$zone->label_matrix}',
        labelX: {$zone->label_x},
        labelY: {$zone->label_y},
        fill: '{$zone->fill}',
        svg_content: '{$zone->svg_content}',
    });

";
            }
        }

        $file_content .= "</script>";

        return response()->attachment($file_content, 'stadium_configuration.blade.php');
    }
}
