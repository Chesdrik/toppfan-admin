<?php

namespace App\Http\Livewire\Events\Accreditations;

use Livewire\Component;
use Livewire\WithPagination;

class AccreditationsList extends Component
{
    // Pagination
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    // Event
    public $event;

    // Mount
    public function mount($event)
    {
        $this->event = $event;
    }

    // Render
    public function render()
    {
        return view('livewire.events.accreditations.accreditations-list', [
            'accreditations' => $this->event->assigned_accreditations()->where('type', 'operative')->paginate(15)
        ]);
    }
}
