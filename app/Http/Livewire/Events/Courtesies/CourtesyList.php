<?php

namespace App\Http\Livewire\Events\Courtesies;

use Livewire\Component;
use Livewire\WithPagination;

class CourtesyList extends Component
{
    // Pagination
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    // Event
    public $event;

    // Mount
    public function mount($event)
    {
        $this->event = $event;
    }

    // Render
    public function render()
    {
        return view('livewire.events.courtesies.courtesy-list', [
            'courtesies' => $this->event->courtesy_orders()->paginate(15)
        ]);
    }
}
