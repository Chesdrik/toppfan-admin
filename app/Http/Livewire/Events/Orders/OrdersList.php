<?php

namespace App\Http\Livewire\Events\Orders;

use Livewire\Component;
use Livewire\WithPagination;

class OrdersList extends Component
{
    // Pagination
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    // Event
    public $event;

    // Mount
    public function mount($event)
    {
        $this->event = $event;
    }

    // Render
    public function render()
    {
        return view('livewire.events.orders.orders-list', [
            'orders' => $this->event->regular_orders()->paginate(15)
        ]);
    }
}
