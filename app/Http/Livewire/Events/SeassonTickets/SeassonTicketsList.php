<?php

namespace App\Http\Livewire\Events\SeassonTickets;

use Livewire\Component;
use Livewire\WithPagination;

class SeassonTicketsList extends Component
{
    // Pagination
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    // Event
    public $event;

    // Mount
    public function mount($event)
    {
        $this->event = $event;
    }

    // Render
    public function render()
    {
        return view('livewire.events.seasson-tickets.seasson-tickets-list', [
            'seasson_tickets' => $this->event->assigned_accreditations()->where('type', 'season_ticket')->paginate(15)
        ]);
    }
}
