<?php

namespace App\Http\Livewire\Events\TicketGenerator;

use Livewire\Component;
use Illuminate\Support\Facades\Artisan;

class TicketGenerator extends Component
{
    // Event
    public $eventId;
    public $orderAmount;
    public $ticketsPerOrder;
    public $ticket_type_id;
    public $type;
    public $courtesy;
    public $ticketPrice;


    // Mount
    public function mount($eventId, $courtesy)
    {
        $this->eventId = $eventId;
        $this->courtesy = $courtesy;
    }

    public function render()
    {
        return view('livewire.events.ticket-generator.ticket-generator', [
            ''
        ]);
    }

    public function createOrders()
    {
        // Cleaning DB
        // Artisan::call('clean:server');
    }

    // create:orders {event_id} {order_amount} {tickets_per_order} {ticket_type_id} {user_id} {type} {courtesy} {ticket_price}
    // php artisan create:orders 71 133 1 313 1 other true 0.00
}
