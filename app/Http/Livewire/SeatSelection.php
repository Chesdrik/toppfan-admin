<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Http;
use Livewire\Component;
use Illuminate\Http\Request;
use App\Event;
use App\Zone;
use App\TicketType;

class SeatSelection extends Component
{
    // Component variables
    public $eventId;
    public $seatLayoutData;
    public $name;
    public $zoneId;
    public $seat_colors;
    public $zone_data;
    public $event_data;
    public $responseErrors;
    public $type;

    // Defining listeners
    protected $listeners = [
        'updateZone' => 'updateZone',
        'saveUpdates' => 'saveUpdates',
        'updateHolding' => 'updateHolding',
        'updateHoldingPercengage' => 'updateHoldingPercengage',
        'updateHoldingPattern' => 'updateHoldingPattern',
        'zoneData' => 'zoneData'
    ];

    // Mounting livewire component
    public function mount($eventId, $type)
    {
        $this->eventId = $eventId;
        $this->seatLayoutData = null;
        $this->name = null;
        $this->zoneId = null;
        $this->responseErrors = null;
        $this->seat_colors = [
            'available' => '#B3E5FC',
            'selected' => '#8BC34A', // '#CDDC39', '#4CAF50'
            'on_hold' => '#BDBDBD'
        ];
        $this->type = $type;

        $this->reDraw($type);
    }

    // Rendering view
    public function render()
    {
        return view('livewire.seat-selection');
    }

    // Redrawing canvas
    public function reDraw($type, $response_errors = "")
    {
        if (!is_null($this->eventId) && !is_null($this->zoneId)) {
            // Requesting event data from main server
            $request = new Request();
            $request->merge([
                'zone_id' => $this->zoneId,
                'event_id' => $this->eventId,
                'type' => $this->type
            ]);

            $zone = Zone::find($this->zoneId);


            if ($zone->type == 'row') {
                // Requesting seating layout
                $response = \App::call('App\Http\Controllers\POS\POSController@seating_layout', ['request' => $request]);

                // Updating variables
                $this->name = $response['seat_layout_data']['zone_name'];
                $this->seatLayoutData = $response['seat_layout_data']['rows'];
                $this->responseErrors = ($response_errors == "" ? null : $response_errors);

                // Dispatching view-updated event
                // dd($this->seatLayoutData);
                $this->dispatchBrowserEvent('view-updated', ['seatLayoutData' => json_encode($this->seatLayoutData), 'responseErrors' => $this->responseErrors]);

                // Updating graph data
                if ($this->type == 'event') {
                    \Artisan::call('ticket:status ' . $this->eventId);
                } else {
                    $ticket_type = TicketType::where('zone_id', $zone->id)->first();
                    \Artisan::call("eventGroup:ticketStatus {$this->eventId} {$ticket_type->id}");
                }

                $this->zoneData($this->zoneId, $this->eventId, $this->type);
            } else {
                // dd('general!');
                $this->dispatchBrowserEvent('hide-seat-layout');
            }
        } else {
            // Updating variables
            $this->seatLayoutData = null;
            $this->name = null;
            $this->zoneId = null;
            $this->responseErrors = null;
        }
    }

    // Updating zone
    public function updateZone($zoneId)
    {
        $this->zoneId = $zoneId;
        $this->reDraw($this->type);
    }

    // Saving seat status for holding / available / courtesy sale
    public function saveUpdates($seats_to_hold, $seats_to_release, $seats_to_courtesy_sale)
    {
        // Reseting responseErrors
        $response_errors = "";

        // Saving on_hold seats in server
        $response = $this->saveSeatsToHold($seats_to_hold);
        if ($response['error'] == true) {
            $response_errors .= $response['message'] . '<br />';
        }

        // Saving available seats in server
        $response = $this->saveSeatsAvailable($seats_to_release);
        if ($response['error'] == true) {
            $response_errors .= $response['message'] . '<br />';
        }

        // Saving courtesy seats sale
        $response = $this->saveSeatsCourtesySale($seats_to_courtesy_sale);
        if ($response['error'] == true) {
            $response_errors .= $response['message'] . '<br />';
        }

        // Redrawing canvas
        $this->reDraw($response_errors);
    }

    // Marking seats to on_hold
    public function saveSeatsToHold($seats_to_hold)
    {
        // Cleaning array
        $seats_to_hold = array_filter($seats_to_hold);

        // Saving on_hold seats in main server
        if (!empty($seats_to_hold)) {
            // Creating request
            $request = new Request();
            $request->merge([
                'tickets' => $seats_to_hold,
                'event_id' => $this->eventId,
                'type' => $this->type
            ]);

            // Requesting data
            return \App::call('App\Http\Controllers\HoldingController@seats_set_on_hold', ['request' => $request]);
        } else {
            return ['error' => false];
        }
    }

    // Marking seats as available
    public function saveSeatsAvailable($seats_to_release)
    {
        // Cleaning array
        $seats_to_release = array_filter($seats_to_release);

        // Saving on_hold seats in main server
        if (!empty($seats_to_release)) {
            // Creating request
            $request = new Request();
            $request->merge([
                'tickets' => $seats_to_release,
                'event_id' => $this->eventId,
                'type' => $this->type
            ]);

            // Requesting data
            return \App::call('App\Http\Controllers\HoldingController@seats_set_available', ['request' => $request]);
        } else {
            return ['error' => false];
        }
    }

    // Saving courtesy sales
    public function saveSeatsCourtesySale($seats_to_courtesy_sale)
    {
        // Cleaning array
        $seats_to_courtesy_sale = array_filter($seats_to_courtesy_sale);
        // dd($seats_to_courtesy_sale);

        // Saving on_hold seats in main server
        if (!empty($seats_to_courtesy_sale)) {
            // Creating request
            $request = new Request();
            $request->merge([
                'tickets' => $seats_to_courtesy_sale,
                'event_id' => $this->eventId,
                'type' => $this->type
            ]);

            // Requesting data
            return \App::call('App\Http\Controllers\HoldingController@seats_courtesy_sale', ['request' => $request]);
        } else {
            return ['error' => false];
        }
    }

    // Saving holding by total
    public function updateHolding($action)
    {
        if ($action == 'unblock_all') {
            \App::call('App\Http\Controllers\HoldingController@unblock_all', ['event_id' => $this->eventId, 'zone_id' => $this->zoneId, 'type' => $this->type]);
        } elseif ($action == 'block_all') {
            \App::call('App\Http\Controllers\HoldingController@block_all', ['event_id' => $this->eventId, 'zone_id' => $this->zoneId, 'type' => $this->type]);
        }

        // Redraw canvas
        $this->reDraw($this->type);
    }

    // Saving holding by percentage
    public function updateHoldingPercengage($percentage)
    {
        // Calling controller action
        \App::call('App\Http\Controllers\HoldingController@block_percentage', ['event_id' => $this->eventId, 'zone_id' => $this->zoneId, 'percentage' => $percentage, 'type' => $this->type]);

        // Redraw canvas
        $this->reDraw($this->type);
    }

    // Saving holding by pattern
    public function updateHoldingPattern($pattern)
    {
        // Calling controller action
        \App::call('App\Http\Controllers\HoldingController@block_pattern', ['event_id' => $this->eventId, 'zone_id' => $this->zoneId, 'pattern' => $pattern, 'type' => $this->type]);

        // Redraw canvas
        $this->reDraw($this->type);
    }


    // Updating graph data
    public function zoneData($zone_id, $event_id, $type)
    {
        if (!is_null($this->eventId) && !is_null($this->zoneId)) {
            $this->selected_zone = $zone_id;
            $event = Event::find($event_id);

            if ($this->type == 'event') {
                $stats = $event->ticket_status();
                $this->zone_data = $stats['data']['zones'][$zone_id];
                $this->event_data = $stats['data']['general'];
            } else {
                $this->zone_data = array();
                $this->event_data = array();
            }

            \Log::info($this->zone_data);


            $this->dispatchBrowserEvent('zoneData', ['data' => $this->zone_data, 'zone_id' => $this->selected_zone, 'event_data' => $this->event_data]);
        } else {
            $this->selected_zone = null;
            $this->zone_data = null;
        }
    }
}
