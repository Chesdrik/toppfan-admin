<?php

namespace App\Http\Livewire\TicketDistributionSystem;

use Livewire\Component;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\ClientUser;
use App\Order;
use App\Ticket;
use App\TicketType;
use App\Event;
use App\TicketDistributionSystem\TicketGroup;
use Auth;
use Validator;

use App\Jobs\SendTicketLotDistributionEmail;

class AssignTicketGroup extends Component
{
    public $ticketGroupId;
    public $ticketGroup;
    public $user;
    public $assignedTicketLots;
    public $name;
    public $last_name;
    public $email;
    public $email_confirmation;
    public $amount;
    public $distributor;
    public $eventId;

    protected $listeners = [
        'triggerGroupConfirm',
        'group_confirmed',
        'group_cancelled',
        'group_submit'
    ];

    // Validation rules
    protected $rules = [
        'name' => 'required|min:3',
        'last_name' => 'required|min:3',
        'email' => 'required|email|required_with:email_confirmation|same:email_confirmation',
        'email_confirmation' => 'required|email',
        'amount' => 'required|numeric|digits_between:1,4',
        'distributor' => 'required'
    ];

    // Validation messages
    protected $messages = [
        'name.required' => 'Es necesario ingresar el nombre',
        'name.min' => 'El nombre debe de ser de al menos tres caracteres de longitud',
        'last_name.required' => 'Es necesario ingresar los apellidos',
        'last_name.min' => 'El apellido debe de ser de al menos tres caracteres de longitud',
        'email.required' => 'Es necesario ingresar el email',
        'email.email' => 'El email ingresado no es válido',
        'email_confirmation.required' => 'Es necesario ingresar la confirmación del email',
        'email_confirmation.email' => 'La confirmación del email ingresado no es válida',
        'email.same' => 'El email no coincide con su confirmación',
        'amount.required' => 'Es necesario ingresar la cantidad de boletos a asignar',
        'amount.numeric' => 'La cantidad debe ser numérica',
        'amount.digits_between' => 'La cantidad debe de ser entre :min y :max números',
        'distributor.required' => 'Es necesario indicar si el usuario será distribuidor'
    ];

    public function triggerGroupConfirm(){
        $string = ($this->amount == 1) ? 'boleto' : 'boletos';
        $this->confirm('Enviarás '.$this->amount.' '.$string.' de la zona '. $this->ticketGroup->ticket_type->name."\n".' a '.$this->name.' '.$this->last_name."\n".'con email '.$this->email."\n \n".'¿Deseas continuar?', [
            'toast' => false,
            'position' => 'center',
            'confirmButtonText' =>  'Continuar', 
            'cancelButtonText' =>  'Cancelar',
            'onConfirmed' => 'group_confirmed',
            'onCancelled' => 'group_cancelled'
        ]);
    }

    public function group_confirmed(){
        $this->alert(
            'success',
            'Procesando...'
        );

        $this->emit('group_submit');
    }

    public function group_cancelled(){}

    public function mount($ticketGroupId)
    {
        $user = ClientUser::find(Auth::guard('client_users')->id());
        $ticketGroup = TicketGroup::find($ticketGroupId);

        $assignedTicketLots = array();
        foreach(TicketGroup::where('assigned_by', $user->id)->get() as $ticket_group){
            $this->assignedTicketLots[] = array_merge($assignedTicketLots, $ticket_group->assigned_ticket_lots()); 
        }

        $this->user = $user;
        $this->ticketGroupId = $ticketGroupId;
        $this->ticketGroup = $ticketGroup;
        $eventId = $ticketGroup->event_id;
        $this->eventId = $eventId;
    }

    public function group_form_validation(){
        if($this->email == Auth::guard('client_users')->user()->email){
            session()->flash('lot_error', 'No está permitido autoasignarse grupos de boletos.');

            return redirect()->to(route('tds.assign', $this->ticketGroupId));
        }

        // Cleaning strings
        $this->email = trim($this->email);
        $this->email_confirmation = trim($this->email_confirmation);
        $this->amount = trim($this->amount);

        $validatedData = $this->validate();

        $ticketGroup = TicketGroup::find($this->ticketGroupId);

        if($this->amount > $ticketGroup->available){
            session()->flash('lot_error', 'Debes asignar una cantidad menor o igual a la disponible.');

            return redirect()->to(route('tds.assign', $this->ticketGroupId));
        }

        $this->emit('triggerGroupConfirm');
    }

    public function group_submit() {
        $ticketGroup = TicketGroup::find($this->ticketGroupId);

        // Checking if user exists
        $user = ClientUser::where('email', $this->email)->first();
        if(!$user){
            $user = ClientUser::create([
                'name' => $this->name,
                'last_name' => $this->last_name,
                'email' => $this->email,
                'password' => Hash::make(Str::random(15)),
                'type' => 'temporary',
                'active' => 0,
            ]);
        }

        // Creating ticket group
        $new_ticket_group = TicketGroup::create([
            'parent_ticket_group' => $this->ticketGroupId,
            'event_id' => $ticketGroup->event_id,
            'client_user_id' => $user->id,
            'assigned_by' => Auth::guard('client_users')->id(),
            'ticket_type_id' => $ticketGroup->ticket_type_id,
            'amount' => $this->amount,
            'available' => $this->amount,
            'distributable' => $this->distributor
        ]);

        // Updating available tickets
        $assigned = $ticketGroup->assigned + $this->amount;
        $ticketGroup->update(['assigned' => $assigned, 'available' => $ticketGroup->amount - $assigned - $ticketGroup->sent_to_fans]);

        // Preparing data for mail
        $data = [
            'id' => $user->id,
            'email' => $this->email,
            'ticket_type_name' => $ticketGroup->ticket_type->name,
            'event_name' => $ticketGroup->event->name,
            'amount' => $this->amount,
            'activation_pending' => ($user->active == 1) ? false : true,
            'group' => false,
            'date' => pretty_short_date($new_ticket_group->created_at)
        ];

        // Dispatching email job
        SendTicketLotDistributionEmail::dispatch($data);

        // session()->flash('lot_success', array($this->name.' '.$this->last_name, $this->email, $this->amount));
        session()->flash('lot_success', 'Grupo de boleto asignado correctamente');

        return redirect()->to(route('tds.assign', $this->ticketGroupId));
    }

    public function render()
    {
        return view('livewire.ticket-distribution-system.assign-ticket-group');
    }
}
