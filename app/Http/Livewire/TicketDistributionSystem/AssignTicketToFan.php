<?php

namespace App\Http\Livewire\TicketDistributionSystem;

use Livewire\Component;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\ClientUser;
use App\Order;
use App\Ticket;
use App\TicketType;
use App\Event;
use App\TicketDistributionSystem\TicketGroup;
use Auth;

use App\Jobs\SendTicketDistributionEmail;

class AssignTicketToFan extends Component
{
    public $ticketGroupId;
    public $ticketGroup;
    public $user;
    public $assignedTickets;
    public $name;
    public $last_name;
    public $email;
    public $email_confirmation;
    public $amount;
    public $distributor;
    public $eventId;

    protected $listeners = [
        'triggerConfirm',
        'confirmed',
        'cancelled',
        'submit'
    ];

    // Validation rules
    protected $rules = [
        'name' => 'required|min:3',
        'last_name' => 'required|min:3',
        'email' => 'required|email|required_with:email_confirmation|same:email_confirmation',
        'email_confirmation' => 'required|email',
        'amount' => 'required|numeric|digits_between:1,4',
    ];

    // Validation messages
    protected $messages = [
        'name.required' => 'Es necesario ingresar el nombre',
        'name.min' => 'El nombre debe de ser de al menos tres caracteres de longitud',
        'last_name.required' => 'Es necesario ingresar los apellidos',
        'last_name.min' => 'El apellido debe de ser de al menos tres caracteres de longitud',
        'email.required' => 'Es necesario ingresar el email',
        'email.email' => 'El email ingresado no es válido',
        'email_confirmation.required' => 'Es necesario ingresar la confirmación del email',
        'email_confirmation.email' => 'La confirmación del email ingresado no es válida',
        'email.same' => 'El email no coincide con su confirmación',
        'amount.required' => 'Es necesario ingresar la cantidad de boletos a asignar',
        'amount.numeric' => 'La cantidad debe ser numérica',
        'amount.digits_between' => 'La cantidad debe de ser entre :min y :max números'
    ];

    public function triggerConfirm(){
        $string = ($this->amount == 1) ? 'boleto' : 'boletos';
        $this->confirm('Enviarás '.$this->amount.' '.$string.' de la zona '. $this->ticketGroup->ticket_type->name."\n".' a '.$this->name.' '.$this->last_name."\n".'con email '.$this->email."\n \n".'¿Deseas continuar?', [
            'toast' => false,
            'position' => 'center',
            'confirmButtonText' =>  'Continuar', 
            'cancelButtonText' =>  'Cancelar',
            'onConfirmed' => 'confirmed',
            'onCancelled' => 'cancelled'
        ]);
    }

    public function confirmed(){
        $this->alert(
            'success',
            'Procesando...'
        );

        $this->emit('submit');
    }

    public function cancelled(){}

    public function mount($ticketGroupId)
    {
        $ticketGroup = TicketGroup::find($ticketGroupId);
        $user = ClientUser::find(Auth::guard('client_users')->id());
        $assignedTickets = array();

        foreach(Order::where('event_id', $ticketGroup->event_id)->where('type', 'courtesy')->get() as $order){
            if(count($order->tickets->where('assigned_by', $user->id)) > 0 ){
                $assignedTickets = array_merge($assignedTickets, $order->assigned_tickets());
            }
        }

        $this->user = $user;
        $this->assignedTickets = $assignedTickets;
        $this->ticketGroupId = $ticketGroupId;
        $this->ticketGroup = $ticketGroup;
        $eventId = $ticketGroup->event_id;
        $this->eventId = $eventId;
    }

    public function form_validation(){
        // Cleaning strings
        $this->email = trim($this->email);
        $this->email_confirmation = trim($this->email_confirmation);
        $this->amount = trim($this->amount);

        $validatedData = $this->validate();

        $ticketGroup = TicketGroup::find($this->ticketGroupId);

        if($this->amount > $ticketGroup->available){
            session()->flash('ticket_error', 'Debes asignar una cantidad menor o igual a la disponible.');

            return redirect()->to(route('tds.assign', $this->ticketGroupId));
        }

        $this->emit('triggerConfirm');
    }

    public function submit() {
        $ticketGroup = TicketGroup::find($this->ticketGroupId);
    
        // Checking if user exists
        $user = ClientUser::where('email', $this->email)->first();
        if(!$user){
            $user = ClientUser::create([
                'name' => $this->name,
                'last_name' => $this->last_name,
                'email' => $this->email,
                'password' => Hash::make(Str::random(15)),
                'type' => 'temporary',
                'active' => 0
            ]);
        }

        // Creating order
        $order = Order::create([
            'client_user_id' => $user->id,
            'event_id' => $ticketGroup->event_id,
            'event_type' => 'App\Event',
            'email' => $this->email,
            'total' => 0,
            'type' => 'courtesy',
            'payment_method' => 'cash',
            'selling_point' => 'ticket_office',
            'status' => 'payed',
            'assigned_by' => Auth::guard('client_users')->id()
        ]);


        for($i = 1; $i <= $this->amount; $i++){
            // Creating ticket
            $ticket = Ticket::create([
                'ticket_type_id' => $ticketGroup->ticket_type_id,
                'order_id' => $order->id,
                'event_id' => $ticketGroup->event_id,
                'event_type' => 'App\Event',
                'amount' => 1,
                // 'amount' => $this->amount,
                'price' => 0,
                'name' => $this->name.' '.$this->last_name,
                'type' => 'courtesy',
                'assigned_by' => Auth::guard('client_users')->id()
            ]);
        }

        // Updating available tickets
        $sent_to_fans = $ticketGroup->sent_to_fans + $this->amount;
        $ticketGroup->update(['sent_to_fans' => $sent_to_fans, 'available' => $ticketGroup->amount - $sent_to_fans - $ticketGroup->assigned]);

        // Preparing data for mail
        $data = [
            'id' => $user->id,
            'email' => $this->email,
            'ticket_type_name' => $ticketGroup->ticket_type->name,
            'event_name' => $ticketGroup->event->name,
            'activation_pending' => ($user->active == 1) ? false : true,
            'group' => false,
            'amount' => $this->amount,
            'date' => pretty_short_date($order->created_at),
            'order_id' => $order->id,
            'username' => $user->name
        ];

        // Dispatching email job
        SendTicketDistributionEmail::dispatch($data);

        // session()->flash('ticket_success', array($this->name.' '.$this->last_name, $this->email, $this->amount));
        session()->flash('ticket_success', 'Boleto(s) enviado(s) correctamente');

        return redirect()->to(route('tds.assign', $this->ticketGroupId));
    }

    public function render()
    {
        return view('livewire.ticket-distribution-system.assign-ticket-to-fan');
    }
}

