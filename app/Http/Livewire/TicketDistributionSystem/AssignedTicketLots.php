<?php

namespace App\Http\Livewire\TicketDistributionSystem;

use Livewire\Component;

use App\ClientUser;
use App\Event;
use App\TicketDistributionSystem\TicketGroup;
use Auth;


class AssignedTicketLots extends Component
{
    public $assignedTicketLots;
    public $eventId;
    public $ticketGroupId;
    public $ticketGroup;

    public function mount($ticketGroupId, $eventId)
    {
        $user = ClientUser::find(Auth::guard('client_users')->id());
        $assignedTicketLots = array();
        if($ticketGroupId){
            foreach(TicketGroup::where('parent_ticket_group', $ticketGroupId)->where('assigned_by', $user->id)->get() as $ticket_group){
                $this->assignedTicketLots[] = array_merge($assignedTicketLots, $ticket_group->assigned_ticket_lots()); 
            }
            
        }else{
            foreach(TicketGroup::where('assigned_by', $user->id)->get() as $ticket_group){
                $this->assignedTicketLots[] = array_merge($assignedTicketLots, $ticket_group->assigned_ticket_lots()); 
            }
        }

        $ticketGroup = TicketGroup::find($ticketGroupId);
        $this->ticketGroup = $ticketGroup;
        $this->eventId = $eventId;
    }

    public function render()
    {
        return view('livewire.ticket-distribution-system.assigned-ticket-lots');
    }
}
