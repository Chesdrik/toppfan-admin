<?php

namespace App\Http\Livewire\TicketDistributionSystem;

use Livewire\Component;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\ClientUser;
use App\Order;
use App\Ticket;
use App\TicketType;
use App\Event;
use App\TicketDistributionSystem\TicketGroup;
use Auth;

use App\Jobs\SendTicketDistributionEmail;

class AssignedTickets extends Component
{
    public $assignedTickets;
    public $totalAssignedTickets;
    public $eventId;
    public $ticketGroupId;
    public $ticketGroup;


    public function mount($ticketGroupId, $eventId)
    {
        $user = ClientUser::find(Auth::guard('client_users')->id());
        $ticketGroup = TicketGroup::find($ticketGroupId);

        $assignedTickets = array();
        $this->totalAssignedTickets = 0;

        foreach (Order::where('assigned_by', $user->id)->where('type', 'courtesy')->where('event_id', $eventId)->get() as $order) {
            if ($ticketGroupId) {
                $this->totalAssignedTickets += $order->tickets->where('assigned_by', $user->id)->where('event_id', $eventId)->where('ticket_type_id', $ticketGroup->ticket_type_id)->count();

                if ($order->tickets->where('assigned_by', $user->id)->where('event_id', $eventId)->where('ticket_type_id', $ticketGroup->ticket_type_id)->count() > 0) {
                    $assignedTickets = array_merge($assignedTickets, $order->assigned_tickets());
                }
            } else {
                $this->totalAssignedTickets += $order->tickets->where('assigned_by', $user->id)->count();

                if ($order->tickets->where('assigned_by', $user->id)->count() > 0) {
                    $assignedTickets = array_merge($assignedTickets, $order->assigned_tickets());
                }
            }
        }

        $this->assignedTickets = $assignedTickets;
        $this->ticketGroup = $ticketGroup;
        $this->eventId = $eventId;
    }

    public function render()
    {
        return view('livewire.ticket-distribution-system.assigned-tickets');
    }
}
