<?php

namespace App\Http\Livewire\TicketDistributionSystem;

use Livewire\Component;
use App\ClientUser;
use App\TicketDistributionSystem\TicketGroup;

use Auth;

class ShowAssignedTicketLots extends Component
{
    public $assignedTicketLots;

    public function mount($eventId)
    {
        $user = ClientUser::find(Auth::guard('client_users')->id());
        $assignedTicketLots = array();
        foreach(TicketGroup::where('client_user_id', $user->id)->get() as $ticket_group){
            $this->assignedTicketLots[] = array_merge($assignedTicketLots, $ticket_group->assigned_ticket_lots()); 
        }

        $this->eventId = $eventId;
    }

    public function render()
    {
        return view('livewire.ticket-distribution-system.show-assigned-ticket-lots');
    }
}
