<?php

namespace App\Http\Livewire\TicketDistributionSystem;

use Livewire\Component;
use App\ClientUser;

use Auth;

class ShowAssignedTickets extends Component
{
    public $assignedTickets;

    public function mount($eventId)
    {
        $user = ClientUser::find(Auth::guard('client_users')->id());
        $assignedTickets = array();
        foreach($user->orders->where('event_id', $eventId)->where('type', 'courtesy') as $order){
            $assignedTickets = array_merge($assignedTickets, $order->assigned_tickets());
        }

        $this->assignedTickets = $assignedTickets;
        $this->eventId = $eventId;
    }

    public function render()
    {
        return view('livewire.ticket-distribution-system.show-assigned-tickets');
    }
}
