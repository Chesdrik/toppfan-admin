<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Venue;

class TicketTypesForVenue extends Component
{
    public $venues;
    public $ticketTypes;

    public $selectedVenue = null;

    public function mount($venues)
    {
        $this->venues = $venues;
        $this->ticketTypes = collect();
    }

    public function render()
    {
        return view('livewire.ticket-types-for-venue');
    }

    public function updatedSelectedVenue($venue_id)
    {
        if ($venue_id == 0) {
            $this->selectedVenue = null;
            $this->ticketTypes = null;
        } else {
            $this->selectedVenue = $venue_id;
            $venue = Venue::find($this->selectedVenue);
            $this->ticketTypes = $venue->ticket_templates()->pluck('name', 'id');
        }
    }
}
