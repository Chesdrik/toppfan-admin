<?php

namespace App\Http\Requests\Access;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class AccessCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return (in_array(Auth::user()->type, ['admin', 'supervisor', 'access']) ? true : false);
    }

    public function rules()
    {
        return [
            'visitor_name' => 'required',
            'start' => 'required',
            'end' => 'required',
            'type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'visitor_name.required' => 'Es necesario ingresar el nombre del visitante.',
            'start.required' => 'Es necesario ingresar el inicio de la visita.',
            'end.required' => 'Es necesario ingresar el fin de la visita.',
            'type.required' => 'Es necesario ingresar el tipo de acceso.',
        ];
    }
}
