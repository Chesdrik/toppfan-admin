<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventGroupStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => ['required', 'unique:event_groups', 'regex:/^[a-z0-9]+(?:[_|-][a-z0-9]+)*$/'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Es necesario ingresar un nombre',
            'slug.required' => 'Es necesario ingresar un slug',
            'slug.unique' => 'El slug ingresado ya existe en el sistema',
            'slug.regex' => 'El slug no debe contener espacios en blanco. Use _ ó - en su lugar.',
        ];
    }
}
