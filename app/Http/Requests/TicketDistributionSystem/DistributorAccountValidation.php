<?php

namespace App\Http\Requests\TicketDistributionSystem;

use Illuminate\Foundation\Http\FormRequest;

class DistributorAccountValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'password' => 'required|required_with:password_confirmation|same:password_confirmation|min:6',
            'password_confirmation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Es necesario ingresar tu nombre.',
            'name.min' => 'El nombre debe de ser de al menos tres caracteres de longitud.',
            'last_name.required' => 'Es necesario ingresar tus apellidos.',
            'last_name.min' => 'El nombre debe de ser de al menos tres caracteres de longitud.',
            'password.required' => 'Es necesario ingresar tu contraseña.',
            'password_confirmation.required' => 'Es necesario ingresar la confirmación de contraseña.',
            'password.same' => 'La contraseña no coincide con la confirmación.',
            'password.min' => 'La contraseña debe contener al menos 6 caracteres.',
        ];
    }
}
