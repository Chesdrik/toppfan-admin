<?php

namespace App\Http\Requests\TicketDistributionSystem;

use Illuminate\Foundation\Http\FormRequest;

class SendTicketDistribution extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'last_name' => 'required|min:3',
            'email' => 'required|email'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Es necesario ingresar el nombre de la persona asignada.',
            'name.min' => 'El nombre debe de ser de al menos tres caracteres de longitud.',
            'last_name.required' => 'Es necesario ingresar los apellidos de la persona asignada.',
            'last_name.min' => 'El nombre debe de ser de al menos tres caracteres de longitud.',
            'email.required' => 'Es necesario ingresar el email de la persona asignada.',
            'email.email' => 'El email ingresado no es válido.'
        ];
    }
}
