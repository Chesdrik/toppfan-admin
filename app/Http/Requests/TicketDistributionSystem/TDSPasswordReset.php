<?php

namespace App\Http\Requests\TicketDistributionSystem;

use Illuminate\Foundation\Http\FormRequest;

class TDSPasswordReset extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|required_with:password_confirmation|same:password_confirmation|min:6',
            'password_confirmation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'Es necesario ingresar una contraseña.',
            'password.min' => 'La contraseña debe contener al menos 6 caracteres.',
            'password_confirmation.required' => 'Es necesario ingresar la confirmación de contraseña.',
            'password.same' => 'La contraseña no coincide con la confirmación.',
        ];
    }
}
