<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:8|confirmed',
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Es necesario ingresar un nombre',
            'email.required' => 'Es necesario ingresar un email',
            'email.unique' => 'El email ingresado ya existe en el sistema',
            'password.required' => 'Es necesario ingresar un password',
            'password.min' => 'El password tiene que ser de al menos 8 caracteres',
            'password.confirmed' => 'Los passwords no coinciden',
        ];
    }
}
