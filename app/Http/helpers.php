<?php

use App\TicketType;
use App\User;
use App\Checkpoint;
use Carbon\Carbon;

// Date formatting
function pretty_date($date)
{
    return substr($date, 0, 16);
}

function pretty_short_date($date)
{
    return substr($date, 0, 10);
}

function pretty_short_hour($hour)
{
    return substr($hour, -8, 5);
}

function months()
{
    return [
        '01' => '01',
        '02' => '02',
        '03' => '03',
        '04' => '04',
        '05' => '05',
        '06' => '06',
        '07' => '07',
        '08' => '08',
        '09' => '09',
        '10' => '10',
        '11' => '11',
        '12' => '12'
    ];
}

function years()
{
    $current = date('y');
    $years = array();
    for ($i = $current; $i <= $current + 10; $i++) {
        $years[$i] = $i;
    }
    return $years;
}

// Base 64 transform
function base64($data)
{
    return rtrim(strtr(base64_encode(json_encode($data)), '+/', '-_'), '=');
}

// Money format
function pretty_money($amount)
{
    return '$' . number_format($amount, 2, '.', ',');
}

function pretty_number($number, $decimals = 2)
{
    return number_format($number, $decimals, '.', ',');
}

function pumas_logo()
{
    return asset('assets/images/logo_pumas.png');
}


function parseo_email($string, $next = '')
{
    $next = $next == '' ? '' : '_' . $next;
    return strtolower(limpiar($string)) . $next . '@pumasgol.lampserv.com';
}

function limpiar($string)
{

    $string = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $string
    );

    //Reemplazamos la E y e
    $string = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $string
    );

    //Reemplazamos la I y i
    $string = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $string
    );

    //Reemplazamos la O y o
    $string = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $string
    );

    //Reemplazamos la U y u
    $string = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $string
    );

    //Reemplazamos la N, n, C y c
    $string = str_replace(
        array('Ñ', 'ñ', 'Ç', 'ç'),
        array('N', 'n', 'C', 'c'),
        $string
    );

    //Reemplazamos espacios
    $string = str_replace(
        array(' '),
        array('_'),
        $string
    );

    return $string;
}

function exists_email($email)
{
    $user = User::where('email', $email)->first();
    return !is_null($user) ? $user : false;
}


function days_short($i = null)
{
    $d = array('Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá', 'Do');

    return (!is_null($i) ? $d[$i] : $d);
}

function getStartAndEndDate($year, $week)
{
    $start = (new DateTime())->setISODate($year, $week);
    $end = (new DateTime())->setISODate($year, $week, 7);

    if ($start->format('m') != $end->format('m')) {
        $text = 'Semana del ' . $start->format('j') . ' de ' . parseMonth($start->format('n') - 1) . ' al ' . $end->format('j') . ' de ' . parseMonth($end->format('n') - 1);
    } else {
        $text = 'Semana del ' . $start->format('j') . ' al ' . $end->format('j') . ' de ' . parseMonth($end->format('n') - 1);
    }

    return [$start->format('Y-m-d'), $end->format('Y-m-d'), $text];
}

function parseMonth($month)
{
    $months = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    return $months[$month];
}

function nextAndPrevDate($date)
{
    $next = strtotime("+7 day", $date);
    $prev = strtotime("-7 day", $date);

    return [
        date('Y-m-d', $prev), // prev week
        date('Y-m-d', $next) // next week
    ];
}

function round_minutes($current)
{
    $minutes = explode(':', $current);
    return substr($minutes[1], 1);
}

function pretty_type_icon($type)
{
    switch ($type) {
        case 'access':
            $icon = 'long-arrow-tab';
            break;
        case 'exit':
            $icon = 'long-arrow-left';
            break;
        case 'error':
            $icon = 'arrow-missed';
            break;
        default:
            $icon = 'alert-circle-o';
            break;
    }

    return "<i class='zmdi zmdi-hc-fw zmdi-$icon'></i>";
}

function parser_device_stats($data)
{
    $parser = array();
    foreach ($data['data']['devices'] as $device => $readings) {
        $parser[] = [
            'label' => "'" . $device . "'",
            'data' => [$readings['string']],
            'backgroundColor' => [$readings['color']],
            'borderColor' => [$readings['color']],
            'borderWidth' => 1
        ];
    }

    return str_replace('"', '', json_encode($parser));
}

function parser_device_readings($ticket_data, $accreditation_data)
{
    $data = array_merge($ticket_data['data']['data'], $accreditation_data['data']['data']);
    $parser = array();
    foreach ($data as $device => $readings) {
        $parser[] = [
            'label' => "'" . $device . "'",
            'data' => [$readings['string']],
            'backgroundColor' => [$readings['color']],
            'borderColor' => [$readings['color']],
            'borderWidth' => 1
        ];
    }

    return str_replace('"', '', json_encode($parser));
}

function parser_pie_chart($data)
{
    $labels = '';
    $colors = '';
    $total = array();

    foreach ($data['data']['data'] as $device => $readings) {
        $labels .= '"' . $device . '",';
        $color = explode(')', $readings['color']);
        $colors .= $color[0] . ")',";
        $total[] = $readings['total_readings'];
    }

    return [
        'labels' => $labels,
        'colors' => $colors,
        'total' => $total
    ];
}

// function get_ticket_type($type)
// {
//     $type = str_replace("\r", '', $type);

//      TicketType
//      ALL_ACCESS
//      CANCHA
//      A1

//      switch ($type) {
//          case 'PUBLICIDAD':
//              return 1;
//              break;
//          case 'MEDICO':
//              return 2;
//              break;
//          case 'SANITIZER':
//              return 3;
//              break;
//          case 'DC':
//              return 4;
//              break;
//          case 'JUGADORES':
//              return 5;
//              break;
//          case 'PALOMAR VIP':
//              return 6;
//              break;
//          case 'MEDIOS':
//              return 7;
//              break;
//          case 'CANCHA':
//              return 8;
//              break;
//          case 'ALL INCLUSIVE':
//              return 9;
//              break;
//          default:
//              # code...
//              break;
//      }
// }

function pretty_type($type){
    switch ($type) {
        case 'cash':
            $type = 'Efectivo';
            break;
        case 'debit_card':
            $type = 'Tarjeta de débito';
            break;
        case 'credit_card':
            $type = 'Tarjeta de crédito';
            break;
        case 'ticket_office':
            $type = 'Taquilla';
            break;
        case 'web':
            $type = 'Web';
            break;
        case 'kiosk':
            $type = 'Kiosko';
            break;
        case 'registered':
            $type = 'Registrada';
            break;
        case 'payed':
            $type = 'Pagada';
            break;
        case 'cancelled':
            $type = 'Cancelada';
            break;
        default:
            $type = $type;
            break;
    }

    return $type;
}

function pretty_reading_type($type){
    switch ($type) {
        case 'access':
            return 'Acceso';
            break;
        case 'exit':
            return 'Salida';
            break;
        default:
            return $type;
            break;
    }
}

function pretty_ticket_type($type){
    switch ($type) {
        case 'parking':
            return 'Estacionamiento';
            break;
        case 'perimeter':
            return 'Perimetral';
            break;
        case 'tunnel':
            return 'Túnel';
            break;
        default:
            return $type;
            break;
    }
}

function ticket_data_parser($data, $reading_type){
    $parser = array();
    foreach($data['data'][$reading_type]['types'] as $id => $types){
        $parser[] = [
            'label' => TicketType::find($id)->name,
            'children' => aux_parser($types)
        ];
    }

    return json_encode($parser);
}

function aux_parser($types){
    $parser = array();
    foreach($types as $name => $type){
        $parser[] = [
            'label' => pretty_ticket_type($name),
            'children' => aux_parser2($type)
        ];
    }

    return $parser;
}

function aux_parser2($type){
    $parser = array();
    foreach($type as $id => $data){
        $parser[] = [
            'label' => Checkpoint::find($id)->name,
            'children' => [
                ['label' => 'Accesos pre autorizados: '.$data['availables']],
                ['label' => 'Accesos leídos: '.$data['access_reads']],
                ['label' => 'Salidas leídas: '.$data['exits_reads']],
                ['label' => 'Accesos restantes: '.$data['remainings']]
            ]
        ];
    }

    return $parser;
}

function checkpoint_data_parser($data, $reading_type){
    $parser = array();
    foreach($data['data'][$reading_type]['checkpoints'] as $name => $data){
        $parser[] = [
            'label' => pretty_ticket_type($name),
            'children' => check_aux_parser($data)
        ];
    }

    return json_encode($parser);
}

function check_aux_parser($data){
    $parser = [
        ['label' => 'Accesos pre autorizados: '.$data['availables']],
        ['label' => 'Accesos leídos: '.$data['access_reads']],
        ['label' => 'Salidas leídas: '.$data['exits_reads']],
        ['label' => 'Accesos restantes: '.$data['remainings']],
    ];

    foreach($data['checkpoints'] as $id => $checkpoint){
        $parser[] = [
            'label' => Checkpoint::find($id)->name,
            'children' => [
                ['label' => 'Accesos pre autorizados: '.$checkpoint['availables']],
                ['label' => 'Accesos leídos: '.$checkpoint['access_reads']],
                ['label' => 'Salidas leídas: '.$checkpoint['exits_reads']],
                ['label' => 'Accesos restantes: '.$checkpoint['remainings']]
            ]
        ];
    }

    return $parser;
}

function data_parser($data){
    $labels = '';
    $colors = '';
    $info = '';
    foreach($data['labels'] as $label){
        $labels .= "'".$label."',";
    }
    
    foreach($data['colors'] as $color){
        $colors .= "'".$color."',";
    }
    
    foreach($data['data'] as $d){
        $info .= "'".$d."',";
    }

    return [
        'labels' => $labels,
        'colors' => $colors,
        'data' => $info
    ];
}
