<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

use App\Mail\OrderConfirmed;


class SendOrderEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $event_title;
    public $order;
    public $qrs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($event_title, $order, $qrs)
    {
        $this->event_title = $event_title;
        $this->order = $order;
        $this->qrs = $qrs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->order->email)->queue(new OrderConfirmed($this->event_title, $this->order, $this->qrs));
        // Mail::to('alinkaafm@gmail.com')->queue(new OrderConfirmed($this->event_title, $this->order, $this->qrs));
    }
}
