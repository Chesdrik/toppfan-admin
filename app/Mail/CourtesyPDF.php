<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CourtesyPDF extends Mailable
{
    use Queueable, SerializesModels;
    public $rutaGuardado;
    public $order;
    public $name;
    public $event;
    public $ticket;
    public $qr;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($rutaGuardado, $order, $name, $event, $ticket, $qr)
    {
        $this->rutaGuardado = $rutaGuardado;
        $this->order = $order;
        $this->name = $name;
        $this->event = $event;
        $this->ticket = $ticket;
        $this->qr = $qr;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $event = $this->event;
        $ticket = $this->ticket;
        $order = $this->order;
        $qr = $this->qr;

        return $this->view('admin.courtesies.courtesyMail', compact('order', 'event', 'ticket', 'qr'))
            ->attachFromStorage($this->rutaGuardado, $this->name, [
                'mime' => 'application/pdf',
            ]);
    }
}
