<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderConfirmed extends Mailable
{
    use Queueable, SerializesModels;
    public $event_title;
    public $order;
    public $qrs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($event_title, $order, $qrs)
    {
        $this->event_title = $event_title;
        $this->order = $order;
        $this->qrs = $qrs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.tickets.template');
    }
}
