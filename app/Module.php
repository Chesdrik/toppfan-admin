<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function system()
    {
        return $this->belongsToMany('App\System');
    }

    public static function selectize()
    {
        return Module::orderBy('name')->pluck('name', 'id')->toArray();
    }
}
