<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class CourtesyAccess extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Código de acceso')
                    ->line('Apreciable '.$name)
                    ->line('Le informamos que usted ha sido acreditado para el evento {{Event_Name}}. Le rogamos leer cuidadosamente el contenido de este correo.')
                    ->line('El Estadio Olímpico Universitario (EOU) está dividido en tres zonas definidas por la autoridad competente en materia de protección civil y por el Club Universidad Nacional (CUN) conforme a lo siguiente:')
                    ->line('ZONA 1 – Cancha')
                    ->line('ZONA 2 – Tribuna')
                    ->line('ZONA 3 – Alrededores del EOU')
                    ->line('Le solicitamos atentamente atender las siguientes indicaciones.')
                    ->line('1. Su acreditación está asignada para la Zona {{Zone_number}}. ')
                    ->line('Su acceso al EOU será {{Tipo_de_acceso (peatonal o en auto)}} por el Acceso I ubicado en las inmediaciones del Estacionamiento 8.')
                    ->line('IMPORTANTE. Su acceso al interior del EOU será únicamente por el túnel {{Número_de_tunel_o_tuneles}} el personal operativo le orientará en caso intentar acceder por otro túnel.')
                    ->line('Su acreditación utiliza la tecnología QR para brindarle el acceso al EOU, favor de presentarla al momento de ingresar.')
                    ->line('qr')
                    ->line('Recomendaciones sanitarias:')
                    ->line('1. Uso de cubrebocas obligatorio en todo momento que permanezca en el evento.')
                    ->line('2. Mantener sana distancia con otros acreditados.')
                    ->line('3. Lavado frecuente de manos. El CUN ha instalado gel antibacterial en las zonas de trabajo, no obstante, se recomienda el uso de agua y jabón.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
