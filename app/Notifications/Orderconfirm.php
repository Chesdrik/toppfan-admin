<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Orderconfirm extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($event_title, $order, $qrs)
    {
        $this->event_title = $event_title;
        $this->order = $order;
        $this->qrs = $qrs;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = (new MailMessage)
            ->subject('Código de acceso')
            ->line('Acceso confirmado para un gran partido ')
            ->line($this->event_title)
            ->line('Total pagado: $' . number_format($this->order->total, 2, '.', ','))
            ->line('El siguiente código QR será tu ingreso al evento');

        foreach ($this->qrs as $qr) {
            $message->embed(asset($qr));
        }

        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
