<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;
use QrCode;
use  \Intervention\Image\Facades\Image;
use App\Encrypter;
use App\ClientUser;
use App\OrderLog;


class Order extends Model
{
    protected $fillable = [
        'client_user_id', 'assigned_by', 'email', 'total', 'event_id', 'type', 'payment_method', 'status', 'selling_point', 'event_type', 'pdf_status'
    ];

    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

    public function seasson_tickets()
    {
        return $this->hasMany('App\Accreditation', 'order_id', 'id');
    }

    public function logs()
    {
        return $this->hasMany('App\OrderLog');
    }

    public function latest_log()
    {
        return $this->logs()->first();
    }

    public function sales_by_type($type_id)
    {
        $sold = 0;
        $tickets = $this->hasMany('App\Ticket')->where('ticket_type_id', $type_id)->get();
        foreach ($tickets as $ticket) {
            $sold += $ticket->amount;
        }
        return $sold;
    }

    public function client_user()
    {
        return $this->belongsTo('App\ClientUser');
    }

    // public function event()
    // {
    //     return $this->belongsTo('App\Event');
    // }

    public function event()
    {
        return $this->morphTo();
    }

    public function pretty_type()
    {
        switch ($this->type) {
            case 'regular':
                return 'Regular';
                break;
            case 'courtesy':
                return 'Cortesía';
                break;
            case 'season_ticket':
                return 'Abono';
                break;
        }
    }

    public function pretty_payment_method()
    {
        switch ($this->payment_method) {
            case 'cash':
                return 'Efectivo';
                break;
            case 'credit_card':
                return 'Tarjeta crédito';
                break;
            case 'debit_card':
                return 'Tarjeta débito';
                break;
        }
    }

    public function pretty_status()
    {
        switch ($this->status) {
            case 'registered':
                return 'Registrada';
                break;
            case 'payed':
                return 'Pagada';
                break;
            case 'cancelled':
                return 'Cancelada';
                break;
        }
    }

    // Generates all qrs for a specific order
    public function generateQRs($print_folio = false)
    {
        // Generating QRs
        $qrs = array();
        foreach ($this->tickets as $ticket) {
            // Generate QR and return path
            $ticket->verify_qr_existance();

            // Appending QR to array
            $qrs[] = [
                'path' => $ticket->qr_path(),
                'qr' => Storage::get($ticket->qr_path())
            ];
        }

        // Returning order with qrs
        return $qrs;
    }

    public function qrPaths($print_folio = false)
    {
        $qrs = array();

        foreach ($this->generateQRs($print_folio) as $qr) {
            $qrs[] = $qr['path'];
        }

        return $qrs;
    }

    public function access_percentage()
    {
        $total_tickets = 0;
        $total_used = 0;

        foreach ($this->tickets as $ticket) {
            $total_tickets += $ticket->amount;
            $total_used += $ticket->used;
        }

        return [
            'total_tickets' => $total_tickets,
            'used_tickets' => $total_used,
            'access_percentage' => 0,
            // 'access_percentage' => number_format((($total_used * 100) / $total_tickets), 2, '.', ''),
        ];
    }

    public static function forEvent($event_id)
    {
        return Order::where('event_id', $event_id)->get();
    }

    public function API_POS($generate_qrs = true)
    {
        \Log::info($this->type);

        if (in_array($this->type, ['regular', 'courtesy'])) {
            // Fetching tickets
            $tickets = array();
            foreach ($this->tickets as $ticket) {
                $tickets[] = $ticket->API_POS($generate_qrs);
            }
        } else {
            // Fetching seasson tickets
            $tickets = array();
            foreach ($this->seasson_tickets as $seasson_ticket) {
                $tickets[] = $seasson_ticket->API_POS($generate_qrs);
            }
        }


        return [
            'id' => $this->id,
            'user_email' => $this->email,
            'username' => $this->client_user->name,
            'eventId' => $this->event_id,
            'eventName' => $this->event->name,
            'eventDate' => pretty_date($this->event->date),
            'eventType' => $this->event_type,
            'eventImage' => $this->event->img_path(),
            'eventVenue' => ($this->event_type == 'App\Event') ? $this->event->venue->name : $this->event->system->venues->first()->name,
            'total' => $this->total,
            'created_at' => $this->created_at->format('Y-m-d H:i:s'),
            'order_type' => $this->type,
            (in_array($this->type, ['regular', 'courtesy']) ? 'tickets' : 'seasson_tickets') => $tickets,
        ];
    }

    public function assigned_tickets()
    {
        $tickets = array();
        foreach ($this->tickets->where('assigned_by', '!=', null) as $ticket) {
            $tickets[] = $ticket->assigned_tickets();
        }

        return $tickets;
    }

    public static function searchBy($event_id, $filter, $term)
    {
        switch ($filter) {
            case 'name':
                return Order::searchByName($event_id, $term);
                break;
            case 'email':
                return Order::searchByEmail($event_id, $term);
                break;
            case 'order_id':
                return Order::searchByOrderId($event_id, $term);
                break;

            default:
                return [];
                break;
        }
    }

    public static function searchByName($event_id, $term)
    {
        $client_users = ClientUser::where('name', 'LIKE', "%{$term}%")->orWhere('last_name', 'LIKE', "%{$term}%")->pluck('id')->toArray();

        if (count($client_users) == 0) {
            return [];
        }

        return Order::where('event_id', $event_id)->whereIn('client_user_id', $client_users)->get();
    }

    public static function searchByEmail($event_id, $term)
    {
        // Searching user_client email
        $client_users = ClientUser::where('email', 'LIKE', "%{$term}%")->pluck('id')->toArray();

        // Searching order email
        $order_email = Order::where('event_id', $event_id)->where('email', 'LIKE', "%{$term}%")->pluck('id')->toArray();

        // Searching order log email (credit card data request)
        $order_ids = Order::where('event_id', $event_id)->pluck('id')->toArray();
        $order_log_emails = OrderLog::whereIn('order_id', $order_ids)->where('request', 'LIKE', "%{$term}%")->pluck('order_id')->toArray();

        // Merging results
        $results = array_unique(array_merge($client_users, $order_email, $order_log_emails));

        if (count($results) == 0) {
            return [];
        }

        return Order::where('event_id', $event_id)->whereIn('id', $results)->get();
    }

    public static function searchByOrderId($event_id, $term)
    {
        $order = Order::find($term);

        if (is_null($order)) {
            return [];
        }

        return [$order];
    }
}
