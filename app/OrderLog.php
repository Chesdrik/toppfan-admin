<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderLog extends Model
{
    protected $fillable = [
        'order_id', 'status', 'request', 'response'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function pretty_status()
    {
        switch ($this->status) {
            case 'pending':
                return 'Pendiente';
                break;
            case 'success':
                return 'Éxito';
                break;
            case 'error':
                return 'Error';
                break;
        }
    }


    public function affipay_response()
    {
        $response = json_decode($this->response);
        $binInformation = $this->affipay_binInformation($response);

        $affipay_response = [
            'requestId' => $this->affipay_getKey('requestId', $response),
            'id' => $this->affipay_getKey('id', $response),
            'date' => $this->affipay_getKey('date', $response),
            'time' => $this->affipay_getKey('time', $response),
            'binInformation' => [
                'bin' => $binInformation->bin,
                'bank' => $binInformation->bank,
                'type' => $binInformation->type,
                'brand' => $binInformation->brand,
            ]
        ];

        $this->affipay_response = $affipay_response;
        return $affipay_response;
    }

    public function affipay_getKey($key, $response = null)
    {
        if (is_null($response)) {
            $response = json_decode($this->response);
        }

        return $response->{$key};
    }

    public function affipay_binInformation($response = null)
    {
        if (is_null($response)) {
            $response = json_decode($this->response);
        }

        return $response->dataResponse->binInformation;
    }
}
