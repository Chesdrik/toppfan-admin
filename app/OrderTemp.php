<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTemp extends Model
{
    protected $fillable = ['user_id', 'ticket_type_id', 'eventable_id', 'eventable_type', 'amount', 'expiration_time', 'attempts', 'client_user_id', 'type', 'seat_id'];
    // public $timestamps = false;

    public function eventable()
    {
        return $this->morphTo();
    }

    public function pretty_type()
    {
        switch ($this->eventable_type == 'App\Event') {
            case 'ticket':
                return 'Boleto';
                break;
            case 'season_ticket':
                return 'Abono';
                break;
        }
    }

    public function eventSeat()
    {
        if ($this->eventable_type == 'App\Event') {
            return EventSeat::where('event_id', $this->eventable_id)->where('seat_id', $this->seat_id)->first();
        } else {
            return EventGroupSeat::where('event_group_id', $this->eventable_id)->where('seat_id', $this->seat_id)->first();
        }
    }

    public static function updateOrderTemp($client_user_id, $eventable_id, $eventable_type, $ticket_type_id, $amount, $expiration_time)
    {
        // Updating order temp
        OrderTemp::updateOrCreate([
            'client_user_id' => $client_user_id,
            'eventable_id' => $eventable_id,
            'eventable_type' => $eventable_type,
            'ticket_type_id' => $ticket_type_id,
        ], [
            'amount' => $amount,
            'expiration_time' => $expiration_time
        ]);
    }

    public static function updateOrderTempExpiration($client_user_id, $eventable_id, $eventable_type, $ticket_type_id, $expiration_time)
    {
        if (OrderTemp::where('client_user_id', $client_user_id)->where('eventable_id', $eventable_id)->where('eventable_type', $eventable_type)->where('ticket_type_id', $ticket_type_id)->exists()) {
            // Updating order temp
            OrderTemp::updateOrCreate([
                'client_user_id' => $client_user_id,
                'eventable_id' => $eventable_id,
                'eventable_type' => $eventable_type,
                'ticket_type_id' => $ticket_type_id,
            ], [
                'expiration_time' => $expiration_time
            ]);
        }
    }

    public static function updateSeatedOrderTemp($client_user_id, $eventable_id, $eventable_type, $ticket_type_id, $seat_id, $expiration_time)
    {
        // Updating order temp
        OrderTemp::updateOrCreate([
            'client_user_id' => $client_user_id,
            'eventable_id' => $eventable_id,
            'eventable_type' => $eventable_type,
            'ticket_type_id' => $ticket_type_id,
            'seat_id' => $seat_id,
        ], [
            'amount' => 1,
            'expiration_time' => $expiration_time
        ]);
    }

    public static function updateSeatOrderTempExpiration($client_user_id, $eventable_id, $eventable_type, $ticket_type_id, $seat_id, $expiration_time)
    {
        // Updating order temp
        OrderTemp::updateOrCreate([
            'client_user_id' => $client_user_id,
            'eventable_id' => $eventable_id,
            'eventable_type' => $eventable_type,
            'ticket_type_id' => $ticket_type_id,
            'seat_id' => $seat_id,
        ], [
            'expiration_time' => $expiration_time
        ]);
    }



    public static function fetchUserSeatForEvent($client_user_id, $eventable_id, $eventable_type, $ticket_type_id, $seat_id)
    {
        return OrderTemp::where('client_user_id', $client_user_id)
            ->where('eventable_id', $eventable_id)
            ->where('eventable_type', $eventable_type)
            ->where('ticket_type_id', $ticket_type_id)
            ->where('seat_id', $seat_id)
            ->first();
    }
}
