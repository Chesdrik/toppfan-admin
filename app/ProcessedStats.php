<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use \App\Team;

class ProcessedStats extends Model
{
    protected $fillable = ['event_id', 'data', 'type', 'stat_type_id', 'team_id', 'venue_id'];
    
    public static function get_stats($type = null, $venue = null, $team = null){
        $stats = ProcessedStats::where('stat_type_id', $type);

        $stats->where('venue_id', $venue); // Filtering by venue
        $stats->where('team_id', $team); // Filtering by team

        // Returning results
        return $stats->first()->data ?? null;
    }

    public static function parser_stats($venue = null, $team = null)
    {
        return [
            'general_stats' => json_decode(ProcessedStats::get_stats(7, null, null), true),
            'team_stats' => json_decode(ProcessedStats::get_stats(8, null, $team), true),
            'venue_stats' => json_decode(ProcessedStats::get_stats(9, $venue, null), true),
        ];
    }

    public static function generalStatsForChart($team = null, $venue = null, $json){
        try {
            // Fetching all events
            if(!is_null($team)){ $events = Event::past_events(null, null, $team->id)->get(); }
            if(!is_null($venue)){ $events = Event::past_events(null, $venue->id, null)->get(); }
            if(is_null($venue) && is_null($team)){ $events = Event::past_events(null, null, null)->get(); }

            $sales_color = '26, 188, 157, 1';
            $tickets_color = '245, 176, 65,  1';
            $access_color = '240, 124, 87,  1';

            // Define readings structure
            $readings = [
                'sales_color' => '',
                'tickets_color' => '',
                'access_color' => '',
                'labels' => [
                    'data' => array(),
                ],
                'data' => [
                ],
            ];

            $total_sales = 0;
            $total_tickets = 0;
            $total_attendance = 0;
            foreach($events as $event){
                // Appending data and labels
                $data = ProcessedStats::total_stats($event);
                $total_sales += $data['sales'];
                $total_tickets += $data['tickets'];
                $total_attendance += $data['attendance'];
                $readings['labels']['data'][] = $event->name;
                $readings['data']['sales'][] = $data['sales'];
                $readings['data']['tickets'][] = $data['tickets'];
                $readings['data']['attendance'][] = $data['attendance'];
                $readings['sales_color'] .= "'rgba($sales_color)', ";
                $readings['tickets_color'] .= "'rgba($tickets_color)', ";
                $readings['access_color'] .= "'rgba($access_color)', ";
            }
            $readings['total_sales'] = $total_sales;
            $readings['total_tickets'] = $total_tickets;
            $readings['total_attendance'] = $total_attendance;

            // Processing data for ChartJS use
            $readings['labels']['label'] = "'".implode("', '", $readings['labels']['data'])."'";
            $readings['sales']['string'] = implode(", ", $readings['data']['sales']);
            $readings['tickets']['string'] = implode(", ", $readings['data']['tickets']);
            $readings['attendance']['string'] = implode(", ", $readings['data']['attendance']);


            // Returning processed data as json or array
            if($json){
                return json_encode($readings, true);
            }else{
                return $readings;
            }

           } catch (\Throwable $th) {
            echo 'No se pudieron procesar estadísticas de eventos';
       }
    }

    public static function total_stats($event){
        $data = [
            'sales' => '',
            'tickets' => '',
            'attendance' => ''
        ];

        $sales = 0;
        $tickets = 0;
        $attendance = 0;
        foreach($event->tickets as $ticket){
            $sales += $ticket->amount * $ticket->price;
            $tickets += $ticket->amount;
        }

        $data['sales'] = $sales;
        $data['tickets'] = $tickets;
        $data['attendance'] = count($event->tickets()->where('used', 1)->get());

        return $data;
    }
}
