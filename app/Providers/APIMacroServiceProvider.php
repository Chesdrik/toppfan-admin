<?php
namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class APIMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        // // General form creation
        // Form::macro('APIForm', function ($name, $url, $form_content = array(), $method = "post") {
        //     $form = array(
        //         'name' => $name,
        //         'url' => url($url),
        //         'method' => $method,
        //         'form' => $form_content);
        //
        //     return $form;
        // });
        //
        // // Input text
        // Form::macro('APIinput', function ($label, $name, $value = '', $disabled = false) {
        //     $element = array(
        //         'name' => $name,
        //         'title' => $label,
        //         'type' => 'input',
        //         'disabled' => $disabled,
        //         'value' => $value
        //     );
        //
        //     return $element;
        // });
        //
        // // Large input text
        // Form::macro('APItext', function ($label, $name, $value = '', $disabled = false) {
        //     $element = array(
        //         'name' => $name,
        //         'title' => $label,
        //         'type' => 'textarea',
        //         'disabled' => $disabled,
        //         'value' => $value
        //     );
        //
        //     return $element;
        // });
        //
        // Form::macro('APIselect', function ($label, $name, $options, $value = '', $disabled = false) {
        //     $element = array(
        //         'name' => $name,
        //         'title' => $label,
        //         'type' => 'select',
        //         'disabled' => $disabled,
        //         'value' => $value,
        //         'options' => $options
        //     );
        //
        //     return $element;
        // });
        //
        // // Hidden input
        // Form::macro('APIhidden', function ($name, $value = '') {
        //     $element = array(
        //         'name' => $name,
        //         'title' => $name,
        //         'type' => 'hidden',
        //         'disabled' => true,
        //         'value' => $value
        //     );
        //
        //     return $element;
        // });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
