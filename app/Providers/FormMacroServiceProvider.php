<?php

namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Input
        Form::macro('MDtext', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false, 'form-group-class' => 'form-group'];
            $options = array_merge($options, $c_options);

            return "<div class='{$options['class']}'>
                        <div class='{$options['form-group-class']}'>
                        " . (!is_null($label) ? '<label>' . $label . '</label>' : '') . "
                            <input type='text'
                                    class='form-control " . ($errors->first($name) ? 'is-invalid' : '') . "'
                                    placeholder='{$label}'
                                    value='{$value}'
                                    name='{$name}'
                                    id='{$name}'
                                    " . ($options['disabled'] ? 'disabled' : '') . "
                                    " . ($options['required'] ? 'required' : '') . "
                                    >
                            <i class='form-group__bar'></i>
                            " . ($errors->first($name) ? "<div class='invalid-feedback'>{$errors->first($name)}</div>" : "") . "
                        </div>
                    </div>";
        });

        // $input = '<div class="'.$options['class'].'">
        //             <div class="form-group">
        //                 <label>'.$label.'</label>
        //                 <input type="text" class="form-control '.($errors->first($name) ? 'is-invalid' : '').'"
        //                     value="'.$value.'"
        //                     name="'.$name.'"
        //                     placeholder="'.$options['placeholder'].'"
        //                     '.($options["disabled"] ? 'disabled' : '').'>';
        //                     // '.($options["disabled"] ? 'disabled', '').';
        //
        //                 // Displaying errors
        //                 if($errors->first($name)){
        //                     $input .= '<div class="invalid-feedback">'.$errors->first($name).'</div>';
        //                 }
        //
        //     $input .= '<i class="form-group__bar"></i>
        //             </div>
        //         </div>';
        // return $input;

        Form::macro('MDtextarea', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return "<div class='{$options['class']}'>
                        <div class='form-group'>
                            <label>{$label}</label>
                            <textarea
                                    class='form-control " . ($errors->first($name) ? 'is-invalid' : '') . "'
                                    placeholder='{$label}'
                                    value='{$value}'
                                    name='{$name}'
                                    " . ($options['disabled'] ? 'disabled' : '') . "
                                    >{$value}</textarea>
                            <i class='form-group__bar'></i>
                            " . ($errors->first($name) ? "<div class='invalid-feedback'>{$errors->first($name)}</div>" : "") . "
                        </div>
                    </div>";
        });

        Form::macro('MDdatepicker', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return Form::MDDatetimepickerMacro($label, $name, $value, 'YYYY-MM-DD', $errors, $options);
        });

        Form::macro('MDtimepicker', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return Form::MDDatetimepickerMacro($label, $name, $value, 'HH:mm', $errors, $options);
        });

        Form::macro('MDdatetimepicker', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false, 'required' => false];
            $options = array_merge($options, $c_options);

            return Form::MDDatetimepickerMacro($label, $name, $value, 'YYYY-MM-DD HH:mm', $errors, $options);
        });

        Form::macro('MDDatetimepickerMacro', function ($label, $name, $value, $format, $errors, $options) {
            return "<div class='{$options['class']}'>
                        <label>{$label}</label>
                        <div class='input-group form-group'>
                            <div class='input-group-prepend'>
                                <span class='input-group-text'><i class='zmdi zmdi-calendar zmdi-hc-fw'></i></span>
                            </div>
                            <input type='text'
                                    class='form-control datetimepicker-input " . ($errors->first($name) ? 'is-invalid' : '') . "'
                                    data-toggle='datetimepicker'
                                    data-target='#{$name}'
                                    data-date-format='{$format}'
                                    placeholder='{$label}'
                                    value='{$value}'
                                    name='{$name}'
                                    id='{$name}'>
                            <i class='form-group__bar'></i>
                            " . ($errors->first($name) ? "<div class='invalid-feedback'>{$errors->first($name)}</div>" : "") . "
                        </div>
                    </div>";
        });



        // Email
        Form::macro('MDemail', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false];
            $options = array_merge($options, $c_options);

            $input = '<div class="' . $options['class'] . '">
                        <div class="form-group">
                            <label>' . $label . '</label>
                            <input type="email" class="form-control ' . ($errors->first($name) ? 'is-invalid' : '') . '"
                                value="' . $value . '"
                                name="' . $name . '"
                                placeholder="' . $options['placeholder'] . '"
                                ' . ($options["disabled"] ? 'disabled' : '') . '>';
            // '.($options["disabled"] ? 'disabled', '').';

            // Displaying errors
            if ($errors->first($name)) {
                $input .= '<div class="invalid-feedback">' . $errors->first($name) . '</div>';
            }

            $input .= '<i class="form-group__bar"></i>
                        </div>
                    </div>';
            return $input;
        });

        // Password
        Form::macro('MDpassword', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'placeholder' => $label, 'disabled' => false];
            $options = array_merge($options, $c_options);

            $input = '<div class="' . $options['class'] . '">
                        <div class="form-group">
                            <label>' . $label . '</label>
                            <input type="password" class="form-control ' . ($errors->first($name) ? 'is-invalid' : '') . '"
                                value="' . $value . '"
                                name="' . $name . '"
                                placeholder="' . $options['placeholder'] . '"
                                ' . ($options["disabled"] ? 'disabled' : '') . '

                                >';
            // '.($options["disabled"] ? 'disabled', '').';

            // Displaying errors
            if ($errors->first($name)) {
                $input .= '<div class="invalid-feedback">' . $errors->first($name) . '</div>';
            }

            $input .= '<i class="form-group__bar"></i>
                        </div>
                    </div>';
            return $input;
        });


        // Select
        Form::macro('MDselect', function ($label, $name, $value, $elements, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'disabled' => false];
            $options = array_merge($options, $c_options);


            $input = '<div class="' . $options['class'] . '">
                        <div class="form-group">
                            <label>' . $label . '</label>

                            <div class="select">
                                <select
                                class="form-control"
                                name="' . $name . '" ' .
                ($options["disabled"] ? 'disabled' : '') .
                (array_key_exists('wire-model', $c_options) ? 'wire:model="' . $options["wire-model"] . '"' : '') .
                '>';
            foreach ($elements as $element => $label) {
                $input .= '<option value="' . $element . '" ' . ($element == $value ? 'selected="selected"' : '') . '>
                                                        ' . $label . '
                                                    </option>';
            }
            $input .= '
                                </select>
                                <i class="form-group__bar"></i>
                            </div>';

            // Displaying errors
            if ($errors->first($name)) {
                $input .= '<div class="invalid-feedback">' . $errors->first($name) . '</div>';
            }

            $input .= '
                        </div>
                    </div>';

            return $input;
        });

        // Selectize
        Form::macro('MDselectize', function ($label, $name, $elements, $selected_elements, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'disabled' => false];
            $options = array_merge($options, $c_options);

            $input = '<div class="' . $options['class'] . '">
                        <div class="form-group">
                            <label for="' . $name . '">' . $label . '</label>
                            <select id="select-' . $name . '" name="' . $name . '[]" multiple class="demo-default" placeholder="Selecciona las opciones...">
                                <option value="">Selecciona las opciones...</option>';
            foreach ($elements as $element_id => $element_name) {
                $selected = in_array($element_id, $selected_elements) ? 'selected' : '';
                $input .= '<option value="' . $element_id . '" ' . $selected . '>' . $element_name . '</option>';
            }
            $input .= '
                            </select>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(document).ready(function(){
                            $("#select-' . $name . '").selectize({maxItems: 1000,});
                        });
                    </script>';

            return $input;
        });


        Form::macro('MDfile', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'disabled' => false];
            $options = array_merge($options, $c_options);

            // Displaying errors
            // if($errors->first($name)){
            //     $input .= '<div class="invalid-feedback">'.$errors->first($name).'</div>';
            // }


            return "<div class='{$options['class']}'>
                        <div class='input-group'>
                            <label for='{$name}'>{$label}</label>
                            <input type='file' class='form-control-file' id='{$name}' name='{$name}' " . ($options["disabled"] ? 'disabled' : '') . ">
                        </div>
                    </div>";
        });

        Form::macro('MDdropzone', function ($label, $name, $post_url, $errors, $c_options = []) {
            $options = ['class' => 'col-md-12', 'disabled' => false, 'accepted-files' => '.png', 'max-files' => null, 'leyend' => null];
            $options = array_merge($options, $c_options);

            return "<div class='{$options['class']}'>
                        <label>{$label}</label><br />
                        <small>Archivos aceptados: {$options['accepted-files']}</small>
                        " . (!is_null($options['leyend']) ? "<br /><small>" . $options['leyend'] . "</small>" : "") . "
                        <div id='{$name}' name='{$name}' class='dropzone dz-clickable'></div>
                    </div>
                    <script type='text/javascript'>
                        $(function() {
                            console.log('{$name} loaded');
                            var myDropzone{$name} = new Dropzone('#{$name}', {
                                url: '{$post_url}',
                                headers: {
                                    'x-csrf-token': '" . csrf_token() . "',
                                },
                                paramName: ['{$name}'],
                                acceptedFiles: '{$options['accepted-files']}',
                                " . (!is_null($options['max-files']) ? "maxFiles: {$options['max-files']}," : "") . "
                                dictDefaultMessage: 'Arrastra los archivos aquí para subir',
                                dictFallbackMessage: 'Tu navegador no soporta drag&drop',
                                dictFallbackText: 'Utiliza el fallback para subir archivos',
                                dictFileTooBig: 'Ela rchivo es demasiado grande ({{filesize}}MiB). Max: {{maxFilesize}}MiB.',
                                dictInvalidFileType: 'No puedes subir archivos de este tipo.',
                                dictResponseError: 'Servidor responde con código {{statusCode}}.',
                                dictCancelUpload: 'Cancelar carga',
                                dictCancelUploadConfirmation: '¿Seguro que quieres cancelar el upload?',
                                dictRemoveFile: 'Eliminar archivo',
                                dictMaxFilesExceeded: 'No puedes subir mas archivos.',
                            });
                        });
                    </script>";
        });

        Form::macro('MDColorPicker', function ($label, $name, $value, $errors, $c_options = []) {
            $value = empty($value) ? '#03A9F4' : $value;
            $options = ['class' => 'col-md-12', 'disabled' => false];
            $options = array_merge($options, $c_options);

            return "<div class='{$options['class']}'>
                            <label>{$label}</label>
                            <div class='input-group'>
                                <div class='input-group-prepend'>
                                    <span class='input-group-text'><i class='zmdi zmdi-palette'></i></span>
                                </div>
                                <input type='text' class='form-control color-picker' value='{$value}' name='{$name}' id='{$name}'>
                                <i class='color-picker__preview' style='background-color: {$value}'></i>
                                <i class='form-group__bar'></i>
                            </div>
                        </div>";
        });

        // Input Slider
        Form::macro('MDInputSlider', function ($id, $color, $min, $max, $start, $c_options = []) {
            $options = ['class' => 'col-md-12', 'disabled' => false];
            $options = array_merge($options, $c_options);

            return "<div class='{$options['class']}'>
                        <div id='{$id}' class='input-slider input-slider--$color'></div>
                    </div>
                    <script src='https://cdn.jsdelivr.net/gh/Dogfalo/materialize@master/extras/noUiSlider/nouislider.min.js'></script>
                    <link href='https://cdn.jsdelivr.net/gh/Dogfalo/materialize@master/extras/noUiSlider/nouislider.css' rel='stylesheet'>

                    <script type='text/javascript'>
                            var slider = document.getElementById('{$id}');

                            noUiSlider.create(slider, {
                                start: {$start},
                                connect: [true,false],
                                range: {
                                    min: {$min},
                                    max: {$max}
                                }
                            });
                    </script>
                    <style>
                    .noUi-target.noUi-horizontal .noUi-tooltip {
                            background-color:{$color}
                        }
                    </style>";
        });

        // Toggle switch
        Form::macro('MDToggleSwitch', function ($label, $name, $value, $errors, $c_options = []) {
            $options = ['class' => 'col-sm-4 col-md-3', 'disabled' => false];
            $options = array_merge($options, $c_options);
            $input = '';

            // Displaying errors
            if ($errors->first($name)) {
                $input .= '<div class="invalid-feedback">' . $errors->first($name) . '</div>';
            }

            $input .=  "<div class='{$options['class']}'>
                        <h3 class='card-body__title'>{$label}</h3>
                        <div class='form-group'>
                            <div class='toggle-switch'>
                                <input type='checkbox' name='{$name}' class='toggle-switch__checkbox' " . ($value ? 'checked=""' : '') . " {$options['disabled']}>
                                <i class='toggle-switch__helper'></i>
                            </div>
                        </div>
                    </div>";

            return $input;
        });
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
