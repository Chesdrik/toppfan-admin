<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\EventSeat;
use App\EventGroupSeat;
use App\Seat;

class Row extends Model
{
    protected $fillable = ['ticket_type_id', 'name', 'start', 'end'];
    public $timestamps = false;

    public function ticket_type()
    {
        // return $this->hasOne('App\TicketType', 'id', 'ticket_type_id');
        return $this->belongsTo('App\TicketType');
    }

    public function seats()
    {
        return $this->hasMany('App\Seat');
    }

    public function seat_status($event)
    {
        if (get_class($event) == 'App\Event') {
            $object = new EventSeat;
            $type = 'event';
        } else {
            $object = new EventGroupSeat;
            $type = 'event_group';
        }
        // $object = (get_class($event) == 'App\Event') ? new EventSeat : new EventGroupSeat;


        $seats = $this->seats->pluck('id')->toArray();

        if ($type == 'event') {
            $on_hold_sale_seats = $object::where('event_id', $event->id)->whereIn('seat_id', $seats)->where('status', 'on_hold_sale')->pluck('seat_id')->toArray();
        } else {
            $on_hold_sale_seats = $object::where('event_group_id', $event->id)->whereIn('seat_id', $seats)->where('status', 'on_hold_sale')->pluck('seat_id')->toArray();
        }
        $on_hold_sale = Seat::whereIn('id', $on_hold_sale_seats)->pluck('name')->toArray();



        if ($type == 'event') {
            $on_hold_seats = $object::where('event_id', $event->id)->whereIn('seat_id', $seats)->where('status', 'on_hold')->pluck('seat_id')->toArray();
        } else {
            $on_hold_seats = $object::where('event_group_id', $event->id)->whereIn('seat_id', $seats)->where('status', 'on_hold')->pluck('seat_id')->toArray();
        }

        $on_hold = Seat::whereIn('id', $on_hold_seats)->pluck('name')->toArray();

        // Returning data
        return [
            'name' => $this->name,
            'row_id' => $this->id,
            'seats' => [
                'start' => $this->start,
                'total' => $this->total,
                'on_hold' => $on_hold,
                'on_hold_sale' => $on_hold_sale,
                'skip_seats' => [],
                // 'skip_seats' => [rand($this->start + 1, $this->total - 1)]
                // 'skip_seats' => [18],
            ]
        ];
    }
}
