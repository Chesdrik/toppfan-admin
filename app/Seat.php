<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seat extends Model
{
    protected $fillable = ['row_id', 'name'];
    public $timestamps = false;

    public function row()
    {
        return $this->hasOne('App\Row', 'id', 'row_id');
    }

    public function event_seats()
    {
        return $this->belongsToMany('App\Event')->withPivot('status');
    }

    /**
     * Get the ticket that owns the Seat
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tickets()
    {
        return $this->belongsToMany('App\Ticket');
    }

    public function full_name()
    {
        return "Fila " . $this->row->name . " asiento " . $this->name;
    }

    public function seat_key()
    {
        return $this->row_id . '_' . $this->name;
    }

    public static function getSeatId($row_id, $seat_index)
    {
        return Seat::where('row_id', $row_id)->where('name', $seat_index)->first()->id;
    }
}
