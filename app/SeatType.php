<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeatType extends Model
{
    protected $table = 'ticket_types';

    public function ticket(){
    	return $this->hasMany('App\Ticket');
    }
}
