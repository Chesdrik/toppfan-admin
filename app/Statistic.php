<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $fillable = [
        'event_id', 'data', 'statistic_type_id'
    ];

    public static function allOrdersForChart($json = false){
        $data = array();

        $all_stats = Statistic::select('event_id', 'data')
                ->where('statistic_type_id', 17)
                ->get();

        $cash = $credit_card = $debit_card = $office = $web = $kiosk = $registered = $payed = $cancelled = 0;
        foreach($all_stats as $stat){
            $stats = json_decode($stat->data, true);

            $cash += $stats['payment_methods']['string']['cash'];
            $credit_card += $stats['payment_methods']['string']['debit_card'];
            $debit_card += $stats['payment_methods']['string']['credit_card'];
            
            $office += $stats['selling_points']['string']['ticket_office'];
            $web += $stats['selling_points']['string']['web'];
            $kiosk += $stats['selling_points']['string']['kiosk']; 
            
            $registered += $stats['status']['string']['registered'];
            $payed += $stats['status']['string']['payed'];
            $cancelled += $stats['status']['string']['cancelled'];
        }

        $data['payment_methods']['colors'] = "'rgba(26, 188, 157, 1)', 'rgba(26, 188, 157, 1)', 'rgba(26, 188, 157, 1)'";
        $data['payment_methods']['labels'] = "'Efectivo','Tarjeta de crédito','Tarjeta de débito'";
        $data['payment_methods']['string'] = [$cash, $credit_card, $debit_card];
        
        $data['selling_points']['colors'] = "'rgba(26, 188, 157, 1)', 'rgba(26, 188, 157, 1)', 'rgba(26, 188, 157, 1)'";
        $data['selling_points']['labels'] = "'Taquilla','Web','Kiosko'";
        $data['selling_points']['string'] = [$office, $web, $kiosk];  
        
        $data['status']['colors'] = "'rgba(26, 188, 157, 1)', 'rgba(26, 188, 157, 1)', 'rgba(26, 188, 157, 1)'";
        $data['status']['labels'] = "'Registrada','Pagada','Cancelada'";
        $data['status']['string'] = [$registered, $payed, $cancelled];        
        
        return ($json ? json_encode($data, true) : $data);
    }
}
