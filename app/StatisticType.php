<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatisticType extends Model
{
    protected $fillable = ['id', 'name'];
    public $timestamps = false;
}
