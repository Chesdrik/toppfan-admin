<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    //protected $table = "support";
    protected $fillable = ['topic_id', 'description', 'status', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }

    public function convesation()
    {
        return $this->hasMany('App\conversation');
    }

    public function pretty_type()
    {
        switch ($this->status) {
            case 'pending':
                return 'Pendiente';
                break;
            case 'finish':
                return 'Terminado';
                break;
            case 'cancel':
                return 'Cancelado';
                break;
            default:
                return 'error';
                break;
        }
    }
}
