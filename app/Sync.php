<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sync extends Model
{
    protected $fillable = [
        'event_id', 'recieved', 'response', 'error', 'type', 'total_records_synced'
    ];

    public function readings(){
        return $this->hasMany('App\TicketReading');
    }
}
