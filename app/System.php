<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $fillable = [
        'name', 'slug',
    ];
    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function modules()
    {
        return $this->belongsToMany('App\Module');
    }

    public function configs()
    {
        return $this->hasMany('App\Config');
    }

    public function config_by_key($key)
    {
        $config = $this->hasMany('App\Config')->where('key', $key)->take(1)->pluck('value');

        return (count($config) > 0 ? $config[0] : null);
    }

    public function event_groups()
    {
        return $this->hasMany('App\EventGroup');
    }

    public function teams()
    {
        return $this->belongsToMany('App\Team');
    }

    public function venues()
    {
        return $this->belongsToMany('App\Venue');
    }

    public function accreditations()
    {
        return $this->hasMany('App\Accreditation');
    }

    public function client_user()
    {
        return $this->belongsTo('App\ClientUser');
    }

    public function img_path($full = true)
    {
        $logo = "public/" . $this->config_by_key('logo');

        return ($full ? asset('storage' . $logo) : $logo);
    }

    public static function bySlug($slug)
    {
        return System::where('slug', $slug)->firstOrFail();
    }

    public static function selectize()
    {
        return System::orderBy('name')->pluck('name', 'id')->toArray();
    }



    // public static function selectize($event_group = null)
    // {
    //     if (is_null($event_group)) {
    //         return Event::orderBy('name')->pluck('name', 'id')->toArray();
    //     } else {
    //         $e = $event_group->events()->orderBy('name')->pluck('name', 'id')->toArray();
    //         dd($e);
    //         return Event::where($types)->orderBy('name')->pluck('name', 'id')->toArray();
    //     }
    // }
}
