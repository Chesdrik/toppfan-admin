<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Team extends Model
{
    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function systems()
    {
        return $this->belongsToMany('App\System');
    }

    public function events_on_sale()
    {
        return $this->events()
            ->where('date', '>', Carbon::now()->startOfDay())
            // ->where('active', 1)
            ->where('on_sale', 1)
            ->orderBy('date', 'ASC')
            ->get();
    }

    public static function dropdown()
    {
        return Team::orderBy('name')->pluck('name', 'id');
    }

    public function accreditations()
    {
        return $this->belongsToMany('App\Accreditation');
    }
}
