<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use QrCode;
use App\Encrypter;
use Storage;
use Image;

class Ticket extends Model
{
    protected $fillable = [
        'event_id', 'ticket_type_id', 'order_id', 'amount', 'used', 'price', 'folio', 'active', 'type', 'name', 'event_type', 'assigned_by'
    ];

    // public function event()
    // {
    //     return $this->belongsTo('App\Event');
    // }

    public function ticket_type()
    {
        return $this->belongsTo('App\TicketType');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function ticket_readings()
    {
        return $this->hasMany('App\TicketReading');
    }

    /**
     * The seats that belong to the Ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function seats()
    {
        return $this->belongsToMany('App\Seat', 'seat_ticket', 'ticket_id', 'seat_id');
        // return $this->belongsToMany('App\Seat');
    }

    public function event()
    {
        return $this->morphTo();
    }

    public function assignment()
    {
        return $this->belongsTo(ClientUser::class, 'assigned_by', 'id');
    }

    public function access_percentage()
    {
        return number_format((($this->used * 100) / $this->amount), 2, '.', '');
    }

    public function generate_qr($print_folio = false)
    {
        // Generating PNG with QR
        $qr_img = QrCode::format('png')
            ->size(500)
            ->margin(7)
            ->generate($this->qr_content());

        // Saving to disk
        Storage::put($this->qr_path(), $qr_img);

        // // TODO verify this, on S3 it works, on local it doesnt
        // if (env('FILESYSTEM_DRIVER', 'local') == 'local') {
        //     $path = "storage/" . $this->qr_path(false);
        // } else {
        //     $path = Storage::url($this->qr_path());
        // }

        // // Resizing QR and adding text
        // $img = Image::make($path);
        // $img->resizeCanvas(500, 550, 'top', false, 'ffffff');

        // //Adding event name to top
        // $img->text(str_pad($this->event->name, 3, '0', STR_PAD_LEFT), 250, 45, function ($font) {
        //     $font->size(25);
        //     $font->align('center');
        //     // $font->file(1);
        //     $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        // });

        // // Adding amount x ticket type
        // $top_margin = 480;
        // $img->text(str_pad($this->amount . ' x ' . $this->ticket_type->name, 3, '0', STR_PAD_LEFT), 250, $top_margin, function ($font) {
        //     $font->size(25);
        //     $font->align('center');
        //     // $font->file(1);
        //     $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        // });

        // // Adding access list
        // $top_margin += 30;
        // $img->text(str_pad("Accesos: " . $this->ticket_type->checkpoints_string(), 3, '0', STR_PAD_LEFT), 250, $top_margin, function ($font) {
        //     $font->size(20);
        //     $font->align('center');
        //     // $font->file(1);
        //     $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        // });

        // if ($print_folio) {
        //     $top_margin += 30;
        //     // Adding folio
        //     $img->text($this->folio, 250, $top_margin, function ($font) {
        //         $font->size(20);
        //         $font->align('center');
        //         // $font->file(1);
        //         $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        //     });
        // }

        // if (count($this->seats) > 0) {
        //     $top_margin += 30;
        //     // Adding seats list
        //     $img->text(str_pad((count($this->seats) > 1 ? "Asientos: " : "Asiento: ") . $this->seats_string(), 3, '0', STR_PAD_LEFT), 250, $top_margin, function ($font) {
        //         $font->size(20);
        //         $font->align('center');
        //         // $font->file(1);
        //         $font->file("./resources/fonts/Roboto-Bold-webfont.ttf");
        //     });
        // }

        // // Saving image
        // Storage::put($this->qr_path(), $img->encode('jpg')->encoded);

        // Returning path
        return Storage::url($this->qr_path());
    }

    public function qr_content($encrypted = true)
    {
        $qr_content = json_encode([
            // 'orderId' => $this->order_id, // Order ID it belongs to
            'id' => $this->id, // Ticket ID
            // 'amount' => $this->amount, // Amount of acces it has
            // 'name' => $this->name, // Name of the user has access
            // 'type' => $this->event_type, // Event or event group
            // 'checkpoints' => $this->ticket_type->checkpoints->pluck('id')->toArray(), // Checkpoints it has access to
        ]);

        // Returning plain json
        if (!$encrypted) {
            return $qr_content;
        } else {
            // Creating encrypter
            $encrypter = new Encrypter();
            $encrypter->setEncryptionKeyAttribute(env('ENCRYPTION_KEY'));
            $encrypter->setInitializationVectorAttribute(env('ENCRYPTION_IV'));

            // Creating QR content in JSON
            $encrypter->setContentAttribute($qr_content);

            return $encrypter->encrypted_content;
        }
    }

    public function qr()
    {
        // Checking if qr exists
        $this->verify_qr_existance();

        // Returning qr url
        return Storage::url($this->qr_path());
    }

    public function qr_path($with_public = true)
    {
        return ($with_public ? "public/" : "") . "events/{$this->event_id}/orders/{$this->order->id}/{$this->id}.png";
    }

    public function verify_qr_existance()
    {
        if (!Storage::exists($this->qr_path())) {
            $this->generate_qr();
        }
    }

    // Generates all qrs for a specific order
    public function generateQRsSVG()
    {
        // Generating SVG with QR
        return QrCode::format('svg')
            ->size(500)
            ->generate($this->qr_content());
    }


    public function seats_string()
    {
        $string = '';
        $i = 0;
        foreach ($this->seats as $seat) {
            $string .= $seat->row->name . '-' . $seat->name;
            ($i < count($this->seats) - 1) ? $string .= ', ' : $string;
            $i++;
        }
        return $string;
    }

    public function pretty_price()
    {
        return "$" . number_format($this->price, 2, '.', ',');
    }

    public function API()
    {
        return [
            'id' => $this->id,
            'eventId' => $this->event_id,
            'amount' => $this->amount,
            'ticketTypeName' => $this->ticket_type->name,
            'ticketTypeColor' => $this->ticket_type->color,
            'ticketTypeAccess' => $this->ticket_type->checkpoints_string(),
            'seatName' => ($this->seats->first()) ? $this->seats->first()->full_name() : 'General',
            'shortName' => ($this->seats->first()) ? $this->seats->first()->row->name . '-' . $this->seats->first()->name : 'General',
            'qr' => $this->generateQRsSVG()->toHtml(),
        ];
    }


    public function API_POS($generate_qrs = true)
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'ticketTypeId' => $this->ticket_type->id,
            'ticketTypeName' => $this->ticket_type->name,
            'seat_name' => ($this->seats->first()) ? $this->seats->first()->full_name() : 'General',
            'short_name' => ($this->seats->first()) ? $this->seats->first()->row->name . '-' . $this->seats->first()->name : 'General',
            'price' => $this->price,
            'access' => $this->ticket_type->checkpoints_string(),
            'qr' => ($generate_qrs ? $this->generateQRsSVG() : ''),
            'qr_url' => ($generate_qrs ? asset($this->qr()) : ''),
        ];
    }

    public function assigned_tickets()
    {
        return [
            'ticket_id' => $this->id,
            'order_id' => $this->order->id,
            'name' => $this->name,
            'email' => $this->order->email,
            'amount' => $this->amount,
            'ticketTypeName' => $this->ticket_type->name,
            'eventName' => $this->event->name,
            'assigned_by' => $this->assignment->name . ' ' . $this->assignment->last_name
        ];
    }
}
