<?php

namespace App\TicketDistributionSystem;

use Illuminate\Database\Eloquent\Model;

use App\ClientUser;
use App\Event;
use App\TicketType;

class TicketGroup extends Model
{
    protected $fillable = [
        'parent_ticket_group', 'event_id', 'client_user_id', 'assigned_by', 'ticket_type_id', 'amount', 'available', 'assigned', 'distributable', 'sent_to_fans'
    ];

        /**
     * The client user that belong to the Ticket
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function client_user()
    {
        return $this->belongsTo(ClientUser::class);
    }
    
    public function assignment()
    {
        return $this->belongsTo(ClientUser::class, 'assigned_by', 'id');
    }

    public function ticket_type()
    {
        return $this->belongsTo(TicketType::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function assigned_ticket_lots(){
        // dd($this, $this->assignment);
        return [
            'ticket_group_id' => $this->id,
            'name' => $this->client_user->name.' '.$this->client_user->last_name,
            'email' => $this->client_user->email,
            'amount' => $this->amount,
            'ticketTypeName' => $this->ticket_type->name,
            'eventName' => $this->event->name,
            'distributable' => ($this->distributable == 1) ? true : false,
            'available' => $this->available,
            'assigned_by' => $this->assignment->name.' '.$this->assignment->last_name
        ];
    }
}
