<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TicketReading extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'ticket_id', 'sync_id', 'checkpoint_id', 'type', 'error', 'reading_time'
    ];

    public function checkpoint()
    {
        return $this->belongsTo('App\Checkpoint');
    }

    public function ticket()
    {
        return $this->belongsTo('App\Ticket');
    }

    public function pretty_error()
    {
        return ($this->error ? 'Si' : 'No');
    }

    public function pretty_type_icon()
    {
        switch ($this->type) {
            case 'access':
                $icon = 'long-arrow-tab';
                break;
            case 'exit':
                $icon = 'long-arrow-left';
                break;
            case 'error':
                $icon = 'arrow-missed';
                break;
            default:
                $icon = 'alert-circle-o';
                break;
        }

        return "<i class='zmdi zmdi-hc-fw zmdi-$icon'></i>";
    }

    public function pretty_type_text()
    {
        switch ($this->type) {
            case 'access':
                $return = 'Acesso';
                break;
            case 'exit':
                $return = 'Salida';
                break;
            case 'error':
                $return = 'Error';
                break;
            default:
                return 'Otro';
                break;
        }
    }

    public static function forEvent(Event $event)
    {
        $tickets = array();
        foreach ($event->orders as $order) {
            $tickets = array_merge($tickets, $order->tickets->pluck('id')->toArray());
        }

        return TicketReading::whereIn('ticket_id', $tickets)->orderBy('reading_time', 'ASC')->get();
    }

    public static function timespan($tickets)
    {
        // dd($tickets);
        $current = TicketReading::whereIn('ticket_id', $tickets)
            ->orderBy('reading_time', 'ASC')
            ->take(1)
            ->pluck('reading_time');

        $end = TicketReading::whereIn('ticket_id', $tickets)
            ->orderBy('reading_time', 'DESC')
            ->take(1)
            ->pluck('reading_time');


        if (count($current) == 0 || count($end) == 0) {
            return null;
        } else {
            return [
                'start' => Carbon::create($current[0]),
                'end' => Carbon::create($end[0]),
            ];
        }
    }
}
