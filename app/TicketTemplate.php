<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketTemplate extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'venue_id'
    ];

    public $timestamps = false;

    public function venue()
    {
        return $this->hasOne('App\Venue', 'id', 'venue_id');
    }

    public function ticket_types()
    {
        return $this->belongsToMany('App\TicketType')->withPivot('price');
    }

    public function ticket_types_simple_array()
    {
        $ticket_types = array();
        foreach ($this->ticket_types as $ticket_type) {
            $ticket_types[$ticket_type->id] = [
                'name' => $ticket_type->name,
                'price' => $ticket_type->pivot->price,
            ];
        }

        return $ticket_types;
    }

    public function options()
    {
        return $this->ticket_types()
            // ->pluck('ticket_type_id', 'ticket_type_templates.price')
            ->pluck('ticket_types.price', 'ticket_type_id')
            ->toArray();
    }
}
