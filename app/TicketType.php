<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'price', 'venue_id', 'active', 'zone_id', 'total_seats'
    ];

    public function venue()
    {
        return $this->hasOne('App\Venue', 'id', 'venue_id');
    }

    public function checkpoints()
    {
        return $this->belongsToMany('App\Checkpoint', 'checkpoint_ticket_type')->withPivot('checkpoint_id', 'ticket_type_id');
    }

    public function pretty_status()
    {
        return '<div style="font-size:18px; color: green;" data-name="' . ($this->active ? 'green' : 'red') . '" data-code="f26b"><span><i class="zmdi zmdi-' . ($this->active ? 'check' : 'close') . ' zmdi-hc-fw"></i></span></div>';
        // return ($this->active == 1) ?
        //     '<div style="font-size:18px; color: green;" data-name="check" data-code="f26b"><span><i class="zmdi zmdi-check zmdi-hc-fw"></i></span></div>' :
        //     '<div style="font-size:18px; color: red;" data-name="check" data-code="f26b"><span><i class="zmdi zmdi-close zmdi-hc-fw"></i></span></div>';
    }

    public function pretty_courtesy()
    {
        return ($this->free == 1) ?
            '<div style="font-size:18px; color: green;" data-name="check" data-code="f26b"><span><i class="zmdi zmdi-check zmdi-hc-fw"></i></span></div>' :
            '<div style="font-size:18px; color: red;" data-name="check" data-code="f26b"><span><i class="zmdi zmdi-close zmdi-hc-fw"></i></span></div>';
    }

    public function checkpoints_array()
    {
        return $this->belongsToMany('App\Checkpoint')->pluck('checkpoint_id')->toArray();
    }

    public function checkpoints_string($filter = null)
    {
        if (is_null($filter)) {
            $checkpoints = $this->checkpoints->pluck('name')->toArray();
        } else {
            $checkpoints = $this->checkpoints()->where('type', $filter)->get()->pluck('name')->toArray();
        }

        return implode(', ', $checkpoints);
    }

    public static function ticket_by_status($status)
    {
        return TicketType::where('free', $status)->pluck('name', 'id');
    }

    public function zone()
    {
        return $this->hasOne('App\Zone', 'id', 'zone_id');
    }

    public function pretty_zone($short = false)
    {
        $zone = $this->hasOne('App\Zone', 'id', 'zone_id')->first();
        $parent = Zone::where('id', $zone->parent_zone)->first();

        if ($short) {
            return $zone->name;
        } else {
            return (!is_null($parent)) ? $parent->name . ' - ' . $zone->name : $zone->name;
        }
    }

    public function rows()
    {
        return $this->hasMany('App\Row')->orderBy('order', 'DESC');
    }

    public function rowsForMap()
    {
        $rows = array();
        if (!is_null($this->rows)) {
            foreach ($this->rows as $row) {
                $rows[] = [
                    'name' => $row->name,
                    'start' => $row->start,
                    'end' => $row->total,
                    // 'availables' => $row->available_seats()
                ];
            }
        }

        return $rows;
    }

    public function ticket_types()
    {
        return $this->hasMany('App\EventGroup')->withPivot(['max', 'sold']);
    }

    public function sold_tickets($event)
    {
        $sales = 0;
        foreach ($event->orders as $order) {
            $sales += $order->sales_by_type($this->id);
        }
        return $sales;
    }

    public function temps_by_event($event_id)
    {
        $temps = OrderTemp::where('ticket_type_id', $this->id)->where('event_id', $event_id)->get();
        $reserved = 0;

        foreach ($temps as $temp) {
            $reserved += $temp->amount;
        }
        return $reserved;
    }

    public function rows_for_map($event)
    {
        $data = array();
        foreach ($this->rows as $row) {
            if (count($row->seats) > 0) {
                $data[] = $row->seat_status($event);
            }
        }

        return $data;
    }

    public function API_POS()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'color' => $this->color,
            'type' => $this->type,
        ];
    }
}
