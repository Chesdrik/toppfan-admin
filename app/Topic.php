<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $fillable = [
        'name', 'parent_topic', 'total', 'order',
    ];
    public $timestamps = false;

    public function pretty_parent()
    {
        if (!is_null($this->parent_topic)) {

            $parent = Topic::where('id', $this->parent_topic)->first()->name;
            return $parent;
        } else {
            $parent = 'No tiene tema padre';
            return $parent;
        }
    }
}
