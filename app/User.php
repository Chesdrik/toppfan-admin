<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use App\System;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function history()
    {
        return $this->hasMany('App\AccreditationHistory');
    }
    // public function tickets(){
    //     return $this->hasMany('App\Ticket');
    // }

    public function systems()
    {
        return $this->belongsToMany(System::class);
    }

    public function associated_systems()
    {
        return (\Auth::user()->type == 'root' ? System::all() : \Auth::user()->systems());
    }


    public function pretty_type()
    {
        switch ($this->type) {
            case 'root':
                return 'Root';
                break;
            case 'admin':
                return 'Administrador';
                break;
            case 'client':
                return 'Cliente';
                break;
            case 'supervisor':
                return 'Supervisor';
                break;
            case 'stats':
                return 'Estadísticas';
                break;
            case 'access':
                return 'Acceso';
                break;
            case 'courtesies':
                return 'Cortesías';
                break;
            default:
                return $this->type;
                break;
        }
    }

    public static function userTypes()
    {
        return [
            'root' => 'Root',
            'admin' => 'Administrador',
            'client' => 'Cliente',
            'supervisor' => 'Supervisor',
            'stats' => 'Estadísticas',
            'access' => 'Acceso',
            'courtesies' => 'Cortesías'
        ];
    }

    public static function restrictedUserTypes()
    {
        return [
            'client' => 'Cliente',
            'supervisor' => 'Supervisor',
            'stats' => 'Estadísticas',
            'access' => 'Acceso',
            'courtesies' => 'Cortesías'
        ];
    }


    public function userAPI()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'type' => $this->type,
            'active' => $this->active,
        ];
    }

    public static function active()
    {
        return User::where('active', 1)->get();
    }

    public static function checkpoint_users()
    {
        return User::where('active', 1)->whereIn('type', ['admin', 'supervisor', 'access'])->get();
    }

    public function ticket_types()
    {
        return $this->belongsToMany('App\TicketType');
    }

    public function user_tickets()
    {
        return $this->ticket_types
            ->pluck('id')->toArray();
    }

    public function tickets_array()
    {
        return $this->belongsToMany('App\TicketType')->pluck('ticket_type_id')->toArray();
    }

    public static function qrServerUsers()
    {
        return User::whereIn('type', ['root', 'admin', 'client', 'supervisor', 'access'])->get();
    }



    public static function selectize($types = 'all')
    {
        if ($types == 'all') {
            return User::orderBy('name')->pluck('name', 'id')->toArray();
        } else {
            return User::where($types)->orderBy('name')->pluck('name', 'id')->toArray();
        }
    }
}
