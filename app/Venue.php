<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Venue extends Model
{
    protected $fillable = ['id', 'name'];

    public function checkpoints()
    {
        return $this->hasMany('App\Checkpoint');
    }

    public function root_checkpoints()
    {
        return $this->hasMany('App\Checkpoint')->where('parent_checkpoint', null);
    }

    public static function venue_options()
    {
        return Venue::pluck('name', 'id')->toArray();
    }

    public function events()
    {
        return $this->hasMany('App\Event');
    }

    public function systems()
    {
        return $this->belongsToMany('App\System');
    }

    public function svg_elements()
    {
        return $this->hasMany('App\VenueSvgElement');
    }

    // public function passed_events($take = 3){
    //     return $this->events()->where('date', '<=', Carbon::now())->orderBy('date', 'desc')->take($take);
    // }
    //
    // public function upcoming_events($take = 3){
    //     return $this->events()->where('date', '>=', Carbon::now()->startOfDay())->orderBy('date', 'desc')->take($take);
    // }

    public function events_by_status($status)
    {
        return $this->hasMany('App\Event')->where('active', 1)->get();
    }

    public function ticket_types_relation()
    {
        return $this->hasMany('App\TicketType');
    }

    public function ticket_templates()
    {
        return $this->hasMany('App\TicketTemplate');
    }

    public function access()
    {
        return $this->hasMany('App\Access\Access');
    }

    public function template_options()
    {
        $tickets = $this->hasMany('App\TicketTemplate')
            ->pluck('name', 'id')
            ->toArray();

        $tickets['na'] = 'Crear nuevo template de costos';
        return $tickets;
    }

    public function options()
    {
        return $this->hasMany('App\TicketTemplate')
            ->pluck('name', 'id')->toArray();
    }

    public static function list()
    {
        return Venue::all();
    }

    public function zones()
    {
        return $this->hasMany('App\Zone', 'venue_id', 'id');
    }

    public function parent_zones()
    {
        return $this->hasMany('App\Zone', 'venue_id', 'id')
            ->where('parent_zone', NULL)
            // ->where('active', 1)
            ->get();
    }

    public function ticket_types()
    {
        $types = collect();
        foreach ($this->zones as $zone) {
            if (!is_null($zone->ticket_types)) {
                foreach ($zone->ticket_types as $ticket_type) {
                    $types[] = $ticket_type;
                }
            }
        }

        return $types;
    }

    public function zone_options()
    {
        return Zone::where('venue_id', $this->id)
            ->where('parent_zone', '!=', NULL)
            ->pluck('name', 'id');
    }

    public function pretty_team()
    {
        switch ($this->name) {
            case 'Estadio Olímpico Universitario':
                return 'Pumas';
                break;
            case 'La Cantera':
                return 'Pumas Femenil';
                break;
            case 'Estadio Olímpico de Villahermosa':
                return 'Pumas Tabasco';
                break;
            default:
                break;
        }
    }

    public function activeAccess()
    {
        $today = Carbon::now()->format('Y-m-d H:i:s');

        $access = $this->access()
            ->where('start', '<=', Carbon::now()->format('Y-m-d H:i:s'))
            ->where('end', '>=', Carbon::now()->format('Y-m-d H:i:s'))
            ->get();

        $return = array();
        foreach ($access as $a) {
            $return[] = [
                'id' => $a->id,
                'userId' => $a->user_id,
                'venueId' => $a->venue_id,
                'visitorName' => $a->visitor_name,
                'start' => $a->start,
                'end' => $a->end,
                'requirements' => $a->access_requirements_API(),
            ];
        }

        return $return;
    }

    public static function selectize()
    {
        return Venue::orderBy('name')->pluck('name', 'id')->toArray();
    }

    public function stadiumConfiguration()
    {
        // Base configuration array
        $stadium_configuration = array();

        // Fetching perimeters and access
        foreach ($this->svg_elements as $element) {
            if ($element->type == 'perimeter') {
                $stadium_configuration['perimeters'][] = [
                    'x' => $element->x,
                    'y' => $element->y,
                    'stroke_width' => $element->stroke_width,
                    'color' => $element->color,
                    'fill' => $element->fill,
                    'svg_content' => $element->svg_content,
                ];
            } elseif ($element->type == 'access') {
                $stadium_configuration['access'][] = [
                    'x' => $element->x,
                    'y' => $element->y,
                    'radius' => $element->radius,
                    'label' => $element->label,
                    'fill' => $element->fill,
                ];
            }
        }

        // Appending zones
        foreach ($this->parent_zones() as $parent_zone) {
            foreach ($parent_zone->childZones() as $zone) {
                $ticket_types = TicketType::where('zone_id', $zone->id)->where('venue_id', $this->id);
                if ($ticket_types->count() == 1) {
                    $stadium_configuration['zones'][] = [
                        'x' => $zone->x,
                        'y' => $zone->y,
                        'id' => $zone->svg_id,
                        'type' => $zone->type,
                        'ticket_type_id' => $ticket_types->first()->id,
                        'zone_id' => $zone->id,
                        'label' => $zone->svg_id,
                        'labelMatrix' => $zone->label_matrix,
                        'labelX' => $zone->label_x,
                        'labelY' => $zone->label_y,
                        'fill' => $zone->fill,
                        'svg_content' => $zone->svg_content,
                    ];
                }
            }
        }


        return $stadium_configuration;
    }
}
