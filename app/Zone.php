<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    protected $fillable = [
        'name', 'type', 'active', 'x', 'y', 'svg_id', 'label_matrix', 'label_x', 'label_y', 'fill', 'svg_content'
    ];
    public $timestamps = false;

    public function ticket_types()
    {
        return $this->hasMany('App\TicketType', 'zone_id', 'id');
    }

    public function subzonesForMap()
    {
        $subzones = Zone::where('parent_zone', $this->id)
            ->where('active', 1)
            ->get();

        $data = array();
        if (!is_null($subzones)) {
            foreach ($subzones as $subzone) {
                $data[] = [
                    'name' => $subzone->name,
                    'ticket_types' => $subzone->typesForMap()
                ];
            }
        }

        return $data;
    }

    public function childZones()
    {
        return Zone::where('parent_zone', $this->id)->get();
    }

    public function typesForMap()
    {
        $types = array();
        if (!is_null($this->ticket_types)) {
            foreach ($this->ticket_types as $ticket_type) {
                $types[] = [
                    'name' => $ticket_type->name,
                    'rows' => $ticket_type->rowsForMap()
                ];
            }
        }

        return $types;
    }

    public function get_parent_zone()
    {
        if (is_null($this->parent_zone)) {
            return null;
        } else {
            return $this->belongsTo('App\Zone', 'parent_zone', 'id');
        }
    }

    public function API()
    {
        return [
            'id' => $this->id,
            'parent_zone' => (is_null($this->parent_zone) ? null : $this->get_parent_zone->API()),
            'name' => $this->name,
            'type' => $this->type,
        ];
    }
}
