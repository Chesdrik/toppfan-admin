<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class conversation extends Model
{
    protected $fillable = ['support_id', 'message', 'user_id'];

     public function date_message(){
        return substr($this->created_at, 0, 16);
    }

    public function users(){
        return $this->hasMany('App\User', 'id', 'user_id');
    }
}
