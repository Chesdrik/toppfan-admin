<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZonesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'zones';

    /**
     * Run the migrations.
     * @table zones
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('venue_id')->references('id')->on('venues')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('parent_zone')->nullable()->references('id')->on('zones')->onDelete('restrict')->onUpdate('cascade');
            $table->string('name');
            $table->enum('type', ['general', 'row'])->default('general');
            $table->tinyInteger('active')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
