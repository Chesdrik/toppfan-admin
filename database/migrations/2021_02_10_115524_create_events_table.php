<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'events';

    /**
     * Run the migrations.
     * @table events
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('venue_id')->references('id')->on('venues')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('ticket_template_id')->references('id')->on('ticket_templates')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('team_id')->references('id')->on('teams')->onDelete('restrict')->onUpdate('cascade');
            $table->text('name')->nullable();
            $table->boolean('on_sale')->nullable();
            $table->dateTime('date')->nullable();
            $table->boolean('active')->nullable()->default(null);
            $table->string('img')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
