<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTypesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ticket_types';

    /**
     * Run the migrations.
     * @table ticket_types
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('zone_id')->references('id')->on('zones')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('venue_id')->references('id')->on('venues')->onDelete('restrict')->onUpdate('cascade');
            $table->string('name');
            $table->string('color', 6)->default("1A202C");
            $table->float('price', 8, 2);
            $table->unsignedMediumInteger('total_seats')->nullable()->default(null);
            $table->unsignedMediumInteger('available_seats')->nullable()->default(null);
            $table->tinyInteger('active')->nullable()->default(0);
            $table->tinyInteger('free')->nullable()->default(0);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
