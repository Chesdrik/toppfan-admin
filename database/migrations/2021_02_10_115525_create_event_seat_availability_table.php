<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSeatAvailabilityTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'event_seat_availability';

    /**
     * Run the migrations.
     * @table order_temps
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('ticket_type_id')->references('id')->on('ticket_types')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('event_id')->references('id')->on('events')->onDelete('restrict')->onUpdate('cascade');
            $table->unsignedMediumInteger('total');
            $table->unsignedMediumInteger('available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
