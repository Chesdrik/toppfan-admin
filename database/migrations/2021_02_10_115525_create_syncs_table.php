<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSyncsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'syncs';

    /**
     * Run the migrations.
     * @table syncs
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('event_id')->references('id')->on('events')->onDelete('restrict')->onUpdate('cascade');
            $table->longText('recieved')->nullable()->default(null);
            $table->longText('response')->nullable();
            $table->tinyInteger('error')->nullable()->default(0);
            $table->enum('type', ['users_sync', 'events_sync', 'config_sync', 'certificates_sync', 'access_upload', 'ticket_readings', 'statistics', 'accreditation_readings'])->nullable();
            $table->unsignedSmallInteger('total_records_synced')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
