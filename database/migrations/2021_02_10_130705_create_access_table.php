<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'access';

    /**
     * Run the migrations.
     * @table access
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('user_id')->references('id')->on('users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('venue_id')->references('id')->on('venues')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('access_role_id')->references('id')->on('access_roles')->onDelete('restrict')->onUpdate('cascade');
            $table->text('visitor_name')->nullable()->default(null);
            $table->timestamp('start')->nullable()->default(null);
            $table->timestamp('end')->nullable()->default(null);
            $table->enum('type', ['foot', 'car', 'other'])->default('car');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
