<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessRequirementTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'access_requirement';

    /**
     * Run the migrations.
     * @table access_requirement
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('access_id')->references('id')->on('access')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('access_requirement_id')->references('id')->on('access_requirement')->onDelete('restrict')->onUpdate('cascade');
            $table->text('request')->nullable()->default(null);
            $table->text('response')->nullable()->default(null);
            $table->enum('data_type', ['text', 'json', 'photo', 'bool'])->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
