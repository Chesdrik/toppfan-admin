<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessRequirementDataTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'access_requirement_data';

    /**
     * Run the migrations.
     * @table access_requirement_data
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('access_requirement_id')->references('id')->on('access_requirement')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('access_log_id')->references('id')->on('access_logs')->onDelete('restrict')->onUpdate('cascade');
            $table->longText('data')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
