<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckpointDataTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'checkpoint_data';

    /**
     * Run the migrations.
     * @table checkpoint_data
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('device_id')->references('id')->on('devices')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('checkpoint_id')->references('id')->on('checkpoints')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('event_id')->references('id')->on('events')->onDelete('restrict')->onUpdate('cascade');
            $table->timestamp('start')->nullable()->default(null);
            $table->timestamp('end')->nullable()->default(null);
            $table->integer('initial_battery')->nullable()->default(null);
            $table->integer('final_battery')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
