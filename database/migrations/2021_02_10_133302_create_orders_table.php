<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'orders';

    /**
     * Run the migrations.
     * @table orders
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('client_user_id')->references('id')->on('client_users')->onDelete('no action')->onUpdate('no action');
            $table->foreignId('event_id')->references('id')->on('events')->onDelete('restrict')->onUpdate('cascade');
            $table->text('email')->nullable()->default(null);
            $table->float('total', 8, 2);
            $table->enum('type', ['regular', 'courtesy'])->default('regular');
            $table->enum('payment_method', ['cash', 'credit_card', 'debit_card'])->default('cash');
            $table->enum('status', ['registered', 'payed', 'cancelled'])->default('registered');
            $table->enum('selling_point', ['ticket_office', 'web', 'kiosk'])->default('web');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
