<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'tickets';

    /**
     * Run the migrations.
     * @table tickets
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('ticket_type_id')->references('id')->on('ticket_types')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('order_id')->references('id')->on('orders')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('event_id')->references('id')->on('events')->onDelete('restrict')->onUpdate('cascade');
            $table->unsignedSmallInteger('amount')->nullable();
            $table->unsignedTinyInteger('used')->default('0');
            $table->float('price', 8, 2)->default(null);
            $table->text('discount')->nullable()->default(null);
            $table->string('name');
            $table->string('folio')->nullable()->default(null);
            $table->enum('type', ['ticket', 'parking', 'courtesy'])->default('ticket');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
