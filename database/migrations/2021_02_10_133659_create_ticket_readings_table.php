<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketReadingsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ticket_readings';

    /**
     * Run the migrations.
     * @table ticket_readings
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('ticket_id')->references('id')->on('tickets')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('sync_id')->references('id')->on('syncs')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('checkpoint_id')->references('id')->on('checkpoints')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('type', ['access', 'exit', 'error'])->nullable();
            $table->tinyInteger('error')->default('0');
            $table->timestamp('reading_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
