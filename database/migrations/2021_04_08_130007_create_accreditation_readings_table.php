<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccreditationReadingsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'accreditation_readings';

    /**
     * Run the migrations.
     * @table accreditation_event
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('accreditation_event_id')->references('id')->on('accreditation_event')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('sync_id')->references('id')->on('syncs')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('checkpoint_id')->references('id')->on('checkpoints')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('type', ['access', 'exit', 'error'])->nullable()->default(null);
            $table->tinyInteger('error')->nullable()->default(null);
            $table->dateTime('reading_time')->nullable()->default(null);
            $table->text('device_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
