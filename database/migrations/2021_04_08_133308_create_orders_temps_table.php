<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTempsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'order_temps';

    /**
     * Run the migrations.
     * @table order_temps
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('client_user_id')->references('id')->on('client_users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('ticket_type_id')->references('id')->on('ticket_types')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('event_id')->references('id')->on('events')->onDelete('restrict')->onUpdate('cascade');
            $table->unsignedSmallInteger('amount');
            $table->unsignedSmallInteger('attempts');
            $table->timestamp('expiration_time');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
