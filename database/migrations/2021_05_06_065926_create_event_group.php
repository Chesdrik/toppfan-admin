<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_groups', function (Blueprint $table) {
            $table->id();
            $table->foreignId('system_id')->references('id')->on('systems')->onDelete('restrict')->onUpdate('cascade');
            $table->text('name')->nullable();
            $table->string('slug', 200)->unique();
            $table->string('img')->nullable()->default(null);
            $table->string('img_slideshow')->nullable()->default(null);
            $table->boolean('accreditations_on_sale')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_groups');
    }
}
