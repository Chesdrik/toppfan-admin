<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccreditationEventGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accreditation_event_group', function (Blueprint $table) {
            $table->id();
            $table->foreignId('accreditation_id')->references('id')->on('accreditations')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('event_group_id')->references('id')->on('event_groups')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accreditation_event_group');
    }
}
