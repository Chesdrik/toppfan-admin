<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrdersForSeassonTicket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Al parecer Laravel 7 no soporta cambiar enums, asi que ejecutamos el script directo. Abajo se queda el codigo para cuando migremos a Laravel 8
        \DB::statement("ALTER TABLE `orders` MODIFY COLUMN `type` ENUM('regular', 'courtesy', 'season_ticket') NOT NULL DEFAULT 'regular'");

        // TODO Al migrar a laravel 8, usar esta migracion
        // Schema::table('orders', function (Blueprint $table) {
        //     $table->enum('type', ['regular', 'courtesy', 'season_ticket'])->default('regular')->change();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `orders` MODIFY COLUMN `type` ENUM('regular', 'courtesy') NOT NULL DEFAULT 'regular'");
        // TODO Al migrar a laravel 8, usar esta migracion
        // Schema::table('orders', function (Blueprint $table) {
        //     $table->enum('type', ['regular', 'courtesy'])->default('regular')->change();
        // });
    }
}
