<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_team', function (Blueprint $table) {
            $table->id();
            $table->foreignId('system_id')->references('id')->on('systems')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('team_id')->references('id')->on('teams')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_team');
    }
}
