<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAccessRequirementColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('access_requirement', function (Blueprint $table) {
            $table->dropForeign('access_requirement_access_requirement_id_foreign');
            $table->dropColumn('access_requirement_id');
            $table->foreignId('requirement_id')->after('access_id')->references('id')->on('requirements')->onDelete('restrict')->onUpdate('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('access_requirement', function (Blueprint $table) {
            $table->dropForeign('access_requirement_requirement_id_foreign');
            $table->dropColumn('requirement_id');
            $table->foreignId('access_requirement_id')->after('access_id')->references('id')->on('access_requirement')->onDelete('restrict')->onUpdate('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }
}
