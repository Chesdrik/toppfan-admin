<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAccreditationsWithSystemAssociation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accreditations', function (Blueprint $table) {
            $table->foreignId('system_id')->after('id')->nullable()->references('id')->on('systems')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('accreditations', function (Blueprint $table) {
            $table->dropForeign('accreditations_system_id_foreign');
            $table->dropColumn('system_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
