<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddEventTypeColumnToTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropForeign('tickets_event_id_foreign');
            $table->unsignedBigInteger('event_id')->change();
            $table->string('event_type')->after('event_id');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('tickets', function (Blueprint $table) {
            $table->dropColumn('event_type');
        });
        Schema::enableForeignKeyConstraints();
    }
}
