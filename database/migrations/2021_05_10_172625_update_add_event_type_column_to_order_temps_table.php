<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAddEventTypeColumnToOrderTempsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('order_temps', function (Blueprint $table) {
            $table->dropForeign('order_temps_event_id_foreign');
            $table->unsignedBigInteger('event_id')->change();
            $table->foreignId('seat_id')->references('id')->on('seats')->nullable()->onDelete('restrict')->onUpdate('cascade')->after('event_id');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('order_temps', function (Blueprint $table) {
            $table->dropColumn('seat_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
