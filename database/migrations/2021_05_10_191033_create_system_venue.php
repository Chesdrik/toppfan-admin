<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemVenue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_venue', function (Blueprint $table) {
            $table->id();
            $table->foreignId('system_id')->references('id')->on('systems')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('venue_id')->references('id')->on('venues')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_venue');
    }
}
