<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClientUserToSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('systems', function (Blueprint $table) {
            $table->foreignId('client_user_id')->references('id')->on('client_users')->onDelete('restrict')->onUpdate('cascade');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('systems', function (Blueprint $table) {
            $table->dropColumn('client_user_id');
        });
        Schema::enableForeignKeyConstraints();
    }
}
