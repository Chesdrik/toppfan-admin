<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeasonTicketTypeToTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            \DB::statement("ALTER TABLE `tickets` CHANGE `type` `type` ENUM('ticket', 'parking', 'courtesy', 'season_ticket') DEFAULT 'ticket';");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tickets', function (Blueprint $table) {
            \DB::statement("ALTER TABLE `tickets` CHANGE `type` `type` ENUM('ticket', 'parking', 'courtesy') DEFAULT 'ticket';");
        });
    }
}
