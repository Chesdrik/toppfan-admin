<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventGroupSeatTable extends Migration
{
    /**
    * Schema table name to migrate
    * @var string
    */
    public $tableName = 'event_group_seat';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('event_group_id')->references('id')->on('event_groups')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('seat_id')->references('id')->on('seats')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('status', ['available', 'on_hold', 'on_hold_sale'])->default('available');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
