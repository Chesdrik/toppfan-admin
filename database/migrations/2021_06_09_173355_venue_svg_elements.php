<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VenueSvgElements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('venue_svg_elements', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('venue_id')->references('id')->on('venues')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('type', ['perimeter', 'access', 'other'])->default('other');
            $table->float('x', 5, 2);
            $table->float('y', 5, 2);
            $table->mediumText('stroke_width');
            $table->mediumText('radius');
            $table->mediumText('color');
            $table->mediumText('fill');
            $table->mediumText('svg_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('venue_svg_elements');
    }
}
