<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateZonesWithSvg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zones', function (Blueprint $table) {
            $table->float('x', 5, 2);
            $table->float('y', 5, 2);
            $table->text('svg_id');
            $table->text('label_matrix');
            $table->float('label_x', 5, 2);
            $table->float('label_y', 5, 2);
            $table->text('fill');
            $table->mediumText('svg_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zones', function (Blueprint $table) {
            $table->dropColumn('x');
            $table->dropColumn('y');
            $table->dropColumn('id');
            $table->dropColumn('label_matrix');
            $table->dropColumn('label_x');
            $table->dropColumn('label_y');
            $table->dropColumn('fill');
            $table->dropColumn('svg_content');
        });
    }
}
