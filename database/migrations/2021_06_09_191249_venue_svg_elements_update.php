<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VenueSvgElementsUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('venue_svg_elements', function (Blueprint $table) {
            $table->mediumText('stroke_width')->nullable()->change();
            $table->mediumText('radius')->nullable()->change();
            $table->mediumText('color')->nullable()->change();
            $table->mediumText('fill')->nullable()->change();
            $table->mediumText('svg_content')->nullable()->change();
            $table->mediumText('label')->nullable()->after('svg_content');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('venue_svg_elements', function (Blueprint $table) {
            //
        });
    }
}
