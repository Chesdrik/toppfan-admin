<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEventSeatAvailabilityWithMoreData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_seat_availability', function (Blueprint $table) {
            $table->unsignedMediumInteger('on_hold')->default(0)->after('available');
            $table->unsignedMediumInteger('on_hold_sale')->default(0)->after('on_hold');
            $table->unsignedMediumInteger('courtesy')->default(0)->after('on_hold_sale');
            $table->unsignedMediumInteger('sold')->default(0)->after('courtesy');
            $table->unsignedMediumInteger('sold_event_group')->default(0)->after('sold');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_seat_availability', function (Blueprint $table) {
            $table->dropColumn('on_hold');
            $table->dropColumn('on_hold_sale');
            $table->dropColumn('courtesy');
            $table->dropColumn('sold');
            $table->dropColumn('sold_event_group');
        });
    }
}
