<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEventGroupTicketTypeWithMoreData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_group_ticket_type', function (Blueprint $table) {
            $table->unsignedMediumInteger('on_hold')->default(0)->after('sold');
            $table->unsignedMediumInteger('on_hold_sale')->default(0)->after('on_hold');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_group_ticket_type', function (Blueprint $table) {
            $table->dropColumn('on_hold');
            $table->dropColumn('on_hold_sale');
            $table->dropTimestamps();
        });
    }
}
