<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEventGroupTicketTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_group_ticket_type', function (Blueprint $table) {
            $table->dropColumn('sold');
            $table->renameColumn('max', 'total');
        });

        Schema::table('event_group_ticket_type', function (Blueprint $table) {
            $table->unsignedMediumInteger('available')->default(0)->after('total');
            $table->unsignedMediumInteger('sold')->default(0)->after('on_hold_sale');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_group_ticket_type', function (Blueprint $table) {
            $table->dropColumn('sold');
        });

        Schema::table('event_group_ticket_type', function (Blueprint $table) {
            $table->unsignedMediumInteger('sold')->default(0)->after('total');
            $table->dropColumn('available');
            $table->renameColumn('total', 'max');
        });
    }
}
