<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEventSeatAvailabilityRemovingSoldEventGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_seat_availability', function (Blueprint $table) {
            $table->dropColumn('sold_event_group');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_seat_availability', function (Blueprint $table) {
            $table->unsignedMediumInteger('sold_event_group')->default(0)->after('sold');
        });
    }
}
