<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderTempsPolymorphic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_temps', function (Blueprint $table) {
            $table->renameColumn('event_id', 'eventable_id');
            $table->string('eventable_type')->default('')->after('event_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_temps', function (Blueprint $table) {
            $table->renameColumn('eventable_id', 'event_id');
            $table->dropColumn('eventable_type');
        });
    }
}
