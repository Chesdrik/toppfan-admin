<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddingOrderIdToAccreditation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accreditations', function (Blueprint $table) {
            $table->dropForeign(['ticket_id']);
            $table->dropColumn('ticket_id');


            $table->foreignId('order_id')
                ->after('system_id')
                ->nullable()
                ->default(null)
                ->references('id')
                ->on('orders')
                ->onDelete('restrict')
                ->onUpdate('cascade');

            $table->foreignId('seat_id')
                ->after('order_id')
                ->nullable()
                ->default(null)
                ->references('id')
                ->on('seats')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accreditations', function (Blueprint $table) {
            $table->dropForeign(['order_id']);
            $table->dropColumn('order_id');

            $table->dropForeign(['seat_id']);
            $table->dropColumn('seat_id');

            $table->foreignId('ticket_id')
                ->after('system_id')
                ->nullable()
                ->default(null)
                ->references('id')
                ->on('tickets')
                ->onDelete('restrict')
                ->onUpdate('cascade');
        });
    }
}
