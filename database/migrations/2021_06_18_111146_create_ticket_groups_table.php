<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketGroupsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'ticket_groups';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->foreignId('parent_ticket_group')->nullable()->default(null)->references('id')->on('ticket_groups')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('event_id')->references('id')->on('events')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('client_user_id')->references('id')->on('client_users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('assigned_by')->nullable()->references('id')->on('client_users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('ticket_type_id')->references('id')->on('ticket_types')->onDelete('restrict')->onUpdate('cascade');
            $table->unsignedSmallInteger('amount')->default(0);
            $table->unsignedSmallInteger('available')->default(0);
            $table->unsignedSmallInteger('assigned')->default(0);
            $table->unsignedSmallInteger('sent_to_fans')->default(0);
            $table->boolean('distributable')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
