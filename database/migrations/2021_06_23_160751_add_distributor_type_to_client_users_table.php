<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDistributorTypeToClientUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `client_users` CHANGE `type` `type` ENUM('ticket_office', 'client', 'temporary', 'distributor') DEFAULT 'client';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `client_users` CHANGE `type` `type` ENUM('ticket_office', 'client', 'temporary') DEFAULT 'client';");
    }
}
