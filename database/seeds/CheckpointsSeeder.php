<?php

use Illuminate\Database\Seeder;

class CheckpointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('checkpoints')->insert(['id' => 1, 'venue_id' => '1', 'name' => 'E-8', 'type' => 'parking']);
        DB::table('checkpoints')->insert(['id' => 2, 'venue_id' => '1', 'name' => 'Acceso I', 'type' => 'perimeter']);
        DB::table('checkpoints')->insert(['id' => 3, 'venue_id' => '1', 'name' => 'T-14', 'type' => 'tunnel']);
        DB::table('checkpoints')->insert(['id' => 4, 'venue_id' => '1', 'name' => 'T-16', 'type' => 'tunnel']);
        DB::table('checkpoints')->insert(['id' => 5, 'venue_id' => '1', 'name' => 'T-18', 'type' => 'tunnel']);
        DB::table('checkpoints')->insert(['id' => 6, 'venue_id' => '1', 'name' => 'T-24', 'type' => 'tunnel']);
        DB::table('checkpoints')->insert(['id' => 7, 'venue_id' => '2', 'name' => 'Caseta Entrada', 'type' => 'parking']);
        DB::table('checkpoints')->insert(['id' => 8, 'venue_id' => '3', 'name' => 'A1', 'type' => 'perimeter']);
        DB::table('checkpoints')->insert(['id' => 9, 'venue_id' => '3', 'name' => 'A2', 'type' => 'perimeter']);
        DB::table('checkpoints')->insert(['id' => 10, 'venue_id' => '3', 'name' => 'A3', 'type' => 'perimeter']);
        DB::table('checkpoints')->insert(['id' => 11, 'venue_id' => '3', 'name' => 'A4', 'type' => 'perimeter']);
        DB::table('checkpoints')->insert(['id' => 12, 'venue_id' => '3', 'name' => 'A5', 'type' => 'perimeter']);
        DB::table('checkpoints')->insert(['id' => 13, 'venue_id' => '3', 'name' => 'A6', 'type' => 'perimeter']);
        DB::table('checkpoints')->insert(['id' => 96, 'venue_id' => '3', 'name' => 'Cancha', 'type' => 'tunnel']);
        DB::table('checkpoints')->insert(['id' => 97, 'venue_id' => '3', 'name' => 'Vestidores', 'type' => 'other']);
    }
}
