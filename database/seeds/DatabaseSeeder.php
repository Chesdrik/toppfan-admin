<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(SupportTopics::class); // Support topics
        $this->call(UsersSeeder::class); // Users

        // $this->call(PumasSeeder::class); //  Pumas seeder (zones, checkpoints, ticket_types)

        $this->call(VenuesSeeder::class);
        $this->call(ZonesSeeder::class);
        $this->call(CheckpointsSeeder::class);
        $this->call(TicketTypeSeeder::class);
        $this->call(TemplateSeeder::class);
        $this->call(TemplateTicketTypeSeeder::class);
        $this->call(CheckpointTicketTypes::class);
        $this->call(StatisticTypeSeeder::class);
    }
}
