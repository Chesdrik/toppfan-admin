<?php

namespace Database\Seeders;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users =  User::all();
        for ($i=0; $i < 30000 ; $i++) {
            DB::table('orders')->insert([
                'user_id' => $users->random()->id,
                'event_id' => 3,
                'email' => Str::random(10).'@gmail.com',
                'total' => 0.00,
                'courtesy' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
