<?php

use Database\Seeders\OrderSeeder;
use Database\Seeders\TicketSeeder;
use Database\Seeders\VenuesSeeder;
use Database\Seeders\ZonesSeeder;
use Database\Seeders\CheckpointsSeeder;
use Database\Seeders\TicketTypeSeeder;
use Database\Seeders\TemplateSeeder;
use Database\Seeders\TemplateTicketTypeSeeder;
use Database\Seeders\CheckpointTicketTypes;
use Database\Seeders\StatisticTypeSeeder;

use Illuminate\Database\Seeder;

class PumasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            VenuesSeeder::class,
            ZonesSeeder::class,
            CheckpointsSeeder::class,
            TicketTypeSeeder::class,
            TemplateSeeder::class,
            TemplateTicketTypeSeeder::class,
            CheckpointTicketTypes::class,
            StatisticTypeSeeder::class,
            // OrderSeeder::class,
            // TicketSeeder::class,
        ]);
    }
}
