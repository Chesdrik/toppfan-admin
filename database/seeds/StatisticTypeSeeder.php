<?php

use Illuminate\Database\Seeder;
use App\StatisticType;

class StatisticTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Inserting data
        // \DB::table('statistic_types')->insert([
        $types = [
            ['id' => '1', 'name' => 'Ventas'],
            ['id' => '2', 'name' => 'Accesos (Tickets)'],
            ['id' => '3', 'name' => 'Salidas (Tickets)'],
            ['id' => '4', 'name' => 'Errores (Tickets)'],
            ['id' => '5', 'name' => 'General (Tickets)'],
            ['id' => '6', 'name' => 'Checkpoints'],
            ['id' => '7', 'name' => 'Accesos (Acreditaciones)'],
            ['id' => '8', 'name' => 'Salidas (Acreditaciones)'],
            ['id' => '9', 'name' => 'Errores (Acreditaciones)'],
            ['id' => '10', 'name' => 'General (Acreditaciones)'],
            ['id' => '11', 'name' => 'Dispositivos'],
            ['id' => '12', 'name' => 'Alertas por checkpoint'],
            ['id' => '13', 'name' => 'Alertas por dispositivo'],
            ['id' => '14', 'name' => 'Batería dispositivos'],
            ['id' => '15', 'name' => 'Incidencias'],
            ['id' => '16', 'name' => 'Lecturas de acreditaciones por dispositivos'],
            ['id' => '17', 'name' => 'Órdenes por evento'],
            ['id' => '18', 'name' => 'Órdenes generales'],
            ['id' => '19', 'name' => 'Análisis de lecturas de boletos'],
            ['id' => '20', 'name' => 'Análisis de lecturas de acreditaciones'],
            ['id' => '21', 'name' => 'Estatus de boletos por evento']
        ];

        foreach ($types as $type) {
            StatisticType::updateOrCreate(['id' => $type['id']], $type);
        }
    }
}
