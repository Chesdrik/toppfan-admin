<?php

use Illuminate\Database\Seeder;

class SupportTopics extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('topics')->insert(['name' => 'Eventos', 'parent_topic' => NULL, 'order' => 1]);
        DB::table('topics')->insert(['name' => 'Boletos', 'parent_topic' => NULL, 'order' => 2]);
        DB::table('topics')->insert(['name' => 'Acreditaciones', 'parent_topic' => NULL, 'order' => 3]);
        DB::table('topics')->insert(['name' => 'Accesos', 'parent_topic' => NULL, 'order' => 4]);
        DB::table('topics')->insert(['name' => 'PDAs', 'parent_topic' => NULL, 'order' => 5]);
        DB::table('topics')->insert(['name' => 'Estadísticas', 'parent_topic' => NULL, 'order' => 6]);
        DB::table('topics')->insert(['name' => 'Usuarios', 'parent_topic' => NULL, 'order' => 7]);
        DB::table('topics')->insert(['name' => 'Configuración', 'parent_topic' => NULL, 'order' => 8]);
        DB::table('topics')->insert(['name' => 'Registrar un evento', 'parent_topic' => 1, 'order' => 1]);
        DB::table('topics')->insert(['name' => 'Editar evento', 'parent_topic' => 1, 'order' => 2]);
        DB::table('topics')->insert(['name' => 'Exportar estadísticas a PDF', 'parent_topic' => 1, 'order' => 3]);
        DB::table('topics')->insert(['name' => 'Detalle de lecturas', 'parent_topic' => 1, 'order' => 4]);
        DB::table('topics')->insert(['name' => 'Detalle de ordenes', 'parent_topic' => 1, 'order' => 5]);
        DB::table('topics')->insert(['name' => 'Detalle de boletos', 'parent_topic' => 1, 'order' => 6]);
    }
}
