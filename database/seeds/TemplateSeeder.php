<?php

use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('ticket_templates')->insert([
            ['id' => '10', 'venue_id' => '1', 'name' => 'Puerta Cerrada'],
            ['id' => '14', 'venue_id' => '2', 'name' => 'Template de la cantera'],
            ['id' => '15', 'venue_id' => '3', 'name' => 'Puerta cerrada Tabasco']
        ]);
    }
}
