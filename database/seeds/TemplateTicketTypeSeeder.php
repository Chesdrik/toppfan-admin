<?php

use Illuminate\Database\Seeder;

class TemplateTicketTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('ticket_template_ticket_type')->insert([
            ['id' => '37', 'ticket_template_id' => '10', 'ticket_type_id' => '1', 'price' => '0.00'],
            ['id' => '38', 'ticket_template_id' => '10', 'ticket_type_id' => '2', 'price' => '0.00'],
            ['id' => '39', 'ticket_template_id' => '10', 'ticket_type_id' => '3', 'price' => '0.00'],
            ['id' => '40', 'ticket_template_id' => '10', 'ticket_type_id' => '4', 'price' => '0.00'],
            ['id' => '41', 'ticket_template_id' => '10', 'ticket_type_id' => '5', 'price' => '0.00'],
            ['id' => '42', 'ticket_template_id' => '10', 'ticket_type_id' => '6', 'price' => '0.00'],
            ['id' => '43', 'ticket_template_id' => '10', 'ticket_type_id' => '7', 'price' => '0.00'],
            ['id' => '44', 'ticket_template_id' => '10', 'ticket_type_id' => '8', 'price' => '0.00'],
            ['id' => '45', 'ticket_template_id' => '10', 'ticket_type_id' => '9', 'price' => '0.00'],
            ['id' => '51', 'ticket_template_id' => '15', 'ticket_type_id' => '23', 'price' => '2.50'],
            ['id' => '52', 'ticket_template_id' => '15', 'ticket_type_id' => '24', 'price' => '1.50'],
            ['id' => '53', 'ticket_template_id' => '15', 'ticket_type_id' => '25', 'price' => '1.50'],
            ['id' => '54', 'ticket_template_id' => '15', 'ticket_type_id' => '26', 'price' => '1.50'],
            ['id' => '55', 'ticket_template_id' => '15', 'ticket_type_id' => '27', 'price' => '1.50'],
            ['id' => '56', 'ticket_template_id' => '15', 'ticket_type_id' => '28', 'price' => '1.50'],
            ['id' => '57', 'ticket_template_id' => '15', 'ticket_type_id' => '29', 'price' => '1.50'],
            ['id' => '58', 'ticket_template_id' => '15', 'ticket_type_id' => '30', 'price' => '1.50'],
            ['id' => '59', 'ticket_template_id' => '15', 'ticket_type_id' => '31', 'price' => '1.50'],
            ['id' => '60', 'ticket_template_id' => '15', 'ticket_type_id' => '32', 'price' => '1.50'],
            ['id' => '61', 'ticket_template_id' => '15', 'ticket_type_id' => '33', 'price' => '1.50'],
            ['id' => '62', 'ticket_template_id' => '15', 'ticket_type_id' => '34', 'price' => '1.50'],
            ['id' => '63', 'ticket_template_id' => '15', 'ticket_type_id' => '35', 'price' => '1.50'],
            ['id' => '64', 'ticket_template_id' => '15', 'ticket_type_id' => '36', 'price' => '1.50'],
            ['id' => '65', 'ticket_template_id' => '15', 'ticket_type_id' => '37', 'price' => '1.50'],
            ['id' => '66', 'ticket_template_id' => '15', 'ticket_type_id' => '38', 'price' => '1.50'],
            ['id' => '67', 'ticket_template_id' => '15', 'ticket_type_id' => '39', 'price' => '1.50']
        ]);

    }
}
