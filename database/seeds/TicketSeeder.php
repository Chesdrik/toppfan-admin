<?php

namespace Database\Seeders;

use App\Order;
use App\TicketType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::where('event_id',3)->get();
        $tickets = TicketType::all();
        for ($i=0; $i < 40000 ; $i++) {
            DB::table('tickets')->insert([
                'event_id' => 3,
                'ticket_type_id' => $tickets->random()->id,
                'order_id' => $orders->random()->id,
                'amount' => 1,
                'used' => 0,
                'type' => 'parking',
                'price' => 0.00,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
