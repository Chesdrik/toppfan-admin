<?php

use Illuminate\Database\Seeder;

class TicketTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('ticket_types')->insert([
            ['id' => '1', 'venue_id' => '1', 'zone_id' => '13', 'name' => 'Publicidad', 'color' => '1A202C', 'price' => '2.50', 'active' => '1', 'free' => '0', 'available_seats' => '100', 'total_seats' => '123', 'created_at' => NULL, 'updated_at' => '2021-02-03 18:19:18'],
            ['id' => '2', 'venue_id' => '1', 'zone_id' => '13', 'name' => 'Televisa', 'color' => '1A202C', 'price' => '1.35', 'active' => '1', 'free' => '0', 'available_seats' => '12', 'total_seats' => '123', 'created_at' => NULL, 'updated_at' => '2021-02-03 18:19:38'],
            ['id' => '3', 'venue_id' => '1', 'zone_id' => '13', 'name' => 'Sanitizer', 'color' => '1A202C', 'price' => '1.00', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => NULL, 'created_at' => NULL, 'updated_at' => NULL],
            ['id' => '4', 'venue_id' => '1', 'zone_id' => '13', 'name' => 'DC', 'color' => '1A202C', 'price' => '1.00', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => NULL, 'created_at' => NULL, 'updated_at' => NULL],
            ['id' => '5', 'venue_id' => '1', 'zone_id' => '13', 'name' => 'Jugadores', 'color' => '1A202C', 'price' => '1.00', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => NULL, 'created_at' => NULL, 'updated_at' => NULL],
            ['id' => '6', 'venue_id' => '1', 'zone_id' => '14', 'name' => 'Palomar VIP', 'color' => '1A202C', 'price' => '1.00', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => NULL, 'created_at' => NULL, 'updated_at' => NULL],
            ['id' => '7', 'venue_id' => '1', 'zone_id' => '13', 'name' => 'Medios', 'color' => '1A202C', 'price' => '1.00', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => NULL, 'created_at' => NULL, 'updated_at' => NULL],
            ['id' => '8', 'venue_id' => '1', 'zone_id' => '13', 'name' => 'Cancha', 'color' => '1A202C', 'price' => '1.00', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => NULL, 'created_at' => NULL, 'updated_at' => NULL],
            ['id' => '9', 'venue_id' => '1', 'zone_id' => '13', 'name' => 'All access', 'color' => '1A202C', 'price' => '1.00', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => NULL, 'created_at' => NULL, 'updated_at' => NULL],
            ['id' => '23', 'venue_id' =>  '3', 'zone_id' => '2', 'name' => 'Z1-4',  'color' => 'EA3F00', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '216', 'total_seats' => '216', 'created_at' => '2021-03-02 15:07:29', 'updated_at' => '2021-03-24 13:50:20'],
            ['id' => '24', 'venue_id' =>  '3', 'zone_id' => '2', 'name' => 'Z1-5',  'color' => 'EA3F00', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '251', 'total_seats' => '251', 'created_at' => '2021-03-02 15:08:07', 'updated_at' => '2021-03-24 13:50:50'],
            ['id' => '25', 'venue_id' =>  '3', 'zone_id' => '3', 'name' => 'Z2-6',  'color' => '281A73', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '268', 'total_seats' => '268', 'created_at' => '2021-03-24 13:51:53', 'updated_at' => '2021-03-24 13:51:53'],
            ['id' => '26', 'venue_id' =>  '3', 'zone_id' => '3', 'name' => 'Z2-7',  'color' => '281A73', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '310', 'total_seats' => '310', 'created_at' => '2021-03-24 13:52:14', 'updated_at' => '2021-03-24 13:52:45'],
            ['id' => '27', 'venue_id' =>  '3', 'zone_id' => '3', 'name' => 'Z2-8',  'color' => '281A73', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '254', 'total_seats' => '254', 'created_at' => '2021-03-24 13:52:39', 'updated_at' => '2021-03-24 13:52:39'],
            ['id' => '28', 'venue_id' =>  '3', 'zone_id' => '3', 'name' => 'Z2-9',  'color' => '281A73', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '314', 'total_seats' => '314', 'created_at' => '2021-03-24 13:53:09', 'updated_at' => '2021-03-24 13:53:09'],
            ['id' => '29', 'venue_id' =>  '3', 'zone_id' => '3', 'name' => 'Z2-10', 'color' => '281A73', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '266', 'total_seats' => '266', 'created_at' => '2021-03-24 13:53:40', 'updated_at' => '2021-03-24 13:53:40'],
            ['id' => '30', 'venue_id' =>  '3', 'zone_id' => '4', 'name' => 'Z3-11', 'color' => '40AFFF', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '252', 'total_seats' => '252', 'created_at' => '2021-03-24 13:55:20', 'updated_at' => '2021-03-24 13:55:20'],
            ['id' => '31', 'venue_id' =>  '3', 'zone_id' => '4', 'name' => 'Z3-12', 'color' => '40AFFF', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => '214', 'total_seats' => '214', 'created_at' => '2021-03-24 13:55:45', 'updated_at' => '2021-03-24 13:55:45'],
            ['id' => '32', 'venue_id' =>  '3', 'zone_id' => '5', 'name' => 'Z4-18', 'color' => 'ED0077', 'price' => '0.00', 'active' => '1', 'free' => '0', 'available_seats' => '49', 'total_seats' => '49', 'created_at' => '2021-03-24 13:56:21', 'updated_at' => '2021-03-24 15:44:28'],
            ['id' => '33', 'venue_id' =>  '3', 'zone_id' => '5', 'name' => 'Z4-19', 'color' => 'ED0077', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => '188',  'created_at' => '2021-03-24 13:57:37', 'updated_at' => '2021-03-24 13:57:37'],
            ['id' => '34', 'venue_id' =>  '3', 'zone_id' => '5', 'name' => 'Z4-20', 'color' => 'ED0077', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => '215',  'created_at' => '2021-03-24 13:59:48', 'updated_at' => '2021-03-24 13:59:48'],
            ['id' => '35', 'venue_id' =>  '3', 'zone_id' => '6', 'name' => 'Z5-21', 'color' => 'EF9F00', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => '196',  'created_at' => '2021-03-24 14:01:34', 'updated_at' => '2021-03-24 15:44:06'],
            ['id' => '36', 'venue_id' =>  '3', 'zone_id' => '6', 'name' => 'Z5-22', 'color' => 'EF9F00', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => '202',  'created_at' => '2021-03-24 14:02:11', 'updated_at' => '2021-03-24 14:02:11'],
            ['id' => '37', 'venue_id' =>  '3', 'zone_id' => '6', 'name' => 'Z5-23', 'color' => 'EF9F00', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => '196',  'created_at' => '2021-03-24 14:02:35', 'updated_at' => '2021-03-24 15:44:13'],
            ['id' => '38', 'venue_id' =>  '3', 'zone_id' => '8', 'name' => 'Z6-24', 'color' => '3B6D12', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => '206',  'created_at' => '2021-03-24 14:02:57', 'updated_at' => '2021-03-24 14:02:57'],
            ['id' => '39', 'venue_id' =>  '3', 'zone_id' => '8', 'name' => 'Z6-25', 'color' => '3B6D12', 'price' => '1.50', 'active' => '1', 'free' => '0', 'available_seats' => NULL, 'total_seats' => '193',  'created_at' => '2021-03-24 14:03:13', 'updated_at' => '2021-03-24 14:03:13'],
            ['id' => '40', 'venue_id' =>  '3', 'zone_id' => '2', 'name' => 'All Access Tabasco', 'color' => '1A202C', 'price' => '0.00', 'active' => '0', 'free' => '0', 'available_seats' => NULL, 'total_seats' => NULL, 'created_at' => '2021-03-26 13:20:41', 'updated_at' => '2021-03-26 13:20:41'],
            ['id' => '41', 'venue_id' =>  '3', 'zone_id' => '2', 'name' => 'Cancha', 'color' => '1A202C', 'price' => '0.00', 'active' => '0', 'free' => '0', 'available_seats' => '5000', 'total_seats' => '5000', 'created_at' => '2021-03-26 13:20:41', 'updated_at' => '2021-03-26 13:20:41'],
            ['id' => '42', 'venue_id' =>  '3', 'zone_id' => '2', 'name' => 'A1', 'color' => '1A202C', 'price' => '0.00', 'active' => '0', 'free' => '0', 'available_seats' => '5000', 'total_seats' => '5000', 'created_at' => '2021-03-26 13:20:41', 'updated_at' => '2021-03-26 13:20:41'],
        ]);
    }
}
