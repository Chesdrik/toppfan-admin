<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon\Carbon::now()->format('Y-m-d H:i:s');

        DB::table('users')->insert(['name' => 'Diego Filloy Ring', 'email' => 'diego@filloy.com.mx', 'email_verified_at' => NULL, 'password' => '$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS', 'remember_token' => NULL, 'type' => 'root', 'active' => '1', 'created_at' => $date, 'updated_at' => $date]);
        DB::table('users')->insert(['name' => 'Diego Filloy Ring', 'email' => 'dfilloyr@hotmail.com', 'email_verified_at' => NULL, 'password' => '$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS', 'remember_token' => NULL, 'type' => 'client', 'active' => '1', 'created_at' => $date, 'updated_at' => $date]);
        DB::table('users')->insert(['name' => 'Alinka Fragoso', 'email' => 'alinkafm@ciencias.unam.mx', 'email_verified_at' => NULL, 'password' => '$2y$12$/Blb6oK1AgDPQqGP1HhB/OZfsmMHt3v4XB7buRl979/igPgAzd2TW', 'remember_token' => NULL, 'type' => 'admin', 'active' => '1', 'created_at' => $date, 'updated_at' => $date]);
        DB::table('users')->insert(['name' => 'Cortesías', 'email' => 'cortesias@pumasgol.com', 'email_verified_at' => NULL, 'password' => '$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS', 'remember_token' => NULL, 'type' => 'courtesies', 'active' => '1', 'created_at' => $date, 'updated_at' => $date]);
        DB::table('users')->insert(['name' => 'Jessica Jaime', 'email' => 'jessjaime05@gmail.com', 'email_verified_at' => NULL, 'password' => '$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS', 'remember_token' => NULL, 'type' => 'admin', 'active' => '1', 'created_at' => $date, 'updated_at' => $date]);
        DB::table('users')->insert(['name' => 'Roberto Hernández', 'email' => 'betohr92@gmail.com', 'email_verified_at' => NULL, 'password' => '$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS', 'remember_token' => NULL, 'type' => 'admin', 'active' => '1', 'created_at' => $date, 'updated_at' => $date]);
        DB::table('users')->insert(['name' => 'Luis Cervantes', 'email' => 'cervants2802@gmail.com', 'email_verified_at' => NULL, 'password' => '$2y$12$KyzcJ9g2iKxXg80tSdnZeuMcXHQ9fNSNbr.gYKNbbF0DiZPr2CxpS', 'remember_token' => NULL, 'type' => 'admin', 'active' => '1', 'created_at' => $date, 'updated_at' => $date]);
    }
}
