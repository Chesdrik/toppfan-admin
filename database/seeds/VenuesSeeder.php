<?php

use Illuminate\Database\Seeder;

class VenuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('venues')->insert([
            ['id' => '1', 'name' => 'Estadio Olímpico Universitario', 'lat' => '19.33207270', 'long' => '-99.19222550', 'created_at' => '2020-03-15 16:21:00', 'updated_at' => '2020-03-15 16:21:00'],
            ['id' => '2', 'name' => 'La Cantera', 'lat' => '19.31503260', 'long' => '-99.17108140', 'created_at' => '2020-03-15 16:21:00', 'updated_at' => '2020-03-15 16:21:00'],
            ['id' => '3', 'name' => 'Estadio Olímpico de Villahermosa', 'lat' => '17.97463380', 'long' => '-92.94478640', 'created_at' => '2020-03-15 16:21:00', 'updated_at' => '2020-03-15 16:21:00']
        ]);
    }
}
