<?php

use Illuminate\Database\Seeder;

class ZonesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('zones')->insert(['id' => 1, 'venue_id' => '3', 'parent_zone' => NULL, 'name' => 'Zona 1', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 2, 'venue_id' => '3', 'parent_zone' => '1', 'name' => 'Sección 1', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 3, 'venue_id' => '3', 'parent_zone' => '1', 'name' => 'Sección 2', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 4, 'venue_id' => '3', 'parent_zone' => '1', 'name' => 'Sección 3', 'type' => 'general', 'active' => 0]);
        DB::table('zones')->insert(['id' => 5, 'venue_id' => '3', 'parent_zone' => '1', 'name' => 'Sección 4', 'type' => 'general', 'active' => 0]);
        DB::table('zones')->insert(['id' => 6, 'venue_id' => '3', 'parent_zone' => '1', 'name' => 'Sección 5', 'type' => 'general', 'active' => 0]);
        DB::table('zones')->insert(['id' => 7, 'venue_id' => '3', 'parent_zone' => NULL, 'name' => 'Zona 2', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 8, 'venue_id' => '3', 'parent_zone' => '7', 'name' => 'Sección 6', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 9, 'venue_id' => '3', 'parent_zone' => '7', 'name' => 'Sección 7', 'type' => 'general', 'active' => 0]);
        DB::table('zones')->insert(['id' => 10, 'venue_id' => '3', 'parent_zone' => '7', 'name' => 'Sección 8', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 11, 'venue_id' => '3', 'parent_zone' => '7', 'name' => 'Sección 9', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 12, 'venue_id' => '3', 'parent_zone' => '7', 'name' => 'Sección 10', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 13, 'venue_id' => '1', 'parent_zone' => NULL, 'name' => 'Palomar', 'type' => 'general', 'active' => 1]);
        DB::table('zones')->insert(['id' => 14, 'venue_id' => '1', 'parent_zone' => '13', 'name' => 'Palomar 1', 'type' => 'general', 'active' => 1]);
    }
}
