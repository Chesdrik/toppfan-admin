
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');
    window.Vapor = require('laravel-vapor');

    const Swal = window.Swal = require('sweetalert2');
    require('bootstrap');
} catch (e) { }

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

global.moment = require('moment');
import 'moment-timezone';
require('tempusdominus-bootstrap-4');
import Dropzone from 'dropzone';



$.fn.datetimepicker.Constructor.Default = $.extend({}, $.fn.datetimepicker.Constructor.Default, {
    icons: {
        time: 'zmdi zmdi-time zmdi-hc-fw',
        date: 'zmdi zmdi-calendar zmdi-hc-fw',
        up: 'zmdi zmdi-chevron-up zmdi-hc-fw',
        down: 'zmdi zmdi-chevron-down zmdi-hc-fw',
        previous: 'zmdi zmdi-chevron-left zmdi-hc-fw',
        next: 'zmdi zmdi-chevron-right zmdi-hc-fw',
        today: 'zmdi zmdi-calendar zmdi-hc-fw',
        clear: 'zmdi zmdi-delete zmdi-hc-fw',
        close: 'zmdi zmdi-close zmdi-hc-fw'
    }
});
