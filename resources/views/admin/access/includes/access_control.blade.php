@if($access->requirements->contains($requirement->id))
    <div class="row p-b-15">
        <div class="col-sm-2">
            <a href="{{ route('system.requirements.associate', [$requirement->id, $access->id]) }}" target="_self">
                <button class="btn btn-sm btn-success btn--raised"><i class="zmdi zmdi-check"></i></button>
            </a>
        </div>
        <div class="col-sm-010">
            <h6>{{ $requirement->description }}</h6>
            {{ $requirement->pretty_type() }}<br />
            <?php
                $access_requirement = $access->requirements()->where('requirement_id', $requirement->id)->first();
            ?>
            @if($access_requirement->pivot->data_type == 'text')
                {{ $access_requirement->pivot->request }}
            @elseif($access_requirement->pivot->data_type == 'bool')
                {{ $access_requirement->pivot->request }}
            @endif
        </div>
    </div>
@else
    <div class="row p-b-15">
        <div class="col-sm-2">
            <a href="{{ route('system.requirements.associate', [$requirement->id, $access->id]) }}" target="_self">
                <button class="btn btn-sm btn-light btn--raised"><i class="zmdi zmdi-refresh"></i></button>
            </a>
        </div>
        <div class="col-sm-010">
            <h6>{{ $requirement->description }}</h6>
            {{ $requirement->pretty_type() }}
        </div>
    </div>
@endif
