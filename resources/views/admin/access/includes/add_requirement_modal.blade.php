<div class="modal fade" id="registrar-requerimiento" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el requerimiento.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif


            {!! Form::open(array('url' => route('system.requirements.store'), 'method' => 'POST')) !!}
                <div class="modal-body">
                    Registra los datos del requerimiento de acceso:
                    <hr />

                    {!! Form::MDtextarea('Descripción', 'description', '', $errors) !!}
                    {!! Form::MDselect('Tipo de requerimiento', 'type', $access->type, ['photo' => 'Fotografía',  'revision' => 'Revisión',  'message' => 'Mensaje',  'request' => 'Solicitud'], $errors) !!}
                </div>
                <div class="modal-footer">
                    {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
