<div class="modal fade" id="editar-acceso" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo editar el acceso.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif


            {!! Form::open(array('url' => route('system.access.update', ['acceso' => $access]), 'method' => 'PATCH')) !!}
                <div class="modal-body">
                    Edita los datos del acceso:
                    <hr />

                    {!! Form::MDtext('Nombre visita', 'visitor_name', $access->visitor_name, $errors) !!}
                    {!! Form::MDdatetimepicker('Inicio acceso', 'start', $access->start, $errors) !!}
                    {!! Form::MDdatetimepicker('Fin acceso', 'end', $access->end, $errors) !!}
                    {!! Form::MDselect('Tipo acceso', 'type', $access->type, ['foot' => ' A pie', 'car' => 'En auto', 'other' => 'Otro'], $errors) !!}
                </div>
                <div class="modal-footer">
                    {!! Form::MDsubmit('Editar', 'create', ['icon' => 'card-sd']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
