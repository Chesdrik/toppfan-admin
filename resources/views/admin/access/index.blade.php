@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Accesos</h1>
        <div class="actions actions--calendar">
            <!-- <a href="{{ route('system.access.reports', [session('system_slug')]) }}" class="actions__item zmdi zmdi-file-text zmdi-hc-fw"></a> -->
            <!-- <a href="{{ route('system.access.reports', [session('system_slug')]) }}" class="actions__item zmdi zmdi-file-text zmdi-hc-fw"></a> -->
            <!-- <a href="#registrar-acceso" data-toggle="modal" data-target="#registrar-acceso" class="actions__item zmdi zmdi-plus"></a> -->

            <a href="#importar-acceso" data-toggle="modal" data-target="#importar-acceso" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-upload"></i> Importar accesos
            </a>

            <a href="{{ route('system.access.reports', [session('system_slug')]) }}" data-toggle="modal" data-target="#registrar-acceso"  class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar acceso
            </a>

            <a href="{{ route('system.access.reports', [session('system_slug')]) }}"  class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-file-text"></i> Ver reportes
            </a>
        </div>
    </header>


    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Accesos</h4>
            <h6 class="card-subtitle">Listado de los accesos registrados en el sistema</h6>

            @if(Session::has('success_message'))
                <div class="alert alert-success alert-dismissible fade show m-b-30">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar">
                        <span aria-hidden="true">×</span>
                    </button>

                    {!! Session::get('success_message') !!}
                </div>
            @endif

            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th>&nbsp;</th>
                            <th>Visitante</th>
                            <th>Solicita</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($access as $a)
                            <tr>
                                <td>
                                    <a href="{!! route('system.access.show', [session('system_slug'),  $a]) !!}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $a->visitor_name }}</td>
                                <td>{{ $a->user->name }}</td>
                                <td>{{ $a->datespan() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Default -->
    <div class="modal fade" id="registrar-acceso" tabindex="-2">
        <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">
                        Registrar acceso<br />
                        <small>Registra los siguientes datos para permitir el acceso a un visitante</small>
                    </h5>
                </div>

                {!! Form::open(array('url' => route('system.access.store', [session('system_slug')]), 'method' => 'POST')) !!}
                    <div class="modal-body">
                        <div class="row">
                            {!! Form::MDtext('Nombre visita', 'visitor_name', '', $errors) !!}
                            {!! Form::MDdatetimepicker('Inicio acceso', 'start', '', $errors) !!}
                            {!! Form::MDdatetimepicker('Fin acceso', 'end', '', $errors) !!}
                            {!! Form::MDselect('Tipo acceso', 'type', 'car', ['foot' => ' A pie', 'car' => 'En auto', 'other' => 'Otro'], $errors) !!}
                            {!! Form::MDselect('Tipo de requerimiento', 'requirement_id', '', $requirements, $errors) !!}
                            {!! Form::MDtext('detalle del requerimiento', 'detail', '', $errors) !!}
                            {!! Form::MDselect('Tipo de rol', 'access_role_id', '', $roles, $errors) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-link">Registrar</button> -->
                        <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button> -->
                        {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="importar-acceso" tabindex="-2">
        <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">
                        Importar acceso<br />
                        <small>Selecciona un archivo TSV para importar los accesos</small>
                    </h5>
                </div>

                {!! Form::open(array('url' => route('system.access.import', [session('system_slug')]), 'method' => 'POST', 'enctype' => 'multipart/form-data')) !!}
                    <div class="modal-body">
                        <div class="row">
                            {!! Form::MDselect('Sede', 'venue_id', '', App\Venue::pluck('name', 'id'), $errors) !!}
                             <input type="file" name="file" class="form-control col-xs-12">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-link">Registrar</button> -->
                        <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button> -->
                        {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable().order([3, 'desc']).draw();
    });

</script>
@endsection
