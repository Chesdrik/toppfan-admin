
@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Accesos</h1>
        <form id="day-charts-form" method="GET">
            <div class="actions actions--calendar">
                <div class="form-group">
                    <label for="max_date">Semana</label>
                    <input name="max_date" type="date" value="{{$max_date}}" onchange="document.getElementById('day-charts-form').submit()" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text" />
                </div>

            </div>
        </form>
    </header>


    <div class="card">
        <div class="card-body">
            <h4 class="card-title">{{ $boundries[2] }}</h4>
            <h6 class="card-subtitle">Listado de los accesos registrados en el sistema</h6>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-xs-4">&nbsp;</th>
                        @foreach($access_data->get('week') as $day)
                            <th class="col-xs-1 text-center">
                                <strong>{{ $day->get('label') }}</strong>
                                <br />
                            </th>
                        @endforeach
                        <th class="col-xs-1">&nbsp;</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($access_data->get('data') as $attendance)
                        <tr>
                            <td>{{ $attendance->get('name') }}</td>

                            @foreach($attendance->get('days_attended') as $day_attended)
                                <td class="{{ $day_attended ? 'widget-time bg-green' : '' }}">&nbsp;</td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
