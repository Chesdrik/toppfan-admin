@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Reporte de accesos</h1>
    </header>

    <div class="row">
        @foreach($reports as $i => $report)
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ $report }}</h4>
                        <h6 class="card-subtitle">Listado de los reportes de accesos</h6>

                        <a href="{{ url('/admin/accesos/reportes/'.$i) }}" target="_self" class="btn btn-light btn-lg btn-block btn--icon-text">
                            <i class="zmdi zmdi-file-text"></i> Ver reporte
                        </a>

                        <a href="{{ route('system.access.qrs', ['id' => $i]) }}" target="_self" class="btn btn-light btn-lg btn-block btn--icon-text">
                            <i class="zmdi zmdi-center-focus-weak"></i> Ver QRs
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>


    <!-- Default -->
    <div class="modal fade" id="registrar-acceso" tabindex="-2">
        <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title pull-left">
                        Registrar acceso<br />
                        <small>Registra los siguientes datos para permitir el acceso a un visitante</small>
                    </h5>
                </div>

                {!! Form::open(array('url' => route('system.access.store'), 'method' => 'POST')) !!}
                    <div class="modal-body">
                        <div class="row">
                            {!! Form::MDtext('Nombre visita', 'visitor_name', '', $errors) !!}
                            {!! Form::MDdatetimepicker('Inicio acceso', 'start', '', $errors) !!}
                            {!! Form::MDdatetimepicker('Fin acceso', 'end', '', $errors) !!}
                            {!! Form::MDselect('Tipo acceso', 'type', 'car', ['foot' => ' A pie', 'car' => 'En auto', 'other' => 'Otro'], $errors) !!}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button type="button" class="btn btn-link">Registrar</button> -->
                        <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button> -->
                        {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
