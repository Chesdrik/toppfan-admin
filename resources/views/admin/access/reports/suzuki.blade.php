@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>
            Reporte Suzuki <br />
            <small>{{ $datespan }}</small>
        </h1>
        <div class="actions actions--calendar">
        <a href="{{ route('system.requirements.calendar') }}"  class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                <i class="actions__item zmdi zmdi-account-calendar"></i> Ver calendario
            </a>
        </div>
    </header>

    @foreach($access_logs as $logs)
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ $logs['data']->visitor_name }}</h4>
                        <h6 class="card-subtitle">Listado de los registros de acceso</h6>

                        Inicio permiso: {{ $logs['data']->start }}<br />
                        Fin permiso: {{ $logs['data']->end }}
                    </div>
                </div>
            </div>


            @foreach($logs['access'] as $access_log)
            <div class="col-sm-8 offset-sm-4 ">
                <div class="card {{ $access_log->pretty_type_card_class() }}">
                    <div class="card-body">
                        <h4 class="card-title ">
                            {!! $access_log->pretty_type_icon() !!}
                            {{ $access_log->pretty_type() }}
                        </h4>
                        <h6 class="card-subtitle">Hora registro: {{ $access_log->timestamp }}</h6>

                        @if($access_log->log)
                            {{ $access_log->log }}
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    @endforeach

@endsection
