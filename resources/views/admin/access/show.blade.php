@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Acceso</h1>
    </header>

    <div class="row">
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Acceso registrado</h4>
                    <h6 class="card-subtitle">Fecha de registro: {{ $access->pretty_created_at() }}</h6>

                    @if(Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show m-b-30">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Cerrar">
                                <span aria-hidden="true">×</span>
                            </button>

                            {!! Session::get('success_message') !!}
                        </div>
                    @endif

                    <div class="actions">
                        <a href="#edit-access-btn" data-toggle="modal" data-target="#edit-access" class="actions__item zmdi zmdi-edit"></a>
                        <a href="#delete-access-btn" id="delete-access" class="actions__item zmdi zmdi-delete"></a>
                    </div>

                    <p class="card-text">
                        <div class="card-body__title">Nombre visita</div>
                        {{ $access->visitor_name }}<hr />

                        <div class="card-body__title">Inicio acceso</div>
                        {{ $access->start }}<hr />

                        <div class="card-body__title">Fin acceso</div>
                        {{ $access->end }}<hr />

                        <div class="card-body__title">Tipo acceso</div>
                        {{ $access->pretty_type() }}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Control de acceso</h4>

                    <div class="actions">
                        <a href="#registrar-requerimiento-btn" data-toggle="modal" data-target="#registrar-requerimiento" class="actions__item zmdi zmdi-plus"></a>
                    </div>

                    @foreach($requirements as $requirement)
                        @include('admin.access.includes.access_control')
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">QR de acceso</h4>
                    <h6 class="card-subtitle">Recuerda que si cambias información del acceso, el QR cambia, por lo tanto es necesario enviarle a tu visita la nueva versión.</h6>
                    <img src="{!! $access->qr() !!}" class="img-fluid" />
                </div>
            </div>
        </div>
    </div>

    <!-- Dismiss form -->
    {!! Form::open(array('url' => route('system.access.destroy', ['acceso' => $access]), 'method' => 'DELETE', 'id' => 'delete-access-form')) !!}
    {!! Form::close() !!}

    @include('admin.access.includes.edit_access_modal')
    @include('admin.access.includes.add_requirement_modal')
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $(function () {
            $('#test').datetimepicker();
        });

        $('#delete-access').click(function(){
            var result = Swal.fire({
                title: 'Seguro que deseas eliminar este acceso?',
                text: 'Una vez eliminado el QR será inválido y se tendrá que generar uno nuevo.',
                icon: 'question',
                buttonsStyling: false,
                confirmButtonText: 'Cancelar',
                confirmButtonClass: 'btn btn-info',
                cancelButtonText: 'Eliminar',
                showCancelButton: true,
                cancelButtonClass: 'btn btn-danger',

            }).then(function(dismiss){
                // Dismiss by click on cancelar button
                if(dismiss.dismiss == 'cancel'){
                    $("#delete-access-form").submit();
                }
            });

            // console.log(result);
        });
    });

    @if($errors->any())
        $(window).on('load',function(){
            $('#edit-access').modal('show');
        });
    @endif
</script>
@endsection
