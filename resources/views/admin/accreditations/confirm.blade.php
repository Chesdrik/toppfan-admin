@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Confirmación de cortesías</h1>
</header>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="card-body" style="padding: 1.5rem;">
                        <div class="row">
                            <div class="col-sm-12 p-b-20 text-center">
                                <img src="{{ asset('/assets/images/pumas_chivas.png') }}" class="img-fluid" style=" max-width: 70%; max-height:100%; padding: 10px 75px;" />
                                {{-- <img class="invoice__logo" src="{{ $event->img_path() }}" alt="" style="max-width: 70%; max-height:100%;"><br> <br> --}}

                                <h4>{{ $event->name }}</h4>
                                <address>
                                    {{ pretty_date($event->date) }}
                                </address>
                                <hr />

                            </div>

                            <table class="table table-bordered invoice__table">
                                <thead>
                                    <tr class="text-uppercase">
                                        <th>TIPO DE CORTESÍA</th>
                                        <th>CANTIDAD</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach($data as $item)
                                    <tr>
                                        <td style="width: 50%">{{ $item['ticket']->name }}</td>
                                        <td>{{ $item['total'] }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="col-md-12 text-right">
                                <a href="{{ route('system.events.accreditations.store', $event->id) }}" class="text-right">
                                    <button class="btn btn-info">
                                        &nbsp;&nbsp;&nbsp;&nbsp; Generar &nbsp;&nbsp;
                                        <span><i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i></span>
                                    </button>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-sm-1">&nbsp;</div>
            <div class="col-sm-6">
                <div id="venue">
                    <img id="venue-background" src="{{ asset('/assets/venues/'.$venue->id.'/background.png') }}" class="img-fluid img-venue" />

                    @foreach($venue->ticket_types as $ticket_type)
                        <img src="{{ asset('/assets/venues/'.$venue->id.'/'.$ticket_type->id.'.png') }}" class="img-fluid img-venue seat-type" data-type="seat_{{ $ticket_type->id }}" />
                    @endforeach
                </div>
            </div> --}}
        </div>
    </div>
</div>
@endsection
