<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
    <style>
        @media only screen and (max-width: 600px) {
            .inner-body {
                width: 100% !important;
            }

            .footer {
                width: 100% !important;
            }
        }

        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }


        /* Base */

        body, body *:not(html):not(style):not(br):not(tr):not(code) {
            font-family: Avenir, Helvetica, sans-serif;
            box-sizing: border-box;
        }

        body {
            background-color: #f5f8fa;
            color: #74787E;
            height: 100%;
            hyphens: auto;
            line-height: 1.4;
            margin: 0;
            -moz-hyphens: auto;
            -ms-word-break: break-all;
            width: 100% !important;
            -webkit-hyphens: auto;
            -webkit-text-size-adjust: none;
            word-break: break-all;
            word-break: break-word;
        }

        p,
        ul,
        ol,
        blockquote {
            line-height: 1.4;
            text-align: left;
        }

        a {
            color: #3869D4;
        }

        a img {
            border: none;
        }

        /* Typography */

        h1 {
            color: #2F3133;
            font-size: 19px;
            font-weight: bold;
            margin-top: 0;
            text-align: left;
        }

        h2 {
            color: #2F3133;
            font-size: 16px;
            font-weight: bold;
            margin-top: 0;
            text-align: left;
        }

        h3 {
            color: #2F3133;
            font-size: 14px;
            font-weight: bold;
            margin-top: 0;
            text-align: left;
        }

        p {
            color: #74787E;
            font-size: 16px;
            line-height: 1.5em;
            margin-top: 0;
            text-align: left;
        }

        p.sub {
            font-size: 12px;
        }

        img {
            max-width: 100%;
        }

        /* Layout */

        .wrapper {
            background-color: #f5f8fa;
            margin: 0;
            padding: 0;
            width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            -premailer-width: 100%;
        }

        .content {
            margin: 0;
            padding: 0;
            width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            -premailer-width: 100%;
        }

        /* Header */

        .header {
            padding: 25px 0;
            text-align: center;
        }

        .header a {
            color: #bbbfc3;
            font-size: 19px;
            font-weight: bold;
            text-decoration: none;
            text-shadow: 0 1px 0 white;
        }

        /* Body */

        .body {
            background-color: #FFFFFF;
            border-bottom: 1px solid #EDEFF2;
            border-top: 1px solid #EDEFF2;
            margin: 0;
            padding: 0;
            width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            -premailer-width: 100%;
        }

        .inner-body {
            background-color: #FFFFFF;
            margin: 0 auto;
            padding: 0;
            width: 570px;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            -premailer-width: 570px;
        }

        /* Subcopy */

        .subcopy {
            border-top: 1px solid #EDEFF2;
            margin-top: 25px;
            padding-top: 25px;
        }

        .subcopy p {
            font-size: 12px;
        }

        /* Footer */

        .footer {
            margin: 0 auto;
            padding: 0;
            text-align: center;
            width: 570px;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            -premailer-width: 570px;
        }

        .footer p {
            color: #AEAEAE;
            font-size: 12px;
            text-align: center;
        }

        /* Tables */

        .table table {
            margin: 30px auto;
            width: 100%;
            -premailer-cellpadding: 0;
            -premailer-cellspacing: 0;
            -premailer-width: 100%;
        }

        .table th {
            border-bottom: 1px solid #EDEFF2;
            padding-bottom: 8px;
            margin: 0;
        }

        .table td {
            color: #74787E;
            font-size: 15px;
            line-height: 18px;
            padding: 10px 0;
            margin: 0;
        }

        .content-cell {
            padding: 35px;
        }

    </style>

    <table class="wrapper" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table class="content" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                    <td class="header">
                            <a href="">

                                ¡Hola!
                            </a>
                        </td>
                    </tr>

                    <!-- Email Body -->
                    <tr>
                        <td class="body" width="100%" cellpadding="0" cellspacing="0">
                            <table class="inner-body" align="center" width="570" cellpadding="0" cellspacing="0">
                                <!-- Body content -->
                                <tr>
                                    <td class="content-cell">
                                     @php 
                                        $name = reset($ticket);
                                     @endphp
                                        <div>Apreciable {{ $name->name }}</div><br />
                                        <div>Le informamos que usted ha sido acreditado para el evento Pumas - {{$event->name}}. Le rogamos leer cuidadosamente el contenido de este correo.</div><br />
                                        <div> El Estadio Olímpico Universitario (EOU) está dividido en tres zonas definidas por la autoridad competente en materia de protección civil y por el Club Universidad Nacional (CUN) conforme a lo siguiente:'</div><br>
                                        <div>ZONA 1 – Cancha</div><br>
                                        <div>ZONA 2 – Tribuna</div><br>
                                        <div>ZONA 3 – Alrededores del EOU</div><br>
                                        <div>Le solicitamos atentamente atender las siguientes indicaciones.</div><br>
                                        <div>1. Su acreditación está asignada para la Zona @foreach($order->tickets as $ticket) {{$ticket->ticket_type->name}}@endforeach.</div><br>
                                        <div>2. Su acceso al EOU será peatonal o en auto por el Acceso I ubicado en las inmediaciones del Estacionamiento 8.</div><br>
                                        <div>3. IMPORTANTE. Su acceso al interior del EOU será únicamente por @foreach($order->tickets as $ticket) @foreach($ticket->ticket_type->checkpoints as $checkpoint)
                                        {{ $checkpoint->name }}, @endforeach  @endforeach el personal operativo le orientará en caso intentar acceder por otro túnel.</div><br>
                                        <div>4. Su acreditación utiliza la tecnología QR para brindarle el acceso al EOU, favor de presentarla al momento de ingresar.</div><br>
                                        @foreach($qr as $img)
                                        <div class="row" style="text-align: center;">
                                            <div class="col-sm-6" style=" background-color: white;">
                                               <img src="{{ $message->embedData($img, 'qr_.png') }}"  style="width: 200px; height: 200px;">
                                            </div>
                                        </div>
                                        @endforeach
                                   
                                        <div>'Recomendaciones sanitarias:</div><br>
                                        <div>1. Uso de cubrebocas obligatorio en todo momento que permanezca en el evento.</div><br>
                                        <div>2. Mantener sana distancia con otros acreditados.</div><br>
                                        <div>3. Lavado frecuente de manos. El CUN ha instalado gel antibacterial en las zonas de trabajo, no obstante, se recomienda el uso de agua y jabón.'</div><br>
                                       

                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="content-cell" align="center">
                                        Atentamente<br>
                                        Dirección de Operaciones del Club Universidad Nacional A. C.
                                        © {{ date('Y') }} {{ 'pumas' }}. @lang('Todos los derecho reservados.')
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
</body>
</html>
