@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        <header class="content__title">
            <h1>Crear acreditación</h1>
        </header>
    </div>
</div>

<div class="row">
    <div class="col-sm-6 offset-sm-3">
        <div class="card">
            <div class="card-body">
                {!! Form::open(array('url' => route('system.users.store', [session('system_slug')]), 'method' => 'POST')) !!}
                    {!! Form::MDtext('Nombre', 'name', old('name'), $errors, ['required' => true]) !!}
                    {!! Form::MDemail('Email', 'email', old('email'), $errors, ['required' => true]) !!}
                    {!! Form::MDselect('Equipo', 'team_id', old('team_id'), $teams, $errors, ['required' => true]) !!}

                    {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

