@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1> Acreditaciones</h1>

                <div class="actions actions--calendar">

                  
                         <a href="" data-toggle="modal" data-target="#motive-{{ $accreditation->id }}">
                            <button class="btn btn-outline-secondary btn--raised">
                                <i class="zmdi zmdi-key zmdi-hc-fw"></i>
                            </button>
                        </a>
                   
                </div>
            </header>
        </div>
    </div>
    @if(count($histories) > 0)
    <div class="card">
        <div class="card-body">
            <h6 class="card-title">Listado de versiones de acreditaciones</h6>
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">Version</th>
                            <th class="col-sm-2">Motivo</th>
                            <th class="col-sm-3">Usuario</th>
                            <th class="col-sm-1 text-center">Eventos</th>
                          
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($histories as $history)
                        <tr>           
                            <td class="col-sm-1 text-center">{{ $history->version }}</td>
                            <td>{{ $history->motive }}</td>
                            <td>{{ $history->user->name }}</td>
                            <td class="col-sm-3">
                                @foreach($accreditation->event as $event)
                                    @if($event->pivot->version == $history->version)
                                        @if($i != 1) ,&nbsp; @endif
                                        {{$event->name}} 
                                        @php
                                            $i++
                                        @endphp

                                    @endif
                                @endforeach
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
        <div class="col-sm-12 text-center" style="opacity: 0.5">
                NO HAY VERSIONES
        </div>
    @endif
    @include('admin.accreditations.modals.motive')
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $(".data-table").DataTable();
    });
</script>
@endsection
