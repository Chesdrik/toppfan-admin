@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1> Acreditaciones</h1>

                <div class="actions actions--calendar">

                    <div class="btn btn-outline-secondary btn--raised btn--icon-text dropdown">
                        <span data-toggle="dropdown">
                            <i class="zmdi zmdi-accounts-list" aria-expanded="false"></i> QRs
                        </span>

                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(30px, 25.8667px, 0px);">
                            <a href="{{ route('system.accreditations.qr', [session('system_slug')]) }}" class="dropdown-item ">
                                Todos
                            </a>
                            @foreach($teams as $team)
                                <a href="{{ route('system.accreditations.qrByTeam', [session('system_slug'), $team->id]) }}" target="_blank" class="dropdown-item ">
                                    QRs {{ $team->name }}
                                </a>
                            @endforeach
                        </div>
                    </div>


                    @if(in_array(Auth::user()->type , ['root', 'admin', 'courtesies']))
                        <a href="{{ route('system.accreditations.import', [session('system_slug')]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                            <i class="zmdi zmdi-upload"></i> Importar
                        </a>

                        <a href="{{ route('system.accreditations.create', [session('system_slug')]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                            <i class="zmdi zmdi-plus"></i> Crear
                        </a>
                    @endif
                </div>
            </header>
        </div>
    </div>
    @if(count($accreditations) > 0)
    <div class="card">
        <div class="card-body">
            <h6 class="card-title">Listado de acreditaciones</h6>
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-2">&nbsp;</th>
                            <th class="col-sm-4">Nombre</th>
                            <th class="col-sm-2">Folio</th>
                            <th class="col-sm-3">Tipo de cortesía</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($accreditations as $accreditation)
                        <tr>
                            <td>
                                <a href="{{ route('system.accreditations.details', [session('system_slug'), $accreditation->id]) }}" target="_self">
                                    <button class="btn btn-outline-secondary btn--raised">
                                        <i class="zmdi zmdi-eye"></i>
                                    </button>
                                </a>
                            </td>
                            <td>{{ $accreditation->name }}</td>
                            <td>{{ $accreditation->folio }}</td>
                            <td>{{ $ticket_types[$accreditation->ticket_type_id] ?? 'no disponible' }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
        <div class="col-sm-12 text-center" style="opacity: 0.5">
                NO HAY ACREDITACIONES
        </div>
    @endif

@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $(".data-table").DataTable().order([1, 'asc']).draw();
    });
</script>
@endsection
