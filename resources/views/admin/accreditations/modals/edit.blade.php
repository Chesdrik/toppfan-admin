<div class="modal fade" id="editar-{{ $accreditation->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo asignar la acreditación.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Asignar acreditación con folio: {{ $accreditation->folio }}</h5>
                <hr>
            </div>

            <div class="modal-body">
                {!! Form::open(array('url' => route('system.events.accreditations.update', [session('system_slug'), $event->id, $accreditation->id]), 'method' => 'put', 'class' => 'row')) !!}
                    {!! Form::MDtext('Nombre', 'name', $accreditation->pivot->name??$accreditation->name, $errors, ['class' => 'col-md-10', 'required' => true]) !!}
                    {!! Form::MDtext('Email', 'email', $accreditation->pivot->email??$accreditation->email, $errors, ['class' => 'col-md-10', '' => true]) !!}

                    {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
