<div class="modal fade" id="motive-{{ $accreditation->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo actulizar la version acreditación.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Motivo por el cual se cambia la version de la acreditación con folio {{ $accreditation->folio }}</h5>
                <hr>
            </div>

            <div class="modal-body">
                {!! Form::open(array('url' => route('system.accreditations.version', [session('system_slug'), $accreditation->id]), 'method' => 'post', 'class' => 'row')) !!}
                    {!! Form::MDtextarea('Motivo', 'motive', '', $errors, ['class' => 'col-md-10', 'required' => true]) !!}

                    {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
