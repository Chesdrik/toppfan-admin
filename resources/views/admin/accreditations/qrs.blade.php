@extends('layouts.app_print')

@section('content')
    <div class="row">
        @foreach($event->courtesies() as $i => $order)
            @foreach($order->tickets as $courtesy)
                <div class="col-sm-3"  style="height: 290px; padding: 0px;">
                    <div class="card" style="box-shadow: none;">
                        <div class="card-body text-center" style="padding: 0px;">
                            <img src="{!! $courtesy->qr() !!}" style="height: 230px; width: 230px;"/>
                        </div>
                    </div>
                </div>

                @if(($i+1)%16 == 0)
                    </div>
                    <div class="row" style="border: 1px solid gray;">
                @endif

                <?php $i++; ?>
            @endforeach
        @endforeach
    </div>
@endsection
