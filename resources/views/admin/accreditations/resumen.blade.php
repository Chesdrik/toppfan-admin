@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Detalles de generación de cortesías</h1>
</header>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-9 offset-sm-1">

                <div class="invoice__header">
                    <!-- <img class="invoice__logo" src="{{ $order->event->img_path() }}" alt="" style="max-width: 70%; max-height:100%;"> -->
                    <img src="{{ asset('/assets/images/pumas_chivas.png') }}" class="img-fluid" style="padding: 10px 75px; max-width: 70%; max-height:100%;" />
                </div>

                <div class="text-center">
                    <h4>{{ $order->event->name }}</h4>
                    <address>
                        {{ pretty_date($order->event->date) }}
                    </address>
                </div><br>

                <div class="col-sm-12" style="margin-bottom: 5%;">
                    <h6>RESUMEN</h6>
                    <h6>{{ $order->client_user->name }}</h6>
                    <hr />
                    Acceso confirmado para un gran partido<br/ >
                    Quedó confirmado tu acceso al evento: {{ $order->event->name }}<br/ >
                </div>

                <div class="row">
                    @foreach($order->tickets as $ticket)
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <h4>{{ $ticket->ticket_type->name }}</h4>
                                     <a href="{{ route('system.events.courtesies.status', $ticket->id) }}" target="_self">

                                            @if($ticket->active == 1)
                                            <i class="zmdi zmdi-edit"></i>Activa
                                            @else
                                            <i class="zmdi zmdi-edit"></i>Desactivada

                                            @endif

                                    </a>
                                </div>
                                @if($ticket->qr())
                                    <div class="col-sm-12" style=" background-color: white;">
                                        <img src="{!! $ticket->qr() !!}" class="img-fluid" />
                                    </div>
                                    <div class="col-sm-12 text-center" style="font-weight: bold; color: black; background-color: white;">
                                        <span >{{ $ticket->folio }}</span>
                                    </div>
                                @endif



                                <div class="col-sm-12 text-center">
                                    @foreach($ticket->ticket_type->checkpoints as $checkpoint)
                                        {{ $checkpoint->name }}<br />
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="col-sm-12">
                        <div class="text-right">
                            <a href="{{ route('pdfresumen', $order->id) }}" target="blank">
                                <button class="btn btn-info">
                                    &nbsp;&nbsp;&nbsp;&nbsp; Imprimir tus boletos &nbsp;&nbsp;
                                    <span><i class="zmdi zmdi-collection-pdf zmdi-hc-fw"></i></span>
                                </button>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
