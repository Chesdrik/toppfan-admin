@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1 class="event-id" id="{{ $event->id }}">{{ $event->name }}</h1>
</header>

<div class="row">
    <div class="col-sm-12">
        <div class="row">
            {{-- <div class="col-sm-8"> --}}
            <div class="col-sm-6 offset-sm-3">
                <div class="card">
                    <div class="card-body" style="padding: 1.5rem;">
                        <div class="row">
                            <div class="col-sm-12 p-b-20">
                                <h4>Cortesías</h4>
                                <hr />
                                <img src="{{ asset('/assets/images/pumas_chivas.png') }}" class="img-fluid" style="padding: 10px 75px;" />
                                <hr />
                            </div>

                            @php
                                $count = 0;
                            @endphp
                            @foreach($event->ticket_template->ticket_types as $ticket_type)
                                @if(in_array($ticket_type->id, $user_tickets))
                                @php
                                    $count ++;
                                @endphp
                                <div class="col-sm-12 btn-group mr-1 m-b-20 ticket_type_row" data-cost="{{ $ticket_type->pivot->price  }}">
                                    {!! Form::MDButtonSeatType($ticket_type->name, $ticket_type->id, $ticket_type->color, ['class' => 'btn btn-light btn--raised btn-block text-left btn-seat']) !!}<br />

                                    <!-- <button type="button" class="btn btn-light btn--raised" style="color: #374564; font-weight: bold;">
                                        ${{ number_format($ticket_type->pivot->price , 2, '.', ',') }}
                                    </button> -->

                                    <button type="button" class="btn btn-light btn--raised total" data-total="0" data-ticket-type-id="{{ $ticket_type->id }}">&nbsp;</button>
                                    <button type="button" class="btn less" data-label="-" data-cost="{{ $ticket_type->pivot->price }}">&nbsp;</button>
                                    <button type="button" class="btn more" data-label="+" data-cost="{{ $ticket_type->pivot->price }}">+</button>
                                </div>
                                @endif
                            @endforeach

                            @if($count > 0)
                                <div class="col-sm-12 mr-1 m-b-40">
                                    <div class="text-right">
                                        <div id="label_total">
                                            <strong>Total</strong>
                                        </div>
                                        <div id="order_total" class="text-right">&nbsp;</div>
                                        <div class="spacer">&nbsp;</div>
                                    </div>
                                </div>

                                <div class="col-sm-12 mr-1 m-b-20">
                                    {{-- @if(Auth::user()->type != 'ticket_office')
                                    <div class="form-group row">
                                        <label for="email" class="col-md-2 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                                        <div class="col-md-8">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                            <i class="form-group__bar"></i>

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    @endif --}}

                                    <button id="comprar" class="btn btn-block btn-info btn--raised p-b-15 p-t-15" >Continuar</button>
                                @else
                                    <div class="col-md-12 text-center" style="color: red">
                                        No tienes autorización para generar cortesías en este evento. Por favor, contacta al Administrador.
                                    </div>
                                @endif

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-sm-1">&nbsp;</div>
            <div class="col-sm-6">
                <div id="venue">
                    <img id="venue-background" src="{{ asset('/assets/venues/'.$venue->id.'/background.png') }}" class="img-fluid img-venue" />

                    @foreach($venue->ticket_types as $ticket_type)
                        <img src="{{ asset('/assets/venues/'.$venue->id.'/'.$ticket_type->id.'.png') }}" class="img-fluid img-venue seat-type" data-type="seat_{{ $ticket_type->id }}" />
                    @endforeach
                </div>
            </div> --}}
        </div>
    </div>

    <!-- <div class="col-sm-4">
    <div class="card">
    <img class="card-img-top" src="https://maps.googleapis.com/maps/api/staticmap?center={{ $venue->lat }},{{ $venue->long }}&zoom=15&size=600x350&key=AIzaSyAKavmRXai4uFCCv8T2Y1I3YjDta9LqlzI" />

    <div class="card-body">
    <h4 class="card-title">{{ $venue->name }}</h4>
    <h6 class="card-subtitle">{{ $venue->lat }}, {{ $venue->long }}</h6>
    <p class="card-text">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
</div>
</div>
</div> -->
</div>
@endsection

@section('after_includes')
<!-- Javascript -->
<script type="text/javascript">

$('.less').on('click', function(){
    total = parseInt($(this).parent().find('.total').attr('data-total'))-1;

    if(total >= 0){
        if(total == 0){
            // Activate -
            $(this).parent().find('.less').html('&nbsp;');
            $(this).parent().find('.total').html('&nbsp;');

            // delete products.
        }else{
            $(this).parent().find('.total').html(total);
        }

        $(this).parent().find('.total').attr('data-total', total);

        update_order_total();
    }
});

$('.more').on('click', function(){
    total = parseInt($(this).parent().find('.total').attr('data-total'));

    if(total == 0){
        // Activate -
        $(this).parent().find('.less').html('-');
    }
    total++;

    $(this).parent().find('.total').attr('data-total', total);
    $(this).parent().find('.total').html(total.toString());

    update_order_total();
});

function update_order_total(){
    // Calculating total
    var total = 0;
    $('.ticket_type_row').each(function(){
        var amount = parseInt($(this).find('.total').attr('data-total'));
        var cost = parseInt($(this).attr('data-cost'));

        total += (amount*cost);
    });

    // Format
    var total_html = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD'
    }).format(total);

    // Show total
    $("#order_total").html(total_html);
}

// Hovering over buttons
$('.btn-seat').mouseenter(function(){
    // $('.seat-type').not('.seat-type[data-type="'+$(this).attr('seat-type')+'"]').css({ 'opacity' : 0.05 });
    $('.seat-type').not('.seat-type[data-type="'+$(this).attr('seat-type')+'"]').fadeTo(80, 0.0);
    $("#venue-background").fadeTo(80, 0.5);
}).mouseleave(function(){
    // $('.seat-type').css({ 'opacity' : 1.0 });
    $("#venue-background").fadeTo(80, 1);
    $('.seat-type').fadeTo(80, 1);
});

$('#comprar').on('click', realizaProceso);

function realizaProceso(){
    var id = $('.event-id').attr('id');
    // Fetching tickets to buy
    var tickets = [];
    $('.total').each(function(){

        if($(this).attr('data-total') > 0){
            tickets.push({
                ticket_type_id: $(this).attr('data-ticket-type-id'),
                total: $(this).attr('data-total'),
            });
        }
    });

    // Creating order
    $.ajax({
        data: { _token: '{{ csrf_token() }}', email: $("#email").val(), tickets: tickets, id: id},
        url:   "{{ url('/admin/cortesias/continuar') }}",
        type:  'post',
        success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve

            window.location.href = '/admin/eventos/'+ response.id + '/acreditaciones/confirmar';
        }
    });
}


</script>
@endsection
