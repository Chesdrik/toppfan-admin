@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Detalle de  @if ($accreditation->type == 'season_ticket')
            abono
        @else
            acreditación
        @endif
        #{{ $accreditation->folio }}</h1>
</header>

<div class="row">
    <div class="col-sm-4">
        <div class="card">
            <div class="card-body" style="padding: 1.5rem;">
                <div class="row">
                    <div class="col-sm-12 p-b-20">
                        <h4>{{ $event->name }}</h4><br /><br />
                        <h6>Fecha evento</h6>{{ pretty_short_date($event->date) }}<br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body" style="padding: 1.5rem;">
                <div class="row">
                    <div class="col-sm-12">
                        <img src="{{ asset($event->img_path()) }}" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body" style="padding: 1.5rem;">
                <div class="row">
                    <div class="col-sm-12 p-b-20">
                        @if ($accreditation->type == 'season_ticket')
                            <h6>
                                Nombre: <small>{{ $accreditation->ticket->order->client_user->name }}</small>
                            </h6><br />
                            <h6>
                                Email: <small>{{ $accreditation->ticket->order->email }}</small>
                            </h6><br />
                            <h6>
                                Versión: <small>{{ $accreditation->version }}</small>
                            </h6>
                        @else
                            <h6>
                                Nombre: <small>{{ $ac->pivot->name }}</small>
                            </h6><br />
                            <h6>
                                Email: <small>{{ $ac->pivot->email }}</small>
                            </h6><br />
                            <h6>
                                Versión: <small>{{ $ac->pivot->version }}</small>
                            </h6>        
                        @endif
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 m-t-25 m-b-25">
        <hr />
    </div>
</div>

<div class="row m-t-25">
        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body" style="padding: 1.5rem;">
                    <div class="row">
                        <div class="col-sm-12 p-b-20 text-center">
                            <h4>{{ $accreditation->ticket_type->name }}</h4>

                            <hr style="border-top: 5px solid #{{ $accreditation->ticket_type->color }};" />

                            <img src="{!! $accreditation->qr() !!}" class="img-fluid" />
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="lgi-heading m-b-5">
                                        @if($ac->pivot->used == 1)
                                            <i style="color:green; font-size:20px;" class="zmdi zmdi-check-all zmdi-hc-fw"></i> &nbsp; Acceso utilizado
                                        @else
                                            <i style="color:red; font-size:20px;" class="zmdi zmdi-close zmdi-hc-fw"></i> &nbsp; Acceso no utilizado
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection
