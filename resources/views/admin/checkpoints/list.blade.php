@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Checkpoints de {{ $venue->name }} </h1>

        <div class="actions">
            <a href="" data-toggle="modal" data-target="#crear-checkpoint" class="btn btn-outline-secondary btn--raised btn--icon-text"">
                <i class="zmdi zmdi-plus"></i> Registrar checkpoint
            </a>
            @include('admin.checkpoints.modals.create')
        </div>
    </header>

    @if(count($checkpoints) > 0)
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th>&nbsp;</th>
                            <th>Nombre</th>
                            <th>Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($checkpoints as $checkpoint)
                            <tr>
                                <td class="text-center">
                                    <a href="" data-toggle="modal" data-target="#editar-{{ $checkpoint->id }}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $checkpoint->name }}</td>
                                <td>{{ $checkpoint->pretty_type() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
    <div class="col-sm-12 text-center" style="opacity: 0.5">
        <br>
            NO HAY CHECKPOINTS REGISTRADOS
    </div>
    @endif

    @foreach($checkpoints as $checkpoint)
        @include('admin.checkpoints.modals.edit')
    @endforeach
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $(".data-table").DataTable().order([2, 'asc']).draw();
    });
</script>
@endsection
