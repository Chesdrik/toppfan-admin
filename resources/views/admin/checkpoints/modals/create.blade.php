<div class="modal fade" id="crear-checkpoint" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el checkpoint.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Crear checkpoint</h5>
                <hr>
            </div>

            <div class="modal-body">
                <form method="POST" action="{{ route('system.venues.checkpoints.store', [session('system_slug'), $venue->id]) }}" class="row">
                    @csrf

                    {!! Form::MDtext('Nombre', 'name', '', $errors, ['class' => 'col-md-10', 'required' => true]) !!}
                    {{-- {!! Form::MDselect('Sede', 'venue_id', '', \App\Venue::venue_options(), $errors, ['class' => 'col-md-7']) !!} --}}
                    {!! Form::MDselect('Tipo', 'type', '', \App\Checkpoint::options(), $errors, ['class' => 'col-md-5']) !!}

                    {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                </form>
            </div>
        </div>
    </div>
</div>
