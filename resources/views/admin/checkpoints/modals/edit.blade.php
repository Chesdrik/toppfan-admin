<div class="modal fade" id="editar-{{ $checkpoint->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo actualizar el checkpoint.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Editar {{ $checkpoint->name }}</h5>
                <hr>
            </div>

            <div class="modal-body">
            {!! Form::open(array('url' => route('system.venues.checkpoints.update', [session('system_slug'), $venue->id, $checkpoint->id]), 'method' => 'put', 'class' => 'row')) !!}
                    @csrf
                    {!! Form::MDtext('Nombre', 'name', $checkpoint->name, $errors, ['class' => 'col-md-10', 'required' => true]) !!}
                    {!! Form::MDselect('Tipo', 'type', $checkpoint->type, \App\Checkpoint::options(), $errors, ['class' => 'col-md-5']) !!}

                    {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                </form>
            </div>
        </div>
    </div>
</div>
