@extends('layouts.app')

@section('content')

    <header class="content__title">
        <h1>Dashboard</h1>
    </header>

    <div class="row">
        <div class="col-sm-6 col-md-3">
            <div class="quick-stats__item bluegray-light-bg">
                <div class="quick-stats__info">
                    <h2>${{ number_format($total, 2, '.', ',') }}</h2>
                    <small>Ventas totales</small>
                </div>

                <div class="quick-stats__chart sparkline-bar-stats">6,4,8,6,5,6,7,8,3,5,9,5,</div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="quick-stats__item bluegray-light-bg">
                <div class="quick-stats__info">
                    <h2>987,459</h2>
                    <small>Vistas sitio web</small>
                </div>

                <div class="quick-stats__chart sparkline-bar-stats">7,8,3,5,9,5,6,4,8,6,5,6,</div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="quick-stats__item bluegray-light-bg">
                <div class="quick-stats__info">
                    <h2>$987,459</h2>
                    <small>Accesos utilizados</small>
                </div>

                <div class="quick-stats__chart sparkline-bar-stats">6,5,6,7,8,6,4,8,3,5,9,5</div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="quick-stats__item bluegray-light-bg">
                <div class="quick-stats__info">
                    <h2>{{ $ticket_shop}}</h2>
                    <small>Total de boletos vendidos</small>
                </div>

                <div class="quick-stats__chart sparkline-bar-stats">6,4,8,6,5,6,7,8,3,5,9,5</div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12 col-md-6">
                <div class="card icons-demo">
                    <div class="card-body">
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card icons-demo">
                <div class="card-body">
                    <h4 class="card-title">Próximos eventos</h4>
                    <h6 class="card-subtitle">Listado de los próximos eventos y su estado</h6>
                    <p class="card-text">
                        @foreach($upcoming_events as $upcoming_event)
                            <a href="{{ route('system.events.show', [session('system_slug'), $upcoming_event->id]) }}" target="_self">
                                <button class="btn btn-light btn-outline-{{ ($upcoming_event->active ? 'success' : 'secondary') }} btn-block btn--raised btn--icon-text text-left m-b-20">
                                    <i class="zmdi zmdi-{{ ($upcoming_event->on_sale ? 'money' : 'money-off') }} zmdi-hc-fw"></i>
                                    {{ $upcoming_event->name }}

                                    <small class="pull-right" style="color: #607D8B;">{{ $upcoming_event->datespan() }}</small>
                                </button>
                            </a>
                        @endforeach
                    </p>


                    <p class="card-text m-t-40">
                        <hr>
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-sm btn-outline-secondary">Inactivo</button>
                            <button type="button" class="btn btn-sm btn-outline-success">Activo</button>
                        </div>
                        <br /><br />
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-sm ">
                                <i class="zmdi zmdi-money zmdi-hc-fw"></i> = A la venta
                            </button>
                            <button type="button" class="btn btn-sm ">
                                <i class="zmdi zmdi-money-off zmdi-hc-fw"></i> = Sin vender
                            </button>
                        </div>
                    </p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="card icons-demo">
                <div class="card-body">
                    <h4 class="card-title">Eventos activos</h4>
                    <h6 class="card-subtitle">Listado de los eventos marcados como activos</h6>
                    <p class="card-text">
                        @foreach($active_events as $active_event)
                            <a href="{{ route('system.events.show', [session('system_slug'), $active_event->id]) }}" target="_self">
                                <button class="btn btn-light btn-outline-success btn-block btn--raised btn--icon-text text-left m-b-20">
                                    <i class="zmdi zmdi-{{ ($active_event->on_sale ? 'money' : 'money-off') }} zmdi-hc-fw"></i>
                                    {{ $active_event->name }}

                                    <small class="pull-right" style="color: #607D8B;">{{ $active_event->datespan() }}</small>
                                </button>
                            </a>
                        @endforeach
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
<script src="{{ Asset('/assets/material/demo/js/other-charts.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<script type="text/javascript">
    var fechas = [@foreach($labes as $l) "{{ $l }}", @endforeach];
    console.log(fechas);


    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: fechas,
            datasets: [{
                label: 'ventas por dia',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: {{ json_encode($data) }}
            }]
        },

        // Configuration options go here
        options: {}
    });



    'use strict';

    $(document).ready(function () {
        /*---------------------------------------
        jQuery Sparklines
        ----------------------------------------*/

        // Quick stats bar chart
        if($('.sparkline-bar-stats')[0]) {
            $('.sparkline-bar-stats').sparkline('html', {
                type: 'bar',
                height: 36,
                barWidth: 3,
                barColor: '#fff',
                barSpacing: 2
            });
        }

        //Sample Sparkline Line Chart
        if ($('.sparkline-line')[0]) {
            $('.sparkline-line').sparkline('html', {
                type: 'line',
                width: '100%',
                height: 50,
                lineColor: '#39bbb0',
                fillColor: 'rgba(0,0,0,0)',
                lineWidth: 1,
                maxSpotColor: '#39bbb0',
                minSpotColor: '#39bbb0',
                spotColor: '#39bbb0',
                spotRadius: 4,
                highlightSpotColor: '#39bbb0',
                highlightLineColor: '#39bbb0'
            });
        }

        //Sample Sparkline Bar Chart
        if ($('.sparkline-bar')[0]) {
            $('.sparkline-bar').sparkline('html', {
                type: 'bar',
                height: 40,
                barWidth: 3,
                barColor: '#03A9F4',
                barSpacing: 2
            });
        }

        //Sample Sparkline Tristate Chart
        if ($('.sparkline-tristate')[0]) {
            $('.sparkline-tristate').sparkline('html', {
                type: 'tristate',
                height: 40,
                posBarColor: '#32c787',
                zeroBarColor: '#32c787',
                negBarColor: '#f5c942',
                barWidth: 3,
                barSpacing: 2
            });
        }

        //Sample Sparkline Discrete Chart
        if ($('.sparkline-discrete')[0]) {
            $('.sparkline-discrete').sparkline('html', {
                type: 'discrete',
                height: 40,
                lineColor: '#00BCD4',
                thresholdColor: '#d066e2',
                thresholdValue: 4
            });
        }

        //Sample Sparkline Bullet Chart
        if ($('.sparkline-bullet')[0]) {
            $('.sparkline-bullet').sparkline('html', {
                type: 'bullet',
                targetColor: '#fff',
                performanceColor: '#03A9F4',
                rangeColors: ['#ff6b68', '#fc7f7d', '#fc918f'],
                width: '100%',
                height: 50
            });
        }

        //Sample Sparkline Pie Chart
        if ($('.sparkline-pie')[0]) {
            $('.sparkline-pie').sparkline('html', {
                type: 'pie',
                height: 50,
                sliceColors: ['#f5c942', '#ff6b68', '#03A9F4', '#32c787']
            });
        }

        /*---------------------------------------
        Easy Pie Charts
        ----------------------------------------*/
        if($('.easy-pie-chart')[0]) {
            $('.easy-pie-chart').each(function () {
                var value = $(this).data('value');
                var size = $(this).data('size');
                var trackColor = $(this).data('track-color');
                var barColor = $(this).data('bar-color');

                $(this).find('.easy-pie-chart__value').css({
                    lineHeight: (size)+'px',
                    fontSize: (size/4)+'px',
                    color: barColor
                });

                $(this).easyPieChart ({
                    easing: 'easeOutBounce',
                    barColor: barColor,
                    trackColor: trackColor,
                    scaleColor: 'rgba(0,0,0,0)',
                    lineCap: 'round',
                    lineWidth: 2,
                    size: size,
                    animate: 3000,
                    onStep: function(from, to, percent) {
                        $(this.el).find('.percent').text(Math.round(percent));
                    }
                })
            });
        }
    });
</script>
@endsection
