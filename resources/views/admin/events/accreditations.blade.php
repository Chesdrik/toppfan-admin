@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->name}} | Acreditaciones</h1>

                <div class="actions actions--calendar">
                    @if(in_array(Auth::user()->type , ['root', 'admin', 'courtesies']))
                        <a href="{{ route('system.events.accreditations.generateForEvent', [session('system_slug'), $event->id]) }}" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                            <i class="zmdi zmdi-accounts-list"></i> Generar acreditaciones
                        </a>
                    @endif
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'accreditations'])
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Acreditaciones asignadas</h4>
                <h6 class="card-subtitle">Listado de acreditaciones asignadas a un usuario</h6>

                @if($event->accreditations->count() > 0)
                    <livewire:events.accreditations.accreditations-list :event="$event" />
                @else
                    <div class="col-sm-12 text-center" style="opacity: 0.5">
                        NO HAY ACREDITACIONES ASIGNADAS
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
    @livewireScripts
@endsection
