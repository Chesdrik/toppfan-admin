@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->name}} | Análisis de Lecturas</h1>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'analysis'])
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Análisis de lecturas</h4>
                <h6 class="card-subtitle">Análisis de las lecturas registradas durante el evento.</h6>

                <div class="tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#tickets" role="tab" aria-selected="true">Boletos</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link show" data-toggle="tab" href="#accreditations" role="tab" aria-selected="false">Acreditaciones</a>
                        </li>
                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane fade active show" id="tickets" role="tabpanel">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4 class="card-title">Tipos de boletos</h4>
                                    <h6 class="card-subtitle">Lecturas de boletos agrupadas por tipo de boleto.</h6>
                                    @if($ticket_data['data'] && $ticket_data['data']['ticket']['types'] > 0)
                                        <div id="tree1"> </div>
                                    @else
                                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                                            NO HAY LECTURAS DE BOLETOS PARA ANALIZAR
                                        </div>
                                    @endif
                                </div>

                                <div class="col-sm-6">
                                    <h4 class="card-title">Puntos de acceso</h4>
                                    <h6 class="card-subtitle">Lecturas de boletos agrupadas por punto de acceso.</h6>
                                    @if($ticket_data['data'] && $ticket_data['data']['ticket']['types'] > 0)
                                        <div id="tree2"> </div>
                                    @else
                                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                                            NO HAY LECTURAS DE BOLETOS PARA ANALIZAR
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="accreditations" role="tabpanel">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4 class="card-title">Tipos de boletos</h4>
                                    <h6 class="card-subtitle">Lecturas de acreditaciones agrupadas por tipo de boleto.</h6>
                                    @if ($accreditation_data['data'] && $accreditation_data['data']['accreditation']['checkpoints'] > 0)
                                        <div id="tree3"> </div>
                                    @else
                                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                                            NO HAY LECTURAS DE ACREDITACIONES PARA ANALIZAR
                                        </div>
                                    @endif
                                </div>

                                <div class="col-sm-6">
                                    <h4 class="card-title">Puntos de acceso</h4>
                                    <h6 class="card-subtitle">Lecturas de acreditaciones agrupadas por punto de acceso.</h6>
                                    @if ($accreditation_data['data'] && $accreditation_data['data']['accreditation']['checkpoints'] > 0)
                                        <div id="tree4"> </div>
                                    @else
                                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                                            NO HAY LECTURAS DE ACREDITACIONES PARA ANALIZAR
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('after_includes')
<script type="text/JavaScript">

    @if($ticket_data['data'] && $ticket_data['data']['ticket']['types'] > 0)
        var ticket_data = {!! ticket_data_parser($ticket_data, 'ticket') !!}
    @endif
    @if($ticket_data['data'] && $ticket_data['data']['ticket']['checkpoints'] > 0)
        var checkpoint_data = {!! checkpoint_data_parser($ticket_data, 'ticket') !!}
    @endif

    $('#tree1').tree({
        data: ticket_data,
        autoOpen: false,
        selectable: false
    });
    $('#tree2').tree({
        data: checkpoint_data,
        autoOpen: false,
        selectable: false
    });

    @if($accreditation_data['data'] && $accreditation_data['data']['accreditation']['types'] > 0)
        var ac_ticket_data = {!! ticket_data_parser($accreditation_data, 'accreditation') !!}
    @endif
    @if($accreditation_data['data'] && $accreditation_data['data']['accreditation']['checkpoints'] > 0)
        var ac_checkpoint_data = {!! checkpoint_data_parser($accreditation_data, 'accreditation') !!}
    @endif

    $('#tree3').tree({
        data: ac_ticket_data,
        autoOpen: false,
        selectable: false
    });
    $('#tree4').tree({
        data: ac_checkpoint_data,
        autoOpen: false,
        selectable: false
    });

</script>
@endsection
