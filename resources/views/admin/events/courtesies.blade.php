@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->name }} | Boletos</h1>

                <div class="actions actions--calendar">
                    @if(in_array(Auth::user()->type , ['root', 'admin', 'courtesies']))
                        <a href="{{ route('system.events.courtesy.generateForEvent', [session('system_slug'), $event->id]) }}" target="_blank" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                            <i class="zmdi zmdi-accounts-list"></i> Imprimir QRs
                        </a>
                    @endif
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'courtesies'])
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Ordenes de cortesía y boletos registrados</h4>
                    <h6 class="card-subtitle">Listado de coretesías registradas</h6>

                    @if($event->courtesy_orders->count() > 0)
                        <livewire:events.courtesies.courtesy-list :event="$event" />
                    @else
                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                            NO HAY ORDENES REGISTRADAS
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
    @livewireScripts
@endsection
