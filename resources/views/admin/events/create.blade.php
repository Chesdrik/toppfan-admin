@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Crear evento</h1>
            </header>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('system.events.store', session('system_slug')), 'method' => 'POST', 'enctype' => 'multipart/form-data')) !!}
                        {!! Form::MDtext('Nombre', 'name', '', $errors) !!}
                        @livewire('ticket-types-for-venue', [$venues])
                        {!! Form::MDselect('Equipo', 'team_id', '', $teams, $errors) !!}
                        {!! Form::MDdatetimepicker('Fecha y hora', 'date', '', $errors) !!}

                        {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                    {!! Form::close() !!}

                    {{-- {!! Form::MDdropzone('Imagen (png)', 'img', '', $errors) !!} --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after_includes')

@livewireScripts

<script type="text/javascript">
// $("#img").dropzone({ url: "/file/post" });
$(document).ready(function(){


    // $("[name='ticket_template_id']").change(function() {
    //     var id = $(this).val();

    //     if(id == 'na'){
    //         var venue_id = $('.venue-id').attr('id');
    //         document.getElementById("new_template").style.display = "block";
    //         document.getElementById("new_template").focus();
    //         document.getElementById("tickets").style.display = "none";

    //     }else{
    //         document.getElementById("new_template").style.display = "none";
    //         document.getElementById("tickets").style.display = "block";
    //         getTickets(id);
    //     }

    // });

    // function getTickets(id) {
    //      var array = [];
    //      $.ajax({
    //         method: 'GET',
    //         url: '/admin/templates/' + id + '/tickets',
    //         success: function( e ) {
    //           if(!e.error){
    //               var string ='<div class="col-md-12">Boletos:</div><br />' +
    //                              '<ul>'

    //              $.each(e.data , function(name, price) {
    //                 string += '<li id="'+ name +'"> '+ name + '<br> Costo: $' + price + '</li><br>'
    //             });

    //          string += '</ul>'

    //           }else{
    //               console.log('error');
    //           }

    //           $('#tickets').html(string);
    //      }
    //     });
    // }


    // $('input[type=checkbox]').each(function () {
    //     $(this).change(function() {
    //         /* console.log($(this)); */
    //         console.log($(this).attr('data-free'));
    //         var free = $(this).attr('data-free');
    //         if(free == 0){
    //             string = '<div id="div2-'+ this.value +'">' +
    //                         '<div class="form-group">' +
    //                             '<label>Costo</label>' +
    //                             '<input type="text" class="form-control " value="" name="costs['+ this.value +']" placeholder="Costo" required><i class="form-group__bar"></i>' +
    //                         '</div>' +
    //                     '</div>';

    //         }else{
    //             string = '<div id="div2-'+ this.value +'">' +
    //                         '<div class="form-group">' +
    //                             '<label>Costo</label>' +
    //                             '<input type="text" class="form-control " value="N/A (Boleto tipo cortesía)" name="costs['+ this.value +']" placeholder="Costo" required disabled><i class="form-group__bar"></i>' +
    //                             '<input type="text" class="form-control " value="0" name="costs['+ this.value +']" placeholder="Costo" hidden><i class="form-group__bar"></i>' +
    //                         '</div>' +
    //                     '</div>';
    //         }

    //         if(this.checked){
    //             $('#div-' + this.value).html(string);
    //         }else{
    //             $('#div2-' + this.value).remove();
    //         }
    //     });
    // });
});
</script>
@endsection
