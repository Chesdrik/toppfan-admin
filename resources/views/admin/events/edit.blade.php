@extends('layouts.app')


@section('content')
<div class="col-sm-12">
    <header class="content__title">
    <h1>Editar {{ $event->name}}</h1>
    </header>
</div>


<div class="row">
    <div class="col-sm-7">
        <div class="card">
            {!! Form::open(array('url' => route('system.events.update', [session('system_slug'), $event->id]), 'method' => 'PUT',  'enctype' => "multipart/form-data")) !!}
                <div class="card-body">
                    {!! Form::MDtext('Nombre', 'name', $event->name, $errors, ['class' => 'col-md-12']) !!}
                    {!! Form::MDselect('Equipo', 'team_id', $event->team_id, $teams, $errors) !!}
                    {!! Form::MDdatetimepicker('Fecha y hora', 'date', $event->date, $errors) !!}
                    {!! Form::MDselect('Template', 'ticket_template_id', $event->ticket_template_id, $venue->template_options(), $errors) !!}
                    @if(is_null($template))
                        <div class="col-md-12" style="margin-top: -20px;">
                            <small style="color: red;">No hay templates para ésta sede, por favor, dirígete al siguiente enlace para registrar uno. <a href="{{ route('system.venues.templates.create') }}">Crear template</a></small>
                        </div>
                    @endif

                    {!! Form::MDselectize('Grupo de eventos', 'event_groups', \App\EventGroup::selectize(), $selected_event_groups, $errors) !!}

                    @if($event->finished_event())
                        {{--  {!! Form::MDtext('Marcador', 'final_score', $event->final_score, $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDselect('¿Partido ganado?', 'won', $event->won, ['1' => 'Sí', 0 => 'No'], $errors, ['class' => 'col-md-6']) !!}  --}}
                    @else
                        {{-- {!! Form::MDselect('¿En venta?', 'on_sale', $event->on_sale, ['1' => 'Sí', 0 => 'No'], $errors) !!} --}}

                        @if (!$info['generated_accreditations'] || !$info['generated_season_tickets'])
                            {!! Form::MDToggleSwitch('¿En venta?', 'on_sale', $event->on_sale, $errors, ['disabled' => 'disabled']) !!}
                            <div class="col-sm-12 m-b-15" style="margin-top: -2%">
                                <small style="color:red;">Antes de poner a la venta, es necesario generar acreditaciones y abonos</small>
                            </div>
                        @else
                            {!! Form::MDToggleSwitch('¿En venta?', 'on_sale', $event->on_sale, $errors) !!}
                        @endif
                    @endif

                    {!! Form::MDsubmit('Guardar', 'update', ['icon' => 'card-sd']) !!}
                    <br />
                </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="col-sm-5">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        {!! Form::MDdropzone('Imágen', 'img', route('system.events.upload.imgs', [session('system_slug'), $event->id]), $errors, ['leyend' => 'Tamaño: 600x600 px']) !!}
                        <div class="col-sm-12 m-t-15">
                            <img class="center img-fluid" src="{{ asset($event->img_path()) }}" />
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        {!! Form::MDdropzone('Imágen promocional', 'img_slideshow', route('system.events.upload.imgs', [session('system_slug'), $event->id]), $errors, ['accepted-files' => '.jpg, .jpeg', 'leyend' => 'Tamaño: 600x600 px']) !!}
                        <div class="col-sm-12 m-t-15">
                            <img class="center img-fluid" src="{{ asset($event->img_slideshow_path()) }}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <header class="content__title">
    <h1>Boletos</h1>
    </header>
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-3">Nombre</th>
                            <th class="col-md-3">Costo</th>
                            <th class="col-md-3">Disponibles / Total</th>
                            <th class="col-md-3">Venta</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($event_seats as $event_seat)
                            <tr>
                                <td>{{ $event_seat->ticket_type->name }}</td>
                                <td>--</td>
                                {{-- <td>{{ ($event_seat->ticket_type->free ? 'Cortesía' : pretty_money($event_seat->price)) }}</td> --}}
                                <td>{{ $event_seat->available }} / {{ $event_seat->total }}</td>
                                <td>
                                    <small>{{ $event_seat->percentage_sold(true) }}</small>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: {{ $event_seat->percentage_sold(true) }}" aria-valuenow="{{ $event_seat->percentage_sold() }}" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

{{-- @if(!is_null(\App\EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket->id)))
                                        <td>
                                            {!! Form::MDtext(null, 'availables['.$ticket->id.']', \App\EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket->id)->available, $errors, ['form-group-class' => '']) !!}
                                        </td>
                                    @endif --}}
                {{-- <div class="card">
                    <div class="card-body">
                        @if(!is_null($template))
                            <div class="row">
                                <div class="col-sm-12" id="tickets">
                                    <h5>Boletos</h5>
                                </div>


                                @foreach($template->ticket_types as $ticket)
                                    <div class="col-sm-6">
                                        <div id="{{ $ticket->id }}" class="row">
                                            <div class="col-sm-12">
                                                <h6>{{ $ticket->name }}</h6>
                                            </div>

                                            <div class="col-sm-4">
                                                Costo: {{ ($ticket->free ? 'Cortesía' : pretty_money($ticket->pivot->price)) }}
                                            </div>

                                            @if(!is_null(\App\EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket->id)))
                                                <div class="col-sm-8">
                                                    {!! Form::MDtext('Boletos disponibles:', 'availables['.$ticket->id.']', \App\EventSeatAvailability::seatsForEventAndTicket($event->id, $ticket->id)->available, $errors) !!}
                                                </div>
                                            @endif

                                            <div class="col-sm-12">
                                                <hr>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        @if(count($venue->ticket_types()) > 0)
                            <div class="col-sm-12" id="new_template" @if($template) style="display: none" @else style="display: block" @endif tabindex="-1">
                                <div class="card">
                                    <div class="card-body">
                                        <hr>
                                        <h4 class="card-title">Nuevo template</h4>
                                        @csrf

                                        {!! Form::MDtext('Nombre del template', 'name_template', '', $errors, ['class' => 'col-md-12']) !!}

                                        <div class="col-md-12" style="margin-bottom: 10px;">
                                            <h6>Selecciona los tipos de boletos que contendrá el template</h6>
                                        </div>
                                        @foreach($venue->ticket_types() as $ticket_type)
                                            <div class="row">
                                                <div class="col-md-4 offset-1" style="margin-top: 5px;">
                                                    <div class="checkbox">
                                                        <input type="checkbox" id="customCheck1-{{ $ticket_type->id }}" name="tickets[{{ $ticket_type->id }}]" value="{{ $ticket_type->id }}" data-free="{{ $ticket_type->free }}">
                                                        <label class="checkbox__label" for="customCheck1-{{ $ticket_type->id }}">{{ $ticket_type->name }}</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" id="div-{{ $ticket_type->id }}"></div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            {!! Form::MDsubmit('Guardar', 'update', ['icon' => 'card-sd']) !!}
                        @endif
                    </div>
                </div> --}}


<div class="row m-t-25">

</div>
@endsection
