<div class="card">
    <div class="card-body" style="padding: 1.5rem 1.1rem 2.1rem 1.1rem;">
        <ul class="nav justify-content-center nav-tabs">
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'general' ? 'active' : '') }}" href="{{ route('system.events.show', [session('system_slug'), $event->id]) }}">General</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'holding' ? 'active' : '') }}" href="{{ route('system.events.tickets.holding', [session('system_slug'), $event->id]) }}">Holding</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'tickets' ? 'active' : '') }}" href="{{ route('system.events.tickets.list', [session('system_slug'), $event->id]) }}">Boletos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'courtesies' ? 'active' : '') }}" href="{{ route('system.events.courtesy.list', [session('system_slug'), $event->id]) }}">Cortesías</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{ ($selected == 'accreditations' ? 'active' : '') }}" href="{{ route('system.events.accreditations.list', [session('system_slug'), $event->id]) }}">Acreditaciones</a>
            </li>
            @if (count($event->event_groups) > 0)
                <li class="nav-item">
                    <a class="nav-link  {{ ($selected == 'season_tickets' ? 'active' : '') }}" href="{{ route('system.events.season.tickets', [session('system_slug'), $event->id]) }}">Abonos</a>
                </li>
            @endif
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'stats' ? 'active' : '') }}" href="{{ route('system.events.statistics', [session('system_slug'), $event->id]) }}">Estadísticas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'readings' ? 'active' : '') }}" href="{{ route('system.events.tickets.readings', [session('system_slug'), $event->id]) }}">Lecturas</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{ ($selected == 'analysis' ? 'active' : '') }}" href="{{ route('system.events.analysis', [session('system_slug'), $event->id]) }}">Análisis</a>
            </li>
        </ul>
    </div>
</div>
