<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	   
	   
		 <style type="text/css">
		 	 @media only screen and (max-width: 600px) {
		            .inner-body {
		                width: 100% !important;
		            }

		            .footer {
		                width: 100% !important;
		            }
		        }

		        @media only screen and (max-width: 500px) {
		            .button {
		                width: 100% !important;
		            }
		        }
		        body, body *:not(html):not(style):not(br):not(tr):not(code) {
		            font-family: Avenir, Helvetica, sans-serif;
		            box-sizing: border-box;
		        }
		 /* Tables */

		        .table table {
		            margin: 30px auto;
		            width: 100%;
		            -premailer-cellpadding: 0;
		            -premailer-cellspacing: 0;
		            -premailer-width: 100%;
		            font-family: inherit;
		        }

		        .table th {
		            border-bottom: 1px solid #EDEFF2;
		            padding-bottom: 8px;
		            margin: 1;
		            font-family: inherit;
		        }

		        .table td {
		            color: #74787E;
		            font-size: 15px;
		            line-height: 18px;
		            padding: 10px 0;
		            margin: 1;
		            padding-bottom: 8px;
		            font-family: inherit;
		        }

		 </style>
	</head>
	<body>
		<h1 style="text-align: center; font-family: inherit;">Evento {{ $event->name}}</h1>
 			<div class="card">
	            <div class="card-body">
	                <div class="table-responsive">
	                    <table id="data-table" class="table table-bordered" style="width:100%">
	                        <thead class="thead-default">
	                           
	                            <tr><th style="text-align: left">Sede</th><td>{!! $event->venue->name !!}</td></tr>
	                            <tr><th style="text-align: left">Fecha</th><td>{!! $event->datespan(true) !!}</td></tr>
	                            <tr><th style="text-align: left">Monto</th><td>$0,000</td></tr>
	                            @if(isset($event_statistics['general_info_tickets']))
		                            <tr><th style="text-align: left">Boletos</th><td>{{ pretty_number($event_statistics['general_info_tickets']['total_tickets'], 0) }}</td></tr>
		                            <tr><th style="text-align: left">Boletos leidos</th><td>{{ pretty_number($event_statistics['general_info_tickets']['total_tickets_read'], 0) }}</td></tr>
		                            <tr><th style="text-align: left">Lecturas</th><td>{{ pretty_number($event_statistics['general_info_tickets']['total_readings'], 0) }}</td></tr>
		                            <tr><th style="text-align: left">Doble lectura</th><td>{{ pretty_number($event_statistics['general_info_tickets']['total_errors'], 0) }}</td></tr>
		                            <tr><th style="text-align: left">Primera lectura</th><td>{{ $event_statistics['general_info_tickets']['first_ticket_reading'] }}</td></tr>
		                            <tr><th style="text-align: left">Última lectura</th><td>{{ $event_statistics['general_info_tickets']['last_ticket_reading'] }}</td></tr>
	                             @endif
	                        </thead>
	     
	                    </table>
	                </div>
	            </div>
	        </div>


	       
	        <div style="page-break-after:always;"></div>


	    <!-- @ if($event->passedEvent()) -->
	    @if(true)
	        @if(!is_null($event_statistics))
	            <div class="row price-table price-table--basic">
	                <div class="col-sm-12 text-center m-t-25 m-b-25">
	                    <hr />
	                </div>

	                <div  class="col-sm-12 text-center">
	                    <h3>Ventas</h3>
	                  
	                   <img src='{{ $img_sales }}'  style="max-height: 125px;">

	                </div>
	                <div style="page-break-after:always;"></div>

	                <div class="col-sm-12 text-center m-t-25 m-b-25">
	                    <hr />
	                </div>

	                <div  class="col-sm-6 text-center">
	                	<br>
	                    <h3>Accesos</h3>
	                   <img src='{{ $img_readings }}' style="max-height: 350px;">

	                </div>
	                <div style="page-break-after:always;"></div>

	                <div class="col-sm-12 text-center m-t-25 m-b-25">
	                    <hr />
	                </div>

	                <div class="col-sm-6 text-center">
	                	<br>
	                    <h3>Salidas</h3>
	                    
	                   <img src='{{ $img_exits }}' style="max-height: 350px;">

	                </div>

	                <div style="page-break-after:always;"></div>

	                <div class="col-sm-6 text-center">
	                	<br>
	                    <h3>Lecturas por dispositivo</h3>
	           
	                   <img src='{{ $img_device }}' style="max-height: 100%; width: 105%;">

	                </div>

	                <div style="page-break-after:always;"></div>

	                <div class="col-sm-6 text-center">
	                	<br>
	                    <h3>Lecturas por dispositivo</h3>
	                	<center>
	                   		<img src='{{ $img_devicesPie }}' style="max-height: 200%; width: 190%; justify-content: center;">
	                    </center>
	                </div>

	                <div style="page-break-after:always;"></div>

	                <div class="col-sm-6 text-center">
	                	<br>
	                    <h3>Incidencias por punto de acceso</h3>
	                	
	                   <img src='{{ $img_checkpoint_alerts }}' style="max-height: 100%; width: 105%;">


	                </div>

	                <div style="page-break-after:always;"></div>

	                <div class="col-sm-6 pull-lefth">
	                	<br>
	                    <h3>Incidencias por dispositivo </h3>
	                	<center>
	                   		<img src='{{ $img_device_alertsPieChart }}' style="max-height: 200%; width: 190%; justify-content: center;">
	                   </center>
	                </div>

	                <div style="page-break-after:always;"></div>

	                <div class="col-sm-6 text-center">
	                	<br>
	                    <h3>Lectores</h3>
	                
	                   <img src='{{ $img_checkpointChart }}' style="max-height: 100%; width: 105%;">

	                </div>

	                <div style="page-break-after:always;"></div>

	                <div class="col-sm-6 text-center">
	                	<br>
	                    <h3>Incidencias</h3>
	                
	                   <img src='{{ $img_incidences }}' style="max-height: 100%; width: 105%;">

	                </div>
	            </div>
	        @else
	            <div class="col-sm-12 text-center" style="opacity: 0.5">
	                <br>
	                    NO HAY ESTADÍSTICAS PARA MOSTRAR
	            </div>
	        @endif
	    @endif
	    <div style="page-break-after:always;"></div>
		<br>
	    @if($readings)
	        <div class="card">
	            <div class="card-body">
	                <div class="table-responsive">
	                    <table id="data-table" class="table table-bordered" style="width:100%">
	                        <thead class="thead-default">
	                            <tr>
	                                <th>ID</th>
	                                <th>Ticket</th>
	                                <th>Checkpoint</th>
	                                <th class="text-center">Tipo</th>
	                                <th>Hora</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            @foreach($readings as $reading)
	                                <tr>
	                                    <td>{{ $reading->id }}</td>
	                                    <td>{{ $reading->ticket->order->user->name }}</td>
	                                    <td>{{ $reading->checkpoint->name }}</td>
	                                    <td class="text-center">{!! $reading->pretty_type_text() !!}</td>
	                                    <td>{{ $reading->reading_time }}</td>
	                                </tr>
	                            @endforeach
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    @endif


	</body>
</html>

        
