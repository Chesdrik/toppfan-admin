@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Crear grupo de eventos</h1>
            </header>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('system.events.groups.store', $system->slug), 'method' => 'POST', 'enctype' => 'multipart/form-data')) !!}
                        {!! Form::MDtext('Nombre', 'name', old('name'), $errors) !!}
                        {!! Form::MDtext('Slug', 'slug', old('slug'), $errors) !!}
                        {!! Form::MDtext('Subtítulo', 'tagline', old('tagline'), $errors) !!}
                        {!! Form::MDtextarea('Descripción', 'description', old('description'), $errors) !!}

                        {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
