@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Editar {{ $event_group->name }}</h1>
            </header>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 offset-2">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Datos generales</h4>
                            {!! Form::open(array('url' => route('system.events.groups.update', [session('system_slug'), $event_group->id]), 'method' => 'PUT', 'enctype' => 'multipart/form-data')) !!}
                                {!! Form::MDtext('Nombre', 'name', old('name') ?? $event_group->name, $errors) !!}
                                {!! Form::MDtext('Slug', 'slug', old('slug') ?? $event_group->slug, $errors, ['disabled' => true]) !!}
                                {!! Form::MDtext('Subtítulo', 'tagline', old('tagline') ?? $event_group->tagline, $errors) !!}
                                {!! Form::MDtextarea('Descripción', 'description', old('description') ?? $event_group->description, $errors) !!}
                                {!! Form::MDselectize('Eventos', 'events', \App\Event::selectize(), $selected_events, $errors) !!}

                                {!! Form::MDToggleSwitch('Abonos en venta', 'accreditations_on_sale', $event_group->accreditations_on_sale, $errors) !!}

                                {!! Form::MDsubmit('Guardar', 'edit', ['icon' => 'plus-circle-o']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Imágen</h4>
                            <div class="row">
                                {!! Form::MDdropzone('', 'img', route('system.events.groups.upload.imgs', [session('system_slug'), $event_group->id]), $errors, ['leyend' => 'Tamaño: 600x600 px']) !!}
                                <div class="col-sm-12 m-t-15">
                                    <img class="center img-fluid" src="{{ asset($event_group->img_path()) }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Imágen promocional</h4>
                            <div class="row">
                                {!! Form::MDdropzone('', 'img_slideshow', route('system.events.groups.upload.imgs', [session('system_slug'), $event_group->id]), $errors, ['accepted-files' => '.jpg, .jpeg', 'leyend' => 'Tamaño: 600x600 px']) !!}
                                <div class="col-sm-12 m-t-15">
                                    <img class="center img-fluid" src="{{ asset($event_group->img_slideshow_path()) }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
