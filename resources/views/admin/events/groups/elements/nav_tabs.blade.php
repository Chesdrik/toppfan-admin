<div class="card">
    <div class="card-body">
        <ul class="nav justify-content-center nav-tabs">
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'general' ? 'active' : '') }}" href="{{ route('system.events.groups.show', [session('system_slug'), $event_group->id]) }}">General</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'holding' ? 'active' : '') }}" href="{{ route('system.events.groups.holding', [session('system_slug'), $event_group->id]) }}">Holding</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ ($selected == 'orders' ? 'active' : '') }}" href="{{ route('system.events.groups.orders', [session('system_slug'), $event_group->id]) }}">Órdenes y abonos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link  {{ ($selected == 'zones' ? 'active' : '') }}" href="{{ route('system.events.groups.zones', [session('system_slug'), $event_group->id]) }}">Zonas y precios</a>
            </li>
        </ul>
    </div>
</div>
