@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Grupos de eventos</h1>

        <div class="actions">
            <a href="{{ route('system.events.groups.create', [session('system_slug')]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar grupo de eventos
            </a>
        </div>
    </header>


    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-md-6">Nombre</th>
                            <th class="col-md-5">Slug</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($system->event_groups as $event_group)
                            <tr>
                                <th class="text-center">
                                    <a href="{{ route('system.events.groups.show', [session('system_slug'), $event_group->id]) }}" target="_self">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-eye zmdi-hc-fw"></i>
                                        </button>
                                    </a>
                                </th>

                                <td>{{ $event_group->name }}</td>
                                <td>{{ $event_group->slug }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $(".data-table").DataTable().order([1, 'asc']).draw();
    });
</script>
@endsection
