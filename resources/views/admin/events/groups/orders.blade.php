@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event_group->system->teams->first()->name }} - {{ $event_group->name }} | Abonos</h1>

                <div class="actions actions--calendar">
                    @if(in_array(Auth::user()->type , ['root', 'admin', 'courtesies']))
                        <a href="{{ route('system.events.groups.season.tickets.generateQRs', [session('system_slug'), $event_group->id]) }}" target="_blank" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                            <i class="zmdi zmdi-accounts-list"></i> Imprimir QRs
                        </a>
                    @endif
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.groups.elements.nav_tabs', ['selected' => 'orders'])
        </div>
    </div>

    @if($event_group->orders->count() > 0)
        <div class="row">
            <div class="col-sm-12">
                <header class="content__title">
                    <h1>Órdenes y abonos registrados</h1>
                </header>
            </div>
        </div>

        <div class="row">
            @foreach($event_group->orders()->orderBy('created_at', 'DESC')->get() as $order)
                <div class="col-sm-6">
                    <div class="card p-0">
                        <div class="card-body">
                            <h4 class="card-title">
                                #{{ $order->id }} - {{ $order->client_user->full_name() }}

                                <div class="actions pull-right">
                                    <a href="#edit-order-{{ $order->id }}" data-toggle="modal" data-target="#edit-order-{{ $order->id }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                        <i class="zmdi zmdi-edit"></i> Orden
                                    </a>
                                    <a href="{{ route('system.orders.resend.email', [session('system_slug'), $order->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text btn-resend">
                                        <i class="zmdi zmdi-mail-send zmdi-hc-fw"></i> Reenviar correo
                                    </a>
                                    <a href="{{ route('system.orders.show', [session('system_slug'), $order->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                        <i class="fas fa-eye"></i> Ver
                                    </a>
                                    @if ($order->type == 'season_ticket')
                                        <a href="{{ route('system.events.groups.order.qrs', [session('system_slug'), $event_group->id, $order->id]) }}" target="_blank" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                            <i class="fas fa-qrcode"></i> QRs
                                        </a>
                                    @else
                                        <a href="{{ route('system.events.order_qrs', [session('system_slug'), $event_group->id, $order->id]) }}" target="_blank" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                            <i class="fas fa-qrcode"></i> QRs
                                        </a>
                                    @endif
                                </div>
                            </h4>
                            <h6 class="card-subtitle">
                                {{ $order->email }} <br />
                                {{ $order->created_at }}
                            </h6>

                            <p class="card-text">
                                <div class="table-responsive">
                                    <table class="table table-bordered data-table2">
                                        <thead class="thead-default">
                                            <tr>
                                                <th class="col-sm-6">Zona | Asiento</th>
                                                <th class="col-sm-2 text-center">Cantidad</th>
                                                <th class="col-sm-2 text-right">Precio</th>
                                                <th class="col-sm-2 text-right">Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($order->seasson_tickets as $ticket)
                                                <tr>
                                                    <td>
                                                        {{ $ticket->ticket_type->name }}
                                                        @if ($ticket->seat)
                                                        | {{ $ticket->seat->row->name }} - {{ $ticket->seat->name }}
                                                        @else
                                                            General
                                                        @endif
                                                        </td>
                                                    <td class="text-center">{{ $ticket->amount }}</td>
                                                    <td class="text-right">{{ pretty_money($ticket->price) }}</td>
                                                    <td class="text-right">{{ pretty_money($ticket->amount*$ticket->price) }}</td>
                                                </tr>
                                            @endforeach

                                            <tr>
                                                <td colspan="3" class="text-right">Total:</td>
                                                <td  class="text-right">
                                                    <strong>{{ pretty_money($order->total) }}</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <div class="col-sm-12 text-center" style="opacity: 0.5">
                NO HAY ORDENES REGISTRADAS
        </div>
    @endif

@foreach($event_group->orders()->orderBy('created_at', 'DESC')->get() as $order)
    @include('admin.orders.modals.edit')
@endforeach
@endsection
    
@section('after_includes')
<script>
    $('.btn-resend').on('click', function(e){
        if(!confirm('¿Estás seguro de querer reenviar el correo de confirmación de compra?')){
            e.preventDefault();
        }
    });
</script>
@endsection
