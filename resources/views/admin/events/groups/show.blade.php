@extends('layouts.app')

@section('content')
    @if($event_group->on_sale)
        <div class="row p-t-25">
            <div class="col-sm-12">
                <div class="alert alert-warning" role="alert">
                    El abono se encuentra a la venta en el portal web
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <header class="content__title">
                <h1>{{ $event_group->name}}</h1>

                <div class="actions actions--calendar">
                    <div class="row">
                        <a href="{{ route('system.events.groups.edit', [session('system_slug'), $event_group->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                            <i class="zmdi zmdi-edit"></i> Editar
                        </a>
                    </div>
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.groups.elements.nav_tabs', ['selected' => 'general'])
        </div>
    </div>

    <div class="row price-table price-table--basic">
        <div class="col-md-12">
            <header class="content__title">
                <h1>Evento</h1>
            </header>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="price-table__item">
                        <header class="price-table__header bg-light-blue">
                            <div class="price-table__title">Evento</div>
                        </header>
                        <div class="price-table__price color-light-blue stat_data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <img src="{{ asset($event_group->img_path()) }}" class="img-fluid event_image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="price-table__item">
                        <div class="price-table__price color-light-blue stat_data event_image_slideshow_card_container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <img src="{{ asset($event_group->img_slideshow_path()) }}" class="img-fluid event_image_slideshow" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="price-table__item">
                        <header class="price-table__header bg-light-blue">
                            <div class="price-table__title">Sede / fecha</div>
                        </header>
                        <div class="price-table__price color-light-blue stat_data">
                            {{ $event_group->system->venues->first()->name }}<br />
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="price-table__item">
                        <header class="price-table__header bg-light-blue">
                            <div class="price-table__title">Monto</div>
                        </header>
                        <div class="price-table__price color-light-blue stat_data">
                            $0.00
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
