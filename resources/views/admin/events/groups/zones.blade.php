@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Zonas y precios | {{ $event_group->name }}</h1>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.groups.elements.nav_tabs', ['selected' => 'zones'])
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 offset-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => route('system.events.groups.update.ticket_types', [session('system_slug'), $event_group->id]), 'method' => 'PUT', 'enctype' => 'multipart/form-data')) !!}
                        <h4 class="card-title">Tipos de tickets</h4>
                        <h6 class="card-subtitle">Selecciona los tipos de tickets a vender como abono.<br />Ingresa el número máximo de abnos a vender y su precio</h6>


                        @foreach($system->venues as $venue)
                            <div class="row p-t-25 p-b-25">
                                <div class="col-sm-12">
                                    <h3 class="card-subtitle">{{ $venue->name }}</h3>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2 strong">Tipo de boleto</div>
                                <div class="col-sm-2 strong">Max. venta</div>
                                <div class="col-sm-2 strong">Precio</div>

                                <div class="col-sm-2 strong">Tipo de boleto</div>
                                <div class="col-sm-2 strong">Max. venta</div>
                                <div class="col-sm-2 strong">Precio</div>

                                <div class="col-sm-12"><hr></div>
                            </div>

                            <div class="row">
                                @foreach($venue->ticket_types_relation as $ticket_type)
                                    {!! Form::MDToggleSwitch($ticket_type->name, 'tt_'.$ticket_type->id, (array_key_exists($ticket_type->id, $compressed_ticket_types) ? true : false), $errors, ['class' => 'col-sm-2']) !!}
                                    {!! Form::MDtext('', $ticket_type->id.'_total', (array_key_exists($ticket_type->id, $compressed_ticket_types) ? $compressed_ticket_types[$ticket_type->id]['total'] : false), $errors, ['class' => 'col-sm-2']) !!}
                                    {!! Form::MDtext('', $ticket_type->id.'_price', (array_key_exists($ticket_type->id, $compressed_ticket_types) ? $compressed_ticket_types[$ticket_type->id]['price'] : false), $errors, ['class' => 'col-sm-2']) !!}
                                @endforeach
                            </div>
                        @endforeach

                        {!! Form::MDsubmit('Guardar', 'edit', ['icon' => 'plus-circle-o']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
