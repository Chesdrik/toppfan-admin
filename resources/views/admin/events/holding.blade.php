@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->name}} | Holding</h1>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'holding'])
        </div>
    </div>

    <div class="row">
        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Holding</h4>
                            <h6 class="card-subtitle">Selecciona los asientos disponibles que desees apartar</h6>
                            <div class="actions">
                                <div class="dropdown actions__item">
                                    <i data-toggle="dropdown" class="zmdi zmdi-code-setting" aria-expanded="false"></i>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(30px, 25.8667px, 0px);">
                                        <div class="dropdown-item">Libre - bloqueado</div>
                                        <div class="dropdown-item block_patter" data-pattern="3-2-r"><i class="zmdi zmdi-border-right zmdi-hc-fw"></i> 3-2</div>
                                        <div class="dropdown-item block_patter" data-pattern="3-2-l"><i class="zmdi zmdi-border-left zmdi-hc-fw"></i> 3-2</div>
                                        <div class="dropdown-item block_patter" data-pattern="4-2-r"><i class="zmdi zmdi-border-right zmdi-hc-fw"></i> 4-2</div>
                                        <div class="dropdown-item block_patter" data-pattern="4-2-l"><i class="zmdi zmdi-border-left zmdi-hc-fw"></i> 4-2</div>
                                    </div>
                                </div>

                                <div class="dropdown actions__item">
                                    <i data-toggle="dropdown" class="zmdi zmdi-dialpad" aria-expanded="false"></i>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(30px, 25.8667px, 0px);">
                                        @for($i = 9; $i >= 1; $i--)
                                            <div class="dropdown-item block_percentage" data-percentage="{{ $i }}0">Bloquear {{ $i }}0%</div>
                                        @endfor
                                    </div>
                                </div>

                                <div class="dropdown actions__item">
                                    <i data-toggle="dropdown" class="zmdi zmdi-refresh" aria-expanded="false"></i>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(30px, 25.8667px, 0px);">
                                        <div class="dropdown-item unblock_all">Desbloquear todos</div>
                                        <div class="dropdown-item block_all">Bloquear todos</div>
                                    </div>
                                </div>
                            </div>

                            <div id="response_errors" style="display: none;">
                                <div class="alert alert-danger" role="alert">
                                    <div id="response_errors_messages"></div>
                                </div>
                            </div>


                            <div id="loading" class="col-sm-12 text-center" style="display: none; height: 650px; border: 3px solid #f0f0f0">
                                <img src="{{ asset('loadings/svg-loaders/puff.svg') }}" style="width: 75px; padding-top: 250px;" /><br />
                                <span id="loading_text">
                                    cargando mapa con asientos...
                                </span>
                            </div>

                            <div id="zone_selection">
                                @livewire('seat-selection', [$event->id, 'event'])
                            </div>

                            <div id="save" class="btn btn-info m-t-15">Guardar cambios</div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Estadísticas de disponibilidad <small>On Hold | Vendidos | Disponibles</small></h4>
                            <h6 class="card-subtitle">Generales y por zona</h6>

                            <div class="row">
                                <div class="col-sm-6">
                                    @include('admin.events.statistics.event_tickets')
                                </div>
                                <div class="col-sm-6">
                                    @include('admin.events.includes.ticket_info')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-2">
            <div class="row">
                <div class="col-sm-12 m-b-15">
                    <a href="{{ route('system.events.general.statistics', [session('system_slug'), $event->id]) }}">
                        <h4 class="card-title">
                            <i class="zmdi zmdi-equalizer"></i> &nbsp; Ver generales</h4>
                    </a> <br />
                    <h4 class="card-title">Tipos de boletos</h4>
                </div>

                @foreach($zones as $zone)
                    <a class="btn btn-outline-secondary btn-block mt-2 text-left" data-toggle="collapse" href="#collapse_{{ $zone['zone']['zone_id'] }}" role="button" aria-expanded="false" aria-controls="collapse_{{ $zone['zone']['zone_id'] }}">
                        <span class="dot m-r-15" style="background-color: #{{ $zone['zone']['color'] }}"></span> {{ $zone['zone']['name'] }}
                    </a>

                    <div class="collapse mt-2 mb-4" id="collapse_{{ $zone['zone']['zone_id'] }}">
                        @foreach($zone['subzones'] as $subzone)
                            {!! Form::MDButtonSeatType($subzone['name'], $subzone['zone_id'], $subzone['color'], ['class' => 'btn btn-light btn--raised col-sm-12 text-left seat-selection-button']) !!}
                        @endforeach
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row price-table price-table--basic">
    </div>

@endsection


@section('after_includes')

    @livewireScripts
    <script src="{{ asset('/svgjs/svg.js') }}"></script>
    <script src="{{ asset('/svgjs/svg.draggy.js') }}"></script>
    <script src="{{ asset('/svgjs/svg.pan-zoom.min.js') }}"></script>
    @include('admin.events.includes.seat_layout_functions')

@endsection
