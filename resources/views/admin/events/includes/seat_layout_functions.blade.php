<script type="text/javascript">
    // Defining update dictionary
    window.seats_to_hold = [];
    window.seats_to_release = [];
    window.seats_to_courtesy_sale = [];

    const event = @json($event->id);
    const type = @json($type);
    $(".unblock_all").on('click', function(){ Livewire.emit('updateHolding', 'unblock_all'); });
    $(".block_all").on('click', function(){ Livewire.emit('updateHolding', 'block_all'); });
    $(".block_percentage").on('click', function(){ Livewire.emit('updateHoldingPercengage', $(this).attr('data-percentage')); });
    $(".block_patter").on('click', function(){ Livewire.emit('updateHoldingPattern', $(this).attr('data-pattern')); });
    $(".unblock_all, .block_all, .block_percentage, .block_patter").on('click', function(){ loadingStart("guardando cambios..."); }); // Loading message

    // Livewire zone update trigger function
    $(".seat-selection-button").on('click', function(){
        loadingStart("cargando mapa con asientos...");

        Livewire.emit('updateZone', $(this).attr('seat-type-id'));
        Livewire.emit('zoneData', $(this).attr('seat-type-id'), event, type);

    });

    // SVG seats functions
    document.addEventListener('hide-seat-layout', event => {
        console.log('Fading out');
        $("#save").fadeOut('fast');
        loadingEnd(false);
        $('#loading').remove();

        // $("#zone_selection").fadeOut('fast');
    });

    // SVG seats functions
    document.addEventListener('view-updated', event => {
        console.log('Redefining variables');
        console.log(event.detail.responseErrors);

        if(event.detail.responseErrors == null){
            $("#response_errors").fadeOut(function(){
                $("#response_errors_messages").html('');
            });
        }else{
            $("#response_errors_messages").html(event.detail.responseErrors);
            $("#response_errors").fadeIn();
        }


        window.seats_to_hold = [];
        window.seats_to_release = [];
        window.seats_to_courtesy_sale = [];

        // Operation variables
        var totalSelected = 0;

        // Drawing SVG canvas
        var svg = SVG($("#layoutContainer").get(0)).size("100%", "100%");
        var nodes = svg.group();

        // Painting each row / seat
        JSON.parse(event.detail.seatLayoutData).forEach(function(element, row_index){
            // Row action
            nodes.rect(16, 16).radius(8).attr({ fill: "#607D8B", x: 10, y: (row_index*25)+17, row_id: element.row_id, row_status: 'original'}).addClass('row_action');

            // Appending row text node
            nodes.text(element.name).attr({ fill: '#607D8B', x: 40, y: (row_index*25)+15}).font({size: 10, weight: 'bold'});

            // Calculating row start, total seats and offset
            var start = parseInt(element.seats.start)+1;
            var total = parseInt(element.seats.total)+element.seats.skip_seats.length;
            var offset = start*25;

            // Appending seat rows
            current_index = 1;
            for(seat_index = 1; seat_index <= total; seat_index++){
                // Checking if we need to skip spot
                if(!element.seats.skip_seats.includes(seat_index)){
                    // Determining position
                    var x_position = offset+(seat_index*25);
                    var y_position = (row_index*25)+15;

                    // Defining node color/class
                    var seatClass = 'seat-selectable';
                    var seat_status = '';

                    if(element.seats.on_hold.includes(current_index.toString())){
                        var fillColor = '#FF8A65';
                        var seat_status = 'on_hold';
                    }else if(element.seats.on_hold_sale.includes(current_index.toString())){
                        var fillColor = '#BDBDBD';
                        var seatClass = '';
                        var seat_status = 'on_hold_sale';
                    }else{
                        var fillColor = '#B3E5FC';
                        var seat_status = 'available';
                    }

                    // Painting node
                    nodes.rect(20, 20).radius(3).attr({ fill: fillColor, x: x_position, y: y_position, row_id: element.row_id, seat_index: current_index, seat_status: seat_status, original_status: seat_status}).addClass(seatClass);

                    // Incrementing index
                    current_index++;
                }
            };
        });

        // Adding pan/zoom functionality
        nodes.panZoom({ zoomSpeed: -2, zoom: [0.8, 2]});

        SVG.select('rect.row_action').click(function(){
            console.log('row_id = '+this.attr('row_id'));

            if(this.attr('row_status') == 'original'){
                // Setting row to on_hold
                this.attr('row_status', 'on_hold');

                // Triggering seats
                $("rect.seat-selectable[row_id='"+this.attr('row_id')+"']").each(function(){
                    if(this.instance.attr('seat_status') == 'available'){
                        this.instance.fire('click');
                    }
                });
            }else if(this.attr('row_status') == 'on_hold'){
                // Setting row to on_hold_sale
                this.attr('row_status', 'on_hold_sale');

                // Triggering seats
                $("rect.seat-selectable[row_id='"+this.attr('row_id')+"']").each(function(){
                    if(this.instance.attr('seat_status') == 'on_hold'){
                        this.instance.fire('click');
                    }
                });
            }else if(this.attr('row_status') == 'on_hold'){
                // Setting row to available
                this.attr('row_status', 'available');

                // Triggering seats
                $("rect.seat-selectable[row_id='"+this.attr('row_id')+"']").each(function(){
                    if(this.instance.attr('seat_status') == 'on_hold_sale'){
                        this.instance.fire('click');
                    }
                });
            }else{
                // Returning row to original status
                this.attr('row_status', 'original');

                // Triggering seats
                $("rect.seat-selectable[row_id='"+this.attr('row_id')+"']").each(function(){
                    console.log(this.instance.attr('seat_status')+" -> "+this.instance.attr('original_status'));

                    if(this.instance.attr('original_status') == 'on_hold'){
                        this.instance.fire('click');
                        this.instance.fire('click');
                    }else if(this.instance.attr('original_status') == 'available'){
                        this.instance.fire('click');
                    }
                });
            }
        });

        // Available seat selection listeners
        SVG.select('rect.seat-selectable').click(function() {
            var seat_status = this.attr('seat_status');
            var original_status = this.attr('original_status');
            var row_id = parseInt(this.attr('row_id'));
            var seat_index = parseInt(this.attr('seat_index'));

            if(seat_status == 'available'){
                // If seat is 'available', we make it 'on_hold'
                var color = (original_status == 'on_hold' ? '#FF8A65' : '#c75b39');
                this.animate(250).fill({ color: color });
                this.attr({seat_status: 'on_hold'});

                // Remove seat from 'release' and append it to 'on_hold'
                removeSeatFromRelease(row_id, seat_index);

                // If it was originally on_hold, we append it to seats_to_release
                if(original_status != 'on_hold'){
                    appendSeatToOnHold(row_id, seat_index);
                }
            }else if(seat_status == 'on_hold'){
                // If seat is 'on_hold', we make it 'courtesy_sale'
                this.animate(250).fill({ color: '#8BC34A' });
                this.attr({seat_status: 'courtesy_sale'});

                // Remove seat from 'on_hold' and append it to 'courtesy_sale'
                removeSeatFromOnHold(row_id, seat_index);
                appendSeatToCourtesySale(row_id, seat_index);
            }else if(seat_status == 'courtesy_sale'){
                // If seat is 'courtesy_sale', we make it 'available'
                this.animate(250).fill({ color: '#B3E5FC' });
                this.attr({seat_status: 'available'});

                // Remove seat from 'courtesy_sale' and append it to 'available'
                removeSeatFromCourtesySale(row_id, seat_index);

                // If it was originally on_hold, we append it to seats_to_release
                if(original_status == 'on_hold'){
                    appendSeatToRelease(row_id, seat_index);
                }

            }
        });

        console.log()
        console.log('loadingEnd');
        loadingEnd();

        $('#loading').remove();
        $("#zone_selection").fadeIn('fast');
        $("#save").fadeIn('fast');
    });

    // Appending seats from to_hold array
    function appendSeatToOnHold(row_id, seat_index){
        if(window.seats_to_hold[row_id] == undefined){
            window.seats_to_hold[row_id] = [seat_index];
        }else{
            window.seats_to_hold[row_id].push(seat_index)
        }
    }

    // Removing seats from to_hold array
    function removeSeatFromOnHold(row_id, seat_index){
        if(window.seats_to_hold[row_id] != undefined){
            window.seats_to_hold[row_id] = rowSeatRemove(window.seats_to_hold[row_id], seat_index);
        }
    }

    // Appending seats from courtesy array
    function appendSeatToCourtesySale(row_id, seat_index){
        if(window.seats_to_courtesy_sale[row_id] == undefined){
            window.seats_to_courtesy_sale[row_id] = [seat_index];
        }else{
            window.seats_to_courtesy_sale[row_id].push(seat_index)
        }
    }

    // Removing seats from courtesy array
    function removeSeatFromCourtesySale(row_id, seat_index){
        if(window.seats_to_courtesy_sale[row_id] != undefined){
            window.seats_to_courtesy_sale[row_id] = rowSeatRemove(window.seats_to_courtesy_sale[row_id], seat_index);
        }
    }

    // Appending seats from release array
    function appendSeatToRelease(row_id, seat_index){
        if(window.seats_to_release[row_id] == undefined){
            window.seats_to_release[row_id] = [seat_index];
        }else{
            window.seats_to_release[row_id].push(seat_index)
        }
    }

    // Removing seats from release array
    function removeSeatFromRelease(row_id, seat_index){
        if(window.seats_to_release[row_id] != undefined){
            window.seats_to_release[row_id] = rowSeatRemove(window.seats_to_release[row_id], seat_index);
        }
    }

    // Cleaning array data
    function arrayCleanup(array){
        i = 0;
        array.forEach(function(row, row_index){
            var j = 0;
            row.forEach(function(seat, seat_index){
                j++;
            });

            if(j > 0){
                j++; i++;
            }else{
                delete array[row_index];
            }
        });

        // Returning empty array
        return (i == 0 ? [] : array);
    }

    // Removes a selected seat from a given row
    function rowSeatRemove(row, selected_seat) {
        return row.filter(function(value, index, row){
            return value != selected_seat;
        });
    }

    $("#save").on('click', function(){
        // Start loading on view
        loadingStart("guardando cambios...");

        // Cleaning arrays
        window.seats_to_hold = arrayCleanup(window.seats_to_hold);
        window.seats_to_release = arrayCleanup(window.seats_to_release);
        window.seats_to_courtesy_sale = arrayCleanup(window.seats_to_courtesy_sale);

        // Emitting updates
        Livewire.emit('saveUpdates', window.seats_to_hold, window.seats_to_release, window.seats_to_courtesy_sale);
    });

    // Fading out loading and fading in zone selection map
    function loadingStart(loading_text){
        $("#loading_text").html(loading_text);
        $("#response_errors").fadeOut('fast');

        $("#zone_selection").fadeOut('fast', function(){
            $("#loading").fadeIn('slow');
        });
    }

    function loadingEnd(fadeInZoneSelection = true){
        $("#loading").fadeOut('fast', function(){
            if(fadeInZoneSelection){
                $("#zone_selection").fadeIn('slow');
            }
        });
    }



    // B3E5FC azul <-- disponible
    // BDBDBD gris <-- on hold vendido
    // 8BC34A verde <--
    // DCE775 Verde <-- a liberar
    // FF8A65 Naranja <-- on hold

    // Fading out loading and fading in zone selection map
    // $("#loading").fadeOut('fast', function(){
    //     $("#zone_selection").fadeIn('slow');
    // });
</script>
