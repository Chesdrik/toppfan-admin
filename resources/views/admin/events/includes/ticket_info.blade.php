<h3>Boletos por zona  </h3>
<canvas id="zonePieChart" width="auto" height="200"></canvas>

<script type="text/javascript">
    window.addEventListener('zoneData', event => {
        // Destroying previous chart
        if(window.eventTicketsZoneChart !== undefined){
            window.eventTicketsZoneChart.destroy();
        }

        var status = event.detail.data['labels'];
        var colors = event.detail.data['colors'];
        var data2 = event.detail.data['data'];

        var readingctx = document.getElementById('zonePieChart').getContext('2d');
        window.eventTicketsZoneChart = new Chart(readingctx, {
            type: 'doughnut',
            data: {
                labels: status,
                datasets: [
                {
                    label: status,
                    backgroundColor: colors,
                    data: data2
                }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Distribución de boletos vendidos, apartados y disponibles por zona'
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var value = data.datasets[0].data[tooltipItem.index];
                            value = value.toString();
                            value = value.split(/(?=(?:...)*$)/);
                            value = value.join(',');
                            value = data.datasets[0].label[tooltipItem.index]+': ' + value;
                            return value;
                        }
                    },
                }
            }
        });
    })
</script>
