@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>Eventos</h1>

                <div class="actions actions--calendar">
                    <a href="" class="actions__item actions__calender-prev"><i class="zmdi zmdi-long-arrow-left"></i></a>
                    <a href="" class="actions__item actions__calender-next"><i class="zmdi zmdi-long-arrow-right"></i></a>
                </div>
            </header>
        </div>

        <div class="col-sm-3">
            @foreach($events as $event)
            <div class="row">
                <div class="col-sm-10">
                    <h5>{{ $event->name }}</h5>
                    {{ pretty_date($event->date) }}
                </div>
                <div class="col-sm-2">
                    <br />
                    <a href="{{ url('/admin/eventos/'.$event->id) }}">
                        <button class="btn btn-secondary btn-sm btn--raised"><i class="zmdi zmdi-trending-up"></i></button>
                    </a>
                </div>
            </div>
            <hr />
            @endforeach
        </div>
        <div class="col-sm-9">
            <div class="calendar card"></div>
        </div>
    </div>
@endsection



@section('after_includes')
<!-- Calendar Script -->
<script type="text/javascript">
    'use strict';
    $(document).ready(function() {
        var date = new Date();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('.calendar').fullCalendar({
            locale: 'es',
            header: false,
            buttonIcons: {
                prev: 'calendar__prev',
                next: 'calendar__next'
            },
            theme: false,
            selectable: true,
            selectHelper: true,
            editable: true,
            events: [
                @foreach($events as $event)
                {
                    id: {{ $event->id }},
                    title: '{{ $event->name }}',
                    start: '{{ $event->date_start() }}',
                    end: '{{ $event->date_end() }}',
                    allDay: false,
                    className: 'bg-blue',
                    description: '{{ $event->name }} - {{ pretty_short_hour($event->date) }}',
                },
                @endforeach
            ],


            viewRender: function (view) {
                var calendarDate = $('.calendar').fullCalendar('getDate');
                var calendarMonth = calendarDate.month();

                //Set data attribute for header. This is used to switch header images using css
                $('.calendar .fc-toolbar').attr('data-calendar-month', calendarMonth);

                //Set title in page header
                $('.content__title--calendar > h1').html(view.title);
            },

            eventClick: function (event, element) {
                console.log('Display modal #event-'+event.id);
                // console.log(element);
                // $('#event-'+event.id+' input[value='+event.className+']').prop('checked', true);
                // $('#event-'+event.id).modal('show');
                // $('.edit-event__id').val(event.id);
                // $('.edit-event__title').val(event.title);
                // $('.edit-event__description').val(event.description);
            }
        });

        // Calendar views switch
        $('body').on('click', '[data-calendar-view]', function(e){
            e.preventDefault();

            $('[data-calendar-view]').removeClass('actions__item--active');
            $(this).addClass('actions__item--active');
            var calendarView = $(this).attr('data-calendar-view');
            $('.calendar').fullCalendar('changeView', calendarView);
        });


        // Calendar Next
        $('body').on('click', '.actions__calender-next', function (e) {
            e.preventDefault();
            $('.calendar').fullCalendar('next');
        });


        // Calendar Prev
        $('body').on('click', '.actions__calender-prev', function (e) {
            e.preventDefault();
            $('.calendar').fullCalendar('prev');
        });
    });
</script>


@endsection
