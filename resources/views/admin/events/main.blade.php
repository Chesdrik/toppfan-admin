@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-10 offset-sm-1">

        <div class="row">
            <div class="col-sm-4 offset-sm-1">
                <header class="content__title text-center">
                    <h1>Eventos por sede</h1>
                </header>

                @foreach($venues as $venue)
                    <a href="{{ route('system.events.byVenue', [session('system_slug'), $venue->id]) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                        {{ $venue->name }}
                    </a>
                @endforeach
            </div>


            <div class="col-sm-4 offset-sm-2">
                <header class="content__title text-center">
                    <h1>Eventos por equipo</h1>
                </header>
                @foreach($teams as $team)
                    <a href="{{ route('system.events.byTeam', [session('system_slug'), $team->id]) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                        {{ $team->name }}
                    </a>
                @endforeach
            </div>
        </div>

        <div class="row m-t-25">
            <div class="col-sm-6 offset-sm-3">
                <a href="{{ route('system.events.all', session('system_slug')) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                    Todos los eventos
                </a>
            </div>
        </div>

        <div class="row m-t-25">
            <div class="col-sm-6 offset-sm-3">
                <a href="{{ route('system.events.groups.index', session('system_slug')) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                    Grupos de eventos
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
