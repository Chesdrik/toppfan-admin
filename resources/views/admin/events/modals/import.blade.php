<div class="modal fade show" id="importar" tabindex="-1" style="display: none;" aria-modal="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Importar archivo</h5>
                <hr>
            </div>
            <form method="POST" action="{{ route('system.events.import.file', $event->id) }}" class="row" enctype="multipart/form-data">
            <div class="modal-body">
                @csrf
                <div class="col-md-12">
                    Selecciona el archivo a importar. <br>
                    <b>Recuerda que debe tener extensión .tsv</b><br><br>
                </div>

                <div class="col-md-8">
                    <div class="input-group">
                        <label for="exampleFormControlFile1">Archivo</label>
                        <input type="file" class="form-control-file" name="file" accept=".tsv" required>
                    </div>
                </div>

            </div>
            <div class="col-md-12 modal-footer text-right">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Importar</button>
            </div>
            </form>
        </div>
    </div>
</div>
