@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->team->name }} - {{ $event->name }} | Boletos</h1>

                <div class="actions actions--calendar">
                    @if(in_array(Auth::user()->type , ['root', 'admin', 'courtesies']))
                        <a href="{{ route('system.events.tickets.generate', [session('system_slug'), $event->id]) }}" target="_blank" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                            <i class="zmdi zmdi-file-plus"></i> Generar
                        </a>

                        <a href="{{ route('system.events.tickets.order_report', [session('system_slug'), $event->id]) }}" target="_blank" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                            <i class="zmdi zmdi-download"></i> Reporte
                        </a>

                        <a href="{{ route('system.events.tickets.generateForEvent', [session('system_slug'), $event->id]) }}" target="_blank" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                            <i class="zmdi zmdi-accounts-list"></i> Imprimir QRs
                        </a>
                    @endif
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 ">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'tickets'])
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body" style="padding: 10px;">
                    {!! Form::open(array('url' => route('system.events.tickets.search', [session('system_slug'), $event->id]), 'method' => 'POST')) !!}
                        <div class="search__inner">
                            <input name="search" type="text" class="search__text" placeholder="Buscar por nombre, correo, id de orden...">
                            <i class="zmdi zmdi-search search__helper" data-ma-action="search-close"></i>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>



        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Ordenes y boletos registrados</h4>
                    <h6 class="card-subtitle">Listado de ordenes registradas</h6>

                    @if($event->regular_orders->count() > 0)
                        <livewire:events.orders.orders-list :event="$event" />
                    @else
                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                            NO HAY ORDENES REGISTRADAS
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
    @livewireScripts
@endsection

