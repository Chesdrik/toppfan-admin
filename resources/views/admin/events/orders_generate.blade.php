@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->team->name }} - {{ $event->name }} | Boletos</h1>

            </header>
        </div>
    </div>

    <div class="row p-t-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'tickets'])
        </div>
    </div>

    <livewire:events.ticket-generator.ticket-generator :eventId="$event->id" :courtesy="false" />

@endsection

@section('after_includes')
    @livewireScripts
@endsection

