@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->team->name }} - {{ $event->name }} | Boletos</h1>

                <div class="actions actions--calendar">
                    @if(in_array(Auth::user()->type , ['root', 'admin', 'courtesies']))
                        <a href="{{ route('system.events.tickets.order_report', [session('system_slug'), $event->id]) }}" target="_blank" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                            <i class="zmdi zmdi-download"></i> Reporte
                        </a>

                        <a href="{{ route('system.events.tickets.generateForEvent', [session('system_slug'), $event->id]) }}" target="_blank" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                            <i class="zmdi zmdi-accounts-list"></i> Imprimir QRs
                        </a>
                    @endif
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 ">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'tickets'])
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        Búsqueda: <small>{{ $search }}</small>
                    </h4>

                    {!! Form::open(array('url' => route('system.events.tickets.search', [session('system_slug'), $event->id]), 'method' => 'POST')) !!}
                        <div class="search__inner">
                            <input name="search" value="{{ $search }}" type="text" class="search__text" placeholder="Buscar por nombre, correo, id de orden...">
                            <i class="zmdi zmdi-search search__helper" data-ma-action="search-close"></i>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    @foreach($search_results as $results)
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ $results['title'] }}</h4>
                        <h6 class="card-subtitle">Resultados</h6>

                        @if(count($results['results']) > 0)
                            <table class="table table-bordered table-sm table-hover">
                                <thead class="thead-default">
                                    <tr>
                                        <th class="col-sm-1">&nbsp;</th>
                                        <th class="col-sm-1">ID</th>
                                        <th class="col-sm-2">Usuario</th>
                                        <th class="col-sm-2">Pago</th>
                                        <th class="col-sm-2">Fecha</th>
                                        <th class="col-sm-4">Boletos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($results['results'] as $order)
                                        <tr>
                                            <td>
                                                <div class="btn-group">
                                                    <a href="{{ route('system.orders.show', [session('system_slug'), $order->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    <a href="{{ route('system.events.order_qrs', [session('system_slug'), $event->id, $order->id]) }}" target="_new" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                                        <i class="fas fa-qrcode"></i>
                                                    </a>
                                                </div>
                                            </td>
                                            <td>{{ $order->id }}</td>
                                            <td>{{ $order->client_user->full_name() }}</td>
                                            <td>{{ $order->pretty_payment_method() }}</td>
                                            <td>{{ pretty_date($order->created_at) }}</td>
                                            <td>
                                                <table class="table">
                                                    <tbody>
                                                        @foreach($order->tickets as $ticket)
                                                            <tr>
                                                                <td class="text-right">{{ $ticket->amount }} x{{ $ticket->ticket_type->name }}</td>
                                                                <td class="text-right">{{ pretty_money($ticket->amount*$ticket->price) }}</td>
                                                            </tr>
                                                        @endforeach
                                                        <tr>
                                                            <td class="text-right">Total:</td>
                                                            <td class="text-right">
                                                                <strong>{{ pretty_money($order->total) }}</strong>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection

@section('after_includes')
    @livewireScripts
@endsection

