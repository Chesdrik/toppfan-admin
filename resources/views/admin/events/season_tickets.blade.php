@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->name}} | Abonos</h1>

                <div class="actions actions--calendar">
                    @if(in_array(Auth::user()->type , ['root', 'admin', 'courtesies']))
                        @if (count($event->event_groups->first()->accreditations->where('type', 'season_ticket')) > 0)
                            <a href="{{ route('system.events.season.tickets.generate', [session('system_slug'), $event->id]) }}" class="btn btn-outline-secondary btn-light1 btn--raised btn--icon-text">
                                <i class="zmdi zmdi-accounts-list"></i> Generar abonos
                            </a>
                        @endif
                    @endif
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'season_tickets'])
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Abonos generados</h4>
                    <h6 class="card-subtitle">Listado de abonos generados para este evento

                    @if($event->assigned_accreditations()->where('type', 'season_ticket')->count() > 0)
                        <livewire:events.courtesies.courtesy-list :event="$event" />
                    @else
                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                            NO HAY ABONOS GENERADOS
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
    @livewireScripts
@endsection
