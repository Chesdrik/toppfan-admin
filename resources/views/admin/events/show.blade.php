@extends('layouts.app')

@section('content')
    @if(!$event->finished_event() && $event->on_sale)
        <div class="row p-t-25">
            <div class="col-sm-12">
                <div class="alert alert-warning" role="alert">
                    El evento se encuentra a la venta en el portal web
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <header class="content__title">
                <h1>{{ $event->name}}</h1>

                <div class="actions actions--calendar">
                    <div class="row">
                        @if (!$event->finished_event())
                            <a href="{{ route('system.events.tickets.distribution', [session('system_slug'), $event->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                <i class="zmdi zmdi-mail-send zmdi-hc-fw"></i> Distribución de boletos
                            </a>&nbsp;&nbsp;
                        @endif

                        <a href="{{ route('system.events.status', [session('system_slug'), $event->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                            @if($event->active == 1)
                                <i class="zmdi zmdi-archive"></i> Desactivar
                            @else
                                <i class="zmdi zmdi-shield-check"></i> Activar
                            @endif
                        </a>&nbsp;
                        <a href="{{ route('system.events.edit', [session('system_slug'), $event->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                            <i class="zmdi zmdi-edit"></i> Editar
                        </a>
                    </div>
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'general'])
        </div>
    </div>

    <div class="row price-table price-table--basic">
        <div class="col-md-12">
            <header class="content__title">
                <h1>Evento</h1>
            </header>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="price-table__item">
                        <header class="price-table__header bg-light-blue">
                            <div class="price-table__title">Evento</div>
                        </header>
                        <div class="price-table__price color-light-blue stat_data">
                            <div class="row">
                                <div class="col-sm-12">
                                    <img src="{{ asset($event->img_path()) }}" class="img-fluid event_image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="price-table__item">
                        <div class="price-table__price color-light-blue stat_data event_image_slideshow_card_container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <img src="{{ asset($event->img_slideshow_path()) }}" class="img-fluid event_image_slideshow" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="price-table__item">
                        <header class="price-table__header bg-light-blue">
                            <div class="price-table__title">Sede / fecha</div>
                        </header>
                        <div class="price-table__price color-light-blue stat_data">
                            {{ $event->venue->name }}<br />
                            <small>{!! $event->datespan() !!}</small>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="price-table__item">
                        <header class="price-table__header bg-light-blue">
                            <div class="price-table__title">Monto</div>
                        </header>
                        <div class="price-table__price color-light-blue stat_data">
                            $0.00
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(isset($event_statistics['general_info']))
            <div class="col-md-12">
                <header class="content__title">
                    <h1>Boletos</h1>
                </header>
            </div>

            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Total</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('total_tickets', $event_statistics['general_info']) ? pretty_number($event_statistics['general_info']['total_tickets'], 0) : 'no disponible' }}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">leidos / lecturas</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('total_tickets_read', $event_statistics['general_info']) ? pretty_number($event_statistics['general_info']['total_tickets_read'], 0) : 'no disponible' }} / {{ array_key_exists('total_readings', $event_statistics['general_info']) ? pretty_number($event_statistics['general_info']['total_readings'], 0) : 'no disponible' }}
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Doble lectura</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('total_errors', $event_statistics['general_info']) ? pretty_number($event_statistics['general_info']['total_errors'], 0) : 'no disponible' }}
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Primera lectura</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('first_ticket_reading', $event_statistics['general_info']) ? $event_statistics['general_info']['first_ticket_reading'] : 'no disponible' }}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Última lectura</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('last_ticket_reading', $event_statistics['general_info']) ? $event_statistics['general_info']['last_ticket_reading'] : 'no disponible' }}
                    </div>
                </div>
            </div>
        @endif


        @if(isset($event_statistics['general_info_accreditations']))
            <div class="col-md-12">
                <hr>
                <header class="content__title">
                    <h1>Acreditaciones</h1>
                </header>
            </div>

            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Total</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('total_tickets', $event_statistics['general_info_accreditations']) ? pretty_number($event_statistics['general_info_accreditations']['total_tickets'], 0) : 'no disponible' }}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">leidas / lecturas</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('total_tickets_read', $event_statistics['general_info_accreditations']) ? pretty_number($event_statistics['general_info_accreditations']['total_tickets_read'], 0) : 'no disponible' }} / {{ array_key_exists('total_readings', $event_statistics['general_info_accreditations']) ? pretty_number($event_statistics['general_info_accreditations']['total_readings'], 0) : 'no disponible' }}
                    </div>
                </div>
            </div>


            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Doble lectura</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('total_errors', $event_statistics['general_info_accreditations']) ? pretty_number($event_statistics['general_info_accreditations']['total_errors'], 0) : 'no disponible' }}
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Primera lectura</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('first_ticket_reading', $event_statistics['general_info_accreditations']) ? $event_statistics['general_info_accreditations']['first_ticket_reading'] : 'no disponible' }}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Última lectura</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ array_key_exists('last_ticket_reading', $event_statistics['general_info_accreditations']) ? $event_statistics['general_info_accreditations']['last_ticket_reading'] : 'no disponible' }}
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <hr>
            </div>
        @endif
    </div>

@endsection
