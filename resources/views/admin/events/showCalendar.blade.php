@extends('layouts.app')

@section('content')
    <div class="row">

        @if(session('success'))
        <div class="col-sm-6 offset-sm-3">
                <div class="col-sm-12">
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif

        <div class="col-sm-12">
            <header class="content__title">
                <h1>Eventos de {{ $title }}</h1>

                <div class="actions actions--calendar">
                    <a href="{{ route('system.events.create', session('system_slug')) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                        <i class="zmdi zmdi-plus"></i> Registrar evento
                    </a>
                </div>
            </header>
        </div>

        <div class="col-sm-3">
            @if(count($upcoming_events) > 0)
                <div class="col-sm-12 text-center m-b-15">
                    PRÓXIMOS EVENTOS
                </div>
                @foreach($upcoming_events as $i => $event)
                    <div class="row p-b-25 @if($i == 0) card calendar-card @endif">
                        <div class="col-sm-12">
                            <a href="{{ route('system.events.show', [session('system_slug'), $event]) }}" id="btn-evento-{{ $event->id }}" class="btn btn-puma-off btn-block text-left btn--icon-text">
                                <i class="zmdi zmdi-trending-up"></i> {{ $event->name }}
                            </a>
                        </div>
                        <div class="col-sm-12">
                            {{ pretty_date($event->date) }}
                        </div>
                    </div>
                @endforeach
                <hr />
            @else
                <div class="col-sm-12 text-center" style="opacity: 0.5">
                    <br />
                    NO HAY PRÓXIMOS EVENTOS
                    <hr />
                </div>
            @endif


            @if(count($past_events) > 0)
                <div class="col-sm-12 text-center m-b-15" style="opacity: 0.5">
                    EVENTOS PASADOS
                </div>
                @foreach($past_events as $event)
                    <div class="row p-b-25">
                        <div class="col-sm-12">
                            <a href="{{ route('system.events.show', [session('system_slug'), $event]) }}" id="btn-evento-{{ $event->id }}" class="btn btn-puma-off btn-block text-left btn--icon-text">
                                <i class="zmdi zmdi-trending-up"></i> {{ $event->name }}
                            </a>
                        </div>
                        <div class="col-sm-12">
                            {{ pretty_date($event->date) }}
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-sm-12 text-center" style="opacity: 0.5">
                    <br />
                    NO HAY EVENTOS PASADOS
                </div>
            @endif
        </div>
        <div class="col-sm-9 card">
            <div class="row">
                <div class="col-sm-12">
                    <div class="calendar"></div>
                </div>

                <div class="col-sm-12 text-right">
                    <br /><br />
                    <div class="container m-b-20">
                        <a href="" class="actions__item actions__calender-prev">
                            <button class="btn btn-light btn--icon-text">
                                <i class="zmdi zmdi-long-arrow-left"></i>
                            </button>
                        </a>

                        <a href="" class="actions__item actions__calender-next">
                            <button class="btn btn-light btn--icon-text">
                                <i class="zmdi zmdi-long-arrow-right"></i>
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @foreach($upcoming_events as $event)
        <!-- Add new event -->
        <div class="modal fade" id="event-{{ $event->id }}" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            Pumas - {{ $event->name }}
                            <br />
                            <small>{{ $event->datespan() }}</small>
                        </h5>
                    </div>
                    <div class="modal-body">
                        <a href="{{ url('/boletos/'.$event->id.'/comprar') }}" target="_self">
                            <button class="btn btn-outline-secondary btn-lg btn-block waves-effect">Comprar boleto</button>
                        </a>
                        <br />

                        <a href="{{ url('/admin/eventos/'.$event->id.'/cortesias') }}" target="_self">
                            <button class="btn btn-outline-secondary btn-lg btn-block waves-effect">Registrar cortesías</button>
                        </a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection



@section('after_includes')
<!-- Calendar Script -->
<script type="text/javascript">
    'use strict';
    $(document).ready(function() {
        var date = new Date();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('.calendar').fullCalendar({
            locale: 'es',
            header: false,
            buttonIcons: {
                prev: 'calendar__prev',
                next: 'calendar__next'
            },
            theme: false,
            selectable: true,
            selectHelper: true,
            editable: true,
            events: [
                @foreach($upcoming_events as $event)
                {
                    id: {{ $event->id }},
                    // title: '{{ $event->name }} - {{ pretty_short_hour($event->date) }}',
                    title: '{{ $event->name }}',
                    start: '{{ $event->date_start() }}',
                    end: '{{ $event->date_end() }}',
                    allDay: false,
                    className: 'bg-blue',
                    description: '{{ $event->name }} - {{ pretty_short_hour($event->date) }}',
                },
                @endforeach
            ],

            viewRender: function (view) {
                var calendarDate = $('.calendar').fullCalendar('getDate');
                var calendarMonth = calendarDate.month();

                //Set data attribute for header. This is used to switch header images using css
                $('.calendar .fc-toolbar').attr('data-calendar-month', calendarMonth);

                //Set title in page header
                $('.content__title--calendar > h1').html(view.title);
            },

            eventClick: function (event, element) {
                var url = $("#btn-evento-"+event.id).attr('href');
                window.location = url;
            }
        });

        //Calendar views switch
        $('body').on('click', '[data-calendar-view]', function(e){
            e.preventDefault();

            $('[data-calendar-view]').removeClass('actions__item--active');
            $(this).addClass('actions__item--active');
            var calendarView = $(this).attr('data-calendar-view');
            $('.calendar').fullCalendar('changeView', calendarView);
        });


        //Calendar Next
        $('body').on('click', '.actions__calender-next', function (e) {
            e.preventDefault();
            $('.calendar').fullCalendar('next');
        });


        //Calendar Prev
        $('body').on('click', '.actions__calender-prev', function (e) {
            e.preventDefault();
            $('.calendar').fullCalendar('prev');
        });
    });
</script>


@endsection
