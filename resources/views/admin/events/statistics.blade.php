@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->name}} | Estadísticas</h1>

                <div class="actions actions--calendar">
                    <div class="row">
                        {{-- <a href="{{ route('system.events.statistics.process', [session('system_slug'), $event->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                            <i class="zmdi zmdi-chart"></i> Procesar
                        </a>&nbsp; --}}
                        {!! Form::open(array('url' => route('system.events.statistics.pdf', [session('system_slug'), $event->id]) , 'method' => 'post', 'enctype' => "multipart/form-data")) !!}
                            <input type="hidden" name="base64_sales" id="base64_sales"/>
                            <input type="hidden" name="base64_readings" id="base64_readings"/>
                            <input type="hidden" name="base64_exits" id="base64_exits"/>
                            <input type="hidden" name="base64_device" id="base64_device"/>
                            <input type="hidden" name="base64_devicesPie" id="base64_devicesPie"/>
                            <input type="hidden" name="base64_checkpoint_alerts" id="base64_checkpoint_alerts"/>
                            <input type="hidden" name="base64_device_alertsPieChart" id="base64_device_alertsPieChart"/>
                            <input type="hidden" name="base64_checkpointChart" id="base64_checkpointChart"/>
                            <input type="hidden" name="base64_incidences" id="base64_incidences"/>

                            <button type="submit" id="sBtn" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                <i class="zmdi zmdi-file zmdi-hc-fw"></i>&nbsp; Estadísticas PDF
                            </button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'stats'])
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <div class="row price-table price-table--basic">
        @include('admin.events.statistics.sales_table')
        @include('admin.events.statistics.sales')
        @include('admin.events.statistics.readings.readings')
        @include('admin.events.statistics.readings.exits')
        @include('admin.events.statistics.checkpoints.checkpoint_alerts')
        @include('admin.events.statistics.devices.device_alerts')
        @include('admin.events.statistics.checkpoints.checkpoint_data')
        @include('admin.events.statistics.incidences')
        @include('admin.events.statistics.orders.payment_methods')
        @include('admin.events.statistics.orders.status')
        @include('admin.events.statistics.orders.selling_points')
    </div>

@endsection


