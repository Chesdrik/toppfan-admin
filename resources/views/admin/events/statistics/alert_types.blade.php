@if(isset($event_statistics['alerts']) && $event_statistics['alerts'] != [])
    <div class="col-sm-6 text-center">
        <h3>Tipos de alertas</h3>
        <canvas id="alertsChart" style="max-height: 350px;"></canvas>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            var errorctx = document.getElementById('typealert').getContext('2d');
            var readingsChart = new Chart(errorctx, {
                type: 'bar',
                data: {
                    labels: [{!! $event_statistics['alerts']['alerts']['label'] !!}],
                    datasets: [{
                        label: 'Tipos de alertar',
                        data: [{!! $event_statistics['alerts']['alerts']['data'] !!}],
                        backgroundColor: [{!! $event_statistics['alerts']['colors'] !!}],
                        borderColor: [{!! $event_statistics['alerts']['colors'] !!}],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    }
                }
            });
        });
    </script>
@endif
