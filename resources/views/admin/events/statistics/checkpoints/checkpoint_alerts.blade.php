@if(isset($event_statistics['checkpoint_alerts']) && $event_statistics['checkpoint_alerts'] != [])
    <div class="col-sm-8 text-center">
        <h3>Incidencias por punto de acceso</h3>
        <canvas id="checkpoint_alertsChart" width="auto" height="100px"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">
        var readingctx = document.getElementById('checkpoint_alertsChart').getContext('2d');
        var readingsChart = new Chart(readingctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['checkpoint_alerts']['labels']['label'] !!}],
                    datasets:
                        {!! parser_device_stats($event_statistics['checkpoint_alerts']) !!}
            },
            options: {
                tooltips: {
                    displayColors: true,
                    callbacks:{
                        mode: 'x',
                    },
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                        display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                        beginAtZero: true,
                        },
                        type: 'linear',
                    }]
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Número de incidencias registradas durante el evento agrupadas por punto de acceso'
                }
            }
        });

        sBtn.addEventListener("click", function() {
            var checkpoint_alerts = document.getElementById("checkpoint_alertsChart");
            var img_checkpoint_alerts = checkpoint_alerts.toDataURL("image/png");
            document.getElementById('base64_checkpoint_alerts').value = img_checkpoint_alerts;
        });
    </script>
@endif
