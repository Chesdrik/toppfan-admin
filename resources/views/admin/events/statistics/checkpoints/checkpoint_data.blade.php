@if(isset($event_statistics['checkpoint_data']) && $event_statistics['checkpoint_data'] != [])
    <div class="col-sm-6 text-center">
        <h3>Lectores</h3>
        <canvas id="checkpointChart" width="auto" height="200"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">
            var checkctx = document.getElementById('checkpointChart').getContext('2d');
            var readingsChart = new Chart(checkctx, {
                type: 'bar',
                data: {
                    labels: [{!! $event_statistics['checkpoint_data']['labels']['label'] !!}],
                    datasets: [{
                        label: 'Batería inicial(%)',
                        data: [{!! $event_statistics['checkpoint_data']['initial_data']['string'] !!}],
                        backgroundColor: [{!! $event_statistics['checkpoint_data']['initial_color'] !!}],
                        borderColor: [{!! $event_statistics['checkpoint_data']['initial_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Batería final (%)',
                        data: [{!! $event_statistics['checkpoint_data']['final_data']['string'] !!}],
                        backgroundColor: [{!! $event_statistics['checkpoint_data']['final_color'] !!}],
                        borderColor: [{!! $event_statistics['checkpoint_data']['final_color'] !!}],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                            }
                        }]
                    }
                }
            });

            sBtn.addEventListener("click", function() {
                var checkpoint = document.getElementById("checkpointChart");
                var img_checkpoint = checkpoint.toDataURL("image/png");
                document.getElementById('base64_checkpointChart').value = img_checkpoint;
            });
    </script>
@endif
