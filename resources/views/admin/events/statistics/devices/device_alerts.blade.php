@if(isset($event_statistics['device_alerts']) && $event_statistics['device_alerts'] != [])
    <div class="col-sm-4 text-center">
        <h3>Incidencias por dispositivo</h3>
        <canvas id="device_alertsPieChart" width="auto" height="200"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">
        var readingctx = document.getElementById('device_alertsPieChart').getContext('2d');
        var readingsChart = new Chart(readingctx, {
            type: 'doughnut',
            data: {
                labels: [{!! $event_statistics['device_alerts']['labels'] !!}],
                datasets: [
                    {
                    label: "Population (millions)",
                    backgroundColor: [ {!! $event_statistics['device_alerts']['colors'] !!}],
                    data: [{!! $event_statistics['device_alerts']['total'] !!}]
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Número de incidencias registradas por dispositivo'
                }
            }
        });

        sBtn.addEventListener("click", function() {
            var device_alertsPie = document.getElementById("device_alertsPieChart");
            var img_device_alertsPie = device_alertsPie.toDataURL("image/png");
            document.getElementById('base64_device_alertsPieChart').value = img_device_alertsPie;
        });
    </script>
@endif
