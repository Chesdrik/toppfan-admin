<h3>Boletos  </h3>
<canvas id="ticket_statusPieChart" width="auto" height="200"></canvas>

<script type="text/javascript">
    window.addEventListener('zoneData', event => {
        // Destroying previous chart
        if(window.eventTicketsChart !== undefined){
            window.eventTicketsChart.destroy();
        }

        // Creating new chart
        var status = event.detail.event_data['labels'];
        var colors = event.detail.event_data['colors'];
        var data2 = event.detail.event_data['data'];

        var readingctx = document.getElementById('ticket_statusPieChart').getContext('2d');
        window.eventTicketsChart = new Chart(readingctx, {
            type: 'doughnut',
            data: {
                labels: status,
                datasets: [
                {
                    label: status,
                    backgroundColor: colors,
                    data: data2
                }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Distribución de boletos vendidos, apartados y disponibles del evento'
                },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var value = data.datasets[0].data[tooltipItem.index];
                            value = value.toString();
                            value = value.split(/(?=(?:...)*$)/);
                            value = value.join(',');
                            value = data.datasets[0].label[tooltipItem.index]+': ' + value;
                            return value;
                        }
                    },
                }
            }
        });
    })
</script>
