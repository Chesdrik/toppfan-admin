@if(isset($data['data']['general']) && $data['data']['general'] != [])
    <div class="col-sm-12 text-center" style="margin-top:5%">
        <h3>General</h3>
        <canvas id="generalChart" width="auto" height="50"></canvas>
    </div>

    <script type="text/javascript">
        var salesctx = document.getElementById('generalChart').getContext('2d');
        var readingsChart = new Chart(salesctx, {
            type: 'doughnut',
            data: {
                labels: [{!! data_parser($data['data']['general'])['labels'] !!}],
                datasets: [
                {
                    label: [{!! data_parser($data['data']['general'])['labels'] !!}],
                    backgroundColor: [{!! data_parser($data['data']['general'])['colors'] !!}],
                    data: [{!! data_parser($data['data']['general'])['data'] !!}]
                }
                ]
            },
            options: {
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var value = data.datasets[0].data[tooltipItem.index];
                            value = value.toString();
                            value = value.split(/(?=(?:...)*$)/);
                            value = value.join(',');
                            value = data.datasets[0].label[tooltipItem.index]+': ' + value;
                            return value;
                        }
                    },
                }
            }
        });
        
    </script>
@endif
