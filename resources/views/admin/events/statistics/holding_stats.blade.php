@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->name}} | Generales</h1>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'holding'])
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <div class="row price-table price-table--basic">
        @include('admin.events.statistics.sales')
        @include('layouts.includes.hr_separator')

        @if ($data['data'])
            <div class="col-sm-12 text-center">
                <h4>Distribución de boletos vendidos, apartados y disponibles del evento</h4>
            </div>
            @include('admin.events.statistics.general')
            @include('layouts.includes.hr_separator')
       
            <div class="col-sm-12 text-center">
                <h4>Distribución de boletos vendidos, apartados y disponibles por zona</h4>
            </div>
            @foreach ($data['data']['zones'] as $id => $zone)
                @include('admin.events.statistics.zones')
            @endforeach
        @else
            <div class="col-sm-12 text-center" style="opacity: 0.5">
                NO HAY INFORMACIÓN DE ZONAS PARA MOSTRAR
            </div>
        @endif

    </div>
@endsection


