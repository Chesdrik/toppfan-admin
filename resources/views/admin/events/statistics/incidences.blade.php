@if(isset($event_statistics['incidences']) && $event_statistics['incidences'] != [])
    <div class="col-sm-12 text-center">
        <h3>Incidencias</h3>
        <canvas id="incidencesChart"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">
        var readingctx = document.getElementById('incidencesChart').getContext('2d');
        var incidencesChart = new Chart(readingctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['incidences']['labels']['label'] !!}],
                datasets: [
                    {
                        label: 'Acceso máximo (T)',
                        data: [{!! $event_statistics['incidences']['max_access']['data']['ticket_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['max_access']['data']['t_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['max_access']['data']['t_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Acceso máximo (A)',
                        data: [{!! $event_statistics['incidences']['max_access']['data']['accreditation_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['max_access']['data']['a_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['max_access']['data']['a_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Salida máxima (T)',
                        data: [{!! $event_statistics['incidences']['max_exit']['data']['ticket_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['max_exit']['data']['t_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['max_exit']['data']['t_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Salida máxima (A)',
                        data: [{!! $event_statistics['incidences']['max_exit']['data']['accreditation_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['max_exit']['data']['a_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['max_exit']['data']['a_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Checkpoint erróneo (T)',
                        data: [{!! $event_statistics['incidences']['checkpoint']['data']['ticket_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['checkpoint']['data']['t_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['checkpoint']['data']['t_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Checkpoint erróneo (A)',
                        data: [{!! $event_statistics['incidences']['checkpoint']['data']['accreditation_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['checkpoint']['data']['a_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['checkpoint']['data']['a_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'QR inválido',
                        data: [{!! $event_statistics['incidences']['qr']['data']['string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['qr']['colors'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['qr']['colors'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Versión inválida',
                        data: [{!! $event_statistics['incidences']['version']['data']['accreditation_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['version']['data']['t_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['version']['data']['t_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Acreditación inválida',
                        data: [{!! $event_statistics['incidences']['accreditation']['data']['accreditation_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['accreditation']['data']['t_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['accreditation']['data']['t_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Ticket inválido',
                        data: [{!! $event_statistics['incidences']['ticket']['data']['ticket_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['ticket']['data']['t_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['ticket']['data']['t_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Evento erróneo (T)',
                        data: [{!! $event_statistics['incidences']['event']['data']['ticket_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['event']['data']['t_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['event']['data']['t_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Evento erróneo (A)',
                        data: [{!! $event_statistics['incidences']['event']['data']['accreditation_string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['event']['data']['a_color'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['event']['data']['a_color'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Alertas',
                        data: [{!! $event_statistics['incidences']['alerts']['data']['string'] !!}],
                        backgroundColor: [{!! $event_statistics['incidences']['alerts']['colors'] !!}],
                        borderColor: [{!! $event_statistics['incidences']['alerts']['colors'] !!}],
                        borderWidth: 1
                    },
                ]
            },
            options: {
                tooltips: {
                    displayColors: true,
                    callbacks:{
                        mode: 'x',
                    },
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                        display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                        beginAtZero: true,
                        },
                        type: 'linear',
                    }]
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Incidencias registradas durante el evento'
                }
            }
        });

        sBtn.addEventListener("click", function() {
            var incidences = document.getElementById("incidencesChart");
            var img_incidences = incidences.toDataURL("image/png");
            document.getElementById('base64_incidences').value = img_incidences;
        });
    </script>
@endif
