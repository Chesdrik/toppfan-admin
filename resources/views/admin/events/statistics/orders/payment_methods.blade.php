@if(isset($event_statistics['order_event']) && $event_statistics['order_event'] != [])
    <div class="col-sm-6 text-center">
        <h3>Órdenes por método de pago</h3>
        <canvas id="order_eventChart" style="max-height: 350px;"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">

        var errorctx = document.getElementById('order_eventChart');
        var readingsChart = new Chart(errorctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['order_event']['payment_methods']['labels']!!}],
                datasets: [
                    {
                        label: "Órdenes por método de pago",
                        backgroundColor: [{!! $event_statistics['order_event']['payment_methods']['colors'] !!}],
                        data: @json(array_values($event_statistics['order_event']['payment_methods']['string']))
                    }
                ]
            },
            options: {
                legend: { display: false },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
                title: {
                    display: true,
                    text: 'Órdenes registradas agrupadas por método de pago.'
                }
            }
        });
    </script>
@endif
