@if(isset($event_statistics['order_event']) && $event_statistics['order_event'] != [])
    <div class="col-sm-6 text-center">
        <h3>Órdenes por punto de venta</h3>
        <canvas id="selling_pointsChart" style="max-height: 350px;"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">
        var errorctx = document.getElementById('selling_pointsChart').getContext('2d');
        var readingsChart = new Chart(errorctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['order_event']['selling_points']['labels']!!}],
                datasets: [
                    {
                    label: "Órdenes por método de pago",
                    backgroundColor: [{!! $event_statistics['order_event']['selling_points']['colors'] !!}],
                    data: @json(array_values($event_statistics['order_event']['selling_points']['string']))
                }
                ]
            },
            options: {
                legend: { display: false },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
                title: {
                    display: true,
                    text: 'Órdenes registradas agrupadas por punto de venta.'
                }
            }
        });
    </script>
@endif
