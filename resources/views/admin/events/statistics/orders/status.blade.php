@if(isset($event_statistics['order_event']) && $event_statistics['order_event'] != [])
    <div class="col-sm-6 text-center">
        <h3>Órdenes por status</h3>
        <canvas id="statusChart" style="max-height: 350px;"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">
        var errorctx = document.getElementById('statusChart').getContext('2d');
        var readingsChart = new Chart(errorctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['order_event']['status']['labels']!!}],
                datasets: [
                    {
                        label: "Órdenes por método de pago",
                        backgroundColor: [{!! $event_statistics['order_event']['status']['colors'] !!}],
                        data: @json(array_values($event_statistics['order_event']['status']['string']))
                    }
                ]
            },
            options: {
                legend: { display: false },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                },
                title: {
                    display: true,
                    text: 'Órdenes agrupadas por estatus.'
                }
            }
        });
    </script>
@endif
