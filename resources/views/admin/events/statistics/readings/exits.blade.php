@if(isset($event_statistics['exits']) && $event_statistics['exits'] != [])
    <div class="col-sm-6 text-center">
        <h3>Salidas</h3>
        <canvas id="exitsChart" style="max-height: 400px;"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">
        var errorctx = document.getElementById('exitsChart').getContext('2d');
        var readingsChart = new Chart(errorctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['exits']['labels']['label'] !!}],
                datasets: [
                    {
                        label: 'Boletos',
                        data: [{!! $event_statistics['exits']['data']['string'] !!}],
                        backgroundColor: [{!! $event_statistics['exits']['colors'] !!}],
                        borderColor: [{!! $event_statistics['exits']['colors'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Acreditaciones',
                        data: [{!! $event_statistics['exits_accreditations']['data']['string'] !!}],
                        backgroundColor: [{!! $event_statistics['exits_accreditations']['colors'] !!}],
                        borderColor: [{!! $event_statistics['exits_accreditations']['colors'] !!}],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                tooltips: {
                    displayColors: true,
                    callbacks:{
                        mode: 'x',
                    },
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                        display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                        beginAtZero: true,
                        },
                        type: 'linear',
                    }]
                },
                responsive: true,
            }
        });

        sBtn.addEventListener("click", function() {
            var exits = document.getElementById("exitsChart");
            var img_exits = exits.toDataURL("image/png");
            document.getElementById('base64_exits').value = img_exits;
            });
    </script>
@endif
