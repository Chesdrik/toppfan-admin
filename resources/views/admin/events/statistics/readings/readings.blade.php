@if(isset($event_statistics['readings']) && $event_statistics['readings'] != [])
    <div class="col-sm-12 text-center">
        <h3>Accesos</h3>
        <canvas id="readingsChart"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    <script type="text/javascript">
        var readingctx = document.getElementById('readingsChart').getContext('2d');
        var readingsChart = new Chart(readingctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['readings']['labels']['label'] !!}],
                datasets: [
                    {
                        label: 'Boletos',
                        data: [{!! $event_statistics['readings']['data']['string'] !!}],
                        backgroundColor: [{!! $event_statistics['readings']['colors'] !!}],
                        borderColor: [{!! $event_statistics['readings']['colors'] !!}],
                        borderWidth: 1
                    },
                    {
                        label: 'Acreditaciones',
                        data: [{!! $event_statistics['readings_accreditations']['data']['string'] !!}],
                        backgroundColor: [{!! $event_statistics['readings_accreditations']['colors'] !!}],
                        borderColor: [{!! $event_statistics['readings_accreditations']['colors'] !!}],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                tooltips: {
                    displayColors: true,
                    callbacks:{
                        mode: 'x',
                    },
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                        display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                        beginAtZero: true,
                        },
                        type: 'linear',
                    }]
                },
                responsive: true,
            }
        });

        sBtn.addEventListener("click", function() {
            var readings = document.getElementById("readingsChart");
            var img_readings = readings.toDataURL("image/png");
            document.getElementById('base64_readings').value = img_readings;
            });
    </script>
@endif
