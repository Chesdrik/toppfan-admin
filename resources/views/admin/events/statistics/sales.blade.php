@if(isset($event_statistics['sales']) && $event_statistics['sales'] != [])
    <div class="col-sm-12 text-center">
        <h3>Ventas</h3>
        <canvas id="salesChart" width="auto" height="50"></canvas>
    </div>
    @include('layouts.includes.hr_separator')

    @section('after_includes')
    <script type="text/javascript">
        var salesctx = document.getElementById('salesChart').getContext('2d');
        var readingsChart = new Chart(salesctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['sales']['labels']['label'] !!}],
                datasets: [{
                    label: 'Ventas',
                    data: [{!! $event_statistics['sales']['data']['string'] !!}],
                    backgroundColor: [{!! $event_statistics['sales']['colors'] !!}],
                    borderColor: [{!! $event_statistics['sales']['colors'] !!}],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                }
            }
        });
    </script>
    @endsection
@endif
