@if(isset($sales) && $sales != [])
    <div class="col-sm-12 text-center">
        <h3>Ventas</h3>

        <div class="card">
            <div class="card-body">
                {{-- <h4 class="card-title">Basic example</h4> --}}
                {{-- <h6 class="card-subtitle">DataTables is a plug-in for the jQuery Javascript library. It is a highly flexible tool, based upon the foundations of progressive enhancement, and will add advanced interaction controls to any HTML table.</h6> --}}

                <div class="table-responsive">
                    <table id="data-table" class="table table-bordered  table-striped table-bordered table-sm">
                        <thead class="thead-default">
                            <tr>
                                <th>Boleto</th>
                                <th>Tipo</th>
                                {{-- <th class="text-right">Total</th> --}}
                                <th class="text-right">Disponibles</th>
                                <th class="text-right">Apartados</th>
                                <th class="text-right">En venta</th>
                                <th class="text-right">Cortesía</th>
                                <th class="text-right">Vendidos</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $total = 0; ?>
                            @foreach($sales as $sale)
                                @if($sale->sold > 0 || $sale->courtesy > 0)
                                    <tr>
                                        <td>{{ $sale->name }}</td>
                                        <td>{{ ($sale->type == 'general' ? 'General' : "Numerado") }}</td>
                                        {{-- <td class="text-right">{{ $sale->total }}</td> --}}
                                        <td class="text-right">{{ $sale->available }}</td>
                                        <td class="text-right">{{ $sale->on_hold }}</td>
                                        <td class="text-right">{{ $sale->on_hold_sale }}</td>
                                        <td class="text-right">{{ $sale->courtesy }}</td>
                                        <td class="text-right">{{ $sale->sold }}</td>
                                    </tr>
                                    <?php $total += $sale->sold; ?>
                                @endif
                            @endforeach
                            <tr>
                                <td colspan="7" class="text-right"><strong>{{ pretty_number($total, 0) }}</strong></td>
                            </tr>
                        </tbody>
                    <table>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.includes.hr_separator')
@endif
