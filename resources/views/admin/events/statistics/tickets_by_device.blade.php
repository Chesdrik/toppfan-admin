@if(isset($event_statistics['tickets_by_device']) && $event_statistics['tickets_by_device'] != [])
    <div class="col-sm-12 text-center">
        <h3>Lecturas por dispositivo</h3>
        <canvas id="devicesChart" width="auto" height="100"></canvas>
    </div>

    <script type="text/javascript">
        var readingctx = document.getElementById('devicesChart').getContext('2d');
        var readingsChart = new Chart(readingctx, {
            type: 'bar',
            data: {
                labels: [{!! $event_statistics['tickets_by_device']['labels']['label'] !!}],
                    datasets: 
                        {!! parser_device_readings($event_statistics['tickets_by_device'], $event_statistics['accreditations_by_device']) !!}
            },
            options: {
                tooltips: {
                    displayColors: true,
                    callbacks:{
                        mode: 'x',
                    },
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        gridLines: {
                        display: false,
                        }
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                        beginAtZero: true,
                        },
                        type: 'linear',
                    }]
                },
                responsive: true,
                title: {
                    display: true,
                    text: 'Número de lecturas por intervalos registradas durante el evento agrupadas por dispositivo. (T) corresponde a lecturas de tickets, mientras que (A) corresponde a lecturas de acreditaciones.'
                }
            }
        });
        sBtn.addEventListener("click", function() {
            var device = document.getElementById("devicesChart");
            var img_device = device.toDataURL("image/png");
            document.getElementById('base64_device').value = img_device;
            });
    </script>
@endif
