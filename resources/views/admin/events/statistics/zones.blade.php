@if(isset($zone) && $zone != [])
    <div class="col-sm-4 text-center" style="margin-top:5%">
        <h3>{{ \App\TicketType::where('zone_id', $id)->first()->name }}</h3>
        <canvas id="zonesChart-{{ $id }}" width="auto" height="150"></canvas>
    </div>

    <script type="text/javascript">
        var salesctx = document.getElementById('zonesChart-@json($id)').getContext('2d');
        var readingsChart = new Chart(salesctx, {
            type: 'doughnut',
            data: {
                labels: [{!! data_parser($zone)['labels'] !!}],
                datasets: [
                {
                    label: [{!! data_parser($zone)['labels'] !!}],
                    backgroundColor: [{!! data_parser($zone)['colors'] !!}],
                    data: [{!! data_parser($zone)['data'] !!}]
                }
                ]
            },
            options: {
                // title: {
                //     display: true,
                //     text: 'Distribución de boletos vendidos, apartados y disponibles por zona'
                // },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, data) {
                            var value = data.datasets[0].data[tooltipItem.index];
                            value = value.toString();
                            value = value.split(/(?=(?:...)*$)/);
                            value = value.join(',');
                            value = data.datasets[0].label[tooltipItem.index]+': ' + value;
                            return value;
                        }
                    },
                }
            }
        });
        
    </script>
@endif
