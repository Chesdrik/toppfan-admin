@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8">
            <h1 class="px-3 py-3 mb-4">Agrega distribuidores</h1>
        </div>
        <div class="col-md-4">
            <div class="row text-center d-flex justify-content-center">
                <img src="{{ asset($event->img_path()) }}" class="header-logo" style="padding-left: 0px;" /><br>
            </div>

            {{-- <div class="vsBoxAsignar row text-center d-flex justify-content-center">
                <span class="vsAsignarAdmin">vs</span>
                <div class="col-6">
                    <img src="/storage/system/2/2_logo.png" class="header-logo" style="padding-left: 0px;"><br>
                    <span>Pumas</span>
                </div>
                <div class="col-6">
                    <img src="/storage/system/2/2_logo.png" class="header-logo" style="padding-left: 0px;"><br>
                    <span>Pumas</span>
                </div>
            </div> --}}

        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 px-4 m-0 mb-5">
            <div class="card">
                <div class="card-body card-body pt-0 px-0 setFooterFixedWidth">
                    {!! Form::open(array('url' => route('system.events.tickets.distribution.send', [session('system_slug'), $event->id]), 'method' => 'POST', 'enctype' => 'multipart/form-data')) !!}
                       
                    <div class="row py-3 px-0  m-auto shadow topBarAsignacion" style="background-color: #f1f9ff;">
                    <div class="col-md-2 offset-1 strong">Tipo de boleto</div>
                    <div class="col-md-2 offset-2 strong">Cantidad</div>
                    <div class="col-md-2 offset-1 strong">¿Se podrá distribuir?</div>
                    </div><br>
                    
                    <div class="row px-5 pt-3">
                        @foreach($ticket_types as $ticket_type)
                        
                        <div class="col-md-2 offset-1" style="margin-top:-10px;">
                            <h3 class="card-body__title">{{ $ticket_type->name }}</h3>
                            <div class="form-group">
                                <div class="toggle-switch">
                                    <input type="checkbox" name="types[{{ $ticket_type->id }}]" class="toggle-switch__checkbox">
                                    <i class="toggle-switch__helper"></i>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-1"></div>
                        
                        <div class="col-md-3" style="margin-top:20px;">
                            <div class="form-group">
                                <input type="text" class="form-control " placeholder="" value="" name="amount[{{ $ticket_type->id }}]" id="total-{{ $ticket_type->id }}">
                                <i class="form-group__bar"></i>
                                
                            </div>
                        </div>
                        
                        <div class="col-md-1"></div>
                        
                        <div class="col-md-2">
                            <div class="form-group">
                                <label></label>
                                
                                <div class="select">
                                    <select class="form-control" name="distributable[{{ $ticket_type->id }}]">
                                        <option value="0" selected="selected">
                                            No
                                        </option>
                                        <option value="1">
                                            Sí
                                        </option>
                                    </select>
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>                                
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class=" shadow nuevDistribuidorFooterFixed bg-white pt-0 px-3">
            <div class="row py-3 resumenHeader px-5">
                <div class=" col-md-4 h-100 d-flex align-items-center m-auto">
                    <div class="align-self-center">
                        <p>Boletos asignados:</p>
                         <h3>{{ $event->ticket_groups->sum('amount') }}</h3>
                     </div>
                </div>
                <div class=" col-md-4 h-100 d-flex align-items-center">
                    <div class="align-self-center">
                        <p>Distribuidores agregados:</p> 
                        <h3>{{ count($distributors) }}</h3>
                    </div>
                </div>
                <div class=" col-md-4 h-100 text-righ " >
                    <i class="fas fa-th-list" data-toggle="modal" data-target=".bd-example-modal-lg"></i>
                </div>
            </div>

            <div class="row w-100 mt-4 formAsignarFooter px-5 mb-2">
                {!! Form::MDtext('Nombre', 'name', '', $errors, ['class' => 'col-md-3']) !!} 
                {!! Form::MDtext('Apellido', 'last_name', '', $errors, ['class' => 'col-md-3']) !!}
                {!! Form::MDtext('Email', 'email', '', $errors, ['class' => 'col-md-3']) !!}

                <div class="col-md-3 d-flex align-items-center">
                    <button class="btn btn-primary btn-lg rounded" type="submit" id="send">
                        <!-- <i class="zmdi zmdi-card-sd mr-2"></i>  -->
                        <i class="fas fa-file-import mr-2"></i>
                        <span style="font-size:18px"><b>Enviar</b></span>
                    </button>
                </div>
            </div>

            {!! Form::close() !!}
               
          
        </div>
    </div>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header border-bottom pb-3">
                <h2 class="modal-title" id="exampleModalLabel"><i class="fas fa-user-plus mr-4 text-primary"></i>Distribuidores asignados</h2>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-sm-2">&nbsp;</div>
                            <div class="col-sm-3"><b>Nombre:</b></div>
                            <div class="col-sm-3"><b>E-mail:</b></div>
                            <div class="col-sm-4"><b>Boletos asignados:</b></div>
                        </div>
                    </li>
                    @foreach ($distributors as $id => $ticket_groups)
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-sm-2"> 
                                <a href="{{ route('system.events.tickets.distribution.resend', [session('system_slug'), $event->id, $ticket_groups->first()->id]) }}">
                                    <button class="btn btn-primary">
                                        <i class="zmdi zmdi-mail-send zmdi-hc-fw"></i>
                                        <span><b>Reenviar email</b></span>
                                    </button>
                                </a>
                            </div>
                            <div class="col-sm-3">{{ $ticket_groups->first()->client_user->name }} {{ $ticket_groups->first()->client_user->last_name }}</div>
                            <div class="col-sm-3">{{ $ticket_groups->first()->client_user->email }}</div>
                            
                            <div class="col-sm-4 tipoBoletosPorUsuarios">
                                @foreach ($ticket_groups as $ticket_group)
                                <div>
                                    <span>{{ $ticket_group->amount }}</span><span> x
                                        <span>{{ $ticket_group->ticket_type->name }}</span>
                                    </div>
                                    
                                    @endforeach
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('after_includes')
<script>
    var setFooterFixedWidth = $('.setFooterFixedWidth').width();
    $(".nuevDistribuidorFooterFixed").width(setFooterFixedWidth);

    var sticky = $('.topBarAsignacion');
    sticky.width(setFooterFixedWidth);
          

    $(window).resize(function() {
        var setFooterFixedWidth = $('.setFooterFixedWidth').width();
        console.log("width: "+setFooterFixedWidth)
        $(".nuevDistribuidorFooterFixed").width(setFooterFixedWidth);
        sticky.css("width",setFooterFixedWidth);
    });

    $(window).scroll(function(){
        var setFooterFixedWidthX = $('.setFooterFixedWidth').width();
        var sticky = $('.topBarAsignacion'),
            scroll = $(window).scrollTop();

        if (scroll >= 170){
            sticky.addClass('fixedTopBarTable');
            sticky.css("width",setFooterFixedWidthX);
        }else{ 
            sticky.removeClass('fixedTopBarTable');
        }
      
    });
</script>

<style>
    .tipoBoletosPorUsuarios div{
        border: 1px solid #e3e3e3;
        padding: 5px;
        display: inline-block;
    }
    .fixedTopBarTable {
    position: fixed;
    top:72px; 
    width: 100%; 
    z-index: 10;
    }

    .topBarAsignacion{
        font-size: 18px;
        color: #484848 !important;
    }
    .nuevDistribuidorFooterFixed{
        position: fixed !important;
        bottom: 0px;
        z-index: 10;
        display: block; 
    } 
    .resumenHeader{
        background-color: #f1f9ff;
    }
     .resumenHeader p{
        font-size: 16px;
    }
    .formAsignarFooter{
        font-size: 14px;
    }
    .pull-right {
        float: inherit !important;
    }
    .fas.fa-th-list{
        position: relative;
        font-size: 24px;
        color: #2197f3;
        float: right;
        right: 65%;
        top: 20px;
        cursor: pointer;
    }
    @media (min-width: 576px){
        .modal-dialog {
            max-width: 80% !important;
            margin: 1.75rem auto;
        }
    }
    .vsBoxAsignar {
        float: right;
        right: 20px;
        position: relative;
        top: 12px;
    }
    .vsAsignarAdmin {
    position: absolute;
    font-size: 25px;
    /*top: 10px;*/
}
</style>
@endsection








































