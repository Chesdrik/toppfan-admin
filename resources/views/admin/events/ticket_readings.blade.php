@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <header class="content__title">
                <h1>{{ $event->name}} | Lecturas</h1>
            </header>
        </div>
    </div>

    <div class="row p-t-25 p-b-25">
        <div class="col-sm-12">
            @include('admin.events.elements.nav_tabs', ['selected' => 'readings'])
        </div>
    </div>

    @if ($readings)
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Lecturas</h4>
            <h6 class="card-subtitle">Lecturas registradas durante el evento.</h6>

            <div class="tab-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" data-toggle="tab" href="#tickets" role="tab" aria-selected="true">Boletos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link show" data-toggle="tab" href="#accreditations" role="tab" aria-selected="false">Acreditaciones</a>
                    </li>
                </ul>

                <div class="tab-content">

                    <div class="tab-pane fade active show" id="tickets" role="tabpanel">
                        @if (count($readings['tickets']) > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered data-table">
                                <thead class="thead-default">
                                    <tr>
                                        <th>ID</th>
                                        <th>Checkpoint</th>
                                        <th>Tipo</th>
                                        <th>Hora de lectura</th>
                                        <th>Dispositivo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($readings['tickets']  as $ticket)
                                    <tr>
                                        <td>{{ $ticket['ticket_id'] }}</td>
                                        <td>{{ $ticket['checkpoint_id']}}</td>
                                        <td>{{ pretty_reading_type($ticket['type']) }}</td>
                                        <td>{{ $ticket['reading_time'] }}</td>
                                        <td>{{ $ticket['device_id'] }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                                NO HAY LECTURAS DE BOLETOS
                        </div>
                        @endif
                    </div>

                    <div class="tab-pane fade" id="accreditations" role="tabpanel">
                        @if (count($readings['accreditations']) > 0)
                        <div class="table-responsive">
                            <table class="table table-bordered" id="data-table-2">
                                <thead class="thead-default">
                                    <tr>
                                        <th>ID</th>
                                        <th>Checkpoint</th>
                                        <th>Tipo</th>
                                        <th>Hora de lectura</th>
                                        <th>Dispositivo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($readings['accreditations']  as $accreditation)
                                    <tr>
                                        <td>{{ $accreditation['accreditation_event_id'] }}</td>
                                        <td>{{ $accreditation['checkpoint_id']}}</td>
                                        <td>{{ pretty_reading_type($accreditation['type']) }}</td>
                                        <td>{{ $accreditation['reading_time'] }}</td>
                                        <td>{{ $accreditation['device_id'] }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                        <div class="col-sm-12 text-center" style="opacity: 0.5">
                                NO HAY LECTURAS DE ACREDITACIONES
                        </div>
                        @endif
                    </div>

                </div>

            </div>

        </div>
    </div>
    @endif

    <div class="col-sm-12 text-center" style="opacity: 0.5">
        NO HAY LECTURAS REGISTRADAS
    </div>
@endsection


@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable().order([3, 'desc']).draw();
    });
    $(document).ready(function(){
        $("#data-table-2").DataTable().order([3, 'desc']).draw();
    });
</script>
@endsection
