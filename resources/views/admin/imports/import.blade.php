@extends('layouts.app')

@section('content')
    <div class="row">

        <div class="col-sm-12">
            <header class="content__title">
                <h1>Importar</h1>
            </header>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('system.accreditations.import.file', [session('system_slug')]) }}" class="row" enctype="multipart/form-data">

                        @csrf
                        <div class="col-md-12">
                            Selecciona el archivo a importar. <br>
                            <b>Recuerda que debe tener extensión .tsv</b><br><br>
                        </div>

                        <div class="col-md-8">
                            <div class="input-group">
                                <label for="exampleFormControlFile1">Archivo</label>
                                <input type="file" class="form-control-file" name="file" accept=".tsv" required>
                            </div>
                        </div>

                        <div class="col-md-12">
                            Equipo (s) asociado (s) a las acreditaciones: <br><br>
                        </div>
                        @foreach($team as $id => $name)
                            <div class="checkbox col-md-12">
                                <input type="checkbox" id="team-{{ $id }}" name="teams[{{ $id }}]" value="{{ $id }}">
                                <label class="checkbox__label" for="team-{{ $id }}">{{ $name }}</label><br>
                            </div>
                        @endforeach

                    <div class="col-md-12 modal-footer text-right">
                        <button type="submit" class="btn btn-primary">Importar</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



