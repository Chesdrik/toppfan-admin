<div class="modal fade show" id="edit-order-{{ $order->id }}" tabindex="-1" style="display: none;" aria-modal="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pull-left">Editar información de orden</h5>
                <hr>
            </div>
            <form method="POST" action="{{ route('system.orders.update', [session('system_slug'), $order->id]) }}" class="row">
            <div class="modal-body">
                @csrf

                <div class="col-sm-10 offset-1">
                    {!! Form::MDtext('Nombre(s)', 'name', $order->client_user->name, $errors) !!}
                    {!! Form::MDtext('Apellido(s)', 'last_name', $order->client_user->last_name, $errors) !!}
                    {!! Form::MDtext('Email', 'email', $order->email, $errors) !!}
                </div>

            </div>
            <div class="col-md-12 modal-footer text-right">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
            </form>
        </div>
    </div>
</div>
