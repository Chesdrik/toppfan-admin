<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>{{ config('app.name', 'Toppfan') }} - orden {{ $order->id }}</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="https://kit.fontawesome.com/57ff8a41d9.js" crossorigin="anonymous"></script>

        <style>
            @page { margin: 0px; padding: 0px; }
            body { margin: 0px; padding: 0px; }
            html { margin: 0px; padding: 0px; }
            table{
                page-break-inside: avoid;
            }
            table,thead,tbody,tfoot,tr, td,th{
                width: 100%
            }
            tr, td,th{
                text-align: left;
                border:1px solid #dedede;
                padding: 1rem;
            }
            .table    { display: table; width: 20%; }
            .tr       { display: table-row;  }
            .thead    { display: table-header-group; }
            .tbody    { display: table-row-group; }
            .tfoot    { display: table-footer-group; }
            .col      { display: table-column; }
            .colgroup { display: table-column-group; }
            .td, .th   { display: table-cell; width: 50%; }
            .caption  { display: table-caption; }

            .table, .thead, .tbody, .tfoot, .tr, .td, .th{
                text-align: center;
                margin: auto;
                padding: 1rem;
            }

            /* tbody{
                border-bottom: 0px solid orange;
            } */

            th {
                height: 70px;
                width: 30px;
                background-color: red;
            }

            td {
                border-collapse: collapse;
            }

            .rotate {
                height: 100px;
            /* FF3.5+ */
            -moz-transform: rotate(-90.0deg);
            /* Opera 10.5 */
            -o-transform: rotate(-90.0deg);
            /* Saf3.1+, Chrome */
            -webkit-transform: rotate(-90.0deg);
            /* IE6,IE7 */
            filter: progid: DXImageTransform.Microsoft.BasicImage(rotation=0.083);
            /* IE8 */
            -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)";
            /* Standard */
            transform: rotate(-90.0deg);
            }
            .dateTicket{
                font-size: 17px;
                font-family: Arial, Helvetica, sans-serif !important
            }
            .listData{
                font-size: 18px;
                font-family: Arial, Helvetica, sans-serif !important
            }
            .topics{
                line-height: 5px;
            }
            .topics2r{
                line-height: 15px;
            }
            .row .col-xs-2, .row .col-xs-10{
                display:inline-block; float: none
            }

            footer {
                position: fixed;
                bottom: 0cm;
                left: 0cm;
                right: 0cm;
                height: 2cm;

                background-color: #1b2855;
                color:white;

                /** Extra personal styles **/
                text-align: center;
                line-height: 1.5cm;
            }
        </style>
    </head>

    <body>
        <table style="border: 1px solid orange;">
            <tbody>
                <tr style="width: 100%;">
                    <td colspan="2" style="width: 40%; background-color: #1b2855; color:white">
                        <img src="{{ asset('/assets/images/pdf/logo_dorado.png') }}" width="50px">
                    </td>
                    <td colspan="1" style="background-color:#1b2855;width: 60%;">
                        <div class="dateTicket text-white">
                            {{ $order->event->date_start() }}
                        </div>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <div style="font-size:14px"><img src="{{ asset('/assets/images/pdf/pin.png') }}" width="35px"><span style="color:#1b2855"><b>{{ $order->event->venue->name }}</b></span></div>
                    </td>
                </tr>

                <tr class="topics">
                    <td colspan="2">
                        <div style="font-size:12px">{{ $order->event->name }}</div>
                    </td>
                    <td colspan="1">
                        <div style="font-size:12px">
                            @if($order->event->event_groups->count() > 0)
                                {{ $order->event->event_groups->first()->name }}
                            @endif
                        </div>
                    </td>
                </tr>

                <tr class="topics pt-2">
                    <td style="border:none; padding-top:15px" colspan="2">
                        <div class="listData topics" style="border:none; padding-top:10px">Acceso</div>
                    </td>
                    <td colspan="1" class="listData" style="border:none; padding-top:25px">
                        <span style="color:#1b2855">
                            <b>{{ $ticket->ticket_type->checkpoints_string('perimeter') }}</b>
                        </span>
                    </td>
                </tr>

                <tr class="topics">
                    <td style="border:none" colspan="2">
                        <div class="listData" style="border:none">Puerta</div>
                    </td>
                    <td colspan="1" class="listData" style="border:none"><span style="color:#1b2855"><b>{{ $ticket->ticket_type->checkpoints_string('tunnel') }}</b></span></td>
                </tr>

                <tr class="topics2r">
                    <td style="border:none;" colspan="2">
                        <div class="listData" style="border:none">Sección</div>
                    </td>
                    <td colspan="1" class="" style="border:none"><span style="color:#1b2855"><b>{{ $ticket->ticket_type->pretty_zone(false) }}</b></span></td>
                </tr>

                <tr class="topics">
                    <td colspan="2">
                        <span>Asiento:</span> <span style="color:#1b2855; font-size: 0.8em"><b>general</b></span>
                    </td>
                    <td colspan="1" class="">
                        <div class=""><span>Fila:</span> <span style="color:#1b2855; font-size: 0.8em"><b>general</b></span></div>
                    </td>
                </tr>

                <tr>
                    <td colspan="3">
                        <div style="padding: 10px 0px 0px 70px;">
                            <img src='{{ asset($ticket->qr()) }}' style="width: 170px; height: auto; position: relative;"/>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td colspan="1">
                    <div class="titular">
                            <span>Titular:</span>
                            <span style="color:red; font-size: 0.7em">{{ $order->client_user->name }}</span>
                        </div>
                    </td>

                    <td colspan="1">
                        <div class="titular">
                            <span>ID:</span>
                            <span>{{ $order->id }}</span>
                        </div>
                    </td>

                    <td colspan="1">
                        <div class="titular">
                            <span>Fecha de orden:</span><br>
                            <span>{{ pretty_short_date($order->created_at) }}</span>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td style="border: none; text-align: center;" colspan="3">
                        <img src="{{ asset('/assets/images/pdf/goya.png') }}" style="width: 55%;">
                    </td>
                </tr>
                {{-- <tr style="background-color: #1b2855; color:white; border: 1px solid yellow;">
                    <td style="border:none" colspan="2"></td>
                    <td style="border:none" colspan="1">
                        <div style="border:none;">
                            <span style="font-size: 18px">$</span>
                            <span style="font-size: 28px">1.00</span>
                            <span style="font-size: 14px">MXN</span>
                        </div>
                    </td>
                </tr> --}}
            </tbody>
        </table>

        <footer>
            <div style="padding-left: 50%;">
                <span style="font-size: 18px">$</span>
                <span style="font-size: 28px">{{ ($order->type == 'courtesy' ? '1.00' : "{$order->total}.00") }}</span>
                <span style="font-size: 14px">MXN</span>
            </div>
        </footer>

        {{-- <script>
            $(document).ready(function() {
                $('.rotate').css('height', $('.rotate').width());
            });
        </script> --}}
    </body>
</html>
