@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Detalle de orden #{{ $order->id }}</h1><br>
    <div class="actions">
        @if($order->status == 'cancelled')
        <div class="row col-sm-10 offset-6">
            <div class="alert alert-warning" role="alert">
                Orden cancelada
            </div>
        </div>
        @endif
        <a href="#edit-order-{{ $order->id }}" data-toggle="modal" data-target="#edit-order-{{ $order->id }}" class="btn btn-primary btn--raised btn--icon-text">
            <i class="zmdi zmdi-edit"></i> Información de orden
        </a>
        <a href="{{ route('system.orders.resend.email', [session('system_slug'), $order->id]) }}" class="btn btn-primary btn-resend" style="margin-left:10px;">
            <i class="zmdi zmdi-mail-send zmdi-hc-fw"></i> Reenviar correo
        </a>
    </div>
    @if($order->status == 'cancelled')<br><br>@endif
</header>

<div class="card-deck p-b-50">
    <div class="card col-xs-12 col-sm-6">
        <div class="card-body">
            <h4 class="card-title">{{ $order->event->name }}</h4>

            <div class="row">
                <div class="col-sm-12">
                    <img src="{{ asset($order->event->img_path()) }}" class="img-fluid" />
                </div>

                <div class="col-sm-12">
                    <hr>
                    <div class="row text-center">
                        <div class="col"><h6>Fecha evento</h6>{{ pretty_short_date($order->event->date) }}</div>
                        <div class="col"><h6>Fecha orden</h6>{{ pretty_short_date($order->created_at) }}</div>
                        <div class="col"><h6>Total</h6>{{ pretty_money($order->total) }}</div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    @if(in_array($order->payment_method, ['credit_card', 'debit_card']))
        @if(!is_null($order->latest_log()))
            @php $response = $order->latest_log()->affipay_response()  @endphp

            <div class="card col-xs-12 col-sm-6">
                <div class="card-body">
                    <h4 class="card-title">Pago Affipay</h4>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <p class="card-text">
                                <h6>ID transacción</h6>{{ $response['id'] }}<br /><br />
                                <h6>Request ID</h6>{{ $response['requestId'] }}<br /><br />
                                <h6>Fecha</h6>{{ $response['date'] }} {{ $response['time'] }}<br /><br />
                            </p>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <p class="card-text">
                                <h6>BIN</h6>{{ $response['binInformation']['bin'] }}<br /><br />
                                <h6>Request ID</h6>{{ $response['binInformation']['bank'] }}<br /><br />
                                <h6>Fecha</h6>{{ $response['binInformation']['type'] }}<br /><br />
                                <h6>Fecha</h6>{{ $response['binInformation']['brand'] }}<br /><br />
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="card col-xs-12 col-sm-6">
                <div class="card-body">
                    <h4 class="card-title">Pago Affipay</h4>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <strong>No hay transacción registrada</strong>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    @else
        <div class="card col-xs-12 col-sm-3">
            <div class="card-body">
                <h4 class="card-title">Pago en efectivo</h4>
            </div>
        </div>
    @endif
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered data-table2">
                        <thead class="thead-default">
                            <tr>
                                <th>Ticket</th>
                                <th>Asiento</th>
                                <th>Cantidad / usados</th>
                                <th>Precio</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($order->tickets as $ticket)
                                <tr>
                                    <td>{{ $ticket->ticket_type->name }}</td>
                                    <td>
                                        @if ($ticket->seats->first())
                                            {{ $ticket->seats->first()->row->name }} - {{ $ticket->seats->first()->name }}
                                        @else
                                            General
                                        @endif
                                    </td>
                                    <td>{{ $ticket->amount }} / {{ $ticket->used }}</td>
                                    <td class="text-right">{{ pretty_money($ticket->price) }}</td>
                                    <td class="text-right">{{ pretty_money($ticket->amount*$ticket->price) }}</td>
                                </tr>
                            @endforeach

                            <tr>
                                <td colspan="4" class="text-right"><b> Total:</b></td>
                                <td  class="text-right">
                                    <strong>{{ pretty_money($order->total) }}</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



{{-- <header class="content__title">
    <h1>Accesos utilizados</h1>
</header>
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="lgi-heading m-b-5">{{ $order_stats['used_tickets'] }} de {{ $order_stats['total_tickets'] }} accesos utiliados <small>({{ $order_stats['access_percentage'] }}%)</small></div>

                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $order_stats['access_percentage'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $order_stats['access_percentage'] }}%">
                                <span class="sr-only">{{ $order_stats['access_percentage'] }}% accesos utilizados</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 m-t-25 m-b-25">
        <hr />
    </div>
</div> --}}


<header class="content__title">
    <h1>QRs de acceso</h1>
</header>
<div class="row m-t-25">
    @foreach($order->tickets as $ticket)
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body" style="padding: 1.5rem;">
                    <div class="row">
                        <div class="col-sm-12 p-b-20">
                            <h4><small>{{ $ticket->amount }} x</small> {{ $ticket->ticket_type->name }}</h4>

                            <hr style="border-top: 5px solid #{{ $ticket->ticket_type->color }};" />

                            @if($ticket->qr())
                                <img src="{!! $ticket->qr() !!}" class="img-fluid" />
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="lgi-heading m-b-5">{{ $ticket->used }} de {{ $ticket->amount }} accesos utilizados <small>({{ $ticket->access_percentage() }}%)</small></div>

                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ $ticket->access_percentage() }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $ticket->access_percentage() }}%">
                                            <span class="sr-only">{{ $ticket->folio }}</span>
                                            <span class="sr-only">{{ $ticket->access_percentage() }}% accesos utilizados</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

@if(in_array($order->payment_method, ['credit_card', 'debit_card']) && !is_null($order->latest_log()))
    <header class="content__title">
        <h1>Logs de transacción</h1>
    </header>

    @foreach($order->logs as $log)
        <div class="row m-t-25">
            <div class="card col-xs-12 col-6">
                <div class="card-body">
                    <h4 class="card-title">Request</h4>

                    <pre>
                        {!! print_r(json_decode($log->request)) !!}
                    </pre>
                </div>
            </div>

            <div class="card col-xs-12 col-6">
                <div class="card-body">
                    <h4 class="card-title">Response</h4>

                    <pre>
                        {!! print_r(json_decode($log->response)) !!}
                    </pre>
                </div>
            </div>
        </div>
    @endforeach
@endif
@include('admin.orders.modals.edit')
@endsection

@section('after_includes')
<script>
    $('.btn-resend').on('click', function(e){
        if(!confirm('¿Estás seguro de querer reenviar el correo de confirmación de compra?')){
            e.preventDefault();
        }
    });
</script>
@endsection