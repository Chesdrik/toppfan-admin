@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Detalle de orden de cortesía #{{ $order->id }}</h1>
</header>

<div class="row">
    <div class="col-sm-8">
        <div class="card">
            <div class="card-body" style="padding: 1.5rem;">
                <div class="row">
                    <div class="col-sm-12 p-b-20">
                        <h4>{{ $order->event->name }}</h4><br />
                        <h6>Fecha evento</h6>{{ pretty_short_date($order->event->date) }}<br /><br />

                        <a href="{{ route('system.courtesy.pdf', [session('system_slug'), $order->id]) }}" target="_blank" class="btn btn-secondary">PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="card">
            <div class="card-body" style="padding: 1.5rem;">
                <div class="row">
                    <div class="col-sm-12">
                        <img src="{{ asset($order->event->img_path()) }}" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="list-group">
            <div class="list-group-item">
                <div class="lgi-heading m-b-5">{{ $order_stats['used_tickets'] }} de {{ $order_stats['total_tickets'] }} accesos utiliados <small>({{ $order_stats['access_percentage'] }}%)</small></div>

                <div class="progress">
                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ $order_stats['access_percentage'] }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $order_stats['access_percentage'] }}%;">
                        <span class="sr-only">{{ $order_stats['access_percentage'] }}% accesos utilizados</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 m-t-25 m-b-25">
        <hr />
    </div>
</div>

<div class="row m-t-25">
    @foreach($order->tickets as $ticket)
        <div class="col-sm-4">
            <div class="card">
                <div class="card-body" style="padding: 1.5rem;">
                    <div class="row">
                        <div class="col-sm-12 p-b-20">
                            <h4><small>{{ $ticket->amount }} x</small> {{ $ticket->ticket_type->name }}</h4>

                            <hr style="border-top: 5px solid #{{ $ticket->ticket_type->color }};" />

                            @if($ticket->qr())
                                <img src="{!! $ticket->qr() !!}" class="img-fluid" />
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="lgi-heading m-b-5">{{ $ticket->used }} de {{ $ticket->amount }} accesos utiliados <small>({{ $ticket->access_percentage() }}%)</small></div>

                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ $ticket->access_percentage() }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $ticket->access_percentage() }}%">
                                            <span class="sr-only">{{ $ticket->folio }}</span>
                                            <span class="sr-only">{{ $ticket->access_percentage() }}% accesos utilizados</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection
