@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Precios</h1>
    </header>


    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card icons-demo">
                <div class="card-body">
                    <h4 class="card-title">Total ventas</h4>
                    <h6 class="card-subtitle">Praesent commodo cursus magna scelerisque consectetur.</h6>
                    <p class="card-text">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="card icons-demo">
                <div class="card-body">
                    <h4 class="card-title">Resumen ventas</h4>
                    <h6 class="card-subtitle">Praesent commodo cursus magna scelerisque consectetur.</h6>
                    <p class="card-text">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
