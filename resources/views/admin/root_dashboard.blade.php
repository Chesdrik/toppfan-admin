@extends('layouts.app')

@section('content')

    <header class="content__title">
        <h1>Root Dashboard</h1>
    </header>

    <div class="col-sm-12 col-md-8 offset-md-2">
        <header class="content__title text-center">
            <h1>Elige el sistema que quieres administrar</h1>
        </header>

        <div class="row">
            @foreach($systems as $system)
                <a href="{{ route('system.dashboard', $system->slug) }}" target="_self" class="col-sm-12 col-md-6 text-center m-b-15 system-card-container">
                    <div class="card system-card">
                        <div class="card-body">
                            <h2>{{ $system->name }}</h2>
                            <div class="col-sm-6 offset-sm-3 m-t-25 m-b-25">
                                <img src="{{ Storage::url($system->img_path(false)) }}" class="img-fluid system-img" />
                            </div>
                            <hr>
                            <small>{{ $system->slug }}</small>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection
