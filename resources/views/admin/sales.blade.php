@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Ventas</h1>
    </header>

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Ventas últimos 14 días</h4>

                    <div id="flot-line-1" class="flot-chart flot-line"></div>
                    <div class="flot-chart-legends flot-chart-legends--line"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Ventas últimos 30 días</h4>

                    <div id="flot-line-2" class="flot-chart flot-line"></div>
                    <div class="flot-chart-legends flot-chart-legends--line"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card icons-demo">
                <div class="card-body">
                    <h4 class="card-title">Total ventas</h4>
                    <h6 class="card-subtitle">Praesent commodo cursus magna scelerisque consectetur.</h6>
                    <p class="card-text">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="card icons-demo">
                <div class="card-body">
                    <h4 class="card-title">Resumen ventas</h4>
                    <h6 class="card-subtitle">Praesent commodo cursus magna scelerisque consectetur.</h6>
                    <p class="card-text">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec sed odio dui. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('after_includes')
<script src="{{ Storage::url('assets/material/vendors/flot/jquery.flot.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/flot/jquery.flot.resize.js') }}"></script>

<script type="text/javascript">
'use strict';

$(document).ready(function(){
    // Chart Data
    var lineChartData1 = [
        {
            label: 'General',
            data: [[1,40], [2,35], [3,37], [4,100], [5,10], [6,90], [7,15]],
            color: '#32c787'
        },
        {
            label: 'Platea',
            data: [[1,25], [2,70], [3,60], [4,70], [5,100], [6,35], [7,75]],
            color: '#03A9F4'
        },
        {
            label: 'Palco',
            data: [[1,10], [2,20], [3,60], [4,90], [5,30], [6,10], [7,5]],
            color: '#f5c942'
        }
    ];

    var lineChartData2 = [
        {
            label: 'General',
            data: [[1,20], [2,90], [3,60], [4,40], [5,100], [6,25], [7,65]],
            color: '#32c787'
        },
        {
            label: 'Platea',
            data: [[1,100], [2,20], [3,60], [4,90], [5,80], [6,10], [7,5]],
            color: '#03A9F4'
        },
        {
            label: 'Palco',
            data: [[1,60], [2,30], [3,50], [4,100], [5,10], [6,90], [7,85]],
            color: '#f5c942'
        }
    ];

    // Chart Options
    var lineChartOptions = {
        series: {
            lines: {
                show: true,
                barWidth: 0.05,
                fill: 0
            }
        },
        shadowSize: 0.1,
        grid : {
            borderWidth: 1,
            borderColor: '#edf9fc',
            show : true,
            hoverable : true,
            clickable : true
        },

        yaxis: {
            tickColor: '#edf9fc',
            tickDecimals: 0,
            font :{
                lineHeight: 13,
                style: 'normal',
                color: '#9f9f9f',
            },
            shadowSize: 0
        },

        xaxis: {
            tickColor: '#fff',
            tickDecimals: 0,
            font :{
                lineHeight: 13,
                style: 'normal',
                color: '#9f9f9f'
            },
            shadowSize: 0,
        },
        legend:{
            container: '.flot-chart-legends--line',
            backgroundOpacity: 0.5,
            noColumns: 0,
            backgroundColor: '#fff',
            lineWidth: 0,
            labelBoxBorderColor: '#fff'
        }
    };

    // Create chart
    $.plot($('#flot-line-1'), lineChartData1, lineChartOptions);
    $.plot($('#flot-line-2'), lineChartData2, lineChartOptions);
});

</script>
@endsection
