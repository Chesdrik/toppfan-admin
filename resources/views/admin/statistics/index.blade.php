@extends('layouts.app')

@section('content')
    <div class="row price-table price-table--basic">
        @if(isset($general_statistics['general_stats']))
            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Ventas totales</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ pretty_money($general_statistics['general_stats']['total_sales']) }}
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Total de boletos vendidos</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ pretty_number($general_statistics['general_stats']['total_tickets'], 0) }}
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Total de asistentes</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ pretty_number($general_statistics['general_stats']['total_attendance'], 0) }}
                    </div>
                </div>
            </div>

        @endif
    </div>


    <!-- @ if($event->passedEvent()) -->
    @if(true)
        @if(!is_null($general_statistics))
            <div class="row price-table price-table--basic">
                <div class="col-sm-12 text-center m-t-25 m-b-25">
                    <hr />
                </div>

                <div class="col-sm-12 text-center">
                    <h3>Estadísticas generales</h3>
                    <canvas id="generalChart" width="auto" height="50"></canvas>
                </div>

                <div class="col-sm-12 text-center m-t-25 m-b-25">
                    <hr />
                </div>

            </div>
        @else
            <div class="col-sm-12 text-center" style="opacity: 0.5">
                <br>
                    NO HAY ESTADÍSTICAS PARA MOSTRAR
            </div>
        @endif
    @endif

@endsection


@section('after_includes')
<script>
$(document).ready(function(){

    @if(isset($general_statistics['general_stats']))
        var checkctx = document.getElementById('generalChart').getContext('2d');
        var readingsChart = new Chart(checkctx, {
            type: 'bar',
            data: {
                labels: [{!! $general_statistics['general_stats']['labels']['label'] !!}],
                datasets: [{
                    label: 'Venta total ($)',
                    data: [{!! $general_statistics['general_stats']['sales']['string'] !!}],
                    backgroundColor: [{!! $general_statistics['general_stats']['sales_color'] !!}],
                    borderColor: [{!! $general_statistics['general_stats']['sales_color'] !!}],
                    borderWidth: 1
                },
                {
                    label: 'Boletos vendidos',
                    data: [{!! $general_statistics['general_stats']['tickets']['string'] !!}],
                    backgroundColor: [{!! $general_statistics['general_stats']['tickets_color'] !!}],
                    borderColor: [{!! $general_statistics['general_stats']['tickets_color'] !!}],
                    borderWidth: 1
                },
                {
                    label: 'Asistentes',
                    data: [{!! $general_statistics['general_stats']['attendance']['string'] !!}],
                    backgroundColor: [{!! $general_statistics['general_stats']['access_color'] !!}],
                    borderColor: [{!! $general_statistics['general_stats']['access_color'] !!}],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                }
            }
        });
    @endif

});

    $(document).ready(function(){
            $("#data-table").DataTable().order([3, 'desc']).draw();
        });
</script>
@endsection
