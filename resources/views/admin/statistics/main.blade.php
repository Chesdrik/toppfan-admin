@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-10 offset-sm-1">

        <div class="row">
            <div class="col-sm-4 offset-sm-1">
                <header class="content__title text-center">
                    <h1>Estadísticas por sede</h1>
                </header>

                @foreach($venues as $venue)
                    <a href="{{ route('system.statistics.byVenue', $venue->id) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                        {{ $venue->name }}
                    </a>
                @endforeach
            </div>


            <div class="col-sm-4 offset-sm-2">
                <header class="content__title text-center">
                    <h1>Estadísticas por equipo</h1>
                </header>
                @foreach($teams as $team)
                    <a href="{{ route('system.statistics.byTeam', $team->id) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                        {{ $team->name }}
                    </a>
                @endforeach
            </div>
        </div>

        <div class="row m-t-25">
            <div class="col-sm-6 offset-sm-3">
                <a href="{{ route('system.statistics.all') }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                    Todas los estadísticas
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
