@extends('layouts.app')

@section('content')

    <header class="content__title">
        <h1>Estadísticas de {{ $venue->name }}</h1>
    </header>
    <div class="row">

        @if(session('success'))
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            </div>
        </div>
        @endif

        @if(session('error'))
        <div class="col-sm-12">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            </div>
        </div>
        @endif

    </div>

    <div class="row price-table price-table--basic">

        @if(isset($venue_statistics['venue_stats']))
            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Ventas totales</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ pretty_money($venue_statistics['venue_stats']['total_sales']) }}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Total de boletos vendidos</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ pretty_number($venue_statistics['venue_stats']['total_tickets'], 0) }}
                    </div>
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="price-table__item">
                    <header class="price-table__header bg-light-blue">
                        <div class="price-table__title">Total de asistentes</div>
                    </header>
                    <div class="price-table__price color-light-blue stat_data">
                        {{ pretty_number($venue_statistics['venue_stats']['total_attendance'], 0) }}
                    </div>
                </div>
            </div>

        @endif
    </div>


    <!-- @ if($event->passedEvent()) -->
    @if(true)
        @if(!is_null($venue_statistics))
            <div class="row price-table price-table--basic">
                <div class="col-sm-12 text-center m-t-25 m-b-25">
                    <hr />
                </div>

                <div class="col-sm-12 text-center">
                    <h3>Estadísticas generales</h3>
                    <canvas id="generalChart" width="auto" height="50"></canvas>
                </div>

                <div class="col-sm-12 text-center m-t-25 m-b-25">
                    <hr />
                </div>
                
            </div>
        @else
            <div class="col-sm-12 text-center" style="opacity: 0.5">
                <br>
                    NO HAY ESTADÍSTICAS PARA MOSTRAR
            </div>
        @endif
    @endif

@endsection


@section('after_includes')
<script>
$(document).ready(function(){
    
    @if(isset($venue_statistics['venue_stats']))
        var checkctx = document.getElementById('generalChart').getContext('2d');
        var readingsChart = new Chart(checkctx, {
            type: 'bar',
            data: {
                labels: [{!! $venue_statistics['venue_stats']['labels']['label'] !!}],
                datasets: [{
                    label: 'Venta total ($)',
                    data: [{!! $venue_statistics['venue_stats']['sales']['string'] !!}],
                    backgroundColor: [{!! $venue_statistics['venue_stats']['sales_color'] !!}],
                    borderColor: [{!! $venue_statistics['venue_stats']['sales_color'] !!}],
                    borderWidth: 1
                },
                {
                    label: 'Boletos vendidos',
                    data: [{!! $venue_statistics['venue_stats']['tickets']['string'] !!}],
                    backgroundColor: [{!! $venue_statistics['venue_stats']['tickets_color'] !!}],
                    borderColor: [{!! $venue_statistics['venue_stats']['tickets_color'] !!}],
                    borderWidth: 1
                },
                {
                    label: 'Asistentes',
                    data: [{!! $venue_statistics['venue_stats']['attendance']['string'] !!}],
                    backgroundColor: [{!! $venue_statistics['venue_stats']['access_color'] !!}],
                    borderColor: [{!! $venue_statistics['venue_stats']['access_color'] !!}],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        }
                    }]
                }
            }
        });
    @endif

});

    $(document).ready(function(){
            $("#data-table").DataTable().order([3, 'desc']).draw();
        });
</script>
@endsection
