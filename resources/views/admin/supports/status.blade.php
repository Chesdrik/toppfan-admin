<div class="modal fade show" id="status" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Cambiar estatus del ticket de soporte</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <a href="{{ url('/admin/soporte/'.$support->id.'/estado/pending') }}" class="btn @if($support->status == 'pending') btn-info @else btn-outline-info @endif btn-block">
                        Pendiente
                    </a>
                </div><br>
                <div class="col-sm-12">
                    <a href="{{ url('/admin/soporte/'.$support->id.'/estado/cancel') }}" class="btn @if($support->status == 'cancel') btn-info @else btn-outline-info @endif btn-block">
                        Cancelada
                    </a>
                </div><br>
                <div class="col-sm-12">
                    <a href="{{ url('/admin/soporte/'.$support->id.'/estado/finish') }}" class="btn @if($support->status == 'finish') btn-info @else btn-outline-info @endif btn-block">
                        Finalizado
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>