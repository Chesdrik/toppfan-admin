@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Crear sistema</h1>
</header>

<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <div class="card">
            <div class="card-body">

                   {!! Form::open(array('url' => route('admin.system.store'), 'method' => 'POST', 'enctype' => 'multipart/form-data')) !!}
                        {!! Form::MDtext('Nombre', 'name', '', $errors) !!}
                        {!! Form::MDtext('Slug', 'slug', '', $errors) !!}
                        {!! Form::MDtext('Titulo', 'title', '', $errors) !!}
                        {!! Form::MDpicker('Color primario', 'primary_color', '', $errors, ['class' => 'col-sm-6 ']) !!}
                        {!! Form::MDpicker('Color secundario', 'secondary_color', '', $errors, ['class' => 'col-sm-6 ']) !!}

                         <div class="col-md-12">
                            Selecione el logo. <br>
                            <b>Recuerda que debe tener extensión .png y no tener fondo</b><br><br>
                        </div>

                        <div class="col-md-8">
                            <div class="input-group">
                                <label for="exampleFormControlFile1">Logo</label>
                                <input type="file" class="form-control-file" name="file" accept=".png" required>
                            </div>
                        </div>


                        {!! Form::MDselectize('Usuarios', 'users', $users, [], $errors) !!}
                        {!! Form::MDselectize('Módulos', 'modules', $modules, [], $errors) !!}

                        {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection

@section('after_includes')
<script type="text/javascript">
    $('#select-user').selectize({
        maxItems: 1000
    });
    $('#select-module').selectize({
        maxItems: 1000
    });
</script>
@endsection
