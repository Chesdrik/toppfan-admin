@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Editar sistema {{ $system->name }}</h1>
</header>

{!! Form::open(array('url' => route('admin.system.update', $system->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data')) !!}
    <div class="row">
        <div class="col-sm-12">
            <div class="card-deck">
                <div class="card">
                    <div class="card-body">
                        {!! Form::MDtext('Nombre', 'name', $system->name, $errors) !!}
                        {!! Form::MDtext('Slug', 'slug', $system->slug, $errors) !!}
                        {!! Form::MDtext('Titulo', 'title', $system_config['title'], $errors) !!}
                        {!! Form::MDselectize('Usuarios', 'users', \App\User::selectize(), $selected_users, $errors) !!}
                        {!! Form::MDselectize('Módulos', 'modules', \App\Module::selectize(), $selected_modules, $errors) !!}
                        {!! Form::MDselectize('Sedes', 'venues', \App\Venue::selectize(), $selected_venues, $errors) !!}
                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        {!! Form::MDColorPicker('Color primario', 'primary_color', $system_config['primary_color'], $errors, ['class' => 'col-sm-12 ']) !!}
                        {!! Form::MDColorPicker('Color secundario', 'secondary_color', $system_config['secondary_color'], $errors, ['class' => 'col-sm-12 ']) !!}

                        <div class="col-md-12">
                            Selecione el logo. <br />
                            <b>Recuerda que debe tener extensión .png y no tener fondo</b><br /><br />
                        </div>

                        <div class="col-md-8">
                            <div class="input-group">
                                <label for="exampleFormControlFile1">Logo</label>
                                <input type="file" class="form-control-file" name="file" accept=".png">
                            </div>
                        </div>

                        <img src="{{ Storage::url($system->img_path(false)) }}" style="max-height:200px"><br /><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-20">
        <div class="col-sm-12">
            {!! Form::MDsubmit('Guardar', 'edit', ['icon' => 'plus-circle-o']) !!}
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('after_includes')
<script type="text/javascript">
    $('#select-user').selectize({
        maxItems: 1000,
    });
    $('#select-module').selectize({
        maxItems: 1000
    });
</script>
@endsection
