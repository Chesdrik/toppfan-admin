@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Sistemas</h1>
        <div class="actions">
            <a href="{{ route('admin.system.create') }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Crear sistema
            </a>
        </div>
    </header>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th class="col-sm-1">Panel</th>
                            <th class="col-sm-5">Nombre</th>
                            <th class="col-sm-5">Slug</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($systems as $system)
                            <tr>
                                <th class="text-center">
                                    <a href="{{ route('admin.system.show', $system->id) }}" target="_self">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                    </a>
                                </th>
                                <td class="text-center">
                                    <a href="{{ route('system.dashboard', $system->slug) }}" target="_self">
                                        <img src="{{ Storage::url($system->img_path(false)) }}" class="img-fluid p-r-15" style="max-height: 50px;">
                                    </a>
                                </td>
                                <td>{{ $system->name }}</td>
                                <td>{{ $system->slug }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $(".data-table").DataTable().order([2, 'asc']).draw();
    });
</script>
@endsection
