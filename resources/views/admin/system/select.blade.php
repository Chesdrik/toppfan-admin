@extends('layouts.app_clean')

@section('content')
    <div class="col-sm-12 col-md-8 offset-md-2">
        <header class="content__title text-center">
            <h1>Elige el sistema que quieres ver</h1>
        </header>

        <div class="row">
            @foreach($systems as $system)
                <a href="{{ route('system.dashboard', $system->slug) }}" target="_self" class="col-sm-12 col-md-6 text-center">
                    <div class="card">
                        <div class="card-body">
                            <h3>{{ $system->name }}</h3>
                            <hr>
                            <small>{{ $system->slug }}</small>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>
    </div>
@endsection
