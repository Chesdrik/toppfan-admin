@extends('layouts.app_clean')

@section('content')


    <!-- Main Content -->
    <div class="container-fluid h-100">
        <div class="row main-contentp-0 text-center h-100">
            <div class="col-md-5 text-center company__info p-0">
                <div class="logInContentLeft">
                    <div class="w-100">
                        <span style="color: white;font-weight: bold;font-size: 4em;">Pumas</span>
                        <span style="color:white; font-size: 4em;">UNAM</span>

                    </div>
                    <br>
                    <br>
                    <span class="company__logo mb-5">
                        <img src="{{ asset('assets/images/logo_pumas.png') }}" alt="" width="150px">
                    </span>

                    <h4 class="company_title text-white mt-3 pt-5" style="color:white !important">Sistema de distribución de boletos</h4>
                </div>
                <br>
                

                <div class="backgroundMetalGrid h-100 w-100 position-absolute"></div>
            </div>
            <div class="col-md-7 bg-light col-xs-12 col-sm-12  d-flex align-items-center justify-content-center">
                <div class="form shadow bg-white pt-5 px-0">

                    <form method="POST" action="{{ route('tds.password.request') }}" class="was-validated">
                        @csrf
                        <div class="form-group">
                            <h1>Restablecer contraseña</h1>
                        </div>

                        @if(session('error'))
                        <div class="col-sm-12">
                            <br>
                            <div class="alert alert-danger" role="alert">
                                {{ session('error') }}
                            </div>
                        </div>
                        @endif

                        @if(session('success'))
                        <div class="col-sm-12">
                            <br>
                            <div class="alert alert-success" role="alert">
                                {{ session('success') }}
                            </div>
                        </div>
                        @endif

                        <div class="col-sm-10 form-group">
                            <label for="email" class="col-md-8 col-form-label p-0 pt-3">Correo electrónico</label>

                            <div class="col-md-12 p-0 m-0">
                                <input id="email" type="email" class="pl-3 form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-sm-12 form-group mt-5 text-center">
                            <button type="submit" class="btn btn-primary">
                                Enviar correo para restablecer contraseña
                            </button>
                        </div>
                      
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection

<style>
    .logInContentLeft{
        position: relative;
        z-index: 10;
    }
    .backgroundMetalGrid{
        opacity: 0.5;
        background:
        radial-gradient(black 15%, transparent 16%) 0 0,
        radial-gradient(black 15%, transparent 16%) 8px 8px,
        radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 0 1px,
        radial-gradient(rgba(255,255,255,.1) 15%, transparent 20%) 8px 9px;
        background-color:#282828;
        background-size:16px 16px;
    }
     .form {
        width: 444px;
        height: auto;
        padding: 62px 48px;
        border: none;
        border-radius: 15px;
        text-align: start;
        font-family: 'Noto Sans', sans-serif !important;
        font-size: 14px !important;
    }
    
    .btn-orange {
        background-color: #f54336 !important;
        width: 100%;
        color: #fff !important;
        border-radius: 30px !important;
        margin-top: 10px !important;
        margin-bottom: 10px !important;
        font-size:13px;
    }
    
    .form h2 {
        margin-bottom: 17px !important;
        color: #212121;
    }
    
    
            .form .ne {
                color: #292f38;
                margin-top: 10px !important;
                line-height: 1.75;
                font-weight: 600;
                font-size: 13px;
                border-bottom: 1px solid #12246d;
                padding-bottom: 10px;
            }
    
            .form-control {
                padding-top: 30px !important;
                padding-left: 0;
                padding-bottom: 30px !important;
                outline: none !important;
                border: none;
                border-bottom: 1px solid #12246d;
                border-radius: 0px;
                margin-top: 10px;
                color: #cacdd2;
                opacity: 0.7;
    
            }
    
            .form-control:focus {
                border-bottom: 1px solid #12246d !important;
                outline: none !important;
                box-shadow: none !important;
            }
    
    
    
        .main-content{
        width: 100%;
        height: 100vh;
        border-radius: 20px;
        margin: 5em auto;
        display: flex;
    }
    .company__info{
    /*background: rgb(255,255,255);
    background: linear-gradient(207deg, rgba(255,255,255,1) 0%, rgba(243,69,18,1) 61%, rgba(0,0,0,1) 100%);*/
    background: rgb(255,255,255);
    background: linear-gradient(207deg, rgba(255,255,255,1) 0%, rgba(204,178,108,1) 35%, rgba(27,40,85,1) 74%);
        display: flex;
        flex-direction: column;
        justify-content: center;
        color: #fff;
    }
    .fa-android{
        font-size:3em;
    }
    @media screen and (max-width: 640px) {
        .main-content{width: 90%;}
        .company__info{
            display: none;
        }
        .login_form{
            border-top-left-radius:20px;
            border-bottom-left-radius:20px;
        }
    }
    @media screen and (min-width: 642px) and (max-width:800px){
        .main-content{width: 70%;}
    }
    .row > h2{
        color:#008080;
    }
    .login_form{
        background-color: #fff;
        border-top-right-radius:20px;
        border-bottom-right-radius:20px;
        border-top:1px solid #ccc;
        border-right:1px solid #ccc;
    }
    form{
        padding: 0 2em;
    }
    .form__input{
        width: 100%;
        border:0px solid transparent;
        border-radius: 0;
        border-bottom: 1px solid #12246d;
        padding: 1em .5em .5em;
        padding-left: 2em;
        outline:none;
        margin:1.5em auto;
        transition: all .5s ease;
    }
    .form__input:focus{
        border-bottom-color: #12246d;
        box-shadow: #12246d; 
        border-radius: 4px;
    }
    </style>
    
