@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Boletos</h1>
    </header>

  @foreach($tickets as $ticket)
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="data-table" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            
                            <th>Orden</th>
                            <th>Evento</th>
                            <th>Sección</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ticket as $details)
                            <tr>
                                <td>{{ $details->order_id }}</td>
                                <td>{{ $details->order->event->name }}</td>
                                <td>{{ $details->ticket_type->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endforeach
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable().order([3, 'desc']).draw();
    });
</script>
@endsection
