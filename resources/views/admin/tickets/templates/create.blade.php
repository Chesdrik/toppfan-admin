@extends('layouts.app')

@section('content')
    <div class="col-sm-6 offset-sm-3">
        <header class="content__title">
            <h1>Crear template de boletos</h1>
        </header>
    </div>


    <div class="row">
        @if(count($errors) > 0)
            <div class="col-sm-6 offset-sm-3">
                <div class="col-sm-12">
                    <div class="alert alert-danger" role="alert">
                        Se encontraron errores con los datos, por favor revisa la información ingresada.
                    </div>
                </div>
            </div>
        @endif

        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('system.venues.templates.store', [session('system_slug'), $venue->id]) }}" class="row">
                        @csrf

                        {!! Form::MDtext('Nombre', 'name', '', $errors, ['class' => 'col-md-6']) !!}

                        @if(count($venue->ticket_types()) > 0)
                            <div class="col-md-12" style="margin-bottom: 10px;">
                                <h6>Selecciona los tipos de boletos que contendrá el template</h6>
                            </div>

                            @foreach($venue->ticket_types() as $ticket_type)
                            <div id="ticket_types" class="col-md-6" style="margin-top: 5px;">
                                <div class="checkbox">
                                    <input type="checkbox" id="customCheck1-{{ $ticket_type->id }}" name="tickets[{{ $ticket_type->id }}]" value="{{ $ticket_type->id }}" data-free="{{ $ticket_type->free }}">
                                    <label class="checkbox__label" for="customCheck1-{{ $ticket_type->id }}">{{ $ticket_type->name }}</label>
                                </div>
                            </div>
                            <div class="col-md-6" id="div-{{ $ticket_type->id }}"></div>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <p style="color: red;">No hay tipos de boletos para ésta sede, por favor, dirígete al panel de registro de tipos de boletos.
                                    {{-- <a href="#crear-tipo-boleto" data-toggle="modal" data-target="#crear-tipo-boleto"> Crear tipos de boletos</a> --}}
                                </p>
                                {{-- @include('admin.tickets.types.modals.create') --}}
                            </div>
                        @endif

                        {{-- {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!} --}}
                        <div class="col-md-12">
                            <button class="btn btn-light btn--icon-text btn-puma-off pull-right" id="create" @if(count($venue->ticket_types()) == 0) disabled @endif>
                                <i class="zmdi zmdi-plus-circle-o"></i> Crear
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('after_includes')
<!-- Javascript -->
<script type="text/javascript">
    $(document).ready(function() {

        $('input[type=checkbox]').each(function () {
            $(this).change(function() {
                var free = $(this).attr('data-free');

                if(free == 0){
                    string = '<div id="div2-'+ this.value +'">' +
                                '<div class="form-group">' +
                                    '<label>Costo</label>' +
                                    '<input type="text" class="form-control " value="" name="costs['+ this.value +']" placeholder="Costo" required><i class="form-group__bar"></i>' +
                                '</div>' +
                            '</div>';

                }else{
                    string = '<div id="div2-'+ this.value +'">' +
                                '<div class="form-group">' +
                                    '<label>Costo</label>' +
                                    '<input type="text" class="form-control " value="N/A (Boleto tipo cortesía)" name="costs['+ this.value +']" placeholder="Costo" required disabled><i class="form-group__bar"></i>' +
                                    '<input type="text" class="form-control " value="0" name="costs['+ this.value +']" placeholder="Costo" hidden><i class="form-group__bar"></i>' +
                                '</div>' +
                            '</div>';
                }

                if(this.checked){
                    console.log(this.value);
                    $('#div-' + this.value).html(string);
                }else{
                    $('#div2-' + this.value).remove();
                }
            });
        });

    });
</script>
@endsection
