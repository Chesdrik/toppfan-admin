@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Templates de boletos</h1>

        <div class="actions">
            <a href="{{ route('system.venues.templates.create', [session('system_slug'), $venue->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar template de boletos
            </a>
        </div>
    </header>

    @if(count($venue->ticket_templates) > 0)
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th>&nbsp;</th>
                            <th>Nombre</th>
                            <th>Sede</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($venue->ticket_templates as $ticket_template)
                            <tr>
                                <td class="text-center">
                                    <a href="{{ route('system.venues.templates.show', [session('system_slug'), $ticket_template->venue_id, $ticket_template->id]) }}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $ticket_template->name }}</td>
                                <td>{{ $ticket_template->venue->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @else
    <div class="col-sm-12 text-center" style="opacity: 0.5">
        <br>
            NO HAY TEMPLATES REGISTRADOS
    </div>
    @endif
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable().order([3, 'desc']).draw();
    });
</script>
@endsection
