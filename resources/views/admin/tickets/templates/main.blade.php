@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Templates por sede</h1>
    </header>

    <div class="row">
        @foreach($venues as $venue)
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title" style="height: 40px;">{{ $venue->name }}</h4>
                        <p class="card-text">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                        <br />
                        {!! Form::MDButton('Ver templates', 'listado_sede', '/admin/sedes/'.$venue->id.'/templates', ['class' => 'btn btn-light btn--raised pull-right']) !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('after_includes')
<!-- Javascript -->
<!-- <script src="{{ Storage::url('assets/material/vendors/datatables/jquery.dataTables.min.js') }}"></script> -->
@endsection
