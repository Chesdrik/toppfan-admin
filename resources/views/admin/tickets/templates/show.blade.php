@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Editar</h1>
    </header>


    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <header class="content__title">
                <h1>{{ $ticket_template->name }}</h1>
            </header>

            <div class="card">
                <div class="card-body">

                    {!! Form::open(array('url' => route('system.venues.templates.update', [session('system_slug'), $venue->id, $ticket_template->id]), 'method' => 'put', 'class' => 'row')) !!}
                        @csrf

                        {!! Form::MDtext('Nombre', 'name', $ticket_template->name, $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDselect('Sede', 'venue_id', $ticket_template->venue_id, \App\Venue::venue_options(), $errors, ['class' => 'col-md-8', 'disabled' => 'disabled']) !!}

                        @foreach($ticket_template->venue->ticket_types() as $ticket_type)
                        <div class="col-md-6" style="margin-top: 5px;">
                            <div class="checkbox">
                                <input type="checkbox" id="customCheck1-{{ $ticket_type->id }}" name="tickets[{{ $ticket_type->id }}]" value="{{ $ticket_type->id }}" @if(in_array($ticket_type->id, array_keys($ticket_template->options()))) checked @endif>
                                <label class="checkbox__label" for="customCheck1-{{ $ticket_type->id }}">{{ $ticket_type->name }}</label>
                            </div>
                        </div>
                        <div class="col-md-6" id="div-{{ $ticket_type->id }}">
                            @foreach($ticket_template->options() as $key => $value)
                                @if($ticket_type->id == $key)
                                    <div id="div2-{{ $ticket_type->id }}">
                                        <div class="form-group">
                                            <label>Costo</label>
                                            @if($template_costs[$ticket_type->id] == 0)
                                                <input type="text" class="form-control " value="N/A (Boleto tipo cortesía)" name="costs[{{ $ticket_type->id }}][price]" placeholder="" disabled><i class="form-group__bar"></i>
                                            @else
                                                <input type="text" class="form-control " value="{{ $template_costs[$ticket_type->id] }}" name="costs[{{ $ticket_type->id }}][price]" placeholder=""><i class="form-group__bar"></i>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        @endforeach

                        {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('after_includes')
<!-- Javascript -->
<script type="text/javascript">
    $(document).ready(function() {

        $('input[type=checkbox]').each(function () {
            $(this).change(function() {
                string = '<div id="div2-'+ this.value +'">' +
                            '<div class="form-group">' +
                                '<label>Costo</label>' +
                                '<input type="text" class="form-control " value="" name="costs['+ this.value +'][price]" placeholder="Costo" required><i class="form-group__bar"></i>' +
                            '</div>' +
                        '</div>';
                if(this.checked){
                    $('#div-' + this.value).html(string);
                }else{
                    $('#div2-' + this.value).remove();
                }
            });
        });

    });
</script>
@endsection
