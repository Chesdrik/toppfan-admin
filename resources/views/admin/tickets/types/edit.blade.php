@extends('layouts.app')

@section('content')

<div class="col-sm-12">
    <header class="content__title">
    <h1>Editar {{ $ticket_type->name}}</h1>
    </header>
</div>

<div class="row">
    {!! Form::open(array('url' => route('system.venues.ticket_types.update', [session('system_slug'), $venue->id, $ticket_type->id]), 'method' => 'put', 'class' => 'row')) !!}
        <div class="col-sm-7">
            <div class="card">
                <div class="card-body">
                    {!! Form::MDtext('Nombre', 'name', $ticket_type->name, $errors, ['class' => 'col-md-10', 'required' => true]) !!}
                    <div class="row col-sm-12">
                        {!! Form::MDselect('Activo', 'active', $ticket_type->active, ['1' => 'Sí', '0' => 'No'], $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDselect('Cortesía', 'free', $ticket_type->free, ['1' => 'Sí', '0' => 'No'], $errors, ['class' => 'col-md-6']) !!}
                    </div>

                    <div class="row col-sm-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Costo</label>
                                <input type="text" class="form-control " value="{{ $ticket_type->price }}" name="price" placeholder="Costo" @if($ticket_type->free == 1) disabled @endif>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Boletos totales</label>
                                <input type="text" class="form-control" id="total-{{ $ticket_type->id }}" placeholder="Boletos totales" value="{{ $ticket_type->total_seats }}" name="total_seats" required>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                        <small class="col-sm-12" id="error-{{ $ticket_type->id }}" style="color:red; margin-top: -5%; text-align:right"></small>
                    </div>

                    {!! Form::MDselect('Zona', 'zone_id', $ticket_type->zone->id, $venue->zone_options(), $errors, ['class' => 'col-md-6']) !!}
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <h6>Selecciona los puntos de acceso permitidos para este tipo de boleto</h6>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-5">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <h6>Perimetrales</h6>
                        </div>

                        @foreach($perimeters as $id => $name)
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="toggle-switch">
                                        <input type="checkbox" class="toggle-switch__checkbox" name="checkpoints[{{$id}}]" value="{{ $id }}" @if(in_array($id, $checkpoints_array)) checked @endif>
                                        <i class="toggle-switch__helper"></i>
                                    </div>
                                    <label style="margin-left:5px;">{{ $name }}</label>
                                </div>
                            </div>
                        @endforeach

                        <div class="col-sm-12 m-b-25">
                            <hr />
                        </div>

                        <div class="col-sm-12">
                            <h6>Túneles</h6>
                        </div>
                        @foreach($tunnels as $id => $name)
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="toggle-switch">
                                        <input type="checkbox" class="toggle-switch__checkbox" name="checkpoints[{{$id}}]" value="{{ $id }}" @if(in_array($id, $checkpoints_array)) checked @endif>
                                        <i class="toggle-switch__helper"></i>
                                    </div>
                                    <label style="margin-left:5px;">{{ $name }}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>


                    <div class="col-md-12">
                        <button class="btn btn-light btn--icon-text btn-puma-off pull-right save" id="create-{{ $ticket_type->id }}">
                            <i class="zmdi zmdi-card-sd"></i> Guardar
                        </button>
                    </div>
            </div>
        </div>
    {!! Form::close() !!}
</div>

@endsection
