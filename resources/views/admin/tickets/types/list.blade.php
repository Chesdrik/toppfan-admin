@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Tipos de boletos</h1>

        <div class="actions">
            <a href="#crear-tipo-boleto" data-toggle="modal" data-target="#crear-tipo-boleto" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar tipo de boleto
            </a>
        </div>

        @include('admin.tickets.types.modals.create')
    </header>


    @if($venue->ticket_types()->count() > 0)
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered data-table">
                    <thead class="thead-default">
                            <tr>
                                <th>&nbsp;</th>
                                <th>Nombre</th>
                                <th>Zona</th>
                                <th>Costo</th>
                                <th>Accesos</th>
                                <th>Asientos totales</th>
                                <th>Activo</th>
                                <th>Cortesía</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($venue->ticket_types() as $ticket_type)
                                <tr>
                                    <td class="text-center">
                                        <a href="{{ route('system.venues.ticket_types.edit', [session('system_slug'), $venue->id, $ticket_type->id]) }}" target="_self">
                                            <button class="btn btn-outline-secondary btn--raised">
                                                <i class="zmdi zmdi-edit"></i>
                                            </button>
                                        </a>
                                    </td>
                                    <td>{{ $ticket_type->name }}</td>
                                    <td>{{ $ticket_type->pretty_zone() }}</td>
                                    <td>
                                        @if($ticket_type->free == 0)
                                            {{ pretty_money($ticket_type->price) }}
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                    <td>{{ $ticket_type->checkpoints_string() }}</td>
                                    <td>{{ $ticket_type->total_seats }}</td>
                                    <td>{!! $ticket_type->pretty_status() !!}</td>
                                    <td>{!! $ticket_type->pretty_courtesy() !!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @else
        <div class="col-sm-12 text-center" style="opacity: 0.5">
            <br />NO HAY BOLETOS REGISTRADOS
        </div>
    @endif
@endsection

@section('after_includes')
    <!-- Javascript -->
    <script type="text/javascript">
        var value = 0;
        $("[name='free']").change(function() {
            value = $(this).val();

            if(value == 1){
                $("[name='price']").prop('disabled', true);
            }else{
                $("[name='price']").prop('disabled', false);
            }
            console.log(value);

        });
    </script>
@endsection
