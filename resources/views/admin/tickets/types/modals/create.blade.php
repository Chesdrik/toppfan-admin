<div class="modal fade" id="crear-tipo-boleto" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el tipo de boleto.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Crear tipo de boleto</h5>
                <hr>
            </div>

            <form method="POST" action="{{ route('system.venues.ticket_types.store', [session('system_slug'), $venue->id]) }}" class="row">
                <div class="modal-body">
                    @csrf

                    {!! Form::MDtext('Nombre', 'name', '', $errors, ['class' => 'col-md-10', 'required' => true]) !!}
                    <div class="row col-sm-12">
                        {!! Form::MDselect('Activo', 'active', 1, ['1' => 'Sí', '0' => 'No'], $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDselect('Cortesía', 'free', 0, ['1' => 'Sí', '0' => 'No'], $errors, ['class' => 'col-md-6']) !!}
                    </div>
                    <div class="row col-sm-12">
                        {!! Form::MDtext('Costo', 'price', '', $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDtext('Boletos totales', 'total_seats', '', $errors, ['class' => 'col-md-6']) !!}

                        <small class="col-sm-12" id="error" style="color:red; margin-top: -5%; text-align:right"></small>
                    </div>
                    {!! Form::MDselect('Zona', 'zone_id', '', $venue->zone_options(), $errors, ['class' => 'col-md-6']) !!}
                    <br>

                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <h6>Selecciona los puntos de acceso permitidos para este tipo de boleto</h6>
                    </div>

                    <div class="row col-sm-12">
                        @foreach(App\Checkpoint::checkpointOptionsForVenue($venue->id) as $id => $name)
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="toggle-switch">
                                    <input type="checkbox" class="toggle-switch__checkbox" name="checkpoints[{{$id}}]" value="{{ $id }}">
                                    <i class="toggle-switch__helper"></i>
                                </div>
                                <label style="margin-left:5px;">{{ $name }}</label>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-12 modal-footer text-right">
                    {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
