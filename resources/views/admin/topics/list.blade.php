@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Temas</h1>
        <div class="actions">
            <a href="" data-toggle="modal" data-target="#crear-tema" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Registrar tema
            </a>
            @include('admin.topics.modals.create')
        </div>
    </header>


    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th>&nbsp;</th>
                            <th>Nombre</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($topics as $topic)
                            <tr>
                                <td class="text-center">
                                    <a href="" data-toggle="modal" data-target="#editar-{{ $topic->id }}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $topic->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @foreach($topics as $topic)
        @include('admin.topics.modals.edit')
    @endforeach
@endsection


