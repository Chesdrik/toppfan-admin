<div class="modal fade" id="crear-tema" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el tema.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Crear tema</h5>
                <hr>
            </div> 

            <div class="modal-body">
                <form method="POST" action="{{ url('/admin/temas/') }}" class="row">
                    @csrf
                    {!! Form::MDtext('Nombre', 'name', '', $errors, ['class' => 'col-md-10', 'required' => true]) !!}

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tema padre</label>
                            <div class="select">
                                <select class="form-control" name="parent_topic">
                                    @foreach(\App\Topic::all() as $type )
                                        <option value="{{ $type->id }}">
                                            {{ $type->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>

                    {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                </form>
            </div>
        </div>
    </div>
</div>
