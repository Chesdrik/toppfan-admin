<div class="modal fade" id="editar-{{ $topic->id }}" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo actualizar el tema.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Editar {{ $topic->name }}</h5>
                <hr>
            </div> 

            <div class="modal-body">
                <form method="POST" action="{{ url('/admin/temas/'.$topic->id) }}" class="row">
                    @method('PUT')
                    @csrf

                    {!! Form::MDtext('Nombre', 'name', $topic->name, $errors, ['class' => 'col-md-10', 'required' => true]) !!}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tema</label>
                            <div class="select">
                                <select class="form-control" name="parent_topic">
                                    @foreach(\App\Topic::all() as $type)
                                        <option value="{{ $type->id }}">
                                            {{ $type->name }}
                                        </option>
                                    @endforeach
                                </select>
                                <i class="form-group__bar"></i>
                            </div>
                        </div>
                    </div>

                    {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                </form>
            </div>
        </div>
    </div>
</div>
