@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Usuarios</h1>
        <div class="actions">
            <a href="" data-toggle="modal" data-target="#create-user" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-plus"></i> Crear
            </a>

            @include('admin.users.modals.create')
        </div>
    </header>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered data-table">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-md-1">&nbsp;</th>
                            <th class="col-md-5">Nombre</th>
                            <th class="col-md-4">E-mail</th>
                            <th class="col-md-2">Tipo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td class="text-center">
                                    <a href="{{ route('system.users.show', [session('system_slug'), $user->id]) }}">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->pretty_type() }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $(".data-table").DataTable().order([1, 'asc']).draw();
    });

    @if($errors->any())
        $("#create-user").modal();
    @endif

    // var value = 0;
    // $(".select-edit").click(function() {
    //     value = $(this).val();
    //     id = $(this).attr("data-id");

    //     if(value == 'courtesies'){
    //         document.getElementById('courtesies-'+ id).style.display = "block";
    //     }else{
    //         document.getElementById('courtesies-'+ id).style.display = "none";
    //     }
    // });

    // var value2 = 0;
    // $(".select-create").click(function() {
    //     value2 = $(this).val();

    //     if(value2 == 'courtesies'){
    //         document.getElementById("courtesies").style.display = "block";
    //     }else{
    //         document.getElementById("courtesies").style.display = "none";
    //     }
    // });
</script>
@endsection
