<div class="modal fade" id="create-user" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered"> <!--  modal-dialog-centered -->
        <div class="modal-content">
            @if($errors->any())
                <div class="alert alert-warning" role="alert">
                    No se pudo registrar el usuario.<br />
                    Revisa la información agregada y corrige los campos indicados.
                </div>
            @endif

            <div class="modal-header">
                <h5 class="modal-title pull-left">Crear usuario</h5>
                <hr>
            </div>

            <div class="modal-body">
                {!! Form::open(array('url' => route('system.users.store', [session('system_slug')]), 'method' => 'POST')) !!}
                    {!! Form::MDtext('Nombre', 'name', old('name'), $errors, ['required' => true]) !!}
                    {!! Form::MDemail('Email', 'email', old('email'), $errors, ['required' => true]) !!}
                    {!! Form::MDpassword('Password', 'password', '', $errors, ['required' => true]) !!}
                    {!! Form::MDpassword('Confirmar Password', 'password_confirmation', '', $errors, ['required' => true]) !!}
                    {!! Form::MDselect('Tipo', 'type', old('type'), $user_types, $errors, ['required' => true]) !!}

                    @if(in_array(Auth::user()->type, ['root', 'admin']))
                        {!! Form::MDselectize('Sistemas', 'systems', $systems, old('systems')??[], $errors) !!}
                    @endif

                    {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
