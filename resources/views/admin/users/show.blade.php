@extends('layouts.app')
@section('content')
    <header class="content__title">
        <h1>Editar {{ $user->name }}</h1>
    </header>

    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-body">
                    {!! Form::open(['url' => route('system.users.update', [session('system_slug'), $user->id]), 'method' => 'PUT']) !!}

                        {!! Form::MDtext('Nombre', 'name', old('name') ?? $user->name, $errors, ['required' => true]) !!}
                        {!! Form::MDselect('Tipo', 'type', old('type') ?? $user->type, $user_types, $errors, ['required' => true]) !!}

                        @if(in_array(Auth::user()->type, ['root', 'admin']))
                            {!! Form::MDselectize('Sistemas', 'systems', \App\System::selectize(), old('systems') ?? $user->systems->pluck('id')->toArray(), $errors) !!}
                        @endif

                        {!! Form::MDsubmit('Guardar', 'create', ['icon' => 'card-sd']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
