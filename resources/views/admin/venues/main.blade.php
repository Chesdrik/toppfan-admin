@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-10 offset-sm-1">

        <div class="row">
            <div class="col-sm-4 offset-sm-1">
                <a href="{{ route('system.venues.ticket_types.index', [session('system_slug'), $venue->id]) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                    Tipos de boletos
                </a>
            </div>

            <div class="col-sm-4 offset-sm-1">
                <a href="{{ route('system.venues.templates.index', [session('system_slug'), $venue->id]) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                    Template de boletos
                </a>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4 offset-sm-1">
                <a href="{{ route('system.venues.checkpoints.index', [session('system_slug'), $venue->id]) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                    Checkpoints
                </a>
            </div>

            <div class="col-sm-4 offset-sm-1">
                <a href="{{ route('system.venues.zones.index', [session('system_slug'), $venue->id]) }}" class="btn btn-outline-secondary btn-block btn-light1 btn--raised p-t-30 p-b-30 m-b-20">
                    Zonas
                </a>
            </div>
        </div>

    </div>
</div>
@endsection
