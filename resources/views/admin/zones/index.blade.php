@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Zonas</h1>

        <div class="actions actions--calendar">
            <a href="{{ route('system.venues.zones.download_config', [session('system_slug'), $venue->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                <i class="zmdi zmdi-download"></i> Descargar config
            </a>
        </div>
    </header>

    <div class="row">
    @if(count($venue->parent_zones()) > 0)
        @foreach($venue->parent_zones() as $zone)
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">{{ $zone->name }}</h4>
                        {{-- <h6 class="card-subtitle"></h6> --}}
                        <div class="actions">
                            <a href="{{ route('system.venues.zones.show', [session('system_slug'), $venue->id, $zone->id]) }}" class="actions__item zmdi zmdi-edit"></a>
                        </div>


                        @foreach($zone->childZones() as $child_zone)
                            <div class="row">
                                <div class="col-sm-2 text-center m-b-10">
                                    <a href="{{ route('system.venues.zones.show', [session('system_slug'), $venue->id, $child_zone->id]) }}">
                                        <button class="btn {{ ($child_zone->active ? 'btn-outline-success' : 'btn-outline-secondary') }} btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-sm-10">{{ $child_zone->name }}</div>
                                {{-- <div class="col-sm-2">{{ $child_zone->type }}</div> --}}
                                {{-- <div class="col-sm-2">{{ $child_zone->active }}</div> --}}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
    @else
    <div class="col-sm-12 text-center" style="opacity: 0.5">
        <br>
            NO HAY TEMPLATES REGISTRADOS
    </div>
    @endif
@endsection

@section('after_includes')
<script type="text/JavaScript">
    $(document).ready(function(){
        $("#data-table").DataTable().draw();
    });
</script>
@endsection
