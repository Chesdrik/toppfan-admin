@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>{{ $zone->name }}</h1>
</header>


{!! Form::open(array('url' => route('system.venues.zones.update', [session('system_slug'), $venue->id, $zone->id]), 'method' => 'PUT')) !!}
<div class="row">
    <div class="col-sm-12">
        <div class="card-deck">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        {!! Form::MDtext('Nombre', 'name', $zone->name, $errors) !!}
                        {!! Form::MDselect('Tipo', 'type', $zone->type, ['general' => 'General', 'row' => 'Numerado'], $errors) !!}
                        {!! Form::MDToggleSwitch('¿Activa?', 'active', $zone->active, $errors) !!}
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        {!! Form::MDtext('SVG ID', 'svg_id', $zone->svg_id, $errors) !!}
                        {!! Form::MDtext('Label matrix', 'label_matrix', $zone->label_matrix, $errors) !!}
                        {!! Form::MDtext('Label x', 'label_x', $zone->label_x, $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDtext('Label y', 'label_y', $zone->label_y, $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDColorPicker('Fill', 'fill', $zone->fill, $errors, ['class' => 'col-sm-12 ']) !!}
                        {!! Form::MDtextarea('SVG Content', 'svg_content', $zone->svg_content, $errors) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row m-t-20">
    <div class="col-sm-12">
        {!! Form::MDsubmit('Guardar', 'edit', ['icon' => 'plus-circle-o']) !!}
    </div>
</div>
{!! Form::close() !!}
@endsection
