@extends('layouts.app_clean')

@section('content')
    <div class="col-sm-6 offset-sm-3">
        <header class="content__title">
            <h1>Correo enviado</h1>
        </header>
    </div>

        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body">
                    <p>Se te ha enviado un correo de verificacion, porfavor revisa tu email </p>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
