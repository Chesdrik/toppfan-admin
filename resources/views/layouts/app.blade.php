<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Toppfan | Admin</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- App styles -->
        <link rel="stylesheet" href="{{ Asset('/css/app.css?t='.time()) }}">

        <!-- App JS -->
        <script type='text/javascript'>
            // Dropzone.autoDiscover = false;
        </script>
        <script src="{{ asset('/js/app.js') }}"></script>


        @livewireStyles
	</head>
	<body>
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>

			@yield('modals')
			@include('layouts.includes.header')
			@include('layouts.includes.sidebar')


			<section class="content">
				@if(isset($breadcrumb))
					@include('layouts.includes.breadcrumb')
				@endif
                <div class="row">
                    @include('layouts.includes.success_messages')
                    @include('layouts.includes.error_messages')
                    @include('layouts.includes.session_message')
                </div>
				@yield('content')


                <!-- Includes -->
				@include('layouts.includes.footer')
			</section>
		</main>
    </body>


    <!-- Includes -->
    @yield('after_includes')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ getenv('GOOGLE_ANALYTICS_ID') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', "{{ getenv('GOOGLE_ANALYTICS_ID') }}");
    </script>
</html>
