<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Toppfan | Admin</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

		<!-- App styles -->
		<link rel="stylesheet" href="{{ asset('/css/app_clean.css') }}">

        @livewireStyles
	</head>
	<body>
		@yield('content')
    </body>

    <!-- App JS + Includes -->
    <script src="{{ asset('/js/app.js') }}"></script>
    @yield('after_includes')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{ getenv('GOOGLE_ANALYTICS_ID') }}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', "{{ getenv('GOOGLE_ANALYTICS_ID') }}");
    </script>
</html>
