
<ol class="breadcrumb">
    @foreach($breadcrumb as $bc)
        <li class="breadcrumb-item"><a href="{{ url($bc['url']) }}" @if($bc['active']) active @endif>{{ $bc['label'] }}</a></li>
    @endforeach
</ol>
