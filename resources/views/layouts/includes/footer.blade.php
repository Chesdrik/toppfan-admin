<footer class="footer hidden-xs-down">
    <p>
        © Toppfan
        <br /><br />
        {{ \Carbon\Carbon::now()->format('Y') }} Todos los derechos reservados
    </p>
</footer>
