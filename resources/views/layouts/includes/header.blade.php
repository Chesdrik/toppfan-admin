<header class="header" style="background-color: #1A202C !important;">
    <div class="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
        <div class="navigation-trigger__inner">
            <i class="navigation-trigger__line"></i>
            <i class="navigation-trigger__line"></i>
            <i class="navigation-trigger__line"></i>
        </div>
    </div>

    <div class="header__logo hidden-sm-down">
        {{-- <h1><a href="{{ url('/admin') }}">Pumas<span class="oro">Gol</span></a></h1> --}}
        <h1>
            <a href="{{ (Auth::user()->type == 'root' ? route('root.dashboard') : route('system.select')) }}">
                <img src="{{ asset('/assets/logo/toppfan_logo_invertido.png') }}" style="width: auto; max-height: 60px; padding-top: 10px;" />
                <span style="color: #f44511; font-weight: bold;">topp</span><span style="color: rgba(244, 69, 17, 0.85);">fan</span>
            </a>
        </h1>
    </div>


    <ul class="top-nav">

        <li class="dropdown top-nav__notifications">
            <a href="" data-toggle="dropdown" class="top-nav__notify2">
                <i class="zmdi zmdi-layers"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu--block">
                <div class="listview listview--hover">
                    <div class="listview__header">
                        Sistemas
                    </div>

                    <div class="listview__scroll scrollbar-inner">
                        @foreach(\Auth::user()->associated_systems() as $system)
                            <a href="{{ route('system.dashboard', $system->slug) }}" target="_self" class="listview__item">
                                <div class="listview__content">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <img src="{{ Storage::url($system->img_path(false)) }}" class="img-fluid p-r-15" style="max-height: 50px;">
                                        </div>
                                        <div class="col-xs-8">
                                            <div class="listview__heading">{{ $system->name }}</div>
                                            <p>{{ $system->slug }}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        @endforeach
                    </div>

                    <div class="p-1"></div>
                </div>
            </div>
        </li>
    </ul>
</header>
