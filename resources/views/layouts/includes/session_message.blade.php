@if(session('session_message'))
    <div class="col-sm-12">
        <div class="alert alert-info" role="alert">
            {{ session('session_message') }}
        </div>
    </div>
@endif
