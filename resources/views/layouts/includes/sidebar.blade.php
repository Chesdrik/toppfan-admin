<aside class="sidebar bg-white col-md-3">
    <div class="scrollbar-inner">
        <div class="m-b-15 text-center">
            @if(!is_null(session('system_slug')))
                <?php $system = App\System::bySlug(session('system_slug')); ?>
                <img src="{{ Storage::url($system->img_path(false)) }}" class="header-logo" style="padding-left: 0px;" /> <!-- m-l-35 -->
            @else
                <img src="{{ Asset('/assets/images/logo_pumas.png') }}" class="header-logo" style="padding-left: 0px;" /> <!-- m-l-35 -->
            @endif
            <hr style="border-top: 2px solid {{ $system->config_by_key('primary_color') }};">
        </div>
        <ul class="navigation">

            <?php
                $menu = array();
                $menu[] = ['text' => 'Root Dashboard', 'url' => route('root.dashboard'), 'target' => '_self', 'icon' => 'home', 'name' => 'root.dashboard', 'users' => ['root']];
                $menu[] = ['text' => 'Sistemas', 'url' => route('admin.system.index'), 'target' => '_self', 'icon' => 'layers', 'name' => 'admin.system', 'users' => ['root']];

                if(!is_null(session('system_slug'))){
                    $menu[] = ['text' => 'Dashboard', 'url' => route('system.dashboard', session('system_slug')), 'target' => '_self', 'icon' => 'home', 'name' => 'system.dashboard', 'users' => ['root', 'admin']];
                    $menu[] = ['text' => 'Eventos', 'url' => route('system.events.index', session('system_slug')), 'target' => '_self', 'icon' => 'calendar', 'name' => 'system.events', 'users' => ['root', 'admin', 'courtesies']];
                    $menu[] = ['text' => 'Acreditaciones', 'url' => route('system.accreditations.index', session('system_slug')), 'target' => '_self', 'icon' => 'zmdi zmdi-accounts-list zmdi-hc-fw', 'name' => 'system.accreditations', 'users' => ['root', 'admin']];
                    $menu[] = ['text' => 'Accesos', 'url' => route('system.access.index', session('system_slug')), 'target' => '_self', 'icon' => 'sign-in', 'name' => 'system.access', 'users' => ['root', 'admin']];
                    $menu[] = ['text' => 'Usuarios', 'url' => route('system.users.index', session('system_slug')), 'target' => '_self', 'icon' => 'accounts', 'name' => 'system.users', 'users' => ['root', 'admin', 'courtesies']];

                    // Getting all venues from DB
                    $m = array();
                    if($system->venues()->count() > 1){
                        $m[] = array('text' => 'Todas', 'url' => route('system.venues.index', [session('system_slug')]), 'target' => '_self', 'icon' => 'receipt', 'name' => 'system.venues');
                        foreach($system->venues as $venue){
                            $m[] = array('text' => $venue->name, 'url' => route('system.venues.config', [session('system_slug'), $venue->id]), 'target' => '_self', 'icon' => 'receipt', 'name' => 'system.venues');
                        }
                        $menu[] = ['text' => 'Sedes', 'url' => '#', 'target' => '_self', 'icon' => 'pin-drop', 'name' => 'system.venues', 'submenu' => $m, 'users' => ['root', 'admin', 'client']];
                    }else{
                        $menu[] = ['text' => $system->venues()->first()->name, 'url' => route('system.venues.config', [session('system_slug'), $system->venues()->first()->id]), 'target' => '_self', 'icon' => 'pin-drop', 'name' => 'system.venues', 'users' => ['root', 'admin', 'client']];
                    }


                    // $menu[] = array('text' => 'Configuración', 'url' => '#', 'target' => '_self', 'icon' => 'settings', 'name' => 'system.venues', 'submenu' => $m, 'users' => ['root', 'admin']);
                }

                // // $menu[] = ['text' => 'Boletos', 'url' => '/admin/boletos', 'target' => '_self', 'icon' => 'money-box', 'name' => 'boletos'];
                // // $menu[] = ['text' => 'Cortesías', 'url' => '/admin/boletos/tipos/templates', 'target' => '_self', 'icon' => 'receipt', 'name' => 'cortesias'];
                // // $menu[] = ['text' => 'Sedes', 'url' => '/admin/sedes', 'target' => '_self', 'icon' => 'pin-drop', 'name' => 'sedes'];
                // // $menu[] = ['text' => 'Precios', 'url' => '/admin/precios', 'target' => '_self', 'icon' => 'money', 'name' => 'precios'];

                // $menu[] = ['text' => 'Estadísticas', 'url' => route('system.statistics.index'), 'target' => '_self', 'icon' => 'zmdi zmdi-chart zmdi-hc-fw', 'name' => 'accesos', 'users' => ['root', 'admin']];



                // $menu[] = ['text' => 'Soporte', 'url' => '/soporte', 'target' => '_self', 'icon' => 'zmdi zmdi-storage zmdi-hc-fw', 'name' => 'soporte', 'users' => ['client']];
                // $menu[] = ['text' => 'Soporte', 'url' => '#', 'target' => '_self', 'icon' => 'zmdi zmdi-storage zmdi-hc-fw', 'name' => 'soporte', 'submenu' => [
                //     ['text' => 'Tickets', 'url' => '/admin/soporte', 'target' => '_self', 'icon' => '', 'name' => 'soporte', 'users' => ['root', 'admin']],
                //     ['text' => 'Temas', 'url' => '/admin/temas', 'target' => '_self', 'icon' => '', 'name' => 'tema', 'users' => ['root', 'admin']],
                // ], 'users' => ['root', 'admin']];
                // dump(Auth::user());
            ?>

            @foreach($menu as $m)
                @if(in_array(Auth::user()->type, $m['users']))
                    @php
                    $class = (strpos(Route::currentRouteName(), $m["name"]) === 0) ? "navigation__active" : '';
                    $class = (isset($m['submenu'])) ? $class." navigation__sub" : $class.'';
                @endphp
                    <li class= {{ $class }}>
                        <a href="{{ url($m['url']) }}">
                            <i class="zmdi zmdi-{{ $m['icon'] }}"></i> {{ $m['text'] }}
                            {{-- <br><small> {{ $m["name"] }} </small> --}}
                        </a>

                        @if(isset($m['submenu']))
                            <ul>
                                @foreach($m['submenu'] as $sm)
                                    <li @if(strpos(Route::currentRouteName(), $sm["name"]) !== false) class="navigation__active" @endif>
                                        <a href="{{ url($sm['url']) }}" target="{{ $sm['target'] }}">- {{ $sm['text'] }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif

                    </li>
                @endif
            @endforeach

            <li>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <hr />

                    <button class="btn logout-button" type="submit">
                        <i class="zmdi zmdi-lock-outline logout-icon"></i> Logout
                    </button>
                </form>
            </li>
        </ul>
    </div>
</aside>
