@if(session('success'))
    <div class="col-sm-12">
        <div class="alert alert-success" role="alert">
            {{ session('success') }}
        </div>
    </div>
@endif
