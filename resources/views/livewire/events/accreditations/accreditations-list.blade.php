<div>
    <table class="table table-bordered table-sm table-hover">
        <thead class="thead-default">
            <tr>
                <th class="col-sm-1">&nbsp;</th>
                <th class="col-sm-2">Folio</th>
                <th class="col-sm-4">Usuario</th>
                <th class="col-sm-3">E-mail</th>
                <th class="col-sm-2">Tipo de cortesía</th>
            </tr>
        </thead>
        <tbody>
            @foreach($accreditations as $accreditation)
                <tr>
                    <td>
                        <div class="btn-group">
                            <a href="" data-toggle="modal" data-target="#editar-{{ $accreditation->id }}">
                                <button class="btn btn-outline-secondary btn--raised">
                                    <i class="zmdi zmdi-edit"></i>
                                </button>
                            </a>
                            <a href="{{ route('system.events.accreditations.show', [session('system_slug'), $event->id, $accreditation->id]) }}" target="_self">
                                <button class="btn btn-outline-secondary btn--raised">
                                    <i class="zmdi zmdi-eye"></i>
                                </button>
                            </a>
                        </div>
                    </td>
                    <td>{{ $accreditation->folio }}</td>
                    <td>{{ $accreditation->pivot->name??$accreditation->name }}</td>
                    <td>{{ $accreditation->pivot->email??$accreditation->email }}</td>
                    <td>{{ $accreditation->ticket_type->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="pagination justify-content-center m-t-15">
        {!! $accreditations->links() !!}
    </div>

    @foreach($accreditations as $accreditation)
        @include('admin.accreditations.modals.edit')
    @endforeach
</div>
