<div>
    <table class="table table-bordered table-sm table-hover">
        <thead class="thead-default">
            <tr>
                <th class="col-md-1">&nbsp;</th>
                <th class="col-md-1">ID</th>
                <th class="col-md-3">Nombre</th>
                <th class="col-md-4">E-mail</th>
                <th class="col-md-3">Boletos</th>
            </tr>
        </thead>
        <tbody>
            @foreach($courtesies as $order)
                <tr>
                    <td class="text-center">
                        <a href="{{ route('system.orders.show', [session('system_slug'), $order->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                            <i class="fas fa-eye"></i>
                        </a>
                    </td>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->client_user->name }}</td>
                    <td>{{ $order->email }}</td>
                    <td>
                        @foreach($order->tickets as $ticket)
                            {{ $ticket->amount }} x {{ $ticket->ticket_type->name }}<br />
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="pagination justify-content-center m-t-15">
        {!! $courtesies->links() !!}
    </div>
</div>
