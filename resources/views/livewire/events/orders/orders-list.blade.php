<div>
    <table class="table table-bordered table-sm table-hover">
        <thead class="thead-default">
            <tr>
                <th class="col-sm-1">&nbsp;</th>
                <th class="col-sm-1">ID</th>
                <th class="col-sm-2">Usuario</th>
                <th class="col-sm-2">Pago</th>
                <th class="col-sm-2">Fecha</th>
                <th class="col-sm-4">Boletos</th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
                <tr>
                    <td>
                        <div class="btn-group">
                            <a href="{{ route('system.orders.show', [session('system_slug'), $order->id]) }}" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                <i class="fas fa-eye"></i>
                            </a>
                            <a href="{{ route('system.events.order_qrs', [session('system_slug'), $event->id, $order->id]) }}" target="_new" class="btn btn-outline-secondary btn--raised btn--icon-text">
                                <i class="fas fa-qrcode"></i>
                            </a>
                        </div>
                    </td>
                    <td>{{ $order->id }}</td>
                    <td>{{ $order->client_user->full_name() }}</td>
                    <td>{{ $order->pretty_payment_method() }}</td>
                    <td>{{ $order->created_at  }}</td>
                    <td>
                        <table class="table">
                            <tbody>
                                @foreach($order->tickets as $ticket)
                                    <tr>
                                        <td class="text-right">{{ $ticket->amount }} x{{ $ticket->ticket_type->name }}</td>
                                        <td class="text-right">{{ pretty_money($ticket->amount*$ticket->price) }}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="text-right">Total:</td>
                                    <td class="text-right">
                                        <strong>{{ pretty_money($order->total) }}</strong>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="pagination justify-content-center m-t-15">
        {!! $orders->links() !!}
    </div>
</div>
