
    <table class="table table-bordered table-sm table-hover">
        <thead class="thead-default">
            <tr>
                <th class="col-md-1">&nbsp;</th>
                <th class="col-md-2">Folio</th>
                <th class="col-md-3">Usuario</th>
                <th class="col-md-4">E-mail</th>
                <th class="col-md-2">Zona</th>
            </tr>
        </thead>
        <tbody>
            @foreach($seasson_tickets as $seasson_ticket)
            <tr>
                <td>
                    <a href="{{ route('system.events.season.tickets.show', [session('system_slug'), $event->id, $seasson_ticket->id]) }}" target="_self">
                        <button class="btn btn-outline-secondary btn--raised">
                            <i class="zmdi zmdi-eye"></i>
                        </button>
                    </a>
                </td>
                <td>{{ $seasson_ticket->folio }}</td>
                <td>{{ $seasson_ticket->pivot->name??$seasson_ticket->name }}</td>
                <td>{{ $seasson_ticket->pivot->email??$seasson_ticket->email }}</td>
                <td>{{ $seasson_ticket->ticket_type->name }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="pagination justify-content-center m-t-15">
        {!! $courtesies->links() !!}
    </div>
</div>
