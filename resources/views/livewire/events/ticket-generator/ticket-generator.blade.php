<div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        Generar boletos
                    </h4>

                    Ticket generator for {{ $this->eventId }}!

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="orderAmount" class="col-form-label">Cantidad de órdenes</label>
                                <input type="number" class="form-control checkout-form" placeholder="Cantidad de órdenes" wire:model.defer="orderAmount">
                                <i class="form-group__bar"></i>
                                @error('orderAmount') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>

                            {{-- boletos por orden
                            tipo de ticket
                            tipo = stunam, aapaunam, otro
                            ticket_price --}}

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="card_number" class="col-form-label">Número de tarjeta</label>
                                <input type="number" class="form-control checkout-form" placeholder="Número de tarjeta" wire:model.defer="card_number">
                                <i class="form-group__bar"></i>
                                @error('card_number') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    resultados...
                </div>
            </div>
        </div>
    </div>
</div>
