<div>
    <div class="col-sm-12 px-3">
        
        @if(session('ticket_error'))
        <div class="col-sm-11 offset-1">
            <div class="alert alert-danger" role="alert">
                {{ session('ticket_error') }}
            </div>
        </div>
        @endif
        
        <div class="card" style="box-shadow:none">
            <div class="card-body">
                @if ($ticketGroup->available == 0)
                <div class="col-sm-12">
                    <div class="alert alert-info" role="alert">
                        Ya se han asignado todos los boletos de este grupo.
                    </div>    
                </div>
                @else
                <form wire:submit.prevent="form_validation">
                    <div class='row'>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="name" class="col-form-label">Nombre(s)</label>
                                <input type="text" class="form-control checkout-form" wire:model.defer="name">
                                <i class="form-group__bar"></i>
                                @error('name') <span style="color: red">{{ $message }}</span> @enderror
                            </div> 
                        </div> 
                        
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="last_name" class="col-form-label">Apellido(s)</label>
                                <input type="text" class="form-control checkout-form"  wire:model.defer="last_name">
                                <i class="form-group__bar"></i>
                                @error('last_name') <span style="color: red">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="email" class="col-form-label">Email</label>
                                <input type="text" class="form-control checkout-form"  wire:model.defer="email">
                                <i class="form-group__bar"></i>
                                @error('email') <span style="color: red">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="email_confirmation" class="col-form-label">Confirmación de email</label>
                                <input type="text" class="form-control checkout-form"  wire:model.defer="email_confirmation">
                                <i class="form-group__bar"></i>
                                @error('email_confirmation') <span style="color: red">{{ $message }}</span> @enderror
                            </div>
                        </div> 
                        
                        <div class="col-sm-4 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="amount" class="col-form-label">Cantidad</label>
                                <input type="text" class="form-control checkout-form"  wire:model.defer="amount">
                                <i class="form-group__bar"></i>
                                @error('amount') <span style="color: red;">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        @if ($ticketGroup->available != 0)
                            <div class="col-md-12 text-right mt-3">
                                <button type="submit" class="btn btn-lg btn-primary">Enviar</button>
                            </div>
                        @endif
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
    <x-livewire-alert::scripts />
</div>

