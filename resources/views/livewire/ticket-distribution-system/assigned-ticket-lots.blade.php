<div>
    <div class="col-sm-12 gruposAsignadosBox">
        <div class="accordion mx-5" id="asignadosCollapseBox" >
            <div class="card"><br>
                <h3 class="ml-4">Grupos de boletos asignados</h3>
                
                @if(session('lot_success'))
                <br>
                <div class="col-sm-11 offset-1">
                    <div class="alert alert-success" role="alert">
                        {{ session('lot_success') }}
                    </div>
                </div>
                @endif
                
                <div class="card-body shadow">
                    @if (!is_null($assignedTicketLots))
                    <div class="table-responsive">
                        <table class="table table-bordered data-table">
                            <thead class="thead-default">
                                <tr>
                                    <th>Nombre</th>
                                    <th>E-mail</th>
                                    <th>Cantidad</th>
                                    <th>Tipo de boleto</th>
                                    @if (!$ticketGroupId)
                                        <th>&nbsp;</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($assignedTicketLots as $assignedTicketLot)
                                <tr>
                                    <td>{{ $assignedTicketLot['name'] }}</td>
                                    <td>{{ $assignedTicketLot['email'] }}</td>
                                    <td>{{ $assignedTicketLot['amount'] }}</td>
                                    <td>{{ $assignedTicketLot['ticketTypeName'] }}</td>
                                    @if (!$ticketGroupId)
                                        <td>
                                            {!! Form::open(array('url' => route('tds.email.resend'), 'method' => 'POST')) !!}
                                                <button class="btn btn-primary" type="submit">
                                                    <i class="zmdi zmdi-mail-send zmdi-hc-fw"></i>
                                                    <span><b>Reenviar email</b></span>
                                                </button>

                                                {!! Form::hidden('email', $assignedTicketLot['email']) !!}
                                                {!! Form::hidden('type', 'group') !!}
                                                {!! Form::hidden('ticket_group_id', $assignedTicketLot['ticket_group_id']) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    @endif
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <div class="col-sm-12 text-center" style="opacity: 0.5">
                        <div class="card">
                            <div class="card-body">
                                AÚN NO HAS ASIGNADO NINGÚN GRUPO DE BOLETOS
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .gruposAsignadosBox i{
        position: absolute;
        font-size: 21px;
        top: 37px;
        float: left;
        left: 9px;
    }
    .dataTables_length:after, .dataTables_filter:after {
        bottom: 0px !important;
        font-size: 2.1rem !important;
    }
    .dataTables_length > label select, .dataTables_length > label input[type=search], .dataTables_filter > label select, .dataTables_filter > label input[type=search]{
        font-size: 21px  !important;
        padding: 0px 0px 10px 39px  !important;
        font-weight: 200 !important;
        color: #484848;
    }
</style>