<div class="col-sm-12 asignadosAfansBox">
    <div class="accordion mx-5" id="enviadosAfansCollapseBox" >
        <div class="card"><br>
            <h3 class="ml-4">Boletos enviados a fans</h3>
            @if(session('ticket_success'))
                <div class="col-sm-11 offset-1">
                    <br>
                    <div class="alert alert-success" role="alert">
                        {{ session('ticket_success') }}
                    </div>
                </div>
            @endif

            <div class="card-body shadow">
                @if ($totalAssignedTickets > 0)
                    <div class="table-responsive">
                        <table class="table table-bordered data-table">
                            <thead class="thead-default">
                                <tr>
                                    @if (!$ticketGroupId)
                                        <th>ID</th>
                                    @endif
                                    <th>Nombre</th>
                                    <th>E-mail</th>
                                    <th>Cantidad</th>
                                    <th>Tipo de boleto</th>
                                    @if (!$ticketGroupId)
                                        <th>&nbsp;</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($assignedTickets as $assignedTicket)
                                    <tr>
                                        @if (!$ticketGroupId)
                                            <td>{{ $assignedTicket['ticket_id'] }}</td>
                                        @endif
                                        <td>{{ $assignedTicket['name'] }}</td>
                                        <td>{{ $assignedTicket['email'] }}</td>
                                        <td>{{ $assignedTicket['amount'] }}</td>
                                        <td>{{ $assignedTicket['ticketTypeName'] }}</td>
                                        @if (!$ticketGroupId)
                                            <td>
                                                {!! Form::open(array('url' => route('tds.email.resend'), 'method' => 'POST')) !!}
                                                    <button class="btn btn-primary" type="submit">
                                                        <i class="zmdi zmdi-mail-send zmdi-hc-fw"></i>
                                                        <span><b>Reenviar email</b></span>
                                                    </button>

                                                    {!! Form::hidden('email', $assignedTicket['email']) !!}
                                                    {!! Form::hidden('type', 'fan') !!}
                                                    {!! Form::hidden('order_id', $assignedTicket['order_id']) !!}
                                                {!! Form::close() !!}
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="col-sm-12 text-center" style="opacity: 0.5">
                        <div class="card">
                            <div class="card-body">
                                AÚN NO HAS ENVIADO NINGÚN BOLETO
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

<style>
    .asignadosAfansBox i{
        position: absolute;
        font-size: 21px;
        top: 37px;
        float: left;
        left: 27px;
    }
    .dataTables_length:after, .dataTables_filter:after {
        bottom: 0px !important;
        font-size: 2.1rem !important;
    }
</style>
