<div class="col-sm-12">
    @if (!is_null($assignedTicketLots))
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered data-table tablaAsignar">
                        <thead class="thead-default">
                            <tr>
                                <th>Estatus</th>
                                <th>Tipo de boleto</th>
                                <th>Evento</th>
                                <th>Disponibilidad</th>
                                <th>¿Puede distribuir?</th>
                                
                                <th> &nbsp; </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($assignedTicketLots as $assignedTicketLot)
                                <tr>
                                    <td class="text-center">
                                        @if ($assignedTicketLot['available'] == 0)
                                            <i class="fas fa-check" data-type="info"></i>
                                        @else
                                            <i class="fas fa-stopwatch" data-type="success"></i>
                                        @endif
                                    </td>
                                    <td>{{ $assignedTicketLot['ticketTypeName'] }}</td>
                                    <td>{{ $assignedTicketLot['eventName'] }}</td>
                                    <td> <b>{{ $assignedTicketLot['available'] }} </b> de <b>{{ $assignedTicketLot['amount'] }}</b></td>
                                    <td>
                                        @if ($assignedTicketLot['distributable'] )
                                            <span style="color:green; font-size: 20px;">Sí</span>
                                        @else
                                            <span style="color:green; font-size: 20px;">No</span>
                                        @endif
                                    </td>
                                    <td>
                                        
                                        @if ($assignedTicketLot['available'] == 0)
                                        <a href="{{ route('tds.assign', $assignedTicketLot['ticket_group_id']) }}" class="btn btn-light  w-100 rounded">
                                             <i class="zmdi zmdi-assignment zmdi-hc-fw"></i> Ver asignación
                                         </a>
                                            @else
                                            <a href="{{ route('tds.assign', $assignedTicketLot['ticket_group_id']) }}" style="position: relative; z-index:5" class="btn btn-primary w-100 rounded">
                                             <i class="zmdi zmdi-mail-send zmdi-hc-fw"></i> Asignar boletos
                                            </a>
                                            @endif
                                        </a>&nbsp;&nbsp;
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="col-sm-12 text-center" style="opacity: 0.5">
        <div class="card">
            <div class="card-body">
                NO TIENES ASIGNADO NINGÚN GRUPO DE BOLETOS
            </div>
        </div>
    </div>
    @endif

</div>

<style>
    .tablaAsignar i.fas{
        font-size: 21px;
    }
</style>









