<div class="col-sm-12">

    <div class="col-sm-12">
        <header class="content__title">
            <h1>Boletos</h1>
        </header>
    </div>

    @if (count($assignedTickets) > 0)
    <div class="col-sm-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered data-table">
                        <thead class="thead-default">
                            <tr>
                                {{-- <th class="col-md-5">Evento</th>
                                <th class="col-md-4">Email</th>
                                <th class="col-md-3">Cantidad</th>
                                <th class="col-md-3">Tipo</th> --}}
                                <th>Evento</th>
                                <th>Email</th>
                                <th>Cantidad</th>
                                <th>Tipo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($assignedTickets as $assignedTicket)
                                <tr>
                                    <td>{{ $assignedTicket['eventName'] }}</td>
                                    <td>{{ $assignedTicket['email'] }}</td>
                                    <td>{{ $assignedTicket['amount'] }}</td>
                                    <td>{{ $assignedTicket['ticketTypeName'] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="col-sm-12 text-center" style="opacity: 0.5">
        <div class="card">
            <div class="card-body">
                NO TIENES ASIGNADO NINGÚN BOLETO
            </div>
        </div>
    </div>
    @endif

</div>
