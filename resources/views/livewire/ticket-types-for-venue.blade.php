<div>
    {!! Form::MDselect('Sede', 'venue_id', '', $venues->pluck('name', 'id')->prepend('Selecciona una sede', 0), $errors, ['wire-model' => 'selectedVenue']) !!}

    @if(!is_null($selectedVenue))
        {!! Form::MDselect('Template boletos', 'ticket_template_id', '', $ticketTypes, $errors) !!}
    @endif
</div>
