@extends('layouts.app')

@section('content')
    <div class="col-sm-6 offset-sm-3">
        <header class="content__title">
            <h1>Crear un ticket de soporte</h1>
        </header>
    </div>


    <div class="row">
        @if(count($errors) > 0)
            <div class="col-sm-6 offset-sm-3">
                <div class="col-sm-12">
                    <div class="alert alert-danger" role="alert">
                        Se encontraron errores con los datos, por favor revisa la información ingresada.
                    </div>
                </div>
            </div>
        @endif

        <div class="col-sm-6 offset-sm-3">
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ url('/soporte/') }}" class="row">
                        @csrf

                        {!! Form::MDtext('Nombre', 'name', Auth::user()->name, $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDtext('Descripcion', 'description', '', $errors, ['class' => 'col-md-6']) !!}
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Tema</label>
                                <div class="select">
                                    <select class="form-control" name="topic_id">
                                        @foreach(\App\Topic::all() as $type)
                                            <option value="{{ $type->id }}">
                                                {{ $type->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        {!! Form::MDsubmit('Crear', 'create', ['icon' => 'plus-circle-o']) !!}
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('after_includes')
<!-- Javascript -->
<!-- Vendors: Data tables -->
<script src="{{ Storage::url('assets/material/vendors/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/datatables-buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/datatables-buttons/buttons.print.min.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/jszip/jszip.min.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/datatables-buttons/buttons.html5.min.js') }}"></script>
@endsection
