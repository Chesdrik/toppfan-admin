@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Soporte</h1>
        <!-- <small>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tincidunt odio magna, at condimentum libero congue nec. Fusce at dolor velit.
        </small> -->
        <div class="actions">
            <a href="{{ url('/soporte/create') }}" class="actions__item zmdi zmdi-plus-circle-o"></a>
        </div>
    </header>


    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="data-table" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th class="col-sm-1">&nbsp;</th>
                            <th>Nombre</th>
                            <th>Tema</th>
                            <th class="col-sm-2">Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($supports as $support)
                            <tr>
                                <th class="text-center">
                                    <a href="{{ url('/soporte/'.$support->id.'/mensajes') }}" target="_self">
                                        <button class="btn btn-outline-secondary btn--raised">
                                            <i class="zmdi zmdi-edit"></i>
                                        </button>
                                    </a>
                                </th>
                              
                                <td>{{ $support->user->name }}</td>
                                <td>{{ $support->topic->name }}</td>
                                <td>{{ $support->created_at }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
<!-- Javascript -->
<script src="{{ Storage::url('assets/material/vendors/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/datatables-buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/datatables-buttons/buttons.print.min.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/jszip/jszip.min.js') }}"></script>
<script src="{{ Storage::url('assets/material/vendors/datatables-buttons/buttons.html5.min.js') }}"></script>
@endsection
