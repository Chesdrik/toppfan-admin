@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-body">
        <div class="messages">           
            <div class="messages__body">
                    <div class="messages__header">
                        <div class="toolbar toolbar--inner mb-0">
                            <div class="toolbar__label">Soporte</div>

                            <div class="actions toolbar__actions">
                                      
                                <a href="" class="actions__item zmdi zmdi-time"></a>
                                <button class="btn btn-outline-primary btn-sm" type="button" data-toggle="modal" data-target="#status">
                                Estado: {{ $support->pretty_type() }}
                            </button>
                                       
                            </div>
                        </div>
                    </div>

                    <div class="messages__content">
                        <div class="scrollbar-inner">
                           
                            @foreach($conversation as $message)
                                    
                                @if($message->users->first()->id != Auth::user()->id )
                                    <div class="messages__item">
                                                    <img src="demo/img/profile-pics/1.jpg" class="avatar-img" alt="">

                                        <div class="messages__details">
                                            <p>{{ $message->message }}</p>
                                            <small><i class="zmdi zmdi-time"></i>{{ $message->created_at }}</small>
                                        </div>
                                    </div>  
                                @else
                                    <div class="messages__item messages__item--right">
                                        <div class="messages__details">
                                            <p>{{ $message->message }}</p>
                                            <small><i class="zmdi zmdi-time"></i>{{ $message->created_at }}</small>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>

                    <div class="messages__reply">

                    <form action="{{ url('/soporte/'.$message->support_id.'/enviar') }}" method="post">
                        @csrf
                         <textarea id="description" name="description" class="messages__reply__text" placeholder="Type a message..."></textarea>
                        {!! Form::MDsubmit('enviar', 'send', ['icon' => 'zmdi zmdi-mail-send zmdi-hc-fw']) !!}
                                   
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

 @include('admin.supports.status')

@endsection