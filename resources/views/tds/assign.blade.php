@extends('layouts.app_clean')
<script src="https://kit.fontawesome.com/57ff8a41d9.js" crossorigin="anonymous"></script>
@section('content')


<header style="background-color:#1A202C;height: 80px;" class="content__title mb-0 pb-2" >
    <h1 style="position:relative;top: 5px;color:white">Sistema de distribución de boletos</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <i class="zmdi zmdi-view-dashboard zmdi-hc-fw"></i>
            <a href="{{ route('tds') }}">Inicio</a>
        </li>
        <li class="breadcrumb-item active">Asignación grupo {{ $ticketGroup->ticket_type->name }}</li>
    </ol>
    <div class="actions">
        <ul class="nav">
            <div style="margin-top:20px; margin-left:20px">
                <li class="nav-item" style="color: white;font-size: 14px">
                    Bienvenid@, {{ Auth::guard('client_users')->user()->name }} {{ Auth::guard('client_users')->user()->last_name }}
                </li>
            </div>
            <form method="POST" action="{{ route('tds.logout') }}" style="margin-top:15px; margin-left: 30px">
                @csrf
                <button class="btn btn-outline-secondary" type="submit" style="color: #ffffff;border-color: #868e96;">
                    <i class="zmdi zmdi-lock-outline logout-icon"></i> Logout
                </button>
            </form>
        </ul>
    </div>
</header>

<div>
    <div class="container-fluid px-0 mb-5 border-0"> <!-- card -->
        <div class="row bg-light">

                <div class="w-100 row bg-white  border-bottom">

                        <ul class="nav nav-tabs nav-fill col-md-6  w-100 distribucionOpciones" role="tablist">
                            @if ($ticketGroup && $ticketGroup->distributable == 1)
                            <li class="nav-item">
                                <a class="nav-link {{ (session('ticket_error') || session('ticket_success')) ? 'show' : 'active show' }}" data-toggle="tab" href="#assign-group" role="tab" aria-selected="true">
                                    <i class="fas fa-layer-group"></i><br><br>
                                    <span>Asignar grupo</span>
                                </a>
                            </li>
                            @endif
                            <li class="nav-item">
                                <a class="nav-link {{ (session('ticket_error') || session('ticket_success')) || $ticketGroup->distributable == 0 ? 'active show' : 'show' }}" data-toggle="tab" href="#assign-fan" role="tab" aria-selected="false">
                                    <i class="fas fa-user-tag"></i><br><br>
                                    <span>Enviar boletos al fan</span>
                                </a>
                            </li>
                        </ul>

                       <div class="col-md-6">
                            <div class="vsBoxAsignar row text-center d-flex justify-content-center">
                                <div class="col-6">
                                    <img src="{{ asset($ticketGroup->event->img_path()) }}" class="header-logo" style="padding-left: 0px; margin-right:15px;"><br>
                                    {{-- <span>{{ $ticketGroup->event->name }}</span> --}}
                                </div>
                            </div>
                    </div>
                </div>


                <div class="tab-content p-0  mt-5 w-100 mx-5">

                    @if ($ticketGroup && $ticketGroup->distributable == 1)

                        <div class="tab-pane m-0 px-4  {{ (session('ticket_error') || session('ticket_success')) ? 'fade' : 'fade active show' }}" id="assign-group" role="tabpanel">

                            <h2 class="mx-4 mb-4">Asigna grupos de boletos | Zona: {{ $ticketGroup->ticket_type->name }}</h2>

                            <div class="row bg-white">
                                <div class="col-sm-5 p-0 border-right">
                                    <livewire:ticket-distribution-system.assign-ticket-group :ticketGroupId='$ticketGroupId' />
                                </div>

                                <div class="col-sm-7 p-0">
                                    <div class="col-md-12 mb-2" style="background-color: #eef8ff;">
                                        <div class="row resumenHeader py-2 pl-5">
                                            <div class="col-md-6 h-100 d-flex align-items-center">
                                                <div class="align-self-center">
                                                    <p>Disponibles:</p>
                                                    <h3><b>{{ $ticketGroup->available }}</b> de {{ $ticketGroup->amount }}</h3>
                                                </div>
                                            </div>
                                            <div class="col-md-6 h-100 d-flex align-items-center">
                                                <div class="align-self-center">
                                                    <p>Asignados:</p>
                                                    <h3>{{ $ticketGroup->assigned }}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <livewire:ticket-distribution-system.assigned-ticket-lots :ticketGroupId='$ticketGroupId' :eventId='$eventId'  />
                                </div>
                            </div>

                        </div>

                    @endif

                    <div class="tab-pane  m-0 px-4 {{ (($ticketGroup && $ticketGroup->distributable == 0) || (session('ticket_error')) || (session('ticket_success'))) ? 'active show' : 'fade' }}" id="assign-fan" role="tabpanel">
                            <h2 class="mx-4 mb-4">Entrega boletos a fans | Zona: {{ $ticketGroup->ticket_type->name }}</h2>

                            <div class="row bg-white">
                                <div class="col-sm-5 p-0  border-right">
                                    <livewire:ticket-distribution-system.assign-ticket-to-fan :ticketGroupId='$ticketGroupId' />
                                </div>

                                <div class="col-sm-7  p-0">
                                    <div class="col-md-12 mb-2">
                                        <div class="row resumenHeader  pl-5">
                                            <div class="col-md-6 h-100 d-flex align-items-center">
                                                <div class="align-self-center">
                                                    <p>Disponibles:</p>
                                                    <h3><b>{{ $ticketGroup->available }}</b> de {{ $ticketGroup->amount }}</h3>
                                                </div>
                                            </div>
                                            <div class="col-md-6 h-100 d-flex align-items-center">
                                                <div class="align-self-center">
                                                    <p>Enviados a fans:</p>
                                                    <h3>{{ $ticketGroup->sent_to_fans }}</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <livewire:ticket-distribution-system.assigned-tickets :ticketGroupId='$ticketGroupId' :eventId='$eventId'  />
                                </div>
                            </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>

@endsection

@section('after_includes')
    @livewireScripts

<style>
.resumenHeader{
        background-color: #eef8ff;
    }
    .resumenHeader p{
        font-size: 14px !important;
    }
    .vsBoxAsignar{
        float: right;
        right: 20px;
        position: relative;
        top: 12px;
    }
    .vsAsignarAdmin{
        position: absolute;
        font-size: 25px;
        top: 10px;
    }

    .distribucionOpciones i{
        font-size: 28px;
    }
    .distribucionOpciones span{
        font-size: 18px !important;
        font-weight: bold;
    }
</style>
@endsection

