<h4>Evento: {{ $event_name }}</h4>
<table></table>

<h4>Boletos enviados a fans</h4>
<h5>Total de boletos enviados: {{ $total }}</h5>
<table></table>

<table>
    <thead>
    <tr>
        <th><b>ID</b></th>
        <th><b>Fan</b></th>
        <th><b>Email</b></th>
        <th><b>Tipo de boleto</b></th>
        <th><b>Cantidad</b></th>
        <th><b>Asignó</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($assigned_tickets as $assigned_ticket)
        <tr>
            <td style="text-align: left">{{ $assigned_ticket['ticket_id'] }}</td>
            <td>{{ $assigned_ticket['name'] }}</td>
            <td>{{ $assigned_ticket['email'] }}</td>
            <td>{{ $assigned_ticket['ticketTypeName'] }}</td>
            <td style="text-align: left">{{ $assigned_ticket['amount'] }}</td>
            <td>{{ $assigned_ticket['assigned_by'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>