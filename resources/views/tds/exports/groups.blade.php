<h4>Evento: {{ $event_name }}</h4>
<table></table>

<h4>Grupos de boletos asignados a distribuidores</h4>
<h5>Total de grupos asignados: {{ $total }}</h5>
<h5>Total de boletos distribuidos: {{ $total_assigned }}</h5>
<table></table>

<table>
    <thead>
    <tr>
        <th><b>Distribuidor</b></th>
        <th><b>Email</b></th>
        <th><b>Tipo de boleto</b></th>
        <th><b>Cantidad</b></th>
        <th><b>Asignó</b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($ticket_groups as $ticket_group)
        <tr>
            <td>{{ $ticket_group['name'] }}</td>
            <td>{{ $ticket_group['email'] }}</td>
            <td>{{ $ticket_group['ticketTypeName'] }}</td>
            <td style="text-align: left">{{ $ticket_group['amount'] }}</td>
            <td>{{ $ticket_group['assigned_by'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>