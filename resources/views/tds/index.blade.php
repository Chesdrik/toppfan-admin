@extends('layouts.app_clean')
<script src="https://kit.fontawesome.com/57ff8a41d9.js" crossorigin="anonymous"></script>
@section('content')

        <header style="background-color:#1A202C;height: 67px;" class="content__title mb-0">
            <h1 style="position:relative;top: 5px;color:white">Sistema de distribución de boletos</h1>
            
            <div class="actions">
                <ul class="nav">
                    <div style="margin-top:20px; margin-left:20px">
                        <li class="nav-item">
                            Bienvenid@, {{ Auth::guard('client_users')->user()->name }} {{ Auth::guard('client_users')->user()->last_name }}
                        </li>
                    </div>
                    <form method="POST" action="{{ route('tds.logout') }}" style="margin-top:15px; margin-left: 30px">
                        @csrf
                        <button class="btn btn-outline-secondary" type="submit" style="color: #ffffff;border-color: #868e96;">
                            <i class="zmdi zmdi-lock-outline logout-icon"></i> Logout
                        </button>
                    </form>
                </ul>
            </div>
        </header>
  
    <div >  <!-- class="content__inner" style="margin-top:3%" -->
        <div class="card container-fluid px-0 bg-light">

            <h1 class="px-5 pt-5 pb-5" style="font-size:2.5em">Asignación de boletos</h1>

            @if(session('success'))
            <br>
            <div class="col-sm-10 offset-1">
                <div class="alert alert-success" role="alert">
                    {{ session('success') }}
                </div>
            </div>
            @endif
            
            @if(session('error'))
            <br>
            <div class="col-sm-10 offset-1">
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            </div>
            @endif

            <div class="card-body row d-block px-0 bg-light pt-0 px-3">
                <div class="row col-sm-12 offset-11" style="margin-bottom: 5px;">
                    <button class="btn shadow bg-white resumenHomeButton" data-toggle="modal" data-target="#download-history">
                        <i class="zmdi zmdi-file-text zmdi-hc-fw" aria-hidden="true"></i>
                    </button>
                </div>
                <div class="w-100 d-flex justify-content-center p-3 " style="position: fixed;bottom: 0px;z-index: 2;background: rgb(255,255,255);background: linear-gradient(270deg, rgba(255,255,255,0) 23%, rgba(255,255,255,1) 50%, rgba(255,255,255,0) 75%);">
                    <button class="btn shadow bg-white resumenHomeButton" data-toggle="modal" data-target="#resuemnHomeModal">
                        <i class="fas fa-list-alt" aria-hidden="true"></i>
                    </button>
                </div>
                
                <div class="active show" id="show-assigned-tickets" role="tabpanel"> <!-- tab-pane -->
                    <livewire:ticket-distribution-system.show-assigned-ticket-lots :eventId='$eventId'  />
                </div>

            </div>
        </div>

    </div>


    <!-- Modal -->
    <div class="modal fade" id="resuemnHomeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modalDialogResumenHome w-100 p-5" role="document">
            <div class="modal-content">
                <div class="modal-header px-0">
                    <h1 class="modal-title border-bottom w-100 pb-4 px-3" id="exampleModalLongTitle"> <b>Resumen de asignación de boletos</b> </h1>
                    <button type="button" class="close m-0" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="" id="assigned-tickets" role="tabpanel">
                        <livewire:ticket-distribution-system.assigned-tickets :ticketGroupId='$ticketGroupId' :eventId='$eventId'  />
                        
                        <livewire:ticket-distribution-system.assigned-ticket-lots :ticketGroupId='$ticketGroupId' :eventId='$eventId'  />
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-lg" data-dismiss="modal">Regresar</button>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('after_includes')
    @livewireScripts
    @include('tds.modals.history')
@endsection

<style>
.modal-dialog.modalDialogResumenHome{
    margin: 0px !important;
    max-width: 100% !important;;
}
.btn.resumenHomeButton:not([class*="btn-outline-"]) {
    border: 1px solid #2196F3 !important;
}
.resumenHomeButton{
    border-radius: 100px !important;
    color: #2196F3 !important;
    font-size: 21px !important;
        width: 50px;
    height: 50px;
}
.distribucionOpciones i{
    font-size: 28px;
}
.distribucionOpciones span{
    font-size: 18px !important;
    font-weight: bold;
}
</style>











