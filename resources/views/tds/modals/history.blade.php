<div class="modal fade" id="download-history" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(array('url' => route('tds.export'), 'method' => 'POST')) !!}

            <div class="modal-header">
                <h1 class="modal-title"> <b>Histórico de asignaciones</b> </h1>
            </div>
            
            <div class="modal-body">
                <div class="col-sm-12"><b>Selecciona el evento y el tipo de reporte que deseas generar</b></div><br>
               {!! Form::MDselect('Evento', 'event_id', '', $events, $errors) !!}
               {!! Form::MDselect('Tipo de reporte', 'type', '', ['fans' => 'Boletos enviados a fans', 'group' => 'Boletos asignados a distribuidores'], $errors) !!}
               {!! Form::hidden('user_id', Auth::guard('client_users')->user()->id) !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary">Exportar</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>