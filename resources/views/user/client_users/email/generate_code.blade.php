<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>




<style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}

    /* iOS BLUE LINKS */
    .appleBody a {color:#68440a; text-decoration: none;}
    .appleFooter a {color:#999999; text-decoration: none;}

    /* MOBILE STYLES */
    @media screen and (max-width: 525px) {

        /* ALLOWS FOR FLUID TABLES */
        table[class="wrapper"]{
          width:100% !important;
        }

        /* ADJUSTS LAYOUT OF LOGO IMAGE */
        td[class="logo"]{
          text-align: left;
          padding: 20px 0 20px 0 !important;
        }

        td[class="logo"] img{
          margin:0 auto!important;
        }

        /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
        td[class="mobile-hide"]{
          display:none;}

        img[class="mobile-hide"]{
          display: none !important;
        }

        img[class="img-max"]{
          max-width: 100% !important;
          height:auto !important;
        }

        /* FULL-WIDTH TABLES */
        table[class="responsive-table"]{
          width:100%!important;
        }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        td[class="padding"]{
          padding: 10px 5% 15px 5% !important;
        }

        td[class="padding-copy"]{
          padding: 10px 5% 10px 5% !important;
          text-align: center;
        }

        td[class="padding-meta"]{
          padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        td[class="no-pad"]{
          padding: 0 0 20px 0 !important;
        }

        td[class="no-padding"]{
          padding: 0 !important;
        }

        td[class="section-padding"]{
          padding: 50px 15px 50px 15px !important;
        }

        td[class="section-padding-bottom-image"]{
          padding: 50px 15px 0 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        td[class="mobile-wrapper"]{
            padding: 10px 5% 15px 5% !important;
        }

        table[class="mobile-button-container"]{
            margin:0 auto;
            width:100% !important;
        }

        a[class="mobile-button"]{
            width:80% !important;
            padding: 15px !important;
            border: 0 !important;
            font-size: 16px !important;
        }

    }
</style>








<!-- HEADER -->
<table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td bgcolor="#1b2855">
            <div style="padding: 0px 15px 0px 15px;" align="center">
                <table class="wrapper" width="500" cellspacing="0" cellpadding="0" border="0">
                    <!-- LOGO/PREHEADER TEXT -->
                    <tbody><tr>
                        <td style="padding: 20px 0px 30px 0px;" class="logo">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tbody><tr>
                                    <td width="100" bgcolor="#1b2855" align="left">

                                            <img alt="Logo" src="{{ $message->embed(asset('assets/images/logo_pumas.png')) }}" style="display: block; font-family: Arial, sans-serif; color: #fff; font-size: 16px;" width="70" border="0">
                                            {{-- <img alt="Logo" src="{{ public_path('assets/images/logo_pumas.png') }}" style="display: block; font-family: Arial, sans-serif; color: #fff; font-size: 16px;" width="70" border="0"> --}}

                                        </td>
                                    <td class="mobile-hide" width="400" bgcolor="#1b2855" align="right">
                                        <table cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                                <tr>
                                                <td style="padding: 0 0 5px 0; font-size: 24px;font-weight: bold; font-family: Arial, sans-serif; color: #1b2855; text-decoration: none;" align="right">


                                                    <span style="color: #fff; text-decoration: none;"> ¡Hola, </span>
                                                    <span style="color: #fff; text-decoration: none;"> {{ $data['name'] }}!</span>


                                            </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody></table>
            </div>
        </td>
    </tr>
</tbody></table>

<!-- ONE COLUMN SECTION -->
<table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td class="section-padding" bgcolor="#ffffff" align="center">
                <table class="responsive-table" width="700" height="400" cellspacing="0" cellpadding="0" border="0" style="background-image: url('https://headtopics.com/images/2020/8/7/milenio/pumas-70-anos-de-la-primera-piedra-del-estadio-ol-mpico-universitario-1291839531671486465.webp');background-position: center; background-size: 170%; background-repeat: no;">
                    <tbody>
                        <tr>
                            <td style="background-color: rgba(0, 0, 0, 0.6);">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <!-- HERO IMAGE -->
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td class="padding-copy">
                                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody><tr>
                                                                        <td>

                                                                                    <!-- <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/48935/responsive-email.jpg" width="500" height="200" border="0" alt="Can an email really be responsive?" style="display: block; padding: 0; color: #666666; text-decoration: none; font-family: Arial, sans-serif; font-size: 16px; width: 500px; height: 200px;" class="img-max"> -->

                                                                        </td>
                                                                        </tr>
                                                                    </tbody></table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <!-- COPY -->
                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="font-size: 32px; font-family: Arial, sans-serif; color: #333333; padding-top: 30px; color:white" class="padding-copy" align="center">
                                                                <img alt="Logo" src=" {{ $message->embed(asset('assets/images/mail/tickets.png')) }} " style="display: block; font-family: Arial, sans-serif; color: #fff; font-size: 16px;" width="120" border="0"><br>
                                                                <span style="font-weight: bold; position: relative; top: 35px;">
                                                                    Recibimos una solicitud para activar tu cuenta.
                                                                </span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <!-- BULLETPROOF BUTTON -->
                                                <table class="mobile-button-container" width="100%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <td style="padding: 25px 0 0 0;" class="padding-copy" align="center">
                                                                <table class="responsive-table" cellspacing="0" cellpadding="0" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center">&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>




<!-- ONE COLUMN W/ BOTTOM IMAGE SECTION -->
<table style="table-layout: fixed;" width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody><tr>
        <td style="padding: 70px 15px 0px 15px;" class="section-padding-bottom-image" bgcolor="#f8f8f8" align="center">
            <table class="responsive-table" width="500" cellspacing="0" cellpadding="0" border="0" style="padding-bottom: 70px;">
                <tbody><tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tbody><tr>
                                <td>
                                    <!-- COPY -->
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td style="font-size: 28px; font-family: Arial, sans-serif; font-weight: bold;color: #1b2855;" class="padding-copy" align="center">
                                                ¿Qué sigue?
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Arial, sans-serif; color: #666666;" class="padding-copy" align="center">        <span>

                                            </span>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                        <tr>
                                            <td align="center"  valign="middle">
                                                <img src="{{ $message->embed(asset('assets/images/mail/phone.png')) }} "  style="display: block; color: #fff; font-family: Helvetica, arial, sans-serif; font-size: 13px;  " width="280px"  height="400px"    alt="Fluid images" border="0" class="img-max">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding: 15px; font-family: Arial, sans-serif; color: #fff; font-size: 20px;" bgcolor="#1b2855">
                                                Utiliza el código {{ $data['code'] }} para ingresar desde el app y ver tus boletos.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="padding: 30px; font-family: Arial, sans-serif; color: #fff; font-size: 14px; line-height: 20px;" bgcolor="#1b2855">
                                                <span class="appleBody"><span style="font-family: Helvetica, Arial, sans-serif; ">
                                                        &nbsp;
                                                </span>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                            <!-- <tr>
                                <td>
                                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                        <tbody><tr>
                                            <td style="padding: 50px 0 0 0;" align="center">
                                                <a href="http://alistapart.com/article/can-email-be-responsive/" target="_blank"><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/48935/line-graph.jpg" alt="Mobile opens are on the rise" class="img-max" style="display: block; padding: 0; font-family: Arial, sans-serif; color: #666666; width: 500px; height: 180px;" width="500" height="180" border="0"></a>
                                            </td>
                                        </tr>
                                    </tbody></table>
                                </td>
                            </tr> -->
                        </tbody></table>
                    </td>
                </tr>
            </tbody></table>
        </td>
    </tr>
</tbody></table>

<table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 70px 15px 70px 15px;" class="section-padding">
            <table border="0" cellpadding="0" cellspacing="0" width="600" class="responsive-table">
                <p style="font-family:'Lato',sans-serif;font-size:18px;color:#7f8c8d;line-height:24px;font-weight:300">
                    Si tienes dudas, escríbenos a <a href="#m_-5830132917227954914_m_-7671692612187574691_">fans@toppfan.com</a>
                </p>
            </table>
        </td>
    </tr>
</table>






<!-- FOOTER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
    <tr>
        <td bgcolor="#1b2855" align="center">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td style="padding: 20px 0px 20px 0px;">
                        <!-- UNSUBSCRIBE COPY -->
                        <table width="500" border="0" cellspacing="0" cellpadding="0" align="center" class="responsive-table">
                            <tr>
                                <td align="center" valign="middle" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#fff;">
                                    <span class="appleFooter" style="color:#fff;">© {{ date('Y') }} {{ 'pumas' }}. @lang('Todos los derecho reservados.')</span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



</body>
</html>
