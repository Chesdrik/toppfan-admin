@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Mi Perfil</h1>
    </header>

    <div class="row">
        @if(session('success'))
            <div class="col-sm-6 offset-sm-3">
                <div class="col-sm-12">
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif

        <div class="col-sm-6 offset-sm-3">
            <header class="content__title">
                <h1>{{ $user->name }}</h1>

                <div class="actions">

                    <div class="dropdown actions__item">
                        <i data-toggle="dropdown" class="zmdi zmdi-more-vert" aria-expanded="false"></i>
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(30px, 26px, 0px);">
                            <a href="{{ url('/usuarios/'.$user->id.'/desactivar_cuenta') }}" class="dropdown-item">Desactivar mi cuenta</a>
                        </div>
                    </div>
                </div>
            </header>

            <div class="card">
                <div class="card-body">
                    {!! Form::open(array('url' => '/usuarios/'.$user->id.'/mi_perfil/update', 'method' => 'post', 'class' => 'row')) !!}
                        {!! Form::MDtext('Nombre', 'name', $user->name, $errors, ['class' => 'col-md-6']) !!}
                        {!! Form::MDemail('Email', 'email', $user->email, $errors, ['class' => 'col-md-6']) !!}

                        {!! Form::MDsubmit('Guardar', 'save') !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
