@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Confirmación de compra</h1>
</header>

<div class="content__inner">

    <div class="invoice">

        <div class="row">

            <div class="col-md-6">
                <div class="invoice__header">
                   {{--  <img class="card-img-top" src="{{ $event->img_path() }}" alt="" style="max-height:200px;"> --}}
                    <img class="invoice__logo" src="{{ $event->img_path() }}" alt="" style="max-width: 70%; max-height:100%;">
                </div>
            </div>
    
            <div class="col-md-6">
                <div class="row invoice__address">
                    <div class="col-12">
                        <div class="text-left"><br><br><br>
                        <h4>{{ $event->name }}</h4>
                            <address>
                                {{ pretty_date($event->date) }}
                            </address>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table table-bordered invoice__table">
                <thead>
                    <tr class="text-uppercase">
                        <th>TIPO DE BOLETO</th>
                        <th>PRECIO UNITARIO</th>
                        <th>CANTIDAD</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total = 0;
                    @endphp
                    @foreach($data as $item)
                    <tr>
                        @php
                            $total += $item['ticket']->pivot->price * $item['total'];
                        @endphp
                        <td style="width: 50%">{{ $item['ticket']->name }}</td>
                        <td>{{ pretty_money($item['ticket']->pivot->price) }}</td>
                        <td>{{ $item['total'] }}</td>
                        <td>{{ pretty_money($item['ticket']->pivot->price * $item['total']) }}</td>
                    </tr>
                    @endforeach
                    <td></td>
                    <td></td>
                    <td></td>
                    <td> <b>{{ pretty_money($total) }}</b></td>
                </tbody>
            </table>
        </div>

        <div class="text-right">
            {{-- <a href="http://127.0.0.1:8000/admin/dashboard" style="margin-right: 2%;">
                <button type="button" class="btn btn-light">
                    <span><i class="zmdi zmdi-long-arrow-left zmdi-hc-fw"></i></span>
                    &nbsp;&nbsp; Regresar
                </button>
            </a> --}}

            <a href="{{ route('pay_form', $event->id) }}">
                <button class="btn btn-info">
                    &nbsp;&nbsp;&nbsp;&nbsp; Continuar &nbsp;&nbsp;
                    <span><i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i></span>
                </button>
            </a>

        </div>
        
    </div>
</div>
@endsection

@section('after_includes')
<!-- Javascript -->
<script type="text/javascript">


</script>
@endsection
