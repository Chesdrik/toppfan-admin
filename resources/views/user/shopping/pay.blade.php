@extends('layouts.app')

@section('content')
<header class="content__title">
    <h1>Pago</h1>
</header>

<div class="content__inner">

    <div class="invoice">
        
        
        <div class="row">
            <div class="col-md-12">
                <h4>Evento: {{ $event->name }}</h4> <br>
                Para procesar el pago es necesario ingresar los siguientes datos. <br><br>
            </div>
            
            {!! Form::open(array('url' => route('pay_order', $event->id), 'method' => 'post', 'class' => 'row col-md-12')) !!}
            
            {!! Form::MDtext('Titular', 'name', '', $errors, ['class' => 'col-md-5 offset-1', 'required' => true]) !!}
            {!! Form::MDtext('Número de tarjeta', 'name', '', $errors, ['class' => 'col-md-5']) !!}
            {!! Form::MDselect('Mes', 'month', '', months() , $errors, ['class' => 'col-md-1 offset-1']) !!}
            {!! Form::MDselect('Año', 'year', '', years() , $errors, ['class' => 'col-md-1']) !!}
            {!! Form::MDtext('CVV', 'cvv', '', $errors, ['class' => 'col-md-1']) !!}


        </div>

        <div class="text-right">
            <a href="{{ route('confirm_form') }}" style="margin-right: 2%;">
                <button type="button" class="btn btn-light">
                    <span><i class="zmdi zmdi-long-arrow-left zmdi-hc-fw"></i></span>
                    &nbsp;&nbsp; Regresar
                </button>
            </a>

            <button type="submit" class="btn btn-info">
                &nbsp;&nbsp;&nbsp;&nbsp; Pagar &nbsp;&nbsp;
                <span><i class="zmdi zmdi-long-arrow-right zmdi-hc-fw"></i></span>
            </button>

        </div>

        </form>
        
    </div>
</div>
@endsection

@section('after_includes')
<!-- Javascript -->
<script type="text/javascript">


</script>
@endsection
