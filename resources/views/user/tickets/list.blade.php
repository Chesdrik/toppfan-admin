@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Mis Boletos</h1>
    </header>
    <!-- <div class="toolbar">
        <div class="toolbar__nav">
            <a href="{{ route('tickets.old.user', ['id' => Auth::user()->id]) }}" target="_self">
                Boletos de eventos anteriores
            </a>
        </div>
    </div> -->

    @foreach($orders as $order)
    <div class="row">
        <div class="col-sm-12 col-md-8 offset-md-2">
            <header class="content__title text-center match-header">
                <h1>{{ $order->event->name }}</h1>
                <small>{{ $order->event->datespan() }}</small>


                <a href="{{ url('/admin/ordenes/'.$order->id) }}">
                    <button class="btn btn-secondary btn--icon"><i class="zmdi zmdi-search"></i></button>
                </a>
            </header>
        </div>


        <div class="col-sm-12 col-md-8 offset-md-2">
            @foreach($order->tickets as $ticket)
                <div class="card card-tickets">
                    <div class="card-body">
                        <div class="cardTWrap">
                            <div class="cardT cardTLeft">
                                <h1><small>
                                    {{ $ticket->amount }} x
                                </small> {{ $ticket->ticket_type->name }}</h1>
                                <div class="title">
                                    <h2>{{ $ticket->event->name }}</h2>
                                    <span>Partido</span>
                                </div>
                                <div class="name">
                                    <h2>{{ Auth::user()->name }}</h2>
                                    <span>nombre</span>
                                </div>
                                <div class="seat">
                                    <h2>{{ $ticket->ticket_type->name }}</h2>
                                    <span>Asiento</span>
                                </div>
                                <div class="time">
                                    <h2>{{ $ticket->event->datespan() }}</h2>
                                    <span>Fecha / Hora</span>
                                </div>
                            </div>
                            <div class="cardT cardTRight">
                                <div class="eye"></div>
                                <div class="number">
                                    <h3>
                                        {{ $ticket->ticket_type->name }}
                                    </h3>
                                    <span>Asiento</span>
                                </div>
                                <div class="barcode"></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            <!-- <a href="{{ route('ticket.view.user', ['id' => $ticket->id]) }}"><i class="zmdi zmdi-eye"></i></a> -->
        </div>
    </div>
    @endforeach
@endsection
