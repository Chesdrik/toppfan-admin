@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Mis Boletos de eventos anteriores</h1>
    </header>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="data-table" class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th>Evento</th>
                            <th>Sección</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tickets as $ticket)
                            <tr>
                                <th>{{ $ticket->event->name }}</th>
                                <th>{{ $ticket->ticket_type->name }}</th>
                                <th style="text-align: center;"><a href="{{ route('ticket.view.user', ['id' => $ticket->id]) }}"><i class="zmdi zmdi-eye"></i></a></th>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('after_includes')
<!-- Javascript -->
<!-- <script src="{{ Storage::url('assets/material/vendors/datatables/jquery.dataTables.min.js') }}"></script> -->
@endsection
