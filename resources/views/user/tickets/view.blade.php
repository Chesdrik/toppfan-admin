@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Boleto para {{ $ticket->event->name }}</h1>
    </header>
    <div class="card">
        <div class="card-body">
            <div class="cardTWrap">
                <div class="cardT cardTLeft">
                  <h1>{{ $ticket->ticket_type->name }}</h1>
                  <div class="title">
                    <h2>{{ $ticket->event->name }}</h2>
                    <span>Partido</span>
                  </div>
                  <div class="name">
                    <h2>{{ Auth::user()->name }}</h2>
                    <span>nombre</span>
                  </div>
                  <div class="seat">
                    <h2>{{ $ticket->ticket_type->name }}</h2>
                    <span>Aciento</span>
                  </div>
                  <div class="time">
                    <h2>{{ $ticket->event->date }}</h2>
                    <span>Hora</span>
                  </div>

                </div>
                <div class="cardT cardTRight">
                  <div class="eye"></div>
                  <div class="number">
                    <h3>{{ $ticket->ticket_type->name }}</h3>
                    <span>Asiento</span>
                  </div>
                  <div class="barcode"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('after_includes')
<!-- Javascript -->
<!-- <script src="{{ Storage::url('assets/material/vendors/datatables/jquery.dataTables.min.js') }}"></script> -->
@endsection
