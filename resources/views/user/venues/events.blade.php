@extends('layouts.app')

@section('content')
    <div class="row">

        @if(session('success'))
            <div class="col-sm-6 offset-sm-3">
                <div class="col-sm-12">
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                </div>
            </div>
        @endif

        <div class="col-sm-12">
            <header class="content__title">
                <h1>Eventos de {{ $venue->pretty_team() }}</h1>

                <div class="actions actions--calendar">
                    <a href="{{ route('system.events.create', $venue->id) }}" class="actions__item zmdi zmdi-plus-circle-o"></a>
                    {{-- <a href="" class="actions__item actions__calender-prev"><i class="zmdi zmdi-long-arrow-left"></i></a>
                    <a href="" class="actions__item actions__calender-next"><i class="zmdi zmdi-long-arrow-right"></i></a> --}}
                </div>
            </header>
        </div>

        <div class="row">
            @if(count($venue->events_by_status(1)) > 0)
                @foreach($venue->events_by_status(1) as $event)
                <div class="col-md-4">
                    <div class="card">
                        <img class="card-img-top" src="{{ $event->img_path() }}" alt="" style="max-height:200px;">
                        <div class="card-body">
                            <h4 class="card-title">{{ $event->name }}</h4>
                            <h6 class="card-subtitle">{{ pretty_date($event->date) }}</h6>

                            <p>Donec ullamcorper nulla non metus auctor fringilla. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Vestibulum id ligula porta felis euismod semper. Nulla vitae elit libero, a pharetra.</p>
                            {!! Form::MDButton('Comprar boletos', 'sede_detalle', route('tickets_sale', [$venue->id, $event->id]), ['class' => 'btn btn-light btn--raised pull-right']) !!}

                        </div>
                    </div>
                </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection


@section('after_includes')
<!-- Calendar Script -->
<script type="text/javascript">
    'use strict';
    $(document).ready(function() {
        var date = new Date();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('.calendar').fullCalendar({
            locale: 'es',
            header: false,
            buttonIcons: {
                prev: 'calendar__prev',
                next: 'calendar__next'
            },
            theme: false,
            selectable: true,
            selectHelper: true,
            editable: true,
            events: [
                @foreach($venue->events_by_status(1) as $event)
                {
                    id: {{ $event->id }},
                    // title: '{{ $event->name }} - {{ pretty_short_hour($event->date) }}',
                    title: '{{ $event->name }}',
                    start: '{{ $event->date_start() }}',
                    end: '{{ $event->date_end() }}',
                    allDay: false,
                    className: 'bg-blue',
                    description: '{{ $event->name }} - {{ pretty_short_hour($event->date) }}',
                },
                @endforeach
            ],

            // dayClick: function(date) {
            //     var isoDate = moment(date).toISOString();
            //     $('#new-event').modal('show');
            //     $('.new-event__title').val('');
            //     $('.new-event__start').val(isoDate);
            //     $('.new-event__end').val(isoDate);
            // },

            viewRender: function (view) {
                var calendarDate = $('.calendar').fullCalendar('getDate');
                var calendarMonth = calendarDate.month();

                //Set data attribute for header. This is used to switch header images using css
                $('.calendar .fc-toolbar').attr('data-calendar-month', calendarMonth);

                //Set title in page header
                $('.content__title--calendar > h1').html(view.title);
            },

            eventClick: function (event, element) {
                console.log('Display modal #event-'+event.id);
                // console.log(element);
                // $('#event-'+event.id+' input[value='+event.className+']').prop('checked', true);
                $('#event-'+event.id).modal('show');
                // $('.edit-event__id').val(event.id);
                // $('.edit-event__title').val(event.title);
                // $('.edit-event__description').val(event.description);
            }
        });


        //Add new Event
        // $('body').on('click', '.new-event__add', function(){
        //     var eventTitle = $('.new-event__title').val();
        //
        //     // Generate ID
        //     var GenRandom =  {
        //         Stored: [],
        //         Job: function(){
        //             var newId = Date.now().toString().substr(6); // or use any method that you want to achieve this string
        //
        //             if( !this.Check(newId) ){
        //                 this.Stored.push(newId);
        //                 return newId;
        //             }
        //             return this.Job();
        //         },
        //         Check: function(id){
        //             for( var i = 0; i < this.Stored.length; i++ ){
        //                 if( this.Stored[i] == id ) return true;
        //             }
        //             return false;
        //         }
        //     };
        //
        //     if (eventTitle != '') {
        //         $('.calendar').fullCalendar('renderEvent', {
        //             id: GenRandom.Job(),
        //             title: eventTitle,
        //             start: $('.new-event__start').val(),
        //             end:  $('.new-event__end').val(),
        //             allDay: true,
        //             className: $('.event-tag input:checked').val()
        //         }, true);
        //
        //         $('.new-event__form')[0].reset();
        //         $('.new-event__title').closest('.form-group').removeClass('has-danger');
        //         $('#new-event').modal('hide');
        //     }
        //     else {
        //         $('.new-event__title').closest('.form-group').addClass('has-danger');
        //         $('.new-event__title').focus();
        //     }
        // });


        //Update/Delete an Event
        // $('body').on('click', '[data-calendar]', function(){
        //     var calendarAction = $(this).data('calendar');
        //     var currentId = $('.edit-event__id').val();
        //     var currentTitle = $('.edit-event__title').val();
        //     var currentDesc = $('.edit-event__description').val();
        //     var currentClass = $('#edit-event .event-tag input:checked').val();
        //     var currentEvent = $('.calendar').fullCalendar('clientEvents', currentId);
        //
        //     //Update
        //     if(calendarAction === 'update') {
        //         if (currentTitle != '') {
        //             currentEvent[0].title = currentTitle;
        //             currentEvent[0].description = currentDesc;
        //             currentEvent[0].className = currentClass;
        //
        //             $('.calendar').fullCalendar('updateEvent', currentEvent[0]);
        //             $('#edit-event').modal('hide');
        //         }
        //         else {
        //             $('.edit-event__title').closest('.form-group').addClass('has-error');
        //             $('.edit-event__title').focus();
        //         }
        //     }
        //
        //     //Delete
        //     if(calendarAction === 'delete') {
        //         $('#edit-event').modal('hide');
        //         setTimeout(function () {
        //             swal({
        //                 title: 'Are you sure?',
        //                 text: "You won't be able to revert this!",
        //                 type: 'warning',
        //                 showCancelButton: true,
        //                 buttonsStyling: false,
        //                 confirmButtonClass: 'btn btn-danger',
        //                 confirmButtonText: 'Yes, delete it!',
        //                 cancelButtonClass: 'btn btn-secondary'
        //             }).then(function() {
        //                 $('.calendar').fullCalendar('removeEvents', currentId);
        //                 swal({
        //                     title: 'Deleted!',
        //                     text: 'Your list has been deleted.',
        //                     type: 'success',
        //                     buttonsStyling: false,
        //                     confirmButtonClass: 'btn btn-primary'
        //                 });
        //             })
        //         }, 200);
        //     }
        // });


        //Calendar views switch
        $('body').on('click', '[data-calendar-view]', function(e){
            e.preventDefault();

            $('[data-calendar-view]').removeClass('actions__item--active');
            $(this).addClass('actions__item--active');
            var calendarView = $(this).attr('data-calendar-view');
            $('.calendar').fullCalendar('changeView', calendarView);
        });


        //Calendar Next
        $('body').on('click', '.actions__calender-next', function (e) {
            e.preventDefault();
            $('.calendar').fullCalendar('next');
        });


        //Calendar Prev
        $('body').on('click', '.actions__calender-prev', function (e) {
            e.preventDefault();
            $('.calendar').fullCalendar('prev');
        });
    });
</script>


@endsection
