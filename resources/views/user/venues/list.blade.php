@extends('layouts.app')

@section('content')
    <header class="content__title">
        <h1>Equipos</h1>
    </header>

    <div class="row">
        @foreach($venues as $venue)
            <div class="col-sm-4">
                <div class="card">
                    <img class="card-img-top" src="https://maps.googleapis.com/maps/api/staticmap?center={{ $venue->lat }},{{ $venue->long }}&zoom=15&size=600x350&key=AIzaSyAKavmRXai4uFCCv8T2Y1I3YjDta9LqlzI" />

                    <div class="card-body">
                        <h4 class="card-title" style="height: 40px;">{{ $venue->pretty_team() }}</h4>
                        <h6 class="card-subtitle">{{ $venue->lat }}, {{ $venue->long }}</h6>
                        <p class="card-text">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                        <br />
                        {{-- {!! Form::MDButton('Comprar boletos', 'sede_detalle', '/admin/sedes/'.$venue->id, ['class' => 'btn btn-light btn--raised pull-right']) !!} --}}
                        {!! Form::MDButton('Ver eventos', 'sede_detalle', '/usuarios/sedes/'.$venue->id.'/eventos', ['class' => 'btn btn-light btn--raised pull-right']) !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection

@section('after_includes')
<!-- Javascript -->
<!-- <script src="{{ Storage::url('assets/material/vendors/datatables/jquery.dataTables.min.js') }}"></script> -->
@endsection
