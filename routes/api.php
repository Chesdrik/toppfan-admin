<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'UserController@loginAPI');
Route::post('/events', 'EventController@eventsAPI');
// Route::post('/event/{id}', 'EventController@eventAPI');
Route::post('/access', 'APIController@access');
Route::post('/my_tickets', 'APIController@tickets');
Route::post('/order/{id}', 'APIController@order');
Route::post('/order/details/{id}', 'APIController@order_qr_sva');
Route::post('/next/tickets/{id}', 'APIController@nextTickets');
Route::post('/past/tickets/{id}', 'APIController@pastTickets');
Route::post('/all/orders/{id}', 'APIController@allOrders')->name('all.orders');
// Route::post('/qr_read', 'APIController@QRRead');
Route::post('event/{id}', 'APIController@getEvent');
Route::post('event/{id}/register_order', 'APIController@registerOrder');
Route::post('add/order', 'APIController@add_order');




// Checkpoints data functions
Route::post('/checkpoints/{id}/battery', 'CheckpointController@batteryStatus');
Route::post('/checkpoints/{id}/{status}', 'CheckpointController@checkpointStatus');

// Tickets functions
Route::post('/tickets/access', 'TicketController@verify_access');

// Stats functions
Route::post('/events/stats', 'StatisticController@statsForEvent');
Route::post('/teams/stats', 'StatisticController@getStats');
Route::post('/venues/stats', 'StatisticController@getStats');
Route::post('/all/stats', 'StatisticController@statsAllEvents');

// Alerts
Route::post('/alerts/register', 'APIController@register_alert');

// Register client
Route::post('/client/register', 'UserController@register_client');

// POS APIs
Route::prefix('/pos')->group(function () {
    // Home
    Route::post('/home', 'POS\POSController@home');

    // Login
    Route::post('/login', 'POS\POSController@login');

    // Events
    Route::post('/events', 'POS\EventController@events'); // Checks ticket availability
    Route::post('/events/slideshow', 'POS\EventController@eventsSlideshow'); // Checks ticket availability
    Route::post('/event', 'POS\EventController@event'); // Checks ticket availability

    // Event groups
    Route::post('/event_groups', 'POS\EventGroupController@event_groups');
    Route::post('/event_groups/{slug}', 'POS\EventGroupController@event_group');
    Route::post('/event_group', 'POS\EventGroupController@event_group_sale');

    // Orders
    Route::post('/orders/active', 'POS\OrderController@active_orders'); // Fetches active orders for user
    Route::post('/orders/inactive', 'POS\OrderController@inactive_orders'); // Fetches innactive orders for user

    // General
    Route::post('/seating_layout', 'POS\POSController@seating_layout');
    // Route::post('/timeout', 'POS\POSController@timeout');

    // Checkout
    Route::prefix('/checkout')->group(function () {
        // Events and Event Groups
        Route::post('/seat_selection', 'POS\CheckoutController@seat_selection'); // Checks ticket availability
        Route::post('/register_order', 'POS\CheckoutController@register_order'); // Process order
        Route::post('/extend_expiration', 'POS\CheckoutController@extend_expiration'); // Extends expiration time of current products
    });

    // Order details
    Route::post('/order/{id}/details', 'POS\OrderController@order_details'); // Order details
});

Route::prefix('/pda')->group(function () {
    Route::post('/accreditations', 'PDAController@accreditations'); // Gets all accreditations for event
});

Route::prefix('/sync')->group(function () {
    // QRServer functions
    Route::post('/users', 'UserController@syncUsers');
    Route::post('/events', 'EventController@syncEvents');
    Route::post('/checkpoints', 'CheckpointController@syncCheckpoints');
    Route::post('/events/{id}/checkpoints', 'CheckpointController@syncEventCheckpoints');
    Route::post('/ticket_types', 'TicketController@syncTicketTypes');
    Route::post('/tickets', 'TicketController@syncTickets');
    Route::post('/ticket_readings', 'TicketController@syncTicketReadings');
    Route::post('/statistics', 'StatisticController@syncStatistics');
    Route::post('/venues', 'VenueController@syncVenues');
    Route::post('/checkpoints/types', 'CheckpointController@checkpointTicket');
    Route::post('/accreditation_readings', 'AccreditationController@syncAcreditationReadings');
    Route::post('/acreditation_version', 'APIController@updateAcreditationVersion');

    // Access functions
    Route::post('/acccess/list', 'Access\AccessController@syncAccessList'); // Sync registered access
    Route::post('/acccess', 'Access\AccessController@syncAccess'); // Upload access
    Route::post('/exits', 'Access\AccessController@syncExits'); // Upload exits
});

// QRServer APIs
Route::prefix('/qrserver')->group(function () {
    Route::post('/sync/clientusers/', 'QRServerController@syncClientUsers'); // Checks ticket availability
    Route::post('/event/finish', 'QRServerController@finish_event'); // Finish event
});

Route::prefix('/app')->group(function () {
    Route::post('/home', 'AppControllers\AppController@home');
    Route::post('/privacy', 'AppControllers\AppController@privacy');
    Route::post('/news', 'AppControllers\AppController@news');
    Route::post('/events', 'AppControllers\AppController@events');
    Route::post('/tickets', 'AppControllers\AppController@tickets');

    Route::prefix('/login')->group(function () {
        Route::post('/', 'AppControllers\LoginController@login'); // Log in for active user
        Route::post('/check/mail', 'AppControllers\LoginController@check_mail'); // Triggers mail with code for validation
        Route::post('/check/code', 'AppControllers\LoginController@check_code'); // Validates user against code sent from mail
    });

    // Route::post('/register', 'AppControllers\LoginController@register');
    // Route::post('/forgot_password', 'AppControllers\LoginController@forgot_password');
});
