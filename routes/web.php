<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

Auth::routes(['verify' => true]);


Route::middleware('auth')->group(function () {
    Route::get('/', 'SiteController@home');
    Route::get('/elegir_sistema', 'SystemController@index')->name('system.select');
    Route::get('/dashboard/root', 'AdminController@root_dashboard')->name('root.dashboard');
});

Route::name('admin.')->middleware('auth')->group(function () {
    Route::resource('/sistemas', 'SystemController', ['names' => 'system']);
});

// TDS Routes
Route::post('/tds/activacion', 'TicketDistributionSystem\TicketDistributionSystemController@account_activation')->name('tds.account.activation');
Route::get('/tds/{id}/registro', 'TicketDistributionSystem\TicketDistributionSystemController@register')->name('tds.register');
Route::post('/tds/logout', 'TicketDistributionSystem\TicketDistributionSystemController@logout')->name('tds.logout');
Route::post('/tds/login', 'TicketDistributionSystem\TicketDistributionSystemController@login')->name('tds.login');
Route::get('/tds/login', 'TicketDistributionSystem\TicketDistributionSystemController@login_form')->name('tds.login.form');
Route::get('/tds', 'TicketDistributionSystem\TicketDistributionSystemController@index')->middleware('auth:client_users')->name('tds');
Route::get('/tds/{id}/asignar', 'TicketDistributionSystem\TicketDistributionSystemController@assign')->middleware('auth:client_users')->name('tds.assign');

// TDS password reset
Route::get('/tds/password/request', 'TicketDistributionSystem\TicketDistributionSystemController@password_request_form')->name('tds.password.request.form');
Route::get('/tds/{id}/password/reset', 'TicketDistributionSystem\TicketDistributionSystemController@password_reset_form')->name('tds.password.reset.form');
Route::post('/tds/email/reenviar', 'TicketDistributionSystem\TicketDistributionSystemController@resend_email')->middleware('auth:client_users')->name('tds.email.resend');
Route::post('/tds/password/request', 'TicketDistributionSystem\TicketDistributionSystemController@password_request')->name('tds.password.request');
Route::post('/tds/password/reset', 'TicketDistributionSystem\TicketDistributionSystemController@password_reset')->name('tds.password.reset');

// TDS Exports
Route::post('/tds/exportar', 'TicketDistributionSystem\TicketDistributionSystemController@export')->name('tds.export');


// Subdomain routes
Route::domain('{system}.' . env('APP_BASE_DOMAIN'))->name('system.')->middleware('auth')->group(function () {
    // Route::prefix('{pumastabasco}')->name('system.')->middleware('auth')->group(function () {
    Route::get('/', 'AdminController@dashboard_redirect');

    // System dashboard
    Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');

    // Events
    Route::prefix('/eventos')->name('events.')->group(function () {
        // Event groups resource
        Route::resource('/grupos', 'EventGroupController', ['names' => 'groups']);
        Route::post('/grupos/{id}/upload_img', 'EventGroupController@upload_img')->name('groups.upload.imgs');
        Route::put('/grupos/{id}/update/ticket_types', 'EventGroupController@updateTicketTypes')->name('groups.update.ticket_types');
        Route::get('/grupos/{id}/general', 'EventGroupController@general')->name('groups.general');
        Route::get('/grupos/{id}/holding', 'EventGroupController@holding')->name('groups.holding');
        Route::get('/grupos/{id}/boletos', 'EventGroupController@orders')->name('groups.orders');
        Route::get('/grupos/{id}/zonas', 'EventGroupController@zones')->name('groups.zones');
        Route::get('/grupos/{id}/{order_id}/qrs', 'OrderController@qrs')->name('groups.order.qrs');
        Route::get('/grupos/{id}/boletos/generar', 'EventGroupController@ticket_qrs_single_page')->name('groups.season.tickets.generateQRs');

        // All, by venue or team
        Route::get('/todos', 'EventController@all')->name('all');
        Route::get('/sede/{venue}', 'EventController@byVenue')->name('byVenue');
        Route::get('/equipo/{team}', 'EventController@byTeam')->name('byTeam');

        // Edit
        Route::post('/{id}/upload_img', 'EventController@upload_img')->name('upload.imgs');
        Route::get('/{id}/change_status', 'EventController@change')->name('status');

        // Holding
        Route::get('/{id}/holding', 'EventController@holding')->name('tickets.holding');

        // Statistics | Readings | Tickets | Courtesy | Accreditations
        Route::get('/{id}/estadisticas', 'EventController@statistics')->name('statistics');
        Route::get('/{id}/estadisticas/generales', 'EventController@general_statistics')->name('general.statistics');
        Route::post('/{id}/estadisticas/pdf', 'EventController@stadistic_pdf')->name('statistics.pdf');
        Route::get('/{id}/estadisticas/procesar', 'EventController@processStatistics')->name('statistics.process');
        Route::get('/{id}/lecturas', 'EventController@ticket_readings')->name('tickets.readings');
        Route::get('/{id}/boletos', 'EventController@ticket_list')->name('tickets.list'); // Ticket
        Route::get('/{id}/boletos/generar', 'EventController@tickets_generate')->name('tickets.generate');

        Route::get('/{id}/boletos/generarQRs', 'EventController@ticket_qrs_single_page')->name('tickets.generateForEvent');
        Route::get('/{id}/boletos/reporte', 'EventController@order_report')->name('tickets.order_report');
        Route::post('/{id}/boletos/buscar', 'EventController@search')->name('tickets.search');
        Route::get('/{id}/cortesias', 'EventController@courtesy_list')->name('courtesy.list');
        Route::get('/{id}/cortesias/generar', 'EventController@courtesy_qrs_single_page')->name('courtesy.generateForEvent');
        Route::get('/{id}/acreditaciones', 'EventController@accreditations_list')->name('accreditations.list');
        Route::post('/{id}/distribucion', 'EventController@send_ticket_distribution')->name('tickets.distribution.send');
        Route::get('/{id}/distribucion/{ticket_group}/reenviar', 'EventController@resend_email')->name('tickets.distribution.resend');
        Route::get('/{id}/distribucion', 'EventController@ticket_distribution')->name('tickets.distribution');

        // Analysis
        Route::get('/{id}/analisis', 'EventController@analysis')->name('analysis');

        // Season tickets
        Route::get('/{id}/abonos', 'AccreditationController@seasonTickets')->name('season.tickets');
        Route::get('/{id}/abonos/generar', 'AccreditationController@generateSeasonTickets')->name('season.tickets.generate');
        Route::get('/{id}/abonos/{season_ticket}', 'AccreditationController@show')->name('season.tickets.show');

        // Order QRs
        Route::get('/{id}/{order_id}/qrs', 'OrderController@qrs')->name('order_qrs');

        // Cortesias group
        Route::prefix('/{id}/acreditaciones')->name('accreditations.')->group(function () {
            // Route::get('/seleccionar', 'EventController@accreditations')->name('selection'); // Vista usuario acreditaciones
            Route::get('/confirmar', 'AccreditationController@confirm_accreditations')->name('confirm');
            Route::get('/guardar', 'AccreditationController@store')->name('store');
            Route::get('/generar', 'EventController@generateAccreditations')->name('generateForEvent'); // Vista usuario acreditaciones

            Route::get('/{order}/detalle', 'OrderController@courtesy_detail')->name('courtesies.detail');

            Route::get('/{accreditation}', 'AccreditationController@show')->name('show');
            Route::put('/{accreditation}/update', 'AccreditationController@update')->name('update');
            // Route::get('/{accreditation}/show', 'AccreditationController@show')->name('show');
        });
    });

    // Resource
    Route::resource('/eventos', 'EventController', ['names' => 'events']);


    // Orders
    Route::resource('/ordenes', 'OrderController', ['names' => 'orders'])->except(['edit, update']);
    Route::post('/ordenes/{id}', 'OrderController@update')->name('orders.update');
    Route::get('/ordenes/{id}/pdf', 'OrderController@pdf')->name('courtesy.pdf');
    Route::get('/ordenes/{id}/email/resend', 'OrderController@resend_email')->name('orders.resend.email');
    // Route::get('/ordenes/{id}/qr', 'OrderController@Qrs');
    // Route::get('qrImage/{order_id}/{type}', 'SiteController@qrImage');
    // Route::resource('/temas', 'TopicController');
    // Route::get('/soporte', 'SupportController@index');
    // Route::get('/soporte/{id}/estado/{status}', 'SupportController@change_status');

    // Accreditations
    Route::prefix('/acreditaciones')->name('accreditations.')->group(function () {
        // Route::get('/', 'AccreditationController@index')->name('index');
        Route::get('/detalle/{id}', 'AccreditationController@accreditation_history')->name('details');
        Route::get('/qrs', 'AccreditationController@qr')->name('qr');
        Route::get('/qrs/{id}', 'AccreditationController@qr')->name('qrByTeam');
        Route::get('/importar', 'ImportController@index')->name('import');
        Route::post('/import', 'ImportController@import')->name('import.file');
        Route::post('/version/{acreditation}', 'AccreditationController@change_version')->name('version');
    });
    Route::resource('/acreditaciones', 'AccreditationController', ['names' => 'accreditations'])->except(['edit', 'destroy']);

    // Access
    Route::prefix('/accesos')->name('access.')->group(function () {
        Route::get('/reportes', 'Access\ReportController@reports')->name('reports');
        Route::get('/reportes/{id}', 'Access\ReportController@report')->name('report');
        Route::post('/importar', 'ImportController@import_access')->name('import');
        Route::get('/qrs/{id}', 'Access\AccessController@qrs')->name('qrs');
    });
    Route::resource('/accesos', 'Access\AccessController', ['names' => 'access'])->except(['create']);

    // Users
    Route::resource('/usuarios', 'UserController', ['names' => 'users'])->except(['edit']);

    // Venues
    Route::prefix('/sedes/{id}/')->name('venues.')->group(function () {
        // Route::get('/templates', 'TicketTemplateController@venue_templates')->name('templates_venue');
        Route::resource('/templates', 'TicketTemplateController', ['names' => 'templates']); // ->except('templates')
        Route::get('/configuracion', 'VenueController@config')->name('config');

        Route::resource('/boletos/tipos', 'TicketTypeController', ['names' => 'ticket_types'])->except(['show', 'create', 'destroy']);
        // Route::prefix('/boletos')->name('ticket_types.')->group(function () {
        //     Route::resource('/boletos/tipos', 'TicketTypeController')->except(['show', 'edit', 'create', 'destroy']);
        // });

        // Checkpoints
        Route::resource('/checkpoints', 'CheckpointController', ['names' => ['checkpoints']]);

        // Zones
        Route::get('/zonas/download_config', 'ZoneController@download_config')->name('zones.download_config');
        Route::resource('/zonas', 'ZoneController', ['names' => 'zones']);
    });

    Route::resource('/sedes', 'VenueController', ['names' => 'venues'])->except(['edit', 'create', 'store', 'edit', 'update', 'destroy']);
});

// , session('system_slug')
// [session('system_slug')]

// Route::get('/send_email', "SiteController@message");
// Route::get('/zip_qr/{event}', 'OrderController@zip_qr');

Route::prefix('admin')->name('admin.')->middleware('auth')->group(function () {
    // Route::resource('/sistemas', 'SystemController', ['names' => 'system']);
    Route::get('/', 'AdminController@dashboard_redirect');
    Route::get('/templates/{id}/tickets', 'TicketTemplateController@getTickets');
    Route::get('/sedes/{id}/tickets', 'VenueController@getTickets');

    // Courtesies
    Route::post('/cortesias/continuar', 'TicketController@confirm_sale');
    // Route::get('/cortesias/{id}/detalle', 'OrderController@courtesy_detail')->name('courtesy.detail');
    // Route::get('/cortesias/{id}/qrs', 'OrderController@courtesy_detail')->name('courtesy.detail');



    Route::prefix('/eventos')->name('events.')->group(function () {
        // No system redirect
        Route::get('/acreditaciones', 'AccreditationController@no_system_redirect');
        // Route::get('/create', 'EventController@create')->name('create');
        // Route::post('/store', 'EventController@store')->name('store');
        // Import
        //*** Commented for changes Route::get('/{id}/cortesias', 'EventController@courtesies_list')->name('courtesies.list');
        // Route::get('/{id}/cortesias', 'EventController@courtesies_list')->name('courtesies.list');

        // Ticket
        Route::get('/{id}/boletos', 'EventController@ticket_list')->name('tickets.list');


        // Courtesy
        Route::get('/{id}/cortesias', 'EventController@courtesy_list')->name('courtesy.list');
        Route::get('/{id}/cortesias/generar', 'EventController@courtesy_qrs_single_page')->name('courtesy.generateForEvent');
        Route::get('/{id}/boletos/seleccionar', 'EventController@seatsForMap');
    });




    Route::get('/templates', 'TicketTemplateController@index')->name('templates');
    // Route::resource('/boletos', 'TicketController');
    // Route::resource('/precios', 'AdminController'); //->name('precios');

    Route::post('/requerimientos/agrega', 'Access\RequirementController@create')->name('requirements.store');
    Route::get('/requerimientos/asocia/{requirement_id}/{access_id}', 'Access\RequirementController@associate')->name('requirements.associate');
    Route::get('/requerimientos/listado', 'Access\AccessController@requirement_checkout')->name('requirements.calendar');

    // Statistics
    Route::prefix('/estadisticas')->name('statistics.')->group(function () {
        Route::get('/', 'StatisticController@index')->name('index');
        Route::get('/equipo/{team}', 'StatisticController@byTeam')->name('byTeam');
        Route::get('/sede/{venue}', 'StatisticController@byVenue')->name('byVenue');
        Route::get('/todas', 'StatisticController@all')->name('all');
    });

    Route::get('/aforo', 'SiteController@capacity');
});



// Route::post('/soporte/', 'SupportController@store')->name('support.store');
// Route::post('/soporte/{id}/enviar', 'ConversationtController@message')->name('support.message');
// Route::get('/soporte/create', 'SupportController@create')->name('support.create');
// Route::get('/soporte/', 'SupportController@index')->name('support');
// Route::get('/soporte/{id}/mensajes', 'ConversationtController@show')->name('support.show');



// Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
//     Route::get('/boleto/{id}','TicketController@myTickets')->name('tickets.user');
// });

Route::group(['prefix' => 'usuarios', 'middleware' => 'auth'], function () {
    Route::get('/{id}/mi_perfil', 'UserController@user_profile');
    Route::get('/{id}/desactivar_cuenta', 'UserController@unsubscribe');
    Route::post('/{id}/mi_perfil/update', 'UserController@update_profile');

    Route::get('/sedes', 'VenueController@list_user')->name('teams');
    Route::get('/sedes/{id}/eventos', 'VenueController@events_by_venue')->name('team_events');
    Route::get('/sedes/{id}/eventos/{event}/boletos', 'TicketController@select_tickets')->name('tickets_sale');
    /*  Route::post('/eventos/{event}/boletos/confirmar', 'TicketController@confirm_sale')->name('confirm_sale'); */
    Route::post('/eventos/boletos/confirmar', 'TicketController@confirm_sale')->name('confirm_sale');
    Route::get('/eventos/boletos/confirmar_compra', 'TicketController@confirm_form')->name('confirm_form');
    Route::get('/eventos/{id}/boletos/pagar', 'TicketController@pay_form')->name('pay_form');
    Route::post('/eventos/{id}/boletos/pagar', 'OrderController@pay_order')->name('pay_order');
});
// Route::get('/mis_boletos', 'TicketController@myTickets')->name('tickets.user');
// Route::get('/mis_boletos/anteriores', 'TicketController@myOldTickets')->name('tickets.old.user');
// Route::get('/view/boleto/{id}', 'TicketController@ticketView')->name('ticket.view.user');


// Route::get('/boletos/{id}/comprar', 'OrderController@buyTickets');
// Route::post('/boletos/{id}/comprar', 'OrderController@createOrder');
// Route::get('/pdf/{id}/imprimir', 'OrderController@PDFresumen')->name('pdfresumen');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'usuarios'], function () {
    // Redirect
    Route::get('/after_password', 'SiteController@client_user_after_password');

    // Auth controllers
    Route::get('/password/reset', 'AuthClientUsers\ForgotPasswordController@showLinkRequestForm')->name('client_user.password.request');
    Route::post('/password/email', 'AuthClientUsers\ForgotPasswordController@sendResetLinkEmail')->name('client_user.password.email');
    Route::get('/password/reset/{token}', 'AuthClientUsers\ResetPasswordController@showResetForm')->name('client_user.password.reset');
    Route::post('/password/reset', 'AuthClientUsers\ResetPasswordController@reset')->name('client_user.password.update');
});

Route::get('/login', 'SiteController@login')->name('login');
