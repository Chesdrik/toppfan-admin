<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class APILoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->postJson('/api/login', ['email' => 'diego@filloy.com.mx', 'password' => '123']);

        $response->assertStatus(200)
            ->assertJson([
                'error' => false
        ]);
    }
}
