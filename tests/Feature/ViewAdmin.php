<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\User;
use App\TicketType;
use App\TicketTemplate;
use App\Topic;
use Log;

class ViewAdmin extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function testDashboard()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.dashboard'));
        //Log::debug();
        $response->assertStatus(200);
    }

    public function tetstAccreditation()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.accreditations.index'));
        $response->assertStatus(200);
    }

    public function testAccreditationQR()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.accreditations.qr'));
        $response->assertStatus(200);
    }

    public function testAccreditationQRbyTeam()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.accreditations.qrByTeam', 1));
        $response->assertStatus(200);
    }

    public function testImport()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.import'));
        $response->assertStatus(200);
    }

    /*  public function testImportPost(){
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('system.import.file'));
        $response->assertStatus(302);
    }*/

    /* public function testPDFevent(){
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('system.events.statistics.pdf'));
        $response->assertStatus(302);
    } */

    public function testEventVenue()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.byVenue', 1));

        $response->assertStatus(200);
    }

    public function testEventTeam()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.byTeam', 1));

        $response->assertStatus(200);
    }

    public function testEventAll()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.all', 1));

        $response->assertStatus(200);
    }
    public function testEventStatistics()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.statistics', 1));

        $response->assertStatus(200);
    }

    public function testEventTicketReaading()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.tickets.readings', 1));

        $response->assertStatus(200);
    }

    public function testAccreditationList()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.accreditations.list', 44));

        $response->assertStatus(200);
    }

    public function testTicketList()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.tickets.list', 44));

        $response->assertStatus(200);
    }

    /*public function testOrderQr(){
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.order_qrs',[44, 1778]));

        $response->assertStatus(200);
    }*/

    public function testAccreditationConfrim()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.accreditations.confirm', 44));

        $response->assertStatus(200);
    }


    public function testAccreditationStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.accreditations.store', 44));

        $response->assertStatus(200);
    }

    public function testAccreditationVersion()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('system.events.accreditations.version', [44, 2104]), ['motive' => 'perdida de la acreditacion']);

        $response->assertStatus(302);
    }

    public function testAccreditationGenerar()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.accreditations.generateForEvent', 44));

        $response->assertStatus(200);
    }


    public function testCourtesyDetails()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.accreditations.courtesies.detail', [44, 1778]));

        $response->assertStatus(200);
    }

    public function testAccreditaionShow()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.accreditations.show', [44, 2104]));

        $response->assertStatus(200);
    }

    public function testAccreditaionShowUpdate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->put(route('system.events.accreditations.update', [44, 2104]));

        $response->assertStatus(200);
    }

    public function testSeatsForMap()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(url('/44/boletos/seleccionar'));

        $response->assertStatus(200);
    }

    public function testEventStatus()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.status', 44));

        $response->assertStatus(200);
    }

    public function testEventProcessStast()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.statistics.process', 44));

        $response->assertStatus(200);
    }

    public function testVenueTempleta()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.venues.templates.index', 1));

        $response->assertStatus(200);
    }


    public function testVenueConfig()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.venues.config', 1));

        $response->assertStatus(200);
    }

    public function testVenueTicketTemplateList()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(url('admin/sedes/1/templates'));

        $response->assertStatus(200);
    }

    public function testVenueTicketTemplateShow()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(url('admin/sedes/1/templates/10'));

        $response->assertStatus(200);
    }
    public function testVenueTicketTemplatStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(url('admin/sedes/1/templates'), ['name' => 'unitariTest', 'tickets' => ['1' => '1', '2' => '2'], 'costs' => ['1' => 1.50, '2' => 1.50]]);

        $response->assertStatus(201);
    }

    public function testVenueTicketTemplateUpdate()
    {
        $user = User::find(1);
        $template = TicketTemplate::where('name', 'unitariTest')->first();
        $this->actingAs($user);
        $response = $this->put(url('admin/sedes/1/templates/' . $template->id), ['name' => 'unitariTestEdit']);

        $response->assertStatus(200);
    }

    public function testVenueTicketType()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(url('/admin/sedes/1/boletos/tipos'));

        $response->assertStatus(200);
    }

    public function testVenueTicketTypeStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(url('/admin/sedes/1/boletos/tipos'), ['name' => 'unitaryTest', 'active' => '1', 'free' => 0, 'price' => 100, 'total_seats' => 100, 'zone_id' => 14, 'checkpoints' => ['3' => 3, '6' => '6']]);

        $response->assertStatus(201);
    }

    public function testVenueTicketTypeUpdate()
    {
        $user = User::find(1);
        $ticket_type = TicketType::where('name', 'unitaryTest')->first();
        $this->actingAs($user);
        $response = $this->put(route('system.tipos.update', [1, $ticket_type->id]), ['name' => 'unitaryTestEdit']);

        $response->assertStatus(200);
    }

    public function testVenueCheckpoint()
    {
        $user = User::find(1);

        $this->actingAs($user);
        $response = $this->get(route('system.venues.checkpoints.index'));

        $response->assertStatus(200);
    }

    public function testVenueCheckpointStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('system.venues.checkpoints.store'), ['name' => 'test', 'venue_id' => 1, 'type' => 'parking']);

        $response->assertStatus(302);
    }

    public function testVenueCheckpointstUpdate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->put(route('system.venues.checkpoints.update', 1),  ['name' => 'test', 'venue_id' => 1, 'type' => 'parking']);

        $response->assertStatus(302);
    }


    public function testEventCourtesyList()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.courtesies.list', 1));

        $response->assertStatus(200);
    }


    public function testEventCourtesySave()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.courtesies.save', 39));

        $response->assertStatus(200);
    }

    public function testEventCourtesyQr()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.courtesies.qrs', 1));

        $response->assertStatus(200);
    }

    public function testEventCreate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.create', session('system_slug')));

        $response->assertStatus(200);
    }

    public function testEventEdit()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.events.edit', 1));

        $response->assertStatus(200);
    }

    public function testTemplate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.venues.templates.index'));

        $response->assertStatus(200);
    }

    public function testTickets()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(url('admin/boletos'));

        $response->assertStatus(200);
    }

    public function testUser()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(url('admin/usuarios'));

        $response->assertStatus(200);
    }

    public function testUserStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('system.users.store'), ['name' => 'unitariTest', 'email' => 'unitari@test.com', 'password' => bcrypt('12345678'), 'type' => 'root']);

        $response->assertStatus(201);
    }

    public function testUserUpdate()
    {
        $user = User::find(1);
        $user_test = User::where('name', 'unitariTest')->first();

        $this->actingAs($user);
        $response = $this->put(route('system.users.store'), ['name' => 'unitariTestEdit']);

        $response->assertStatus(200);
    }

    public function testOrderQr()
    {
        $user = User::find(1);


        $this->actingAs($user);
        $response = $this->get(url('/admin/ordenes/1777/qr'));

        $response->assertStatus(200);
    }

    public function testQrImagen()
    {
        $user = User::find(1);


        $this->actingAs($user);
        $response = $this->get(url('admin/qrImage/1777/regular'));

        $response->assertStatus(200);
    }

    public function testAccess()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.access.index'));

        $response->assertStatus(200);
    }

    public function testAccessReport()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.access.reports'));

        $response->assertStatus(200);
    }

    public function testAccessReportId()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.access.report', 1));

        $response->assertStatus(200);
    }

    public function testAccessQr()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.access.qrs', 1));

        $response->assertStatus(200);
    }

    public function testRequirementsAssociate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.requirements.associate', [1, 1]));

        $response->assertStatus(200);
    }

    public function testRequirementsCalendar()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('system.requirements.calendar', 1));

        $response->assertStatus(200);
    }

    public function testSupport()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('support', 1));

        $response->assertStatus(200);
    }
    public function testSupportCreate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('support.create', 1));

        $response->assertStatus(200);
    }
    public function testSupportShow()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(route('support.show', 1));

        $response->assertStatus(200);
    }
    /* post */

    public function testSEventStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('system.events.store', session('system_slug')), ['name' => 'Test', 'venue_id' => 1, 'team_id' => 1, 'date' => '2021-05-02 21:00:00']);

        $response->assertStatus(302);
    }

    public function testSEventUpdate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->put(route('system.events.update', 1), ['name' => 'Test', 'venue_id' => 1, 'team_id' => 1, 'date' => '2021-05-02 21:00:00', 'ticket_template_id' => 10, 'name_template' => 10]);

        $response->assertStatus(200);
    }

    public function testTicketStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(url('/admin/boletos/tipos'), ['name' => 'test', 'active' => 1, 'free' => 1, 'price' => 0.00, 'venue_id' => 1, 'checkpoints[1]' => 1]);

        $response->assertStatus(201);
    }
    public function testTicketUpdate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->put(url('/admin/boletos/tipos/1'), ['name' => 'test', 'active' => 1, 'free' => 1, 'price' => 0.00, 'venue_id' => 1, 'checkpoints[1]' => 1]);

        $response->assertStatus(200);
    }

    public function testTopic()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->get(url('/admin/temas'));

        $response->assertStatus(200);
    }

    public function testTopicStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(url('/admin/temas'),  ['name' => 'unitariTest', 'parent_topic' => '']);

        $response->assertStatus(201);
    }
    public function testTopicUpdate()
    {
        $user = User::find(1);
        $topic = Topic::where('name', 'unitariTest')->first();
        $this->actingAs($user);
        $response = $this->put(url('/admin/temas/' . $topic->id),  ['name' => 'unitariTestUpdate']);

        $response->assertStatus(200);
    }
    public function testAccessStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('system.access.store'),  ['visitor_name' => 'test', 'start' => '2020-11-12 00:00:00', 'end' => '2021-11-12 23:59:59', 'type' => 'car']);

        $response->assertStatus(201);
    }

    public function testAccessUpdate()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->put(route('system.access.update'),  ['visitor_name' => 'test', 'start' => '2020-11-12 00:00:00', 'end' => '2021-11-12 23:59:59', 'type' => 'car']);

        $response->assertStatus(200);
    }

    public function testRequirementsStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('system.requirements.store'),  ['description' => 'test', 'type' => 'photo']);

        $response->assertStatus(201);
    }


    public function testSupportsStore()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('support.store'),  ['user_id' => $user->id, 'description' => 'test', 'topic_id' => 1,]);

        $response->assertStatus(201);
    }

    public function testSupportsMessage()
    {
        $user = User::find(1);
        $this->actingAs($user);
        $response = $this->post(route('support.message', 1),  ['description' => 'test']);

        $response->assertStatus(201);
    }
}
